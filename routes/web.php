<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Route::get('/mobile/camera', function(){
//   return view('mobile.camera');
// });
// Route::get('/welcome', function(){
//     return view('welcome');
// });
Route::get('/under_mt', function(){
  return view('mt');
});
Route::get('/register', 'LoginController@form_register');
Route::post('/register', 'LoginController@save_register');

Route::get('/changepwd/{link}', 'LoginController@form_changepwd');
Route::post('/changepwd/{link}', 'LoginController@save_changepwd');

Route::get('/forgotpwd', 'LoginController@form_forgotpwd');
Route::post('/forgotpwd', 'LoginController@save_forgotpwd');

Route::get('/login', 'LoginController@loginPage');
Route::post('/login', 'LoginController@login');
Route::get('/logout', 'LoginController@logout');

// Route::get('/modifydata', 'WorkController@modifydata');
Route::get('/work/report', 'WorkController@periodicReport');
Route::get('/getdatabywasakaid/{id}', 'AdminController@getdatabywasakaid');
Route::get('/getState/{id}', 'AdminController@getState');
Route::get('/getStatePID/{id}/{state}', 'AdminController@getStatePID');
Route::get('/getWitelByFiberzone/{id}', 'UserController@getWitelByFiberzone');
Route::get('/getWitelByRegional/{id}', 'UserController@getWitelByRegional');

Route::get('/map/ajax/odp-keyless', 'MapController@ajax_odp_keyless');
Route::get('/map/ajax/detail/odp-keyless/{unit}/{status}', 'MapController@ajax_detail_odp_keyless');
Route::get('/map/ajax/log-activity/odp-keyless/{id}', 'MapController@ajax_logactivity_odp_keyless');
Route::get('/map/ajax/info-odp-keyless', 'MapController@ajax_info_odp_keyless');

Route::group(['middleware' => 'tomman.auth'], function () {
  Route::get('/', 'AdminController@homepage');
  Route::get('/lama', 'GembokController@index');
  // Route::get('/mobile', 'AdminController@homepage');
  // Route::get('/', 'GembokController@index');
  Route::get('/mobile/profile', function(){
    return view('mobile.profile');
  });
  Route::get('/mobile/scan', 'MobileController@scan');
  Route::get('/mobile/profile', 'MobileController@profile');
  Route::post('/mobile/profile', 'MobileController@profileSave');
  Route::post('/mobile/search', 'MobileController@search');
  Route::get('/mobile/ajaxHistory/{id}', 'MobileController@ajaxHistory');
  // Route::get('/mobile/login', function(){
  //   return view('mobile.login');
  // });
  Route::get('/mobile/work/start/{idGembok}', 'MobileController@form_start');
  Route::post('/mobile/work/start/{idGembok}', 'MobileController@save_start');
  Route::get('/mobile/work/{idWork}/progress', 'MobileController@form_progress');
  Route::post('/mobile/work/{idWork}/progress', 'MobileController@save_progress');
  Route::get('/mobile/work/{idWork}/close', 'MobileController@form_close');
  Route::post('/mobile/work/{idWork}/close', 'MobileController@save_close');


  

  Route::get('/unclose', 'WorkController@jsonGetOpen');
  Route::get('/allGembok', 'WorkController@jsonGetAllGembok');
  Route::get('/dashboard', function(){
    return view('dashboard');
  });
  Route::get('/sop-user', function(){
    return view('faq.user');
  });
  Route::get('/sop-admin', function(){
    return view('faq.admin');
  });
  Route::get('/profile', 'UserController@profile');
  Route::get('/listgembok/{witel}', 'GembokController@list');
  Route::post('/profile', 'UserController@updateProfile');
  Route::get('/profile/{nik}', 'UserController@profileEdit');
  Route::post('/profile/{nik}', 'UserController@updateProfile');
  Route::get('/acl', 'UserController@acl');
  Route::get('/acl/{id}', 'UserController@formAC');
  Route::post('/acl/{id}', 'UserController@updateAC');
//    Route::post('/', 'GembokController@search');
  Route::get('/top10/{priode}/{witel}', 'DashboardController@index');
  Route::get('/gembok/create', 'GembokController@createForm');
  Route::post('/gembok/create', 'GembokController@create');
  Route::get('/gembok/{idGembok}', 'GembokController@read');
  Route::post('/gembok/{idGembok}', 'GembokController@update');
  Route::get('/gembok/{idGembok}/history', 'GembokController@history');

  Route::get('/work/download', 'DownloadController@downloadForm');
  Route::post('/work/download', 'DownloadController@download');
  Route::get('/work/start/{idGembok}', 'WorkController@startForm');
  Route::post('/work/start/{idGembok}', 'WorkController@create');
  Route::get('/work/{idWork}/progress', 'WorkController@progressForm');
  Route::post('/work/{idWork}/progress', 'WorkController@update');
  Route::get('/work/{idWork}/close', 'WorkController@closeForm');
  Route::post('/work/{idWork}/close', 'WorkController@close');
  Route::get('/work/{idWork}', 'WorkController@read');
  Route::get('/work', 'WorkController@listPage');

  Route::get('/workUser/{id}', 'WorkController@listByUser');
  Route::get('/mywork', 'WorkController@listByCurrentUser');
  Route::get('/user', 'UserController@index');
  Route::get('/user/{id}', 'UserController@userForm');
  Route::post('/user/{id}', 'UserController@update');
  Route::post('/historySearch', 'WorkController@search');
  Route::get('/lib/{id}', 'WorkController@library');
  Route::get('/scale', 'WorkController@scale');
  Route::get('/sendTeleManual/{id}', 'WorkController@sendTeleManual');
  Route::get('/saran', 'UserController@saran');
  Route::post('/saran', 'UserController@saranSave');


  //admin
  Route::get('/home', 'AdminController@homepage');
  Route::get('/home/{fz}/{wtl}/{owner}', 'AdminController@home');
  Route::get('/ajaxUnakses/{fz}/{wtl}/{owner}', 'AdminController@ajaxGetUnakses');
  Route::get('/admin/user/{fz}/{wtl}', 'AdminController@userList');
  Route::get('/admin/user/{id}', 'AdminController@userForm');
  Route::post('/admin/user/{id}', 'AdminController@userSave');
  Route::get('/ajx/gembokdata/{fz}/{wtl}/{owner}', 'AdminController@gembokListData');
  Route::get('/admin/gembok/{fz}/{wtl}/{owner}', 'AdminController@gembokList');
  Route::get('/admin/gembok/{id}', 'AdminController@gembokForm');
  Route::post('/admin/gembok/{id}', 'AdminController@gembokSave');
  Route::get('/admin/history/{date}/{fz}/{wtl}/{owner}', 'AdminController@history');
  Route::get('/admin/detail/{id}', 'AdminController@detail');

  Route::get('/download/history/{date}/{fz}/{wtl}/{owner}', 'DownloadController@downloadHistory');
  Route::get('/admin/theme', 'AdminController@themes');
  Route::post('/admin/theme', 'AdminController@themeSave');
  Route::post('/admin/insertInstansiNew', 'AdminController@instansiSave');
  Route::get('/admin/getInstansi', 'AdminController@instansiGet');
  Route::get('/admin/lib/{id}', 'AdminController@library');
  Route::get('/adminsearch', 'AdminController@gembokListSearch');

  Route::get('/admin/lemari', 'LokerController@admin_list_lemari');
  Route::get('/admin/lemari/{id}', 'LokerController@admin_form_lemari');
  Route::post('/admin/lemari/{id}', 'LokerController@admin_post_lemari');
  Route::post('/admin/lemari/{id}/loker', 'LokerController@admin_post_loker');
  Route::get('/admin/loker/items/{id}', 'LokerController@admin_json_loker');
  Route::get('/admin/loker/ajax/{id}', 'LokerController@admin_ajax_loker');

  Route::POST('/loker/approvePinjam', 'LokerController@approvePinjam');
  Route::POST('/loker/approveReturn', 'LokerController@approveReturn');

  Route::get('/loker/start', 'LokerController@start');
  Route::POST('/loker/start/{id}', 'LokerController@start_post');
  Route::get('/loker/close/{id}', 'LokerController@close');
  Route::POST('/loker/close/{id}', 'LokerController@close_post');
  
  //MC
  Route::get('/mc/{id}', 'MCController@layoutodc');
  Route::get('/ajxkabelcore/{id}', 'MCController@getKabelCore');
  Route::get('/ajxbasetrayport/{odc}/{basetray}', 'MCController@getPortByBasetray');
  Route::post('/mc/saveKabel', 'MCController@kabel_save');
  Route::post('/mc/saveLink', 'MCController@link_save');
  Route::post('/mc/saveStatusPort', 'MCController@saveStatusPort');
  Route::post('/mc/saveBookingSPL', 'MCController@saveBookingSPL');
  Route::post('/mc/saveLepasSPL', 'MCController@saveLepasSPL');
  Route::post('/mc/saveSetRedaman', 'MCController@saveSetRedaman');
  Route::post('/mc/saveBasetrayAdd', 'MCController@saveBasetrayAdd');
  Route::post('/mc/saveBasetray', 'MCController@saveBasetray');
  Route::post('/mc/saveChangePort', 'MCController@saveChangePort');


  Route::get('/downloadGembok/{fz}/{wtl}/{owner}', 'DownloadController@downloadGembok');
  Route::get('/downloadUser/{fz}/{wtl}', 'DownloadController@downloadUser');
  Route::get('/downloadTiket/{fz}/{wtl}', 'DownloadController@downloadTiket');

  Route::get('/get_live_gembok_open', 'AdminController@live_gembok_open');

  Route::get('/map/odp-keyless', 'MapController@map_odp_keyless');
  Route::get('/map/detail/odp-keyless/{unit}/{status}', 'MapController@detail_odp_keyless');
  Route::get('/map/log-activity/odp-keyless/{id}', 'MapController@logactivity_odp_keyless');


  Route::get('/admin/listTiket/{reg}/{wtl}', 'AdminController@tiketList');
  Route::get('/admin/ajaxTiket/{id}', 'AdminController@tiketAjax');
  Route::get('/admin/tiket/{id}', 'AdminController@tiketForm');
  Route::post('/admin/tiket/{id}', 'AdminController@tiketUpdate');

});
