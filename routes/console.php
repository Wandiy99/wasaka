<?php

use Illuminate\Foundation\Inspiring;
use App\DA\Work;
use App\Http\Controllers\WorkController;
/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');
Artisan::command('sendTele {id} {flag}', function ($id, $flag) {
	Work::sendReportTelegram2($id, $flag);
});
Artisan::command('sendAdmin {id}', function ($id) {
	Work::sendReportAdmin($id);
});
Artisan::command('sendOpened {id}', function ($id) {
	Work::sendReportTelegramOpen($id);
});
Artisan::command('sendClosed {id}', function ($id) {
	Work::sendReportTelegramClose($id);
});
Artisan::command('sendNotif {witel}', function ($witel) {
	WorkController::sendReportGembok(0,$witel);
});