# Instalasi
Siapkan folder kosong untuk menampung instalasi. 
Dalam primbon ini, folder diberi nama `mancing` 

## 01 Download source code dari Bitbucket
```
git clone --depth 1 https://ta_deploy@bitbucket.org/telkomakses/mancing.git mancing
```
silahkan hubungi maintainer tomman untuk password

## 02 Download Library
masuk ke folder aplikasi

```
cd mancing
```

install library untuk PHP
```
composer install
```

masuk ke folder `public` dan download library untuk HTML/CSS/JS 
```
cd public
bower install
```

## 03 Setting Aplikasi
pastikan berada di folder utama, jika melanjutkan dari step sebelumnya,
kembali ke folder `mancing`
```
cd ..
```

copy file contoh konfigurasi dan buat key secara acak
```
cp .env.example .env
php artisan key:generate
```

## 04 Setting WebServer
gunakan folder `mancing/public` sebagai `DocumentRoot`
```
cd etc/apache2/sites-available
cp 002-tomman_opt.conf 002-new_sites.conf

```