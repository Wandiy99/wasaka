<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;
class User
{
    const TABLE = 'user';
    // const KARYAWAN = 'karyawan';
    const PROFILE = 'manci_profile';
    public static function getRegional()
    {
        return DB::select('SELECT regional as id, regional as text
            FROM  manci_witel 
            GROUP BY regional
            ORDER BY regional');
    }
    public static function getFiberzone()
    {
        return DB::select('SELECT fiberzone as id, fiberzone as text
            FROM  manci_witel 
            GROUP BY fiberzone
            ORDER BY fiberzone');
    }
    public static function getWitel()
    {
        return DB::select('SELECT witel as id, witel as text
            FROM  manci_witel 
            GROUP BY witel
            ORDER BY witel');
    }
    public static function getWitelByFiberzone($fz)
    {
        return DB::select('SELECT witel as id, witel as text
            FROM  manci_witel where fiberzone="'.$fz.'"');
    }
    public static function getWitelByRegional($reg)
    {
        return DB::select('SELECT witel as id, witel as text
            FROM  manci_witel where regional="'.$reg.'"');
    }
    private static function tableUser()
    {
        return DB::table('user')->select('*','fz as fiberzone','wtl as witel', 'instansi as nama_instansi','id_user as nik')->whereIn('status',['0','1','3']);
    }
    public static function userExists($id)
    {
        return DB::table('user')->where('id_user', $id)->first();
    }
    public static function getUser()
    {
        return self::tableUser()->where('login_type', 1)->get();
    }
    public static function getRecordedUser($req)
    {
        $witel = $req->input('witel');
        $fz = $req->input('fiberzone');
        $query = self::tableUser();
        if($fz){
            $query->where('fz', $fz);
        }
        if($witel){
            $query->where('wtl', $witel);
        }
        return $query->get();
    }
    public static function getProfile($id)
    {
        return self::tableUser()->where('id_user', $id)->first();
    }
    public static function updateAcl($user, $update)
    {
        return DB::table('user')->where('id_user', $user)->update($update);
    }
    public static function updateProfile($req, array $input)
    {
        return DB::table('user')->where('id_user', $req->nik)
        ->update($input);
    }
    public static function updateProfileByNik($nik, array $input)
    {
        return DB::table('user')->where('id_user', $nik)
        ->update($input);
    }
    public static function getByRememberToken($token)
    {
        return self::tableUser()
        ->where('wsk_remember_token', $token)->first();
    }
    // public static function storeProfile(array $input)
    // {
    //     return DB::table('manci_profile')->insert($input);
    // }
    public static function getUserById($id)
    {
        return self::tableUser()->where('id_user', $id)->first();
    }
    public static function userCreate($req)
    {
        DB::table(self::TABLE)->insert([
            "id_user" => $req->id_user,
            "password" => MD5($req->pwd),
            "status" => $req->status,
            "nama" => $req->nama,
            "instansi" => $req->nama_instansi,
            "login_type" => 1
        ]);
    }
    public static function userSSOCreate($input)
    {
        DB::table(self::TABLE)->insert($input);
    }
    public static function userUpdate($req)
    {
        if($req->pwd){
            DB::table(self::TABLE)
            ->where("id_user" , $req->hid)
            ->update([
                "password" => MD5($req->pwd)
            ]);
        }
        DB::table(self::TABLE)
        ->where("id_user" , $req->hid)
        ->update([
            "id_user" => $req->id_user,
            // "id_karyawan" => $req->id_user,
            "status" => $req->status,
            "nama" => $req->nama,
            "instansi" => $req->nama_instansi,
        ]);
    }
    // public static function karyawanExists($id)
    // {
    //     return DB::table(self::KARYAWAN)->where('id_karyawan', $id)->first();
    // }
    // public static function karyawanCreate($req)
    // {
    //     DB::table(self::KARYAWAN)->insert([
    //         "id_karyawan" => $req->id_user,
    //         "nama" => $req->nama,
    //         "nama_instansi" => $req->nama_instansi,
    //         "status_kerja" => "WASAKA"
    //     ]);
    // }
    // public static function karyawanUpdate($req)
    // {
    //     DB::table(self::KARYAWAN)
    //     ->where("id_karyawan", $req->hid)
    //     ->update([
    //         "id_karyawan" => $req->id_user,
    //         "nama" => $req->nama,
    //         "nama_instansi" => $req->nama_instansi
    //     ]);
    // }

}