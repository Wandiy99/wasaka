<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

Class MapModel
{
	public static function odp_keyless()
    {
        $data = DB::table('manci_gembok AS mg')
        ->leftJoin('manci_work AS mw', 'mg.id', '=', 'mw.id_gembok')
        ->select('mg.id AS gembok_id', 'mg.kode', 'mg.regional', 'mg.witel', 'mg.kode', 'mg.last_akses as timestamp', 'mg.pin', 'mg.ts', 'mw.id AS work_id', 'mw.time_start', 'mw.time_close', 'mg.kategori', 'mg.koordinat')
        ->where([
            ['mg.gembok', 'kunci-elektrik'],
            ['mg.isHapus', 0]
        ])
        ->whereIn('mg.kategori', [2,8])
        ->orderBy('mw.id', 'DESC')
        ->get();

        $groupedArray = [];
        foreach ($data as $item)
        {
            $item->latitude = explode(",",$item->koordinat)[0];
            $item->longitude = trim(explode(",",$item->koordinat)[1]);
            $groupedArray[$item->kode][] = $item;
        }

        $result = [];
        foreach ($groupedArray as $group)
        {
            $result[] = $group[0];
        }

        return $result;
    }

    public static function info_odp_keyless()
    {
        return DB::table('manci_gembok AS mg')
        ->leftJoin('manci_work AS mw', 'mg.id', '=', 'mw.id_gembok')
        ->leftJoin('user AS u', 'mw.id_user', '=', 'u.id_user')
        ->select(DB::raw('
            u.loker AS unit,
            SUM(CASE WHEN mw.time_start != 0 AND mw.time_close = 0 THEN 1 ELSE 0 END) AS is_open,
            SUM(CASE WHEN mw.time_start != 0 AND mw.time_close != 0 AND from_unixtime(mw.time_close) LIKE "'.date('Y-m-d').'%" THEN 1 ELSE 0 END) AS is_close
        '))
        ->where([
            ['mg.gembok', 'kunci-elektrik'],
            ['mg.isHapus', 0]
        ])
        ->whereIn('mg.kategori', [2,8])
        ->groupBy('u.loker')
        ->get();
    }
    public static function info_total_alpro()
    {
        return DB::table('manci_gembok AS mg')
        ->leftJoin('jenis_alpro AS j', 'mg.kategori', '=', 'j.id')
        ->select(DB::raw('
            j.jenis_alpro,mg.kategori,count(mg.id) as live
        '))
        ->where([
            ['mg.gembok', 'kunci-elektrik'],
            ['mg.isHapus', 0]
        ])
        ->whereIn('mg.kategori', [2,8])
        ->groupBy('mg.kategori')
        ->get();
    }

    public static function detail_odp_keyless($unit, $status)
    {
        $data = DB::table('manci_gembok AS mg')
        ->leftJoin('manci_work AS mw', 'mg.id', '=', 'mw.id_gembok')
        ->leftJoin('manci_jenis_pekerjaan AS mjp', 'mw.jenis_pekerjaan_id', '=', 'mjp.id')
        ->leftJoin('user AS u', 'mw.id_user', '=', 'u.id_user')
        ->select(DB::raw('
            u.id_user,
            u.nama,
            u.fz,
            u.wtl,
            u.regional,
            u.instansi,
            u.loker,
            from_unixtime(mw.time_start) AS time_start,
            from_unixtime(mw.time_close) AS time_close,
            mw.timezone,
            mw.anggota,
            mw.uraian,
            mg.id,
            mg.kode,
            mjp.jenis_pekerjaan
        '))
        ->whereIn('mg.kategori', [2,8])
        ->where([
            ['mg.gembok', 'kunci-elektrik'],
            ['mg.isHapus', 0]
        ]);

        if ($unit != 'all')
        {
            $data->where('u.loker', $unit);
        }
        else if ($unit == 'other')
        {
            $data->where('u.loker', '');
        }

        if ($status == 'is_open')
        {
            $data->where([
                ['mw.time_start', '!=', 0],
                ['mw.time_close', 0]
            ]);
        }
        else if ($status == 'is_close')
        {
            $data->where([
                ['mw.time_start', '!=', 0],
                ['mw.time_close', '!=', 0]
            ])
            ->whereRaw('from_unixtime(mw.time_close) LIKE "'.date('Y-m-d').'%"');
        }

        return $data->orderBy('mw.id', 'DESC')->get();
    }

    public static function logactivity_odp_keyless($id)
    {
        return DB::table('manci_gembok AS mg')
        ->leftJoin('manci_work AS mw', 'mg.id', '=', 'mw.id_gembok')
        ->leftJoin('manci_jenis_pekerjaan AS mjp', 'mw.jenis_pekerjaan_id', '=', 'mjp.id')
        ->leftJoin('user AS u', 'mw.id_user', '=', 'u.id_user')
        ->select(DB::raw('
            u.id_user,
            u.nama,
            u.fz,
            u.wtl,
            u.regional,
            u.instansi,
            u.loker,
            from_unixtime(mw.time_start) AS time_start,
            from_unixtime(mw.time_close) AS time_close,
            mw.timezone,
            mw.anggota,
            mw.uraian,
            mg.id,
            mg.kode,
            mjp.jenis_pekerjaan
        '))
        ->where([
            ['mg.gembok', 'kunci-elektrik'],
            ['mg.isHapus', 0],
            ['mg.id', $id]
        ])
        ->whereIn('mg.kategori', [2,8])
        ->orderBy('mw.id', 'DESC')
        ->get();
    }
}