<?php

namespace App\DA;

use cURL;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;

Class LoginModel{
	
	public static function do_login($username,$password){
		
	  $url = 'https://api.telkomakses.co.id/API/sso/auth_sso_post.php';
		$data = 'username=' . $username . '&password=' . $password;	
		$ch = curl_init( $url );
		curl_setopt( $ch, CURLOPT_POST, 1);
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt( $ch, CURLOPT_HEADER, 0);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec( $ch );
		$hasil = json_decode($response, true);	
		
		
		return $hasil;	
	} 
	
}