<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;
use Telegram;
use Imagick;
DEFINE('UPLOAD_PATH', getcwd().'/upload/work/');
DEFINE('PIN_PATH', getcwd().'/upload/pin/');
DEFINE('ACAK_PIN_PATH', getcwd().'/upload/pin/acak/');
class Work
{
    const TABLE = 'manci_work';
    public static function isOpen($id)
    {
        return DB::table(self::TABLE)->where('id_gembok', $id)->where('time_close', 0)->first();
    }
    public static function isHavingUnclosed($id)
    {
        return DB::table(self::TABLE)->where('id_user', $id)->where('time_close', 0)->first();
    }
    public static function create(array $data)
    {
        DB::table('manci_gembok')->where('id',$data['id_gembok'])->increment('akses_count');
        DB::table('manci_gembok')->where('id',$data['id_gembok'])->update(['last_akses'=>DB::raw('now()')]);
        $data['timezone'] = Master::getTimezoneGembokID($data['id_gembok'])->timezone;
        return DB::table(self::TABLE)->insertGetId($data);
    }

    private static function joinTableGembok()
    {
        return DB::table(self::TABLE)
            ->leftJoin(
                Gembok::TABLE,
                Gembok::TABLE.'.id',
                '=',
                self::TABLE.'.id_gembok'
            )
            ->leftJoin(
                'user',
                'user.id_user',
                '=',
                self::TABLE.'.id_user'
            )
            ->leftJoin(
                'manci_jenis_pekerjaan',
                'manci_work.jenis_pekerjaan_id',
                '=',
                'manci_jenis_pekerjaan.id'
            )
            ->select(DB::raw(self::TABLE.'.*,
                from_unixtime(manci_work.time_start, "%Y-%m-%d %H:%i:%s") as date_start,
                from_unixtime(manci_work.time_close, "%Y-%m-%d %H:%i:%s") as date_close,
                manci_work.id_user,
                manci_jenis_pekerjaan.jenis_pekerjaan,
                manci_work.nama_user,
                manci_work.jenis_pekerjaan_id,
                user.id_user as id_karyawan,
                user.nama,
                user.loker,
                kode,manci_gembok.regional,
                manci_gembok.witel as wtl,manci_gembok.kategori,manci_gembok.produk_id,manci_gembok.owner_id,
                pin,koordinat,concat(MOD(TIMESTAMPDIFF(HOUR, from_unixtime(time_start), from_unixtime(time_close)),24) , "Jam" , MOD(TIMESTAMPDIFF(MINUTE, from_unixtime(time_start), from_unixtime(time_close)),60),  "Menit") as durasi')
                
            );
    }

    public static function getById($id)
    {
        return self::joinTableGembok()->where(self::TABLE.'.id', $id)->first();
    }

    public static function getLastOpen($id)
    {
        return self::joinTableGembok()->where(self::TABLE.'.id_gembok', $id)
        ->limit(3)->orderBy('time_start', 'DESC')->get();
    }

    public static function getByUser($login)
    {
        return self::joinTableGembok()->where(self::TABLE.'.id_user', $login)->orderBy('time_start','desc')->get();
    }

    public static function countUnclosed()
    {
        $witel = session('auth')->witel;
        return DB::table(self::TABLE)
                ->leftJoin(
                    Gembok::TABLE,
                    Gembok::TABLE.'.id',
                    '=',
                    self::TABLE.'.id_gembok'
                )
                ->where('time_close', '0')
                ->where('manci_gembok.witel', $witel)
                ->count();
    }

    public static function getUnclosed()
    {
        $witel = session('auth')->witel;
        return self::joinTableGembok()
                ->where('time_close', '0')
                ->where('manci_gembok.witel', $witel)
                ->orderBy('time_start')
                ->get();
    }
    public static function getUnclosedAll($reg, $wtl)
    {
        $query = self::joinTableGembok()
                ->where('time_close', '0')
                ->orderBy('time_start');
        if($reg!='all'){
            $query->where('manci_gembok.regional', $reg);
        }
        if($wtl!='all'){
            $query->where('manci_gembok.witel', $wtl);
        }
        return $query->get();
    }

    public static function getAllOnlyAdmin($date,$reg,$wtl,$owner)
    {
        // $witel = session('auth')->witel;
        $start=explode(':', $date)[0];
        $end=explode(':', $date)[1];
        $query = self::joinTableGembok()->whereRaw('(from_unixtime(time_close, "%Y-%m-%d") between "'.$start.'" and "'.$end.'")');
        if($reg!='all'){
            $query->where('manci_gembok.regional', $reg);
        }
        if($wtl!='all'){
            $query->where('manci_gembok.witel', $wtl);
        }
        // dd($query);
        return $query->where('manci_gembok.owner_id', $owner)->orderBy('time_start')->get();
    }

    public static function getUnclosedByWitel($witel)
    {
        return self::joinTableGembok()
                ->where('time_close', '0')
                ->where('manci_gembok.witel', $witel)
                ->where('manci_gembok.owner_id', 0)
                ->where('manci_gembok.kategori', '!=', 9)
                ->orderBy('time_start')
                ->get();
    }

    public static function getClosed($pageSize = false, $tgl = false)
    {
        $witel = session('auth')->witel;
        $q = self::joinTableGembok()
                ->where('time_close', '<>', '0')
                ->where('manci_gembok.fiberzone', session('auth')->fiberzone)
                ->orderBy('time_close', 'DESC');
        if($witel!="ALL"){
            $q->where('manci_gembok.witel', $witel);
        }
        if ($pageSize)
            return $q->paginate($pageSize);
        else{
            if(!$tgl){
                $tgl = date('Y-m');
            }
            return $q->whereRaw('from_unixtime(time_close, "%Y-%m-%d") like "%'.$tgl.'%"')->get();
        }
    }
    public static function getSearched($req)
    {
        $odc = $req->odc;
        $start = $req->start;
        $close = $req->close;
        $work = $req->work;
        $user = $req->user;
        $witel = session('auth')->witel;
        return self::joinTableGembok()
            ->where('time_close', '<>', '0')
            ->where('manci_gembok.witel', $witel)
            ->when($odc, function ($query) use ($odc) {
                return $query->where('kode', 'like', '%' . $odc . '%');
            })
            ->when($user, function ($query) use ($user) {
                return $query->where('manci_work.nama_user', 'like', '%' . $user . '%');
            })
            ->when($close, function ($query) use ($close) {
                return $query->whereRaw('from_unixtime(time_close, "%d/%m/%Y") like "%'.$close.'%"');
            })
            ->when($start, function ($query) use ($start) {
                return $query->whereRaw('from_unixtime(time_start, "%d/%m/%Y") like "%'.$start.'%"');
            })
            ->when($work, function ($query) use ($work, $req) {
                return $query->whereNotNull('pekerjaan')
                    ->whereNotNull('jenis_pekerjaan')
                    ->where('pekerjaan', 'like', '%' . $work . '%')
                    ->orWhere('jenis_pekerjaan', 'like', '%' . $work . '%');
            })
            ->orderBy('time_close', 'DESC')
            ->get();
        
    }
    public static function countUnclosedByUser($login)
    {
        return DB::table(self::TABLE)
                ->where([
                    [self::TABLE.'.id_user', $login],
                    ['time_close', '0']
                ])
                ->count();
    }

    public static function getUnclosedByUser($login)
    {
        return self::joinTableGembok()
                ->where([
                    [self::TABLE.'.id_user', $login],
                    ['time_close', '0']
                ])
                ->orderBy('time_start')
                ->get();
    }

    public static function getClosedByUser($login, $pageSize = false)
    {
        $q = self::joinTableGembok()
                ->where([
                    [self::TABLE.'.id_user', $login],
                    ['time_close', '<>', '0000-00-00 00:00:00']
                ])
                ->orderBy('time_start', 'DESC');

        if ($pageSize)
            return $q->paginate($pageSize);
        else
            return $q->get();
    }
    public static function getClosedByUserHome($login)
    {
        $q = self::joinTableGembok()
                ->where([
                    [self::TABLE.'.id_user', $login],
                    ['time_close', '<>', '0000-00-00 00:00:00']
                ])
                ->orderBy('time_start', 'DESC')->limit('3');
            return $q->get();
    }

    public static function getUnclosedByGembok($idGembok)
    {
        return self::joinTableGembok()->where([
                ['id_gembok', $idGembok],
                ['time_close', '0']
            ])
            ->get();
    }

    public static function getUnclosedByFzWitel($auth)
    {
        $query = self::joinTableGembok()->where([
                ['manci_gembok.fiberzone', $auth->fiberzone],
                ['time_close', '0']
            ]);
        if($auth->witel != 'all'){
            $query->where('manci_gembok.witel', $auth->witel);
        }
        return $query->get();
    }

    public static function getClosedByGembok($idGembok, $pageSize = false)
    {
        $q = self::joinTableGembok()->where([
                    ['id_gembok', $idGembok],
                    ['time_close', '<>', '0']
                ])
                ->orderBy('time_start', 'DESC');

        if ($pageSize)
            return $q->paginate($pageSize);
        else
            return $q->get();
    }
    public static function getLibraryByGembok($idGembok, $pageSize = false)
    {
        $q = self::joinTableGembok()->where([
                    ['id_gembok', $idGembok],
                    ['time_close', '<>', '0']
                ])
                ->orderBy('id', 'DESC');

        if ($pageSize)
            return $q->paginate($pageSize);
        else
            return $q->get();
    }
    public static function updateById($id, $data)
    {
        return DB::table(self::TABLE)->where('id', $id)->update($data);
    }

    public static function close($id, $pin)
    {
        $work = self::getById($id);

        DB::transaction(function() use ($work, $pin)
        {
            Gembok::updateById($work->id_gembok, [
                'pin' => $pin
            ]);

            Work::updateById($work->id, [
                'time_close' => time(),
                'pin_new' => $pin
            ]);
        });
    }

    public static function ajaxTiket($id)
    {
        $currentWork = self::getById($id);
        $pastId = DB::table('manci_work')->select('id')->where('id','<',$id)->where('id_gembok',$currentWork->id_gembok)->orderBy('id','desc')->first()->id;
        $pastWork = self::getById($pastId);
        // dd($currentWork);
        return ['current'=>$currentWork,'past'=>$pastWork];
    }

    public static function hasPhoto($id, $fname)
    {
        $path = UPLOAD_PATH . "$id/$fname.jpg";
        if (!file_exists($path)) return false;

        // TODO: return url
        return filemtime($path);
    }
    public static function hasPhotoName($id, $fname)
    {
        $path = UPLOAD_PATH . "$id/$fname";
        if (!file_exists($path)) return false;

        // TODO: return url
        return filemtime($path);
    }

    public static function storePhoto($id, $fname, UploadedFile $fd)
    {
//        $ext = $fd->getClientOriginalExtension();
        $ext = 'jpg';
        $path = UPLOAD_PATH.$id;
        self::ensurePath($path);

        try {
            $moved = $fd->move($path, "$fname.$ext");

            // TODO: resize only if larger than 1000px
            $i = new \Imagick($moved->getRealPath());
            self::autoRotateImage($i);
            $i->scaleImage(100,150, true);
            $i->writeImage("$path/$fname-th.$ext");
        }
        catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
            return 'gagal menyimpan foto evidence '.$fname;
        }
    } 
    // Note: $image is an Imagick object, not a filename! See example use below.
    public static function autoRotateImage($image) {
        $orientation = $image->getImageOrientation();

        switch($orientation) {
            case imagick::ORIENTATION_BOTTOMRIGHT: 
                $image->rotateimage("#000", 180); // rotate 180 degrees
                break;

            case imagick::ORIENTATION_RIGHTTOP:
                $image->rotateimage("#000", 90); // rotate 90 degrees CW
                break;

            case imagick::ORIENTATION_LEFTBOTTOM: 
                $image->rotateimage("#000", -90); // rotate 90 degrees CCW
                break;
        }

        // Now that it's auto-rotated, make sure the EXIF data is correct in case the EXIF gets saved with the image!
        $image->setImageOrientation(imagick::ORIENTATION_TOPLEFT);
    }
    public static function storePhotoPin($id, UploadedFile $fd)
    {
        $ext = 'jpg';
        $path = PIN_PATH;
        self::ensurePath($path);

        try {
            $moved = $fd->move($path, "$id.$ext");
            $i = new \Imagick($moved->getRealPath());
            self::autoRotateImage($i);
            $i->scaleImage(300,300, true);
            $i->writeImage("$path/$id-th.$ext");

            clearstatcache();
        }
        catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
            return 'gagal menyimpan foto evidence '.$fname;
        }
    }
    public static function storePhotoAcakPin($id, UploadedFile $fd)
    {
        $ext = 'jpg';
        $path = ACAK_PIN_PATH;
        self::ensurePath($path);

        try {
            $moved = $fd->move($path, "$id.$ext");
            $i = new \Imagick($moved->getRealPath());
            self::autoRotateImage($i);
            $i->scaleImage(300,300, true);
            $i->writeImage("$path/$id-th.$ext");

            clearstatcache();
        }
        catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
            return 'gagal menyimpan foto evidence '.$fname;
        }
    }
    public static function ensurePath($path)
    {
        if (!file_exists($path))
            if (!mkdir($path, 0770, true))
                throw new \Exception('Gagal menyiapkan folder foto');
    }
    // public static function sendReportTelegram($id, $files, $flag)
    // {
    //     $data = self::getById($id);
    //     if($flag){
    //         $message = "Laporan Close WASAKA \n";
    //     }else{
    //         $message = "Laporan Open WASAKA \n";
    //     }
    //     $message .= 'Perangkat: '.$data->kode."\n\n";
    //     $message .= 'Pekerjaan : '.$data->pekerjaan."\n\n";
    //     $message .= 'Waktu Mulai : '.date('d/m/Y H:i', $data->time_start)."\n";
    //     if($flag){
    //         $message .= 'Waktu Selesai : '.date('d/m/Y H:i', $data->time_close)."\n";
    //         if($data->jenis_pekerjaan_id==15){
    //             $message .= 'Uraian Pekerjaan : '.$data->uraian."\n";
    //         }else{
    //             $message .= 'Uraian Pekerjaan : '.$data->jenis_pekerjaan."\n";
    //         }
    //     }
    //     $message .= 'Anggota : '.$data->anggota."\n";
    //     $message .= 'Oleh : '.($data->nama ?: $data->nama_user)."\n";
    //     $chat = [
    //         'KALSEL' => array(-1001144753925),
    //         'KALBAR' => array(-204295195),
    //         'KALTENG' => array(-112369316),
    //         'SAMARINDA' => array(-207019299),
    //         'BALIKPAPAN' => array(-219960426),
    //         'KALTARA' => array(-1001113873874, -1001122830321),
    //     ];
    //     $chat_id = $chat[$data->wtl];
    //     //$chat_id = 52369916;
    //     foreach($chat[$data->wtl] as $chatloop){
    //         Telegram::sendMessage(["chat_id" => 52369916,
    //             "text" => $chatloop
    //         ]);
    //         /*Telegram::sendMessage(["chat_id" => $chatloop,
    //             "text" => $message
    //         ]);
    //         foreach($files as $file){
    //             $path = UPLOAD_PATH.$id.'/'.$file.'.jpg';
    //             if (file_exists($path)){
    //                 Telegram::sendPhoto([
    //                     "chat_id" => $chatloop,
    //                     "photo" => $path,
    //                     "caption" => $file
    //                 ]);
    //             }
    //         }*/
    //     }
    // }
    
    public static function getPekerjaan()
    {
        return DB::table('manci_jenis_pekerjaan')->get();
    }
    public static function sendReportTelegram2($id, $flag)
    {
        $data = self::getById($id);
        $timezone = Master::getTimezoneByWitel($data->wtl)->timezone;
        // dd($data,$timezone,date('d/m/Y H:i', strtotime($data->date_start.''.$timezone)));
        if($flag){
            $message = "Logbook WASAKA \n======================\n";
            $files = [
                'before',
                'item-before',
                'work-progress',
                'item-after'
            ];
        }else{
            $message = "Laporan Open WASAKA \n======================\n";
            $files = array('before');
        }
        $message .= 'Perangkat: '.$data->kode."\n\n";
        if($data->jenis_pekerjaan_id==15){
                $message .= 'Pekerjaan : '.$data->pekerjaan."\n\n";
            }else{
                $message .= 'Pekerjaan : '.$data->jenis_pekerjaan."\n\n";
            }

        $message .= 'Waktu Mulai : '.date('d/m/Y H:i', strtotime($data->date_start.''.$timezone))."\n";
        if($flag){
            $dteStart = new \DateTime(date('Y-m-d H:i:s', $data->time_start)); 
            $dteEnd   = new \DateTime(date('Y-m-d H:i:s', $data->time_close)); 
            $dteDiff  = $dteStart->diff($dteEnd); 
            $durasi   = $dteDiff->format("%ad %Hh %Im %Ss");
            $message .= 'Waktu Selesai : '.date('d/m/Y H:i', strtotime($data->date_close.''.$timezone))."\n";
            $message .= 'Durasi : '.$durasi."\n";
            $message .= 'Uraian Pekerjaan : '.$data->uraian."\n";
        }
        $message .= 'Anggota : '.$data->anggota."\n";
        $message .= 'Oleh : '.($data->nama ?: $data->nama_user)."\n";
        
        $witel = DB::table('manci_witel')->where('witel', $data->wtl)->first();
        $chat_id = $witel->chat_id;
        $kord = explode(",",$data->koordinat);
        if($data->wtl == 'KALSEL' && $data->kategori==1){
            Telegram::sendMessage(["chat_id" => "-1001350160667",
                "text" => $message
            ]);
            if(isset($kord[0]) && isset($kord[1])){
                if(count($kord) == 2){
                    Telegram::sendLocation([
                        'chat_id' => "-1001350160667", 
                        'latitude' => $kord[0],
                        'longitude' => $kord[1]
                    ]);
                }else{
                    Telegram::sendMessage([
                        'chat_id' => "-1001350160667", 
                        "text" => "Location Not Valid"
                    ]);
                }
            }
            foreach($files as $file){
                //$path = UPLOAD_PATH.$id.'/'.$file.'.jpg';
                $path = public_path()."/upload/work/".$id."/".$file.".jpg";
                
                if (file_exists($path)){
                    $cap = '-';
                    if($file == 'before')
                        $cap = 'Foto Sebelum Pekerjaan';
                    if($file == 'item-before')
                        $cap = 'Foto Perangkat Sebelum Pekerjaan';
                    if($file == 'work-progress')
                        $cap = 'Foto Kegiatan';
                    if($file == 'item-after')
                        $cap = 'Foto Perangkat Setelah Pekerjaan';
                    Telegram::sendPhoto([
                        "chat_id" => "-1001350160667",
                        "photo" => $path,
                        "caption" => $cap
                    ]);
                }
            }    
        }
        Telegram::sendMessage(["chat_id" => $chat_id,
            "text" => $message
        ]);

        if(isset($kord[0]) && isset($kord[1])){
            if(count($kord) == 2){
                Telegram::sendLocation([
                    'chat_id' => $chat_id, 
                    'latitude' => $kord[0],
                    'longitude' => $kord[1]
                ]);
            }else{
                Telegram::sendMessage([
                    "chat_id" => $chat_id,
                    "text" => "Location Not Valid"
                ]);
            }
        }
        foreach($files as $file){
            //$path = UPLOAD_PATH.$id.'/'.$file.'.jpg';
            $path = public_path()."/upload/work/".$id."/".$file.".jpg";
            if (file_exists($path)){
                $cap = '-';
                if($file == 'before')
                    $cap = 'Foto Sebelum Pekerjaan';
                if($file == 'item-before')
                    $cap = 'Foto Perangkat Sebelum Pekerjaan';
                if($file == 'work-progress')
                    $cap = 'Foto Kegiatan';
                if($file == 'item-after')
                    $cap = 'Foto Perangkat Setelah Pekerjaan';
                Telegram::sendPhoto([
                    "chat_id" => $chat_id,
                    "photo" => $path,
                    "caption" => $cap
                ]);
            }
        }
        
    }
    public static function sendReportAdmin($id)
    {
        $data = self::getById($id);
        if($data->owner_id == 0){
            $message = 'Perangkat: #'.$data->kode."\n";
            $message .= 'Pin Baru: '.$data->pin_new."\n";
            if($data->jenis_pekerjaan_id==15){
                $message .= 'Pekerjaan : '.$data->pekerjaan."\n";
            }else{
                $message .= 'Pekerjaan : '.$data->jenis_pekerjaan."\n";
            }
            $message .= 'Uraian Pekerjaan : '.$data->uraian."\n";
            $message .= 'Oleh : #'.($data->nama ?: $data->nama_user)."\n";
            $witel = DB::table('manci_witel')->where('witel', $data->wtl)->first();
            $chat_id = $witel->admin_chat;

            
            $path = public_path()."/upload/pin/".$id.".jpg";
            if (file_exists($path)){
                Telegram::setTimeOut(60);
                Telegram::sendPhoto([
                    "chat_id" => $chat_id,
                    "photo"   => $path,
                    "caption" => $message
                ]);
            }
            $path = public_path()."/upload/pin/acak/".$id.".jpg";
            if (file_exists($path)){
                Telegram::setTimeOut(60);
                Telegram::sendPhoto([
                    "chat_id" => $chat_id,
                    "photo"   => $path,
                    "caption" => "PIN ACAK ".$data->kode."\nPIN ACAK:".$data->pin_acak
                ]);
            }
        }
    }

    public static function sendReportTelegramOpen($id)
    {
        $data = self::getById($id);
        $timezone = Master::getTimezoneByWitel($data->wtl)->timezone;
        $message = "Laporan Open WASAKA \n======================\n";
        $files = array('before');
        
        $message .= 'ID Wasaka : '.$data->id."\n\n";
        $message .= 'Perangkat : '.$data->kode."\n\n";
        $message .= 'Pekerjaan : '.$data->jenis_pekerjaan."\n\n";
        $message .= 'Waktu Mulai : '.date('d/m/Y H:i', strtotime($data->date_start.''.$timezone))."\n";
        $message .= 'Anggota : '.$data->anggota."\n";
        $message .= 'Oleh : '.$data->id_user.'/'.($data->nama ?: $data->nama_user)."\n";
        $message .= 'Loker : '.$data->loker."\n";
        
        $witel = DB::table('manci_witel')->where('witel', $data->wtl)->first();
        // $chat_id = '-200897635';//innovator
        if($data->owner_id == 0){
            $chat_id = $witel->chat_id;
        }else{
            $chat_id = $witel->mtel_chat;
        }
        
        $kord = explode(",",$data->koordinat);
        Telegram::sendMessage(["chat_id" => $chat_id,
            "text" => $message
        ]);
        if(isset($kord[0]) && isset($kord[1])){
            if(count($kord) == 2){
                Telegram::sendLocation([
                    'chat_id' => $chat_id, 
                    'latitude' => $kord[0],
                    'longitude' => $kord[1]
                ]);
            }else{
                Telegram::sendMessage([
                    "chat_id" => $chat_id,
                    "text" => "Location Not Valid"
                ]);
            }
        }
        foreach($files as $file){
            //$path = UPLOAD_PATH.$id.'/'.$file.'.jpg';
            $path = public_path()."/upload/work/".$id."/".$file.".jpg";
            if (file_exists($path)){
                $cap = '-';
                if($file == 'before')
                    $cap = 'Foto Sebelum Pekerjaan';
                Telegram::sendPhoto([
                    "chat_id" => $chat_id,
                    "photo" => $path,
                    "caption" => $cap
                ]);
            }
        }
        
    }
    public static function sendReportTelegramClose($id)
    {
        $data = self::getById($id);
        $timezone = Master::getTimezoneByWitel($data->wtl)->timezone;
        $message = "Logbook WASAKA \n======================\n";
        $files = [
            'before',
            'item-before',
            'work-progress',
            'item-after'
        ];
        $message .= 'ID Wasaka: '.$data->id."\n\n";
        $message .= 'Perangkat: '.$data->kode."\n\n";
        $message .= 'Pekerjaan : '.$data->jenis_pekerjaan."\n\n";
        $message .= 'Waktu Mulai : '.date('d/m/Y H:i', strtotime($data->date_start.''.$timezone))."\n";
        $dteStart = new \DateTime(date('Y-m-d H:i:s', $data->time_start)); 
        $dteEnd   = new \DateTime(date('Y-m-d H:i:s', $data->time_close)); 
        $dteDiff  = $dteStart->diff($dteEnd); 
        $durasi   = $dteDiff->format("%ad %Hh %Im %Ss");
        $message .= 'Waktu Selesai : '.date('d/m/Y H:i', strtotime($data->date_close.''.$timezone))."\n";
        $message .= 'Durasi : '.$durasi."\n";
        $message .= 'Uraian Pekerjaan : '.$data->uraian."\n";
        $message .= 'Anggota : '.$data->anggota."\n";
        $message .= 'Oleh : '.$data->id_user.'/'.($data->nama ?: $data->nama_user)."\n";
        $message .= 'Loker : '.$data->loker."\n";
        
        $witel = DB::table('manci_witel')->where('witel', $data->wtl)->first();
        if($data->owner_id == 0){
            $chat_id = $witel->chat_id;
        }else{
            $chat_id = $witel->mtel_chat;
        }
        // $chat_id = '-200897635';
        $kord = explode(",",$data->koordinat);
        
        Telegram::sendMessage(["chat_id" => $chat_id,
            "text" => $message
        ]);
        if(isset($kord[0]) && isset($kord[1])){
            if(count($kord) == 2){
                Telegram::sendLocation([
                    'chat_id' => $chat_id, 
                    'latitude' => $kord[0],
                    'longitude' => $kord[1]
                ]);
            }else{
                Telegram::sendMessage([
                    "chat_id" => $chat_id,
                    "text" => "Location Not Valid"
                ]);
            }
        }
        foreach($files as $file){
            //$path = UPLOAD_PATH.$id.'/'.$file.'.jpg';
            $path = public_path()."/upload/work/".$id."/".$file.".jpg";
            if (file_exists($path)){
                $cap = '-';
                if($file == 'before')
                    $cap = 'Foto Sebelum Pekerjaan';
                if($file == 'item-before')
                    $cap = 'Foto Perangkat Sebelum Pekerjaan';
                if($file == 'work-progress')
                    $cap = 'Foto Kegiatan';
                if($file == 'item-after')
                    $cap = 'Foto Perangkat Setelah Pekerjaan';
                Telegram::sendPhoto([
                    "chat_id" => $chat_id,
                    "photo" => $path,
                    "caption" => $cap
                ]);
            }
        }
        
    }
}