<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;
class MC
{
  public static function get_port_odc($id)
  {
  	return self::query()->where('mc_odc_port.odc_id', $id)->get();
  }
  public static function get_port_odc_by_id($id)
  {
  	return self::query()->where('mc_odc_port.id', $id)->first();
  }
  public static function get_splitter_link_by_id($id)
  {
  	return self::query()->where('mc_splitter_link.id', $id)->first();
  }
  public static function get_kabel_link_by_id($id)
  {
  	return self::query()->where('mc_kabel_link.id', $id)->first();
  }
  public static function get_splitter($id)
  {
  	return DB::table('mc_splitter')
  	->select('mc_splitter.*','mc_spl_count.in_count','mc_spl_count.out_count')
  	->leftJoin(DB::raw('(SELECT SUM(case when number = 0 and mc_odc_port.front_link is not null then 1 else 0 end) as in_count,SUM(case when number >= 1 and status is not null then 1 else 0 end) as out_count, `spl_id` FROM `mc_splitter_link` left join mc_odc_port on mc_splitter_link.id=mc_odc_port.front_link GROUP BY `spl_id` ORDER BY `spl_id`) as mc_spl_count'), 'mc_splitter.id','=','mc_spl_count.spl_id')
  	->where('terminal_id', $id)->get();
  }
  public static function get_splitter_link($id)
  {
  	return DB::table('mc_splitter_link')
  	->select('mc_splitter_link.*', 'jenis')
  	->leftJoin('mc_splitter', 'mc_splitter_link.spl_id','=','mc_splitter.id')
  	->where('terminal_id', $id)->get();
  }
  private static function query(){
  	return DB::table('mc_odc_port')
  	->leftJoin('mc_splitter_link','mc_odc_port.front_link', '=', 'mc_splitter_link.id')
  	->leftJoin('mc_splitter','mc_splitter_link.spl_id', '=', 'mc_splitter.id')
  	->leftJoin('mc_kabel_link','mc_odc_port.back_link', '=', 'mc_kabel_link.id')
  	->leftJoin('mc_kabel','mc_kabel_link.kbl_id', '=', 'mc_kabel.id')
  	->select('mc_odc_port.*','mc_splitter_link.number as number_spl','mc_splitter.id as spl_id','mc_kabel_link.kbl_id', 'mc_kabel_link.core','mc_kabel.nama_kabel','mc_splitter_link.status as status_link_spl', 'mc_kabel_link.status_core', 'mc_kabel_link.redaman','mc_odc_port.user as status_port_user','mc_odc_port.ts as status_port_ts');
  }

  public static function get_kabel_odc($id)
  {
  	return DB::table('mc_kabel')
  	->leftJoin(DB::raw('(SELECT SUM(case when status_core = "reti" then 1 else 0 end) as reti_count,`kbl_id` FROM `mc_kabel_link` GROUP BY `kbl_id` ORDER BY `kbl_id`) as mc_core_count'), 'mc_kabel.id','=','mc_core_count.kbl_id')
  	->where('mc_kabel.odc_id', $id)->where('mc_kabel.status_kabel', 1)->get();
  }
  public static function get_kabel_by_id($id)
  {
  	return DB::table('mc_kabel')
  	->where('id', $id)->where('status_kabel', 1)->first();
  }
  public static function get_spl_link_by_number($number,$spl_id)
  {
  	return DB::table('mc_splitter_link')
  	->where('spl_id', $spl_id)->where('number', $number)->first();
  }
  public static function get_kabel_link_by_core($id,$kbl_id)
  {
  	return DB::table('mc_kabel_link')
  	->where('core', $id)->where('kbl_id', $kbl_id)->first();
  }
  public static function get_kabel_core($id)
  {
  	return self::query()->where('mc_kabel.id', $id)->get();
  }
  public static function insert_kabel($param)
  {
  	return DB::table('mc_kabel')->insert($param);
  }
  public static function update_kabel($param,$id)
  {
  	return DB::table('mc_kabel')->where('id', $id)->update($param);
  }
  public static function insert_log($param)
  {
  	return DB::table('mc_log_odc')->insert($param);
  }
  public static function get_logs($odc_id)
  {
  	return DB::table('mc_log_odc')->where('odc_id', $odc_id)->orderBy('id','desc')->get();
  }
  public static function insert_kbl_link($param)
  {
  	return DB::table('mc_kabel_link')->insertGetId($param);
  }
  public static function update_kbl_link($param,$id)
  {
  	return DB::table('mc_kabel_link')->where('id', $id)->update($param);
  }
  public static function insert_spl_link($param)
  {
  	return DB::table('mc_splitter_link')->insertGetId($param);
  }
  public static function upate_spl_link($param,$id)
  {
  	return DB::table('mc_splitter_link')->where('id', $id)->update($param);
  }
  public static function insert_spl($param)
  {
  	return DB::table('mc_splitter')->insertGetId($param);
  }
  public static function insert_port($param)
  {
  	return DB::table('mc_odc_port')->insertGetId($param);
  }
  public static function update_port($param,$id)
  {
  	return DB::table('mc_odc_port')->where('id', $id)->update($param);
  }
  public static function add_basetray($param)
  {
  	return DB::table('mc_basetray')->insert($param);
  }
  public static function update_basetray($id,$param)
  {
  	return DB::table('mc_basetray')->where('id',$id)->update($param);
  }
  public static function get_basetray($odc_id)
  {
  	$count = DB::select("SELECT sum(case when (mop.status_port is null and mop.front_link is null and mop.back_link is not null) then 1 else 0 end) as free_count,
			sum(case when mkl.status_core in ('reti') then 1 else 0 end) as reti_count, basetray_number 
			FROM mc_odc_port mop
			left join mc_kabel_link mkl on mop.back_link=mkl.id
      where odc_id = ".$odc_id."
			GROUP BY mop.basetray_number ORDER BY mop.basetray_number");
  	$basetray = DB::table('mc_basetray')->selectRaw('*,(case when id is null then 1 else 0 end) as free_count')->where('odc_id',$odc_id)->get();
    // dd($basetray,$count);
  	foreach($count as $c){
  		$basetray[$c->basetray_number-1]->free_count = $c->free_count-$c->reti_count;
  	}
  	return $basetray;
  }
  public static function get_port_by_basetray($odc_id,$basetray)
  {
  	return self::query()->where('mc_odc_port.odc_id', $odc_id)->where('mc_odc_port.basetray_number', $basetray)->get();
  }

}