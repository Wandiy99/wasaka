<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;
class Loker
{
	private static function joinTable()
  {
  	return DB::table('manci_gembok')->select('*', 'manci_gembok.id as gembok_id')->leftJoin('manci_lemari','manci_gembok.lemari_id', '=', 'manci_lemari.id')->where('manci_gembok.kategori',9)->where('manci_gembok.isHapus',0);
  }
  public static function get_list_lemari()
  {
  	return DB::table('manci_lemari')->get();
  }
  public static function get_lemari_by_id($id)
  {
  	return DB::table('manci_lemari')->where('id', $id)->first();
  }
  public static function get_list_loker($id)
  {
  	return self::joinTable()->where('lemari_id', $id)->get();
  }
  public static function get_loker_by_id($id)
  {
  	return self::joinTable()->where('manci_gembok.id', $id)->first();
  }
  public static function insert($table, $data)
  {
  	return DB::table($table)->insertGetId($data);
  }
  public static function insertOnly($table, $data)
  {
  	return DB::table($table)->insert($data);
  }
  public static function update($table, $data, $id)
  {
  	return DB::table($table)->where('id', $id)->update($data);
  }

  public static function insertItems($table, $req, $gembok_id)
  {
  	foreach($req['items'] as $k => $i){
      $insert[] = ['gembok_id'=>$gembok_id,'item'=>$k];
    }
    DB::table($table)->where('gembok_id', $gembok_id)->delete();
  	return DB::table($table)->insert($insert);
  }

  public static function get_list_approval()
  {
  	return DB::table('manci_work')->select('manci_work.*', 'manci_gembok.produk_id', 'manci_gembok.kode')->leftJoin('manci_gembok', 'manci_work.id_gembok', '=', 'manci_gembok.id')->whereIn('step', [1,4])->get();
  }
  public static function get_data_transaksi($id)
  {
  	return DB::table('manci_work')->select('manci_work.*', 'manci_gembok.produk_id', 'manci_gembok.kode', 'manci_gembok.merk', 'manci_gembok.sn_splicer','manci_lemari.so', 'manci_gembok.last_online', DB::raw('MOD(TIMESTAMPDIFF(MINUTE, last_online, now()), 60) AS last_online_menit'))
  	->leftJoin('manci_gembok', 'manci_work.id_gembok', '=', 'manci_gembok.id')
  	->leftJoin('manci_lemari','manci_gembok.lemari_id', '=', 'manci_lemari.id')
  	->where('manci_work.id', $id)->first();
  }
  public static function get_data_items($id)
  {
  	return DB::table('kabinet_item_pinjam')->where('work_id', $id)->get();
  }
  public static function get_items($id)
  {
  	return DB::table('kabinet_item')->where('gembok_id', $id)->get();
  }
  public static function get_items_pinjam($id)
  {
  	return DB::table('kabinet_item_pinjam')->where('work_id', $id)->get();
  }
  //1:permohonan pinjam,2:rejected,3:dipinjam,4:permohonan return,5:rejected,6:returned;
  public static function cek_loker_open($idgembok)
  {
  	return DB::table('manci_work')->where('id_gembok', $idgembok)->whereIn('step', [1,3,4,5])->first();
  }
  public static function insert_start($idgembok, $req)
  {
  	$work_id = 0;
  	DB::transaction(function() use($req, $idgembok,&$work_id) {
  		$pekerjaan = DB::table('manci_jenis_pekerjaan')->where('id', $req->jenis_pekerjaan_id)->first()->jenis_pekerjaan;
      $insertWork = ["step"=>1,"jenis_pekerjaan_id"=>$req->jenis_pekerjaan_id, "id_user" =>session('auth')->id_user, "nama_user"=>session('auth')->nama, "pekerjaan"=> $pekerjaan, "time_start"=> time(),"id_gembok"=>$idgembok];
      $work_id = self::insert('manci_work', $insertWork);
	    $insertItems = [];
	    foreach($req->items as $k => $i){
	      $insertItems[] = ["work_id"=> $work_id,"kabinet_item_id"=> $i,"item"=> $k];
	    }
	    DB::table('kabinet_item_pinjam')->where('work_id', $work_id)->delete();
	    self::insertOnly('kabinet_item_pinjam', $insertItems);
	    
    });
  	return $work_id;
  }
	public static function query_close($work_id, $req)
  {
  	DB::transaction(function() use($req, $work_id) {
  		$queryWork = ["step"=>4, "arc_count"=> $req->arc, "catatan"=> $req->catatan];
      self::update('manci_work', $queryWork, $work_id);
	    foreach($req->ratings as $k => $i){
	      self::update('kabinet_item_pinjam', ["feedback"=>$i], $k);
	    }
    });
  }
  
}