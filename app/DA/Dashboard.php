<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;
class Dashboard
{
    public static function top10odc($priode, $witel)
    {
        return DB::select('SELECT COUNT(kode) AS Baris, b.kode 
        	FROM manci_work a 
        	LEFT JOIN manci_gembok b ON a.id_gembok=b.id
        	WHERE '.$witel.' kategori != 1 AND from_unixtime(time_start) like "'.$priode.'%" 
        	GROUP BY b.kode 
        	ORDER BY Baris DESC LIMIT 0,10');
    }
    public static function top10user($priode, $witel)
    {
        return DB::select('SELECT COUNT(nama_user) AS Baris, nama_user 
        	FROM manci_work a
        	LEFT JOIN manci_gembok b ON a.id_gembok = b.id
        	WHERE '.$witel.' kategori != 1 AND from_unixtime(time_start) like "'.$priode.'%"
        	GROUP BY nama_user 
        	ORDER BY Baris DESC LIMIT 0,10');
    }
    public static function top10time($priode, $witel)
    {
        return DB::select('SELECT TIME_FORMAT(SEC_TO_TIME(sum(TIMESTAMPDIFF(SECOND,from_unixtime(time_start),from_unixtime(time_close)))),"%Hh %im") AS Baris, b.kode 
        	FROM manci_work a 
        	LEFT JOIN manci_gembok b ON a.id_gembok = b.id 
        	WHERE '.$witel.' kategori != 1 AND from_unixtime(time_start) like "'.$priode.'%"
        	GROUP BY b.kode ORDER BY Baris DESC Limit 0,10');
    }
    public static function top10work($priode, $witel)
    {
        return DB::select('SELECT TIME_FORMAT(SEC_TO_TIME(avg(TIMESTAMPDIFF(SECOND,from_unixtime(time_start),from_unixtime(time_close)))),"%Hh %im") as average, COUNT(jenis_pekerjaan_id) AS Baris, jenis_pekerjaan 
            FROM manci_work a
            LEFT JOIN manci_jenis_pekerjaan b ON a.jenis_pekerjaan_id = b.id
            LEFT JOIN manci_gembok c ON a.id_gembok = c.id 
            WHERE '.$witel.' kategori != 1 AND jenis_pekerjaan_id != 15 AND from_unixtime(time_start) like "'.$priode.'%"
            GROUP BY jenis_pekerjaan 
            ORDER BY Baris DESC LIMIT 0,10');
    }
}