<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;
DEFINE('PROFILE_PATH', getcwd().'/upload/profile/');
class Gembok
{
    const TABLE = 'manci_gembok';

    public static function create(array $input)
    {
        return DB::table(self::TABLE)->insertGetId($input);
    }

    public static function getById($id)
    {
        return DB::table(self::TABLE)->where('id', $id)->first();
    }
    public static function getByPID($pid)
    {
        return DB::table(self::TABLE)->where('produk_id', $pid)->first();
    }
    public static function getByKode($kode)
    {
        return DB::table(self::TABLE)->where('kode', $kode)->first();
    }
    public static function getListByWitel($witel)
    {
        return DB::table(self::TABLE)->where('witel', $witel)->get();
    }
    public static function getAll()
    {
        return DB::table(self::TABLE)->whereNotNull('koordinat')->where('koordinat', '<>', '')->where('isHapys', 0)->get();
    }

    public static function listAll()
    {
        return DB::select('SELECT *,witel as w,(select COUNT(*) FROM manci_gembok where witel=w and kategori is not null and kategori <> 0) as Rows from manci_gembok
        WHERE kategori is not null and kategori <> 0 and witel is not null ORDER BY Rows desc, witel asc, kategori asc');
    }

    public static function updateById($id, array $input)
    {
        return DB::table(self::TABLE)->where('id', $id)->update($input);
    }
    public static function updateLogPin($input)
    {
        return DB::table('adm_pin_log')->insert($input);
    }

    public static function search($keyword)
    {
        $criteria = '%'.str_replace([' ','-','_'], '%', $keyword).'%';
        $q = DB::table(self::TABLE)
            ->select(self::TABLE.'.*', 'manci_witel.fiberzone', 'manci_witel.regional','jenis_alpro.jenis_alpro')
            ->leftJoin('manci_witel', self::TABLE.'.witel', '=', 'manci_witel.witel')
            ->leftJoin('jenis_alpro', self::TABLE.'.kategori', '=', 'jenis_alpro.id')
            ->where('isHapus',0)
            ->where('kode', 'LIKE', $criteria);
        // if(!empty(session('auth')->fiberzone) && session('auth')->fiberzone != "ALL"){
        //     $q->where('manci_gembok.fiberzone', session('auth')->fiberzone);
        // }
        if(session('auth')->witel != "ALL"){
            $q->where(self::TABLE.'.witel', session('auth')->witel);
        }

        return $q->get();
    }
    public static function ensurePath($path)
    {
        if (!file_exists($path))
            if (!mkdir($path, 0770, true))
                throw new \Exception('Gagal menyiapkan folder foto');
    }
    public static function hasPhoto($id)
    {
        $path = PROFILE_PATH . "$id.jpg";
        if (!file_exists($path)) return false;

        // TODO: return url
        return filemtime($path);
    }
    public static function storePhotoProfile($id, UploadedFile $fd)
    {
        $ext = 'jpg';
        $path = PROFILE_PATH;
        self::ensurePath($path);

        try {
            $moved = $fd->move($path, "$id.$ext");
            $i = new \Imagick($moved->getRealPath());
            $i->scaleImage(1000,1000, true);
            $i->writeImage("$path/$id.$ext");

            $i->scaleImage(300,300, true);
            $i->writeImage("$path/$id-th.$ext");

            clearstatcache();
        }
        catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
            return 'gagal menyimpan foto evidence '.$fname;
        }
    }
    public static function merkList()
    {
        return DB::table('manci_merk')->select('merk as id', 'merk as text')->get();
    }
    public static function getUnakses($reg,$wtl,$owner)
    {
        $start = date('Y-m-d',strtotime(date('Y-m-d') . "-30 days"));
        $now = date('Y-m-d',strtotime(date('Y-m-d') . "+1 days"));
        $query = DB::table('manci_gembok')->where('isHapus', 0)->where('owner_id', $owner)->whereNotBetween('last_akses', [$start, $now]);
        if($wtl!=='all'){
           $query->where('witel',$wtl);
        }if($reg!=='all'){
           $query->where('regional',$reg);
        }
        return $query->orderBy('last_akses', 'desc')->get();
    }

}