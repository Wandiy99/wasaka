<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;
class Master
{
    //master
    public static function getWitelChatID()
    {
        return DB::table('manci_witel')->whereNotNull('chat_id')->get();
    }
    public static function getFiberzoneAll()
    {
        return DB::table('manci_witel')->select('fiberzone as id','fiberzone as text')->groupBy('fiberzone')->get();
    }
    public static function getRegionalAll()
    {
        return DB::table('manci_witel')->select('regional as id','regional as text')->groupBy('regional')->get();
    }
    public static function getWitelByFiberzone($fz)
    {
        return DB::table('manci_witel')->select('witel as id','witel as text')->where('fiberzone', $fz)->groupBy('witel')->get();
    }
    public static function getWitelByRegional($reg)
    {
        return DB::table('manci_witel')->select('witel as id','witel as text')->where('regional', $reg)->groupBy('witel')->get();
    }
    public static function getRegionalByFiberzone($fz)
    {
        $data = DB::table('manci_witel')->select('regional')->where('fiberzone', $fz)->first();
        if(count($data))
            $return = $data->regional;
        else
            $return = null;
        return $return;
    }
    public static function getInstansi()
    {
        return DB::table('instansi')->select('instansi as id', 'instansi as text')->get();
    }
    public static function getLoker()
    {
        return DB::table('loker')->select('loker as id', 'loker as text')->get();
    }

    //user
    public static function getUser($reg,$wtl)
    {
        $query = DB::table('user');
        if($reg!='all'){
            $query->where('regional',$reg);
        }
        if($wtl!='all'){
            $query->where('wtl',$wtl);
        }
        // dd($query);
        return $query->get();
    }
    public static function getUserByID($id)
    {
        return DB::table('user')->where('id_user',$id)->first();
    }
    public static function getUserByRecoveryLink($link)
    {
        return DB::table('user')->where('recovery_link',$link)->first();
    }
    public static function insertUser($input)
    {
        return DB::table('user')->insertGetId($input);
    }
    public static function updateUser($id, $update)
    {
        DB::table('user')->where('id_user',$id)->update($update);
    }

    //gembok
    public static function getGembok($fz,$wtl)
    {
        $query = DB::table('manci_gembok as mg')->leftJoin('jenis_alpro as ja', 'mg.kategori','=','ja.id')->select('mg.*','ja.jenis_alpro');
        if($fz!='all'){
            $query->where('fiberzone',$fz);
        }
        if($wtl!='all'){
            $query->where('witel',$wtl);
        }
        // dd($query);
        return $query->orderBy('mg.ts', 'asc')->get();
    }
    public static function getGembokByRegional($reg,$wtl,$owner)
    {
        $query = DB::table('manci_gembok as mg')->leftJoin('jenis_alpro as ja', 'mg.kategori','=','ja.id')->select('mg.*','ja.jenis_alpro');
        if($reg!='all'){
            $query->where('regional',$reg);
        }
        if($wtl!='all'){
            $query->where('witel',$wtl);
        }
        // dd($query);
        return $query->where('owner_id',$owner)->orderBy('mg.ts', 'asc')->get();
    }

    public static function getGembokSearch($q)
    {
        $auth = session('auth');
        // dd($auth);
        $query = DB::table('manci_gembok as mg')
        ->leftJoin('jenis_alpro as ja', 'mg.kategori','=','ja.id')
        ->select('mg.*','ja.jenis_alpro')
        ->where('kode','like','%'.$q.'%')
        ->orderBy('mg.ts', 'asc');
        if($auth->wtl!='all'){
            $query->where('witel',$auth->wtl);
        }
        return $query->get();
    }
    public static function getGembokByID($id)
    {
        return DB::table('manci_gembok')->where('id',$id)->first();
    }
    public static function getGembokByKode($kode)
    {
        return DB::table('manci_gembok')->where('kode',$kode)->first();
    }
    public static function insertGembok($input)
    {
        return DB::table('manci_gembok')->insertGetId($input);
    }
    public static function updateGembok($id, $update)
    {
        DB::table('manci_gembok')->where('id',$id)->update($update);
    }
    public static function profileSave($id, $update)
    {
        DB::table('user')->where('id_user',$id)->update($update);
    }
    public static function getTimezoneByWitel($witel)
    {
        return DB::table('manci_witel')->where('witel',$witel)->first();
    }
    public static function getTimezoneGembokID($id)
    {
        return DB::table('manci_witel')->where('witel',self::getGembokByID($id)->witel)->first();
    }
    //list gembok
    public static function getRecordsGembok($owner,$reg,$wtl,$start,$rowperpage,$columnName,$columnSortOrder,$searchValue){
        // dd($rowperpage);
        $query = DB::table('manci_gembok as mg')->leftJoin('jenis_alpro as ja', 'mg.kategori','=','ja.id')->select('mg.*','ja.jenis_alpro');
        if($reg!='all'){
            $query->where('regional',$reg);
        }
        if($wtl!='all'){
            $query->where('witel',$wtl);
        }
        return $query
        ->where('isHapus', 0)
        ->where('owner_id', $owner)
        ->where('kode', 'like', '%' .$searchValue . '%')
        ->orderBy($columnName,$columnSortOrder)
        ->skip($start)
        ->take($rowperpage)
        ->get();
    }
    public static function countTotalRecordGembok($owner,$reg,$wtl){
        $query = DB::table('manci_gembok as mg');
        if($reg!='all'){
            $query->where('regional',$reg);
        }
        if($wtl!='all'){
            $query->where('witel',$wtl);
        }
        return $query->select('count(*) as allcount')->where('isHapus', 0)
        ->where('owner_id', $owner)->count();
    }
    public static function countTotalRecordswithFilterGembok($owner,$reg,$wtl,$search){
        $query = DB::table('manci_gembok as mg');
        if($reg!='all'){
            $query->where('regional',$reg);
        }
        if($wtl!='all'){
            $query->where('witel',$wtl);
        }
        return $query->select('count(*) as allcount')
        ->where('owner_id', $owner)->where('isHapus', 0)->where('kode', 'like', '%' .$search . '%')
        ->count();
    }
    public static function insertLOGAPI($param){
        DB::table('API_LOG')->insert($param);
    }
    //tiket
    public static function getTiket($reg,$wtl)
    {
        $query = DB::table('tiket');
        if($reg!='all'){
            $query->where('regional',$reg);
        }
        if($wtl!='all'){
            $query->where('wtl',$wtl);
        }
        return $query->get();
    }
    public static function getTiketByID($id)
    {
        return DB::table('tiket')->where('id_tiket',$id)->first();
    }
    public static function tiketExists($id)
    {
        return DB::table('tiket')->where('work_id',$id)->first();
    }
    public static function insertTiket($input)
    {
        return DB::table('tiket')->insertGetId($input);
    }
    public static function updateTiket($id, $update)
    {
        DB::table('tiket')->where('id_tiket',$id)->update($update);
    }
}