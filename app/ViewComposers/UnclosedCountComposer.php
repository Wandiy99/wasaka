<?php

namespace App\ViewComposers;

use Illuminate\Http\Request;
use Illuminate\View\View;

use App\DA\Work;

class UnclosedCountComposer
{
    protected $req;

    public function __construct(Request $req)
    {
        $this->req = $req;
    }

    public function compose(View $view)
    {
        $auth = $this->req->session()->get('auth');
        $all = false;
        $byUser = false;
        $witel = [];
        // $all = Work::countUnclosed();
        // $byUser = Work::countUnclosedByUser($auth->id_user);
        // $witel = Work::getUnclosedByFzWitel($auth);
        // dd($witel);
        $view->with('unclosedCount', ['all' => $all, 'byUser' => $byUser, 'witel' => $witel]);
    }
}