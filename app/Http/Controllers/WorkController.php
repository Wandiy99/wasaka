<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DA\Gembok;
use App\DA\Work;
use App\DA\User;
use App\DA\Master;
use DB;
use Telegram;
use Validator;
use DateTime;
class WorkController extends Controller
{
    private $files = [
        'before',
        'item-before',
        'work-progress',
        'item-after'
    ];
    private $chat = [
        'KALSEL' => -1001144753925,
        'KALBAR' => -204295195,
        'KALTENG' => -112369316,
        'SAMARINDA' => -207019299,
        'BALIKPAPAN' => -219960426,
        'KALTARA' => -199643626,
    ];
    private $witel = [
            'KALSEL',
            'KALBAR',
            'KALTENG',
            'SAMARINDA',
            'BALIKPAPAN',
            'KALTARA'
        ];
    // public function modifydata(){
    //     $data = DB::select("SELECT * FROM `manci_gembok` WHERE `koordinat` NOT LIKE '%.%' and koordinat != '' ORDER BY `manci_gembok`.`koordinat` DESC");
    //     foreach($data as $d){
    //         $kord = explode(",", $d->koordinat);
    //         if(count($kord)==4){

    //         echo "update manci_gembok set koordinat = '".$kord[0].".".$kord[1].", ".$kord[2].".".$kord[3]."' where id=".$d->id.";</br>";
    //         }
    //     }
    // }
    public function startForm($idGembok)
    { 
        // TODO: prevent work on unclosed
        $userid = session('auth')->id_user;
        $havingUnclosed = Work::isHavingUnclosed($userid);

        $gembok = Gembok::getById($idGembok);
        $akses = User::getProfile($userid)->akses_perangkat;
        //dd($gembok->kategori);
        if(!str_contains($akses, [$gembok->kategori])){
            return view('errors.denied');
        }
        $gembok->profile = Gembok::hasPhoto($idGembok);
        $pekerjaan = Work::getPekerjaan();
        $flag = Work::isOpen($idGembok);
        return view('work/start', compact('gembok', 'flag', 'pekerjaan', 'havingUnclosed'));
    }

    public function create(Request $req, $idGembok)
    {
        $flag = Work::isOpen($idGembok);
        if(!$flag){
            $data = Gembok::getById($idGembok);
            $rules = array(
                'anggota' => 'required',
                'before' => 'required|max:1000',
            );
            $messages = [
                'anggota.required' => 'Silahkan isi kolom anggota Bang!',
                'before.required' => 'Silahkan isi kolom upload Bang!',
                'pekerjaan.required' => 'Silahkan isi kolom pekerjaan Bang!',
                'before.max' => 'Ukuran Photo harus dibawah 1mb, Silahkan install GPS Map Camera dan setting resolusi menjadi 720x1280 Bang!'
            ];
            $validator = Validator::make($req->all(), $rules, $messages);
            $validator->sometimes('pekerjaan', 'required', function ($input) {
                return $input->jenis_pekerjaan_id == 15;
            });
            $input = $req->only(['jenis_pekerjaan_id', 'pekerjaan', 'anggota']);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withInput($req->input())
                            ->withErrors($validator);
                            
            }
            $np = str_pad(rand(0,9999), 4, "0", STR_PAD_LEFT);
            if($data->pin == $np){
                $np++;
            }
            $pa = rand(0,9);
            $pin_acak = $pa.$pa.$pa.$pa;
            $input['id_user'] = $req->session()->get('auth')->id_user;
            $input['id_gembok'] = $idGembok;
            $input['time_start'] = time();
            $input['nama_user'] = $req->session()->get('auth')->nama;
            $input['pin_lama'] = $data->pin;
            $input['pin_new'] = $np;
            $input['pin_acak'] = $pin_acak;
            $id = 0;
            DB::transaction(function() use($input,$req, &$id) {
                $id = Work::create($input);
                if ($req->hasFile('before')) {
                    Work::storePhoto($id, 'before', $req->file('before'));
                }
            });
            if($id){
                exec('cd ..;php artisan sendTele '.$id.' 0 > /dev/null &', $out);
                if(!$data->owner_id){
                    $this->sendReportGembok(0, $data->witel);
                }
                return redirect("/work/{$id}/progress");
            }else{
                $req->session()->flash('alerts', ['type' => 'danger', 'text' => 'Ada error hub admin']);
            }
            //self::sendReportTelegram($id, 0);
        }else{
            $req->session()->flash('alerts', ['type' => 'danger', 'text' => 'Tidak dapat request PIN karena pekerjaan sedang berlangsung.']);
        }
    }

    public function listPage(Request $req)
    {
        //if ($req->session()->get('auth')->level == '2')
        if($req->path() == "work")
            return $this->listAll();
        else
            return $this->listByCurrentUser($req);
    }
    public function search(Request $req)
    {
        $workUnclosed = Work::getUnclosed();
        $workClosed = Work::getSearched($req);
        //dd($workClosed);
        return view('work/search', compact('workUnclosed', 'workClosed'));
    }
    public function jsonGetOpen(){
        return json_encode(Work::getUnclosed());
    }
    public function jsonGetAllGembok(){
        return json_encode(Gembok::getAll());
    }
    public function listAll()
    {
        $workUnclosed = Work::getUnclosed();
        $workClosed = Work::getClosed(env('PAGESIZE_LIST_ALL', 25));
        return view('work/list-all', compact('workUnclosed', 'workClosed'));
    }
    public function library($idgembok)
    {
        $gembok = Gembok::getById($idgembok);
        $lib = Work::getLibraryByGembok($idgembok, 4);
        return view('work/library', compact('lib', 'gembok'));
    }
    public function listByUser($id)
    {
        $login = $id;
        $workUnclosed = Work::getUnclosedByUser($login);
        $workClosed = Work::getClosedByUser($login, env('PAGESIZE_LIST_BYUSER', 25));

        return view('work/list-all', compact('workUnclosed', 'workClosed'));
    }
    public function listByCurrentUser(Request $req)
    {
        $login = $req->session()->get('auth')->id_user;
        $workUnclosed = Work::getUnclosedByUser($login);
        $workClosed = Work::getClosedByUser($login, env('PAGESIZE_LIST_BYUSER', 25));

        return view('work/list-all', compact('workUnclosed', 'workClosed'));
    }

    public function progressForm($idWork) 
    {
        $data = Work::getById($idWork);
        $pekerjaan = Work::getPekerjaan();
        if ($data->time_close != '0' && session('auth')->level != 2) {
            return redirect('work/'.$idWork);
        }
        $last = Work::getLastOpen($data->id_gembok);
        if(isset($last[2]))
            $last->pin_last = $last[2]->pin_new;
        else
            $last->pin_last = "NULL";
        if(isset($last[1]))
            $last->id_last = $last[1]->id;
        else
            $last->id_last = 0;
        $data->hasPhoto = [];
        foreach($this->files as $field) {
            $data->hasPhoto[$field] = Work::hasPhoto($data->id, $field);
        }
        return view('work/progress', compact('data', 'last', 'pekerjaan'));
    }
    public function scale() 
    {
        foreach($this->files as $field) {
            Work::scalee(360, $field);
        }
    }
    public function update(Request $req, $idWork)
    {
        $data = Work::getById($idWork);
        if ($data->time_close != '0') {
            return redirect('work/'.$idWork);
        }

        $input = $req->only([
            'pekerjaan',
            'anggota',
            'jenis_pekerjaan_id',
            'uraian'
        ]);
        $rules = array(
            'uraian' => 'required'
        );
        $messages = [
            'uraian.required' => 'Silahkan isi kolom "Uraian Pekerjaan" Bang!',
            'item-before.required' => 'Silahkan isi kolom upload "Foto Perangkat Sebelum Pekerjaan" Bang!',
            'item-before.max' => 'Ukuran Photo harus dibawah 1mb, Silahkan install GPS Map Camera dan setting resolusi menjadi 720x1280 Bang!',
            'work-progress.required' => 'Silahkan isi kolom upload "Foto Kegiatan" Bang!',
            'work-progress.max' => 'Ukuran Photo harus dibawah 1mb, Silahkan install GPS Map Camera dan setting resolusi menjadi 720x1280 Bang!',
            'item-after.required' => 'Silahkan isi kolom upload "Foto Perangkat Setelah Pekerjaan" Bang!',
            'item-after.max' => 'Ukuran Photo harus dibawah 1mb, Silahkan install GPS Map Camera dan setting resolusi menjadi 720x1280 Bang!'
        ];
        if(!$req->item_before_exist){
            $rules = array_merge($rules, array('item-before' => 'required|max:1000'));
        }
        if(!$req->work_progress_exist){
            $rules = array_merge($rules, array('work-progress' => 'required|max:1000'));
        }
        if(!$req->item_after_exist){
            $rules = array_merge($rules, array('item-after' => 'required|max:1000'));
        }
        $validator = Validator::make($req->all(), $rules, $messages);
        // $validator->sometimes('item-before', 'required|max:1000', function ($input) {
        //     return $input->item_before_exist == 0;
        // });
        // $validator->sometimes('work-progress', 'required|max:1000', function ($input) {
        //     return $input->work_progress_exist == 0;
        // });
        // $validator->sometimes('item-after', 'required|max:1000', function ($input) {
        //     return $input->item_after_exist == 0;
        // });
        if ($validator->fails()) {
                return redirect()->back()
                    ->withInput($req->input())
                            ->withErrors($validator);
                            
        }

        $alerts = [];
        foreach($this->files as $field) {
            if ($req->hasFile($field)) {
                try {
                    Work::storePhoto($idWork, $field, $req->file($field));
                }
                catch (\Exception $e) {
                    $alerts[] = [
                        'type' => 'danger',
                        'text' => 'Gagal menyimpan file<br>'.$e->getMessage()
                    ];
                }
            }
        }
        if (count($alerts)) {
            $req->session()->flash('alerts', $alerts);
            return redirect()->back();
        }
        Work::updateById($idWork, $input);
 
        return redirect("work/{$idWork}/close");
    }
 
    public function closeForm($idWork)
    {
        $data = Work::getById($idWork);
        if($data->time_close != 0){
            return redirect('/')->with('alerts', [
                ['type' => 'success', 'text' => 'Pekerjaan sudah close']
            ]);
        }
        return view('work/close', compact('data'));
    }

    public function close(Request $req, $idWork)
    {
        $data = Work::getById($idWork);
        $gembok = Gembok::getById($data->id_gembok);
        if ($req->session()->get('auth')->id_user != $data->id_user) {
            throw new \Exception('UNAUTHORIZED');
        }
        // TODO: pin validation
        if($data->time_close != 0){
            return redirect('/')->with('alerts', [
                ['type' => 'success', 'text' => 'Pekerjaan sudah close']
            ]);
        }
        if($gembok->pin == $req->input('pin')){
            return redirect()->back()->with('alerts', [
                ['type' => 'danger', 'text' => 'Gagal : PIN Lama harus beda dengan PIN Baru!']
            ]);
        }else if(strlen($req->input('pin'))!=4){
            return redirect()->back()->with('alerts', [
                ['type' => 'danger', 'text' => 'Gagal : PIN Baru harus 4 Digit!']
            ]);
        }else{
            $rules = array(
                'resetPin' => 'required|max:1000',
                'acakPin' => 'required|max:1000'
            );
            $messages = [
                'resetPin.required' => 'Silahkan isi kolom upload "Foto Pin Reset" Bang!',
                'resetPin.max' => 'Ukuran Photo harus dibawah 1mb, Silahkan install GPS Map Camera dan setting resolusi menjadi 720x1280 Bang!',
                'acakPin.required' => 'Silahkan isi kolom upload "Foto Acak PIN" Bang!',
                'acakPin.max' => 'Ukuran Photo harus dibawah 1mb, Silahkan install GPS Map Camera dan setting resolusi menjadi 720x1280 Bang!'
            ];
            $validator = Validator::make($req->all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withInput($req->input())
                            ->withErrors($validator);
            }
            try {
                Work::storePhotoPin($idWork, $req->file('resetPin'));
                Work::storePhotoAcakPin($idWork, $req->file('acakPin'));
            }
            catch (\Exception $e) {
                $alerts[] = [
                    'type' => 'danger',
                    'text' => 'Gagal menyimpan file<br>'.$e->getMessage()
                ];
            }
            Work::close($idWork, $req->input('pin'));
            if(!$gembok->owner_id){
                $this->sendReportGembok($idWork, $gembok->witel);
            }
            //self::sendReportTelegram($idWork, 1);
            exec('cd ..;php artisan sendTele '.$idWork.' 1 > /dev/null &', $out);
            exec('cd ..;php artisan sendAdmin '.$idWork.' > /dev/null &', $out);
            return redirect('/')->with('alerts', [
                ['type' => 'success', 'text' => 'Sukses menyimpan data']
            ]);
        }  
    }
    public function sendTeleManual($id)
    {
        exec('cd ..;php artisan sendTele '.$id.' 1 > /dev/null &', $out);
        return back();
    }
    public function read(Request $req, $idWork)
    {
        $data = Work::getById($idWork);

        $auth = $req->session()->get('auth');
        //if ($auth->level != '2' && $data->id_user != $auth->id_user) {
        //    throw new \Exception('UNAUTHORIZED');
        //}
        return view('work/read', compact('data'));
    }
    public function periodicReport()
    {
        $witel = Master::getWitelChatID();
        // dd($witel);
        foreach($witel as $w) {
            // $this->sendReportGembok(0, $w->witel);
            exec("cd ..;php artisan sendNotif '".$w->witel."' > /dev/null &", $out);
            sleep(2);
        }
    }
    public static function sendReportGembok($id, $witel)
    {
        $message = '';
        $wtl = DB::table('manci_witel')->where('witel', $witel)->first();
        $chat_id = $wtl->chat_id;

        //$chat_id = $chat[$witel];
        if($id){
            $data = Work::getById($id);
            $dteStart = new DateTime(date('Y-m-d H:i:s', $data->time_start)); 
            $dteEnd   = new DateTime(date('Y-m-d H:i:s', $data->time_close)); 
            $dteDiff  = $dteStart->diff($dteEnd); 
            $message .= "(".$dteDiff->format("%ad %Hh %Im %Ss").") ".$data->kode."|CLOSE|".$data->id_user."|".($data->nama ?: $data->nama_user)."\n";
            
        }
        $opens = Work::getUnclosedByWitel($witel);
        foreach($opens as $row){
            $dteStart = new DateTime(date('Y-m-d H:i:s', $row->time_start)); 
            $dteEnd   = new DateTime(date('Y-m-d H:i:s')); 
            $dteDiff  = $dteStart->diff($dteEnd); 
            $message .= "(".$dteDiff->format("%ad %Hh %Im %Ss").") ".$row->kode."|OPEN|".$row->id_user."|".($row->nama ?: $row->nama_user)."\n";
        }
        // dd($message,$chat_id);
        //$chat_id = 52369916;
        if($message){
            Telegram::sendMessage(["chat_id" => $chat_id,
                "text" => $message
            ]);  
        }
    }
    /*
    public static function sendReportTelegram($id, $flag)
    {
        $data = Work::getById($id);
        if($flag){
            $message = "Logbook WASAKA \n======================\n";
            $files = [
                'before',
                'item-before',
                'work-progress',
                'item-after'
            ];
        }else{
            $message = "Laporan Open WASAKA \n======================\n";
            $files = array('before');
        }
        $message .= 'Perangkat: '.$data->kode."\n\n";
        if($data->jenis_pekerjaan_id==15){
                $message .= 'Pekerjaan : '.$data->pekerjaan."\n\n";
            }else{
                $message .= 'Pekerjaan : '.$data->jenis_pekerjaan."\n\n";
            }
        $message .= 'Waktu Mulai : '.date('d/m/Y H:i', $data->time_start)."\n";
        if($flag){
            $dteStart = new \DateTime(date('Y-m-d H:i:s', $data->time_start)); 
            $dteEnd   = new \DateTime(date('Y-m-d H:i:s', $data->time_close)); 
            $dteDiff  = $dteStart->diff($dteEnd); 
            $durasi   = $dteDiff->format("%Hh %Im %Ss");
            $message .= 'Waktu Selesai : '.date('d/m/Y H:i', $data->time_close)."\n";
            $message .= 'Durasi : '.$durasi."\n";
            $message .= 'Uraian Pekerjaan : '.$data->uraian."\n";
        }
        $message .= 'Anggota : '.$data->anggota."\n";
        $message .= 'Oleh : '.($data->nama ?: $data->nama_user)."\n";
        $chat = [
            'KALSEL' => -1001144753925,
            'KALBAR' => -1001107713933,
            'KALTENG' => -1001101905868,
            'SAMARINDA' => -1001112566511,
            'BALIKPAPAN' => -1001084299216,
            'KALTARA' => -1001113873874,
        ];
        $chat_id = $chat[$data->wtl];
        $kord = explode(",",$data->koordinat);
        
        Telegram::sendMessage(["chat_id" => $chat_id,
            "text" => $message
        ]);
        if(isset($kord[0]) && isset($kord[1])){
            $response = Telegram::sendLocation([
                'chat_id' => $chat_id, 
                'latitude' => $kord[0],
                'longitude' => $kord[1]
            ]);
        }
        foreach($files as $file){
            $path = UPLOAD_PATH.$id.'/'.$file.'.jpg';
            if (file_exists($path)){
                $cap = '-';
                if($file == 'before')
                    $cap = 'Foto Sebelum Pekerjaan';
                if($file == 'item-before')
                    $cap = 'Foto Perangkat Sebelum Pekerjaan';
                if($file == 'work-progress')
                    $cap = 'Foto Kegiatan';
                if($file == 'item-after')
                    $cap = 'Foto Perangkat Setelah Pekerjaan';
                Telegram::sendPhoto([
                    "chat_id" => $chat_id,
                    "photo" => $path,
                    "caption" => $cap
                ]);
            }
        }
    }*/
}
