<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DA\MC;
use App\DA\Gembok;
use Validator;
class MCController extends Controller
{
  protected static $bg_color = ["#00ffff","#0000ff","#ffa500","#00ff00","#cb3434","#999999","#ffffff","#ff0000","#000000","#ffff00","#8000ff","#ff00bf"];
  protected static $text_color = ["#ffffff","#ffffff","#000000","#000000","#ffffff","#ffffff","#000000","#ffffff","#ffffff","#ffffff","#ffffff","#ffffff"];
  public static function layoutodc($id){

    $odc = Gembok::getById($id);
    $basetray = MC::get_basetray($id);
    // dd("asd",$basetray);
    // dd($basetray);
    $port = MC::get_port_odc($id);
    $spl = MC::get_splitter($id);
    $spl_link = MC::get_splitter_link($id);
    $kbl = MC::get_kabel_odc($id);
    $splarray=$portarray=$kblarray=[];
    foreach($kbl as $k){
      $link = [];
      for($i=1;$i<=$k->kap;$i++){
        $link[$i] = (object)['back_link_var'=>'KOSONG','basetray'=>'','port'=>'','core'=>$i,'status_port'=>null];
      }
      $k->link = $link;
      $kblarray[$k->id] = $k;
    }
   
    foreach($spl as $no => $s){
      $no+=1;
      $link = [];
      for($i=0;$i<=$s->jenis;$i++){
        $link[] = ['back_link_var'=>'','basetray'=>'','port'=>'','status'=>null];
      }
      foreach($spl_link as $sl){
        if($sl->spl_id == $s->id){
          $link[$sl->number] = ['back_link_var'=>'','basetray'=>'','port'=>'','status'=>$sl->status];
        }
      }
      $splarray[$s->id] = ['bg_color'=>self::$bg_color[$no%12],'text_color'=>self::$text_color[$no%12],'out'=>$s->jenis,'urutan'=>$no,'free'=>$s->jenis-$s->out_count,'used'=>$s->out_count,'link'=>$link];
    }

    foreach($port as $p){
      $bg = $urutan =$text_color = null;
      if($p->front_link){
        $splitter = $splarray[$p->spl_id];
        $bg = $splitter['bg_color'];
        $text_color = $splitter['text_color'];
        $urutan = $splitter['urutan'];

        $splarray[$p->spl_id]['link'][$p->number_spl]=['back_link_var'=>$p->back_link_var,'basetray'=>$p->basetray_number,'port'=>$p->port_number,'status'=>$p->status_link_spl];
        // dd($splitter['link'][$p->number_spl]);
      }
      $portarray[$p->basetray_number][$p->port_number] = ['port_id'=>$p->id,'spl_id'=>$p->spl_id,'back_link'=>$p->back_link,'back_link_var'=>$p->back_link_var,'front_link'=>$p->front_link,'bg_color'=>$bg,'text_color'=>$text_color,'urutan'=>$urutan,'spl_number'=>$p->number_spl,'status_port'=>$p->status_port,'kbl_id'=>$p->kbl_id,'core'=>$p->core,'nama_kabel'=>$p->nama_kabel,'status_core'=>$p->status_core,'redaman'=>$p->redaman,'status_port_user'=>$p->status_port_user,'status_port_ts'=>$p->status_port_ts];
      if($p->back_link){
        if($p->status_port=="reti"){
          $kblarray[$p->kbl_id]->link[$p->core]->back_link_var = "RETI";
        }else{
          $kblarray[$p->kbl_id]->link[$p->core]->back_link_var = $p->back_link_var;
        }
        $kblarray[$p->kbl_id]->link[$p->core]->basetray = $p->basetray_number;
        $kblarray[$p->kbl_id]->link[$p->core]->port = $p->port_number;
        $kblarray[$p->kbl_id]->link[$p->core]->status_port = $p->status_port;
      }
    }
    // dd($kblarray);
    // dd($splarray,$portarray,$odc);
    // $spl_link = MC::get_splitter_link($id);
    $logs = MC::get_logs($id);
    return view('MC.layoutodc',compact('odc','basetray','splarray','portarray','kblarray','logs'));
  }
  public static function getKabelCore($kbl_id){
    $kbl = MC::get_kabel_by_id($kbl_id);
    $core = MC::get_kabel_core($kbl_id);
    $kabelcore = [];
    for($i=0;$i<$kbl->kap;$i++){
      $kabelcore[$i] = (object)['back_link_var'=>'KOSONG','basetray'=>'','port'=>'','id'=>$i+1,'text'=>$i+1];
    }
    // dd($kabelcore);
    foreach($core as $c){
      if($c->status_core=='reti'){
        $kabelcore[$c->core-1] = (object)['back_link_var'=>'RETI','basetray'=>$c->basetray_number,'port'=>$c->port_number,'id'=>$c->core,'text'=>$c->core,'disabled'=> true];
      }else{
        $kabelcore[$c->core-1] = (object)['back_link_var'=>$c->back_link_var?:'Label Not Set','basetray'=>$c->basetray_number,'port'=>$c->port_number,'id'=>$c->core,'text'=>$c->core,'disabled'=> true];
      }
    }
    return $kabelcore;
  }
  public static function getPortByBasetray($odc_id,$basetray){
    $ports = MC::get_port_by_basetray($odc_id,$basetray);
    // dd($ports);
    $odc = Gembok::getById($odc_id);
    $portarray = [];
    for($i=0;$i<$odc->basetray_port;$i++){
      $portarray[$i] = (object)['link'=>'Link Not Set','status'=>'Set Link Dulu','id'=>0,'text'=>$i+1,'disabled'=> true];
    }
    foreach($ports as $c){
      // dd($c);
      $link ='';$disabled=false;
      if($c->back_link){
        $link ='TO '.$c->nama_kabel.' core '.$c->core;
        $status = 'Set Link Dulu';
        if($c->front_link){
          $status = 'In Use';
          $disabled=true;
        }else{
          $status = 'Free';
        }
        if($c->status_core=='reti'){
          $status = 'reti';
          $disabled=true;
        }
        if($c->status_port=='book'){
          $status = 'Booked';
          $disabled=true;
        }
        $portarray[$c->core-1] = (object)['link'=>$link,'status'=>$status,'id'=>$c->id,'text'=>$c->core,'disabled'=> $disabled];
      }else{
        $link = 'link not set';
      }
      
      
    }
    // dd($odc->basetray_port,$portarray,$ports);
    return $portarray;
  }
  public static function kabel_save(Request $req){
    $rules = array(
      'nama_kabel' => 'required',
    );
    $messages = [
      'nama_kabel.required' => 'Silahkan isi kolom nama_kabel Bang!',
    ];
    $validator = Validator::make($req->all(), $rules, $messages);
    $input = $req->only(['nama_kabel']);
    if ($validator->fails()) {
      return redirect()->back()
          ->withInput($req->input())
          ->with('alerts', [
                ['type' => 'danger', 'text' => 'Inputan Kurang Lengkap.']
            ]);
    }else{
      $exist = MC::get_kabel_by_id($req->kabel_id);
      if($exist){
        MC::update_kabel(['jenis_kabel'=>$req->jenis_kabel,'nama_kabel'=>$req->nama_kabel,'kap'=>$req->kapasitas,'odc_id'=>$req->odc_id,'status_kabel'=>1],$req->kabel_id);
        MC::insert_log(['odc_id'=>$req->odc_id,'ket'=>'edit kabel '.$req->jenis_kabel_old.':'.$req->nama_kabel_old.' kap:'.$req->kapasitas_old.' menjadi '.$req->jenis_kabel.':'.$req->nama_kabel.' kap:'.$req->kapasitas.'.','user'=>session('auth')->id_user]);
      }else{
        MC::insert_kabel(['jenis_kabel'=>$req->jenis_kabel,'nama_kabel'=>$req->nama_kabel,'kap'=>$req->kapasitas,'odc_id'=>$req->odc_id,'status_kabel'=>1]);
        MC::insert_log(['odc_id'=>$req->odc_id,'ket'=>'input kabel '.$req->jenis_kabel.' baru dengan nama '.$req->nama_kabel.' kap:'.$req->kapasitas.'.','user'=>session('auth')->id_user]);
      }
      return redirect()->back()
          ->with('alerts', [
                ['type' => 'success', 'text' => 'Berhasil input.']
            ]);
    }
  }
  public static function link_save(Request $req){
    $portExist = MC::get_port_odc_by_id($req->portid);
    $spl_id = $req->select_splitter;
    $back_link = null;
    $front_link = null;
    $logs=[];
    $param = ['odc_id'=>$req->odc_id,'basetray_number'=>$req->basetray,'port_number'=>$req->port,'back_link_var'=>$req->back_link_var];
    if($req->spl_link!=null){
      if($req->splitter=='new'){
        $spl_id = MC::insert_spl(['terminal_id'=>$req->odc_id,'jenis'=>$req->new_splitter]);
        //log spl new
        $logs[]=['odc_id'=>$req->odc_id,'ket'=>'Input SPLITTER baru.','user'=>session('auth')->id_user];
      }
      //set front link
      $splLinkExist = MC::get_spl_link_by_number($req->spl_link,$spl_id);
      if($splLinkExist){
        $front_link = $splLinkExist->id;
        MC::upate_spl_link(['status'=>'in_use'],$splLinkExist->id);
      }else{
        $front_link = MC::insert_spl_link(['spl_id'=>$spl_id,'number'=>$req->spl_link,'status'=>'in_use']);
      }
      if($portExist){
        if($portExist->front_link != $front_link){
          //log merubah link
          $logs[]=['odc_id'=>$req->odc_id,'ket'=>'edit front link spl_id:'.$spl_id.' link:'.$req->spl_link.' ke spl_id:'.$spl_id.' link :'.$req->spl_link.' di port: '.$req->basetray.'/'.$req->port.'.','user'=>session('auth')->id_user];
        }
      }else{
        //log set link baru
        $logs[]=['odc_id'=>$req->odc_id,'ket'=>'input sambungan front link port '.$req->basetray.'/'.$req->port.' dengan spl_id:'.$spl_id.' link:'.$req->spl_link.'.','user'=>session('auth')->id_user];
      }
      $param['front_link']=$front_link;
      $param['status_port']='in_use';
    }
    if($req->select_core){
      //set back link
      $kabelLinkExist = MC::get_kabel_link_by_core($req->select_core,$req->select_kabel);  
      if($kabelLinkExist){
        $back_link = $kabelLinkExist->id;
      }else{
        //insert baru
        $back_link = MC::insert_kbl_link(['kbl_id'=>$req->select_kabel,'core'=>$req->select_core]);
      }
      if($portExist){
        if($portExist->back_link != $back_link){
          //log merubah link
          $logs[]=['odc_id'=>$req->odc_id,'ket'=>'edit sambungan backlink port '.$req->basetray.'/'.$req->port.' dengan kabel_id:'.$req->select_kabel.' core:'.$req->select_core.' ke kabel_id:'.$req->select_kabel.' core:'.$req->select_core.'.','user'=>session('auth')->id_user];
        }
      }else{
        //log set link baru
        $logs[]=['odc_id'=>$req->odc_id,'ket'=>'input sambungan backlink port '.$req->basetray.'/'.$req->port.' dengan kabel_id:'.$req->select_kabel.' core: '.$req->select_core.'.','user'=>session('auth')->id_user];
      }
      $param['back_link']=$back_link;
    }
    // dd($param);
    if($portExist){
      // update port
      MC::update_port($param,$portExist->id);
    }else{
      MC::insert_port($param);
    }
    MC::insert_log($logs);
    return redirect()->back()
          ->with('alerts', [
                ['type' => 'success', 'text' => 'Berhasil input.']
            ]);
  }
  public static function saveLepasSPL(Request $req){
    //mc_odc_port.front_link=null,mc_splitter_link.status=null
    $link = MC::get_splitter_link_by_id($req->ls_front_link);
    // dd($link);
    if($link){
      MC::insert_log(['odc_id'=>$link->odc_id,'ket'=>'lepas sambungan front link spl_id:'.$req->ls_front_link.' IN/OUT '.$link->number_spl.' di port:'.$link->basetray_number.'/'.$link->port_number.', catatan:'.$req->catatan.'.','user'=>session('auth')->id_user]);
      MC::update_port(['front_link'=>null,'status_port'=>null],$link->id);
      MC::upate_spl_link(['status'=>null],$req->ls_front_link);
    }
    return redirect()->back()
          ->with('alerts', [
                ['type' => 'success', 'text' => 'Berhasil Melepas SPL.']
            ]);
  }
  public static function saveStatusPort(Request $req){
    //mc_odc_port.status=status
    $portExist  =MC::get_port_odc_by_id($req->portid);
    if($portExist){
      MC::update_port(['status_port'=>$req->status_port],$portExist->id);
      $log = ['odc_id'=>$portExist->odc_id,'ket'=>'Edit status port '.$portExist->basetray_number.'/'.$portExist->port_number.' dari '.$portExist->status_port.' menjadi '.$req->status_port.', catatan:'.$req->catatan.'.','user'=>session('auth')->id_user];
    }else{
      MC::insert_port(['odc_id'=>$req->odc_id,'basetray_number'=>$req->basetray,'port_number'=>$req->port,'status_port'=>$req->status_port]);
      $log = ['odc_id'=>$req->odc_id,'ket'=>'Edit status port '.$req->basetray.'/'.$req->port.' dari null menjadi '.$req->status_port.', catatan:'.$req->catatan.'.','user'=>session('auth')->id_user];
    }
    MC::insert_log($log);
    return redirect()->back()
          ->with('alerts', [
                ['type' => 'success', 'text' => 'Berhasil Merubah Status Port.']
            ]);
  }
  public static function saveBookingSPL(Request $req){
    //mc_splitter_link.status=book
    // dd($req->only('bs_spl_id','bs_spl_number','catatan'));
    $splLinkExist = MC::get_spl_link_by_number($req->bs_spl_number,$req->bs_spl_id);
    if($splLinkExist){
      MC::upate_spl_link(['status'=>'book'],$splLinkExist->id);
    }else{
      MC::insert_spl_link(['spl_id'=>$req->bs_spl_id,'number'=>$req->bs_spl_number,'status'=>'book']);
    }
    MC::insert_log(['odc_id'=>$req->odc_id,'ket'=>'Booking spl id:'.$req->bs_spl_id.' IN/OUT:'.$req->bs_spl_number.', catatan:'.$req->catatan.'.','user'=>session('auth')->id_user]);
    return redirect()->back()
            ->with('alerts', [
              ['type' => 'success', 'text' => 'Berhasil Booking SPL.']
            ]);
  }
  public static function saveSetRedaman(Request $req){
    $kl = MC::get_kabel_link_by_id($req->back_link);
    if($kl){
      MC::update_kbl_link(['status_core'=>$req->status_back_link,'redaman'=>$req->redaman],$req->back_link);
      MC::insert_log(['odc_id'=>$kl->odc_id,'ket'=>'Set Status Core '.$req->status_back_link.' di kabel '.$kl->nama_kabel.' core '.$kl->core.' port '.$kl->basetray_number.'/'.$kl->port_number.' redaman '.$req->redaman.', catatan:'.$req->catatan.'.','user'=>session('auth')->id_user]);
      // dd($req->only('back_link','status_back_link','redaman','catatan'),$kl,$param);
      return redirect()->back()
              ->with('alerts', [
                ['type' => 'success', 'text' => 'Berhasil Set Redaman.']
              ]);
    }else{
      return redirect()->back()
              ->with('alerts', [
                ['type' => 'success', 'text' => 'Error']
              ]);
    }
  }
  public static function saveChangePort(Request $req){
    $portNew=MC::get_port_odc_by_id($req->port_id_new);
    if($portNew){
      MC::update_port(['front_link'=>null,'status_port'=>null],$req->portid_old);
      MC::update_port(['front_link'=>$req->front_link,'status_port'=>'in_use'],$req->port_id_new);
      MC::insert_log(['odc_id'=>$req->odc_id,'ket'=>'Change Port dari '.$req->basetray_old.'/'.$req->port_old.' ke '.$portNew->basetray_number.'/'.$portNew->port_number.'.','user'=>session('auth')->id_user]);
      return redirect()->back()
              ->with('alerts', [
                ['type' => 'success', 'text' => 'Berhasil Set Redaman.']
              ]);
    }else{
      return redirect()->back()
              ->with('alerts', [
                ['type' => 'success', 'text' => 'Error']
              ]);
    }
  }
  public static function saveBasetrayAdd(Request $req){
    $basetray=["jumlah_port"=>$req->jumlah_port,"odc_id"=>$req->odc_id];
    $param=[];
    if($req->jumlah_basetray){
      for($i=1;$i<=$req->jumlah_basetray;$i++){
        $param[] = $basetray;
      }
      Gembok::updateById($req->odc_id,["basetray_port"=>$req->jumlah_port]);
      MC::add_basetray($param);
      MC::insert_log(['odc_id'=>$req->odc_id,'ket'=>'Add Basetray sebanyak '.$req->jumlah_basetray.' dengan jumlah port sebanyak '.$req->jumlah_port.'.','user'=>session('auth')->id_user]);
      return redirect()->back()
              ->with('alerts', [
                ['type' => 'success', 'text' => 'Berhasil input basetray']
              ]);
    }else{
      return redirect()->back()
              ->with('alerts', [
                ['type' => 'success', 'text' => 'Basetray tidak boleh kosong.']
              ]);
    }
  }
  public static function saveBasetray(Request $req){
    MC::update_basetray($req->basetray_id,['label'=>$req->label_basetray]);
    MC::insert_log(['odc_id'=>$req->odc_id,'ket'=>'Edit Basetray label menjadi '.$req->label_basetray.'.','user'=>session('auth')->id_user]);
    return redirect()->back()
              ->with('alerts', [
                ['type' => 'success', 'text' => 'Berhasil merubah basetray.']
              ]);
  }
}
