<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\DA\User;
use App\DA\Gembok;
use App\DA\Work;
use App\DA\Loker;
use Telegram;
use Validator;

DEFINE('UPLOAD_PATH2', public_path().'/upload/loker/');
class LokerController extends Controller
{
  private $kelengkapan = ["unit_splicer","holder","battery","charger","stripper","cleaver"];
  //admin
  public function admin_list_lemari()
  {
    $data = Loker::get_list_lemari();
    // dd($data);
    $approval = Loker::get_list_approval();
    // dd($approval);
    return view('admin.loker.list', compact('data', 'approval'));
  }
  public function admin_form_lemari($lemari_id)
  {
    $data = Loker::get_lemari_by_id($lemari_id);
    $list = Loker::get_list_loker($lemari_id);
    // dd($list);
    $reg = User::getRegional();
    $wtl = User::getWitel();
    $kelengkapan = $this->kelengkapan;
    return view('admin.loker.form', compact('data', 'list', 'reg', 'wtl', 'kelengkapan'));
  }
  public function admin_post_lemari($lemari_id, Request $req)
  {
    $data = Loker::get_lemari_by_id($lemari_id);
    // dd($req->all());
    if($data){
      //todo edit
      Loker::update('manci_lemari', $req->all(), $data->id);
      $id = $data->id;
    }else{
      //todo insert
      $id = Loker::insert('manci_lemari', $req->all());
    }
    return redirect('/admin/lemari/'.$id);
  }
  public function admin_post_loker($lemari_id, Request $req)
  {
    $rules = array(
      'kode' => 'required',
      'produk_id' => 'required',
      'isHapus' => 'required',
      'sn_splicer' => 'required'
    );
    $messages = [
      'kode.required' => 'Silahkan isi kolom kode Bang!',
      'produk_id.required' => 'Silahkan isi produk_id before Bang!',
      'isHapus.required' => 'Silahkan isi Status Bang!',
      'sn_splicer.required' => 'Silahkan isi sn_splicer Bang!',
    ];
    $validator = Validator::make($req->all(), $rules, $messages);
    if ($validator->fails()) {
      return redirect()->back()->withInput($req->all())
              ->withErrors($validator)->with('alerts', [
                'type' => 'Gagal',
                'text' => 'Isi semua Field yang ada!'
              ]);
    }
    if(!isset($req->items)){
      return redirect()->back()->withInput($req->all())
              ->with('alerts', [
                'type' => 'Gagal',
                'text' => 'Silahkan Pilih Item!'
              ]);
    }else{
      $data = Gembok::getById($req->gembok_id);
      // dd($req->all());
      $insert = [
        'gembok'=>$req->gembok,
        'pin'=>$req->pin,
        'kategori'=>$req->kategori,
        'witel'=>$req->witel,
        'regional'=>$req->regional,
        'kode'=>$req->kode,
        'produk_id'=>$req->produk_id,
        'isHapus'=>$req->isHapus,
        'lemari_id'=>$lemari_id,
        'sn_splicer'=>$req->sn_splicer
      ];
      if($data){
        //todo edit
        Loker::update('manci_gembok', $insert, $data->id);
        $gembok_id = $data->id;
      }else{
        //todo insert
        $gembok_id = Loker::insert('manci_gembok', $insert);
      }
      Loker::insertItems('kabinet_item', $req->all(), $gembok_id);
      return redirect('/admin/lemari/'.$lemari_id);
    }
  }
  //1:permohonan pinjam,2:rejected,3:dipinjam,4:permohonan return,5:rejected,6:returned;
  public function approvePinjam(Request $req)
  {
    // dd($req->all());
    Loker::update('manci_work', ['step'=>$req->status], $req->work_id);
    return redirect('/admin/lemari');
  }
  public function approveReturn(Request $req)
  {
    // dd($req->all());
    if($req->status==6){
      $query = ['step'=>$req->status, 'time_close'=>time()];
    }else{
      $query = ['step'=>$req->status];
    }
    Loker::update('manci_work', $query, $req->work_id);
    return redirect('/admin/lemari');
  }
  public function admin_json_loker($gembok_id)
  {
    return json_encode(DB::table('kabinet_item')->where('gembok_id', $gembok_id)->get());
  }
  public function admin_ajax_loker($work_id)
  {
    $data = Loker::get_data_transaksi($work_id);
    $items = Loker::get_data_items($work_id);
    // dd($data);
    return view('admin.loker.ajax', compact('data', 'items'));
  }

  //mobile
  // public function start()
  // {
  //   $userid = session('auth')->id_user;
  //   $havingUnclosed = Work::isHavingUnclosed($userid);
  //   $kelengkapan = $this->kelengkapan;
  //   $pekerjaan = Work::getPekerjaan();
  //   return view('mobile.loker.start', compact('pekerjaan', 'havingUnclosed', 'kelengkapan'));
  // }
  public function start_post($idgembok, Request $req)
  {
    // dd($req);
    
    $rules = array(
      'jenis_pekerjaan_id' => 'required',
      // 'depan_so' => 'required'
    );
    $messages = [
      'jenis_pekerjaan_id.required' => 'Silahkan isi kolom anggota Bang!',
      'depan_so.required' => 'Silahkan isi kolom before Bang!',
    ];
    $validator = Validator::make($req->all(), $rules, $messages);
    if ($validator->fails()) {
      return redirect()->back()->withInput($req->all())
              ->withErrors($validator)->with('alerts', [
                'type' => 'Gagal',
                'text' => 'Isi semua Field yang ada!'
              ]);
    }
    if(!isset($req->items)){
      return redirect()->back()->withInput($req->all())
              ->with('alerts', [
                'type' => 'Gagal',
                'text' => 'Silahkan Pilih Item!'
              ]);
    }else{
      //do transaction

      if(Loker::cek_loker_open($idgembok)){
        return redirect()->back()->withInput($req->all())
              ->withErrors($validator)->with('alerts', [
                'type' => 'Gagal',
                'text' => 'Sudah ada Peminjam!'
              ]);
      }else{
        $work_id = Loker::insert_start($idgembok,$req);
        foreach(["depan_so"] as $field) {
          if ($req->has($field)) {
            try {
              $ext = 'jpg';
              $path = UPLOAD_PATH2.$work_id;
              if (!file_exists($path)){
                  if (!mkdir($path, 0770, true)){
                      throw new \Exception('Gagal menyiapkan folder foto');
                  }
              }
              file_put_contents("$path/$field.$ext", base64_decode(explode(',',$req->input($field))[1]));
              $i = new \Imagick("$path/$field.$ext");
              // self::autoRotateImage($i);
              $i->scaleImage(100,150, true);
              $i->writeImage("$path/$field-th.$ext");
            }
            catch (\Exception $e) {
              return redirect()->back()
                  ->withInput($req->input())->with('alerts', [
                          'type' => 'Gagal',
                          'text' => 'Gagal menyimpan file, hubungi admin. trace : '.$e->getMessage()
                      ]);
            }
          }
        }
        return redirect('/')
              ->with('alerts', [
                'type' => 'Berhasil',
                'text' => 'Berhasil Submit!'
              ]);
      }
    }
  }
  public function close($work_id)
  {
    $info = Loker::get_data_transaksi($work_id);
    $kelengkapan = Loker::get_items_pinjam($work_id);
    // dd($kelengkapan);
    return view('mobile.loker.close', compact('kelengkapan', 'info'));
  }
  public function close_post($work_id, Request $req)
  {
    // dd($req);
    $rules = array(
      'arc' => 'required',
      'catatan' => 'required',
      // 'evidence_arc' => 'required',
      // 'evidence_penyerahan' => 'required'
    );
    $messages = [
      'arc.required' => 'Silahkan isi Arc',
      'catatan.required' => 'Silahkan isi Catatan',
      'evidence_arc.required' => 'Silahkan isi Evidence Arc',
      'evidence_penyerahan.required' => 'Silahkan isi Evidence Penyerahan'
    ];
    $validator = Validator::make($req->all(), $rules, $messages);
    if ($validator->fails()) {
      return redirect()->back()->withInput($req->all())
              ->with('alerts', [
                'type' => 'Gagal',
                'text' => 'Isi semua Field yang ada!'
              ]);
    }
    if(!isset($req->ratings)){
      return redirect()->back()->withInput($req->all())
              ->with('alerts', [
                'type' => 'Gagal',
                'text' => 'Silahkan Rating Item!'
              ]);
    }else{
      //do transaction
      foreach(["evidence_arc", "evidence_penyerahan"] as $field) {
        if ($req->has($field)) {
          try {
            $ext = 'jpg';
            $path = UPLOAD_PATH2.$work_id;
            if (!file_exists($path)){
                if (!mkdir($path, 0770, true)){
                    throw new \Exception('Gagal menyiapkan folder foto');
                }
            }
            file_put_contents("$path/$field.$ext", base64_decode(explode(',',$req->input($field))[1]));
            $i = new \Imagick("$path/$field.$ext");
            // self::autoRotateImage($i);
            $i->scaleImage(100,150, true);
            $i->writeImage("$path/$field-th.$ext");
          }
          catch (\Exception $e) {
            return redirect()->back()
                ->withInput($req->input())->with('alerts', [
                        'type' => 'Gagal',
                        'text' => 'Gagal menyimpan file, hubungi admin. trace : '.$e->getMessage()
                    ]);
          }
        }
      }
      Loker::query_close($work_id,$req);
      return redirect('/')
            ->with('alerts', [
              'type' => 'Berhasil',
              'text' => 'Berhasil Submit!'
            ]);
    }
  }
}
