<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DA\Work;
use App\DA\Master;
use Maatwebsite\Excel\Facades\Excel;
class DownloadController extends Controller
{
    public function downloadForm(){
      return view('work.download');
    }
    public function download(Request $req){
      $data = Work::getClosed(false,$req->tgl);
        // dd($data);
      Excel::create('wasaka', function($excel) use($data){
        $excel->sheet("wasaka", function($sheet) use($data){
          
          $sheet->loadView('work.excel', compact('data'));
        });
      })->download('xlsx');
      return redirect('/work/download');
    }

    public function downloadHistory($date,$reg,$wtl,$owner){
      $data = Work::getAllOnlyAdmin($date,$reg,$wtl,$owner);
        // dd($data);
      Excel::create('wasaka', function($excel) use($data){
        $excel->sheet("wasaka", function($sheet) use($data){
          
          $sheet->loadView('work.excel', compact('data'));
        });
      })->download('xlsx');
      return redirect('/work/download');
    }

    public function downloadGembok($reg,$wtl,$owner){
      $data = Master::getGembokByRegional($reg,$wtl,$owner);
      // dd($data);
      Excel::create('wasaka', function($excel) use($data){
        $excel->sheet("list", function($sheet) use($data){
          $sheet->loadView('gembok.excel', compact('data'));
        });
      })->download('xlsx');
    }
    public function downloadUser($reg,$wtl){
      $data = Master::getUser($reg,$wtl);
      // dd($data);
      Excel::create('wasaka_user', function($excel) use($data){
        $excel->sheet("list", function($sheet) use($data){
          $sheet->loadView('user.excel', compact('data'));
        });
      })->download('xlsx');
    }
    public function downloadTiket($reg,$wtl){
      $data = Master::getTiket($reg,$wtl);
      // dd($data);
      Excel::create('wasaka_tiketing', function($excel) use($data){
        $excel->sheet("list", function($sheet) use($data){
          $sheet->loadView('admin.tiket.excel', compact('data'));
        });
      })->download('xlsx');
    }
}
