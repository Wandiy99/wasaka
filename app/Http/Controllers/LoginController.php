<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DA\LoginModel;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use Jenssegers\Agent\Agent as Agent;
use App\DA\User;
use DB;
use Validator;
use Telegram;
use App\DA\Master;

class LoginController extends Controller
{
    protected $agentview;
    public function __construct()
    {
        $agent = new Agent();
        if($agent->isMobile()){
            $this->agentview = 'mobile';
        }else{
            $this->agentview = 'admin';
        }
    }
    public function loginPage()
    {
        return view("$this->agentview.login");
    }

    public function login(Request $request)
    {
        $username  = $request->input('login');
        $password  = $request->input('password');
        $rules = array(
            'login' => 'required|max:16',
            'password' => 'required|max:25',
        );

        $messages = [
          'login.required' => 'Silahkan isi kolom user Bang!max:15',
          'password.required' => 'Silahkan isi password Bang!max:25',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()->back()
                ->withInput($request->input())
                        ->withErrors($validator)->with('alertblock', ['type' => 'danger', 'text' => 'isian tidak lengkap']);
                        
        }
        $result = DB::table('user')->select('*','fz as fiberzone', 'wtl as witel')
        ->where('user.login_type', 1)
        ->where('user.id_user', $username)
        ->where('user.password', MD5($password))
        ->first();
        
        if ($result) {
            $nama = null;
            // dd($result);
            Session::put('auth', $result);
            $this->ensureLocalUserHasRememberToken($result);
            return $this->successResponse($result->wsk_remember_token);  
        } else {
            // $sso = LoginModel::do_login($username,$password);
            // if ($sso['auth'] == "Yes"){
            //     $this->insertUserIfNoExist($username, $sso['nama']);

            //     $result = DB::table('user')->select('*','fz as fiberzone', 'wtl as witel')
            //     ->where('user.login_type', 2)
            //     ->where('user.id_user', $username)
            //     ->first();
            //     if($result->status == 2){
            //         return redirect()->back()->with('alertblock', ['type' => 'danger', 'text' => 'User Anda dihapus, Silahkan Call Admin.']);
            //     }else{
            //         $this->ensureLocalUserHasRememberToken($result);
            //         Session::put('auth', $result);
            //         return $this->successResponse($result->wsk_remember_token);
            //     }
            // }
            // return $this->failureResponse($request, $sso);
            return redirect()->back()
                ->withInput($request->input())
                        ->withErrors($validator)->with('alertblock', ['type' => 'danger', 'text' => 'silahkan hub https://t.me/widyasetya']);
        }
    }
    
    public function insertUserIfNoExist($nik, $nama){
      $user = User::userExists($nik);
      if(!count($user)){
        User::userSSOCreate([
          'id_user'     => $nik,
          'nama'        => $nama,
          'level'       => 0,
          'status'      => 0,
          'login_type'  => 2
        ]);
      }
    }
    public function logout()
    {
        //Session::flush();
        Session::forget('auth');
        //return redirect('/login')->cookie('persistent-token', '', 0, '', '', true, true);
        return redirect('/login')->withCookie(cookie()->forever('presistent-token', ''));
    }

    private function ensureLocalUserHasRememberToken($localUser)
    {
        // dd($localUser);
        $token = $localUser->wsk_remember_token;

        if (!$localUser->wsk_remember_token) {
            $token = $this->generateRememberToken($localUser->id_user);
            User::updateProfileByNik($localUser->id_user, [
                'wsk_remember_token' => $token
            ]);
            $localUser->wsk_remember_token = $token;
        }

        return $token;
    }

    private function generateRememberToken($nik)
    {
        return md5($nik . microtime());
    }

    private function successResponse($rememberToken)
    {
        // if (Session::has('auth-originalUrl')) {
        //     $url = '/';
        // }
        $response = redirect('/');
        if($this->agentview=='admin'){
            $auth = session('auth');
            $fz = $auth->fz?:'unset';
            $witel = $auth->witel?:'unset';
            $response = redirect("/home/".$fz."/".$witel."/0");
        }
        if ($rememberToken) {
            $response->withCookie(cookie()->forever('presistent-token', $rememberToken));
        }

        return $response;
    }

    private function failureResponse(Request $request, $ssoResult)
    {
        // flash old input
        $request->flash();

        switch ($ssoResult['auth']) {
            // Valid User, Wrong Password
            case 'WP':
                $alertText = 'Password salah. User akan dikunci jika 3x salah password.';
                break;

            // User not exists
            case 'No':
                $alertText = 'NIK tidak terdaftar.';
                break;
            default:
                $alertText = 'Login GAGAL.';
                break;
        }
        return redirect()->back()->with('alertblock', ['type' => 'danger', 'text' => $alertText]);
    }

    public function form_register()
    {
        $witel = Master::getWitelChatID();
        return view('mobile.register', compact('witel'));
    }
    public function save_register(Request $req)
    {
        $rules = array(
            'nik' => 'required|max:16',
            'nama' => 'required|max:25',
            'witel' => 'required|max:15',
            'instansi' => 'required|max:25',
            'password' => 'required|max:25'
        );
        $messages = [
          'nik.required' => 'Silahkan isi nik Bang!max:15',
          'nama.required' => 'Silahkan isi nama Bang!max:25',
          'witel.required' => 'Silahkan isi witel Bang!max:15',
          'instansi.required' => 'Silahkan isi instansi Bang!max:25',
          'password.required' => 'Silahkan isi password Bang!max:25',
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()->back()
                ->withInput($req->all())
                ->with('alertblock', ['type' => 'danger', 'text' => 'Isi semua field!']);
        }
        $exists = User::userExists($req->nik);
        if($exists){
            return redirect()->back()
                ->withInput($req->all())
                ->with('alertblock', ['type' => 'danger', 'text' => 'User dengan nik tsb sudah ada!!']);
        }else{
            //create
            //-747040158
            $id = Master::insertUser(["id_user"=>$req->nik,"nama"=>$req->nama,"wtl"=>$req->witel,"instansi"=>$req->instansi,"password"=>MD5($req->password)]);
            Telegram::sendMessage(["chat_id" => "-747040158",
                "text" => "new user from ".$req->witel." a/n:".$req->nama." need approval profile.\nlink :\nwasaka.telkomakses.co.id/admin/user/".$req->nik
            ]);
            return redirect()->back()
                ->withInput($req->all())
                ->with('alertblock', ['type' => 'success', 'text' => 'Berhasil Membuat User, Mohon menunggu Approval admin!!']);
        }
    }
    public function form_forgotpwd()
    {
        return view('mobile.forgot');
    }
    public function save_forgotpwd(Request $req)
    {
        $rules = array(
            'nik' => 'required|max:16',
            'id_telegram' => 'required|max:20'
        );

        $messages = [
          'nik.required' => 'Silahkan isi nik Bang!max:15',
          'id_telegram.required' => 'Silahkan isi id_telegram Bang!max:20',
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()->back()
                ->withInput($req->all())
                ->with('alertblock', ['type' => 'danger', 'text' => 'Isi semua field!']);
        }
        $exists = User::userExists($req->nik);
        if($exists){
            $link = time();
            Master::updateUser($req->nik, ['recovery_link'=>$link]);
            Telegram::sendMessage(["chat_id" => $req->id_telegram,
                "text" => "change pwd link :\nwasaka.telkomakses.co.id/changepwd/".$link
            ]);
            return redirect()->back()
                ->withInput($req->all())
                ->with('alertblock', ['type' => 'success', 'text' => 'Berhasil Mengirim link ke telegram!!']);
        }else{
            return redirect()->back()
                ->withInput($req->all())
                ->with('alertblock', ['type' => 'danger', 'text' => 'Tidak ada user dengan nik tsb!!']);
        }
        
    }

    public function form_changepwd($link)
    {
        $user = Master::getUserByRecoveryLink($link);
        if($user){
            return view('mobile.changepwd', compact('user'));
        }else{
            dd("link tidak tersedia / expired.");
        }

    }
    public function save_changepwd($link,Request $req)
    {
        $rules = array(
            'password' => 'required|max:25'
        );
        $messages = [
          'password.required' => 'Silahkan isi password Bang!max:25',
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()->back()
                ->withInput($req->all())
                ->with('alertblock', ['type' => 'danger', 'text' => 'Isi semua field!']);
        }
        $exists = User::userExists($req->nik);
        if($exists){
            Master::updateUser($req->nik, ['password'=>MD5($req->password),'recovery_link'=>null]);
            return redirect('/login')->with('alertblock', ['type' => 'success', 'text' => 'Berhasil Merubah Password!!']);
        }else{
            dd("silahkan hubungi admin.");
        }
        

    }
}
