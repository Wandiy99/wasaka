<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DA\Master;
use App\DA\User;
use App\DA\Gembok;
use App\DA\Work;
use DB;
use Jenssegers\Agent\Agent as Agent;
class AdminController extends Controller
{
    protected $agentview;
    public function __construct()
    {
        $agent = new Agent();
        if($agent->isMobile()){
            $this->agentview = 'mobile';
        }else{
            $this->agentview = 'admin';
        }
    }
    public function homepage()
    {   

        $ogp = Work::getUnclosedByUser(session('auth')->id_user);
        $work = Work::getClosedByUserHome(session('auth')->id_user);
        // dd($work);
        return view("$this->agentview.home", compact('ogp', 'work'));
        // return view('mobile.home', compact('ogp', 'work'));
    }
    public function userList($reg,$wtl)
    {   
        $region = Master::getRegionalAll();
        $dwtl = Master::getWitelByRegional($reg);
        $data = Master::getUser($reg,$wtl);
        // dd(count($data));
        $all = (object) array("id" => "All_User", "nama" => "All User", "data"=>array());
        $aktif = (object) array("id" => "Aktif", "nama" => "Aktif", "data"=>array());
        $non_aktif = (object) array("id" => "Non_Aktif", "nama" => "Non Aktif", "data"=>array());
        $userlevel = (object) array("id" => "User_Level", "nama" => "User Level", "data"=>array());
        $viewlevel = (object) array("id" => "View_Level", "nama" => "View Level", "data"=>array());
        
        foreach($data as $d){
            $all->data[] = $d;
            if($d->status){
                $non_aktif->data[] = $d;
            }else{
                $aktif->data[] = $d;
            }
            if($d->level == '1'){
                $userlevel->data[] = $d;
            }
            if(!$d->level){
                $viewlevel->data[] = $d;
            }
        }
        $user=[$all,$aktif,$non_aktif,$userlevel,$viewlevel];
        // dd($user);
        return view('admin.user.list', compact('region','dwtl', 'data','user'));
    }
    public function userForm($id)
    {   
        // dd($dwtl);
        $reg = User::getRegional();
        $wtl = User::getWitel();
        $data = Master::getUserByID($id);
        $instansi = Master::getInstansi();
        $work = Work::getByUser($id);
        return view('admin.user.form', compact('data','reg','wtl', 'instansi','work'));
    }
    public function userSave($id, Request $req)
    {   
        // dd($dwtl);
        $input = $req->only(['id_user', 'no_hp', 'nama', 'level', 'status', 'regional', 'wtl', 'instansi', 'akses_perangkat']);
        if($req->password){
            $input['password'] = MD5($req->password);
        }
        if(Master::getUserByID($id)){
            //update
            Master::updateUser($id,$input);
            return redirect()->back()
                ->with('alertblock', [
                  ['type' => 'success', 'text' => '<strong>Berhasil</strong> Mengupdate User!']
                ]);
        }else{
            //insert
            if(Master::getUserByID($req->id_user)){
                return redirect()->back()
                    ->withInput($req->all())
                    ->with('alertblock', [
                      ['type' => 'danger', 'text' => '<strong>GAGAL</strong> Id_User Sudah Tersedia!']
                    ]);
            }else{
                Master::insertUser($input);
                return redirect()->back()
                    ->with('alertblock', [
                      ['type' => 'success', 'text' => '<strong>Berhasil</strong> Menambahkan User Baru!']
                    ]);
            }

        }
        // dd($input);
        // return view('admin.user.form', compact('data','fz','wtl', 'instansi'));
    }


    //gembok
    public function gembokListSearch(Request $req)
    {   
        if($this->isAdmin()){
            $data = Master::getGembokSearch($req->q);
            return view('admin.gembok.listsearch', compact('data'));
        }else{
            return view('401');
        }
        
    }

    public function gembokList($reg,$wtl)
    {   
        $region = Master::getRegionalAll();
        $dwtl = Master::getWitelByRegional($reg);
        // $data = Master::getGembokByRegional($reg,$wtl);
        // dd($data);
        return view('admin.gembok.list', compact('region','dwtl'));
    }
    public function gembokListData($reg,$wtl,$owner,Request $req)
    {   
        // dd('a0');
        $draw = $req->get('draw');
        $start = $req->get("start");
        $rowperpage = $req->get("length"); // Rows display per page

        $columnIndex_arr = $req->get('order');
        $columnName_arr = $req->get('columns');
        $order_arr = $req->get('order');
        $search_arr = $req->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = Master::countTotalRecordGembok($owner,$reg,$wtl);
        $totalRecordswithFilter = Master::countTotalRecordswithFilterGembok($owner,$reg,$wtl,$searchValue);

        // Fetch records
        $records = Master::getRecordsGembok($owner,$reg,$wtl,$start,$rowperpage,$columnName,$columnSortOrder,$searchValue);

        $data_arr = array();
        $sno = $start+1;
        foreach($records as $record){
            $data_arr[] = array(
                "id" => $record->id,
                "kode" => $record->kode,
                "witel" => $record->witel,
                "pin" => $record->pin,
                "jenis_alpro" => $record->jenis_alpro,
                "koordinat" => $record->koordinat,
                "kap" => $record->kap,
                "merk" => $record->merk,
                "last_akses" => $record->last_akses,
                "akses_count" => $record->akses_count,
                "isHapus" => $record->isHapus,
                "scaningadmin" => $record->scaningadmin
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data_arr
        );

        echo json_encode($response);
        exit;
    }

    public function gembokForm($id)
    {   
        if($this->isAdmin()){
            $reg = User::getRegional();
            $wtl = User::getWitel();
            $data = Master::getGembokByID($id);
            $merk = Gembok::merkList();
            $work = Work::getClosedByGembok($id);
            return view('admin.gembok.form', compact('data','reg','wtl', 'merk','work'));
        }else{
            return view('401');
        }
        
    }
    public function gembokSave($id, Request $req)
    {   
        $input = $req->only(['kode', 'pin', 'kap', 'merk', 'regional', 'witel', 'koordinat', 'kategori','gembok','kerapian_odc','engsel_odc','scaningadmin', 'isHapus', 'produk_id']);
        // dd($input);
        if(Master::getGembokByID($id)){
            //update
            Master::updateGembok($id, $input);
            $this->handleUploadGembok($id, $req);
            return redirect()->back()
                    ->with('alertblock', [
                      ['type' => 'success', 'text' => '<strong>Berhasil</strong> Menyimpan Data Gembok!']
                    ]);
        }else{
            //insert
            if(Master::getGembokByKode($req->kode)){
                return redirect()->back()
                    ->withInput($req->all())
                    ->with('alertblock', [
                      ['type' => 'danger', 'text' => '<strong>GAGAL</strong> Kode Sudah ada!Silahkan input kode lain!']
                    ]);
            }else{
                $input['create_at'] = date('Y-m-d H:i:S');
                $id = Master::insertGembok($input);
                $this->handleUploadGembok($id, $req);
                return redirect()->back()
                    ->with('alertblock', [
                      ['type' => 'success', 'text' => '<strong>Berhasil</strong> Menyimpan Data Gembok!']
                    ]);
            }
        }
        
    }
    private function handleUploadGembok($id, $req)
    {
        $input = ["Depan_ODC", "Splitter"];
        foreach($input as $inp){
            if ($req->hasFile("photo-".$inp)) {
                $name = $id;
                //dd($input);
                $path = public_path().'/upload/'.$inp;
                if (! file_exists($path)) {
                    if (! mkdir($path, 0770, true)) {
                        return redirect()->back()
                            ->with('alertblock', [
                              ['type' => 'success', 'text' =>'Gagal Menyiapkan Folder']
                        ]);
                    }
                }
                $file = $req->file("photo-".$inp);
                $ext = 'jpg';
                //TODO: path, move, resize
                try {
                    $moved = $file->move("$path", "$name.$ext");
                    $img = new \Imagick($moved->getRealPath());
                    $img->scaleImage(100, 150, true);
                    $img->writeImage("$path/$name-th.$ext");
                } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
                    return redirect()->back()
                        ->with('alertblock', [
                          ['type' => 'success', 'text' =>'Gagal upload foto']
                    ]);
                }
            }
        }
    }

    public function history($date,$reg,$wtl,$owner)
    {   
        
        if($this->isAdmin()){
            $region = Master::getRegionalAll();
            $dwtl = Master::getWitelByRegional($reg);
            $data = Work::getAllOnlyAdmin($date,$reg,$wtl,$owner);
            return view('admin.history', compact('region','dwtl', 'data'));
        }else{
            return view('401');
        }
        
    }
    public function detail($id)
    {   
        if($this->isAdmin()){
            $data = Work::getById($id);
            return view('admin.detail', compact('data'));
        }else{
            return view('401');
        }
        
    }
    public function themes()
    {
        return view('admin.theme');
    }
    public function themeSave(Request $req)
    {
        session('auth')->pixeltheme=$req->theme;
        DB::table('user')->where('id_user', session('auth')->id_user)->update([
          'pixeltheme'  => $req->theme
        ]);
        return back();
    }
    public function home($fz,$wtl,$owner)
    {   
        $sql = $sqlw = "";
        if($wtl!=='all'){
            $sql = " and wtl = '".$wtl."'";
            $sqlw = " and witel = '".$wtl."'";
        }
        $query_fz = $query_fz_mg = "";
        if($fz!="all"){
            $query_fz = " and regional = '".$fz."'";
            $query_fz_mg = " and mg.regional = '".$fz."'";
        }
        $dfz = Master::getRegionalAll();
        $dwtl = Master::getWitelByRegional($fz);
        $data = Master::getUser($fz,$wtl);
        $user = DB::select("SELECT COUNT(*) AS `Baris`, `level`, (CASE
            WHEN level = 0 THEN 'New User View'
            WHEN level = 1 THEN 'User'
            WHEN level = 2 THEN 'Admin'
            WHEN level = 3 THEN 'View'
            WHEN level = 4 THEN 'Super Admin'
            ELSE 'undefined'
        END) as Actor FROM `user` where `level` IN ('0','1','2','3','4') ".$query_fz." ".$sql."
        GROUP BY `level` ORDER BY `level`");
        $userCount=0;
        $newUser=0;
        $gembokChart=$userChart=$workChart=$kunciChart=array();
        $kunciChart=[["Unscanning",0],["Versi Lama",0],["Versi Baru",0],["Tidak Ada Gembok",0],["Null",0]];
        $engselChart=[["Unscanning",0],["Bagus",0],["Rusak",0]];
        $odcChart=[["Unscanning",0],["Rapih",0],["Tidak Rapih",0]];
        foreach($user as $w){
            if($w->level ==0){
                $newUser=$w->Baris;
            }
            $userChart[] = [$w->Actor,$w->Baris];
            $userCount+=$w->Baris;
        }
        // dd($user);
        $gembok = DB::select("SELECT COUNT(*) AS `Baris`, `merk` FROM `manci_gembok` where isHapus=0 and owner_id=".$owner." ".$query_fz." ".$sqlw." GROUP BY `merk` ORDER BY `merk`");
        $gembokCount=0;

        foreach($gembok as $g){
            $gembokChart[] = [$g->merk,$g->Baris];
            $gembokCount+=$g->Baris;
        }
        $unclose = Work::getUnclosedAll($fz,$wtl);
        // dd($unclose);
        $work = DB::select("SELECT COUNT(*) AS Baris, mjp.jenis_pekerjaan FROM manci_work mw 
            left join manci_jenis_pekerjaan mjp on mw.jenis_pekerjaan_id=mjp.id 
            left join manci_gembok mg on mw.id_gembok=mg.id 
            where isHapus=0 and owner_id=".$owner." ".$query_fz_mg." ".$sqlw." and from_unixtime(mw.time_close, '%Y/%m/%d') like '".date('Y/m/d')."' GROUP BY mjp.jenis_pekerjaan ORDER BY mjp.jenis_pekerjaan");
        $workCount=0;
        foreach($work as $w){
            $workChart[] = [$w->jenis_pekerjaan,$w->Baris];
            $workCount+=$w->Baris;
        }
        $start = date('Y-m-d',strtotime(date('Y-m-d') . "-30 days"));
        $now = date('Y-m-d',strtotime(date('Y-m-d') . "+1 days"));
        $aksesLog = DB::select("SELECT count(*) as baris,from_unixtime(time_close, '%Y-%m-%d') ItemDate FROM manci_work mw 
            left join manci_gembok mg on mw.id_gembok=mg.id 
            where (from_unixtime(time_close, '%Y-%m-%d') BETWEEN '".$start."' and '".date('Y-m-d')."') 
             and owner_id=".$owner." ".$query_fz_mg." ".$sqlw." 
            GROUP BY from_unixtime(time_close, '%Y-%m-%d') ORDER BY ItemDate ASC");
        $countnotakseslastmonth = DB::select("SELECT count(*) as akses FROM manci_gembok mg WHERE isHapus=0 and owner_id=".$owner." and (last_akses NOT BETWEEN '".$start."' and '".$now."') ".$query_fz_mg." ".$sqlw);
        $countunscaning = DB::select("SELECT count(*) as akses FROM manci_gembok mg WHERE isHapus=0 and owner_id=".$owner." and scaningadmin=0 ".$query_fz_mg." ".$sqlw);
        $gembok =  DB::select("SELECT * FROM manci_gembok mg WHERE isHapus=0 and owner_id=".$owner." ".$query_fz_mg." ".$sqlw);
        foreach($gembok as $g){
            if($g->scaningadmin==1){
                //versi gembok
                if($g->gembok == "versi-lama"){
                    $kunciChart[1][1] += 1;
                }else if($g->gembok == "versi-Baru"){
                    $kunciChart[2][1] += 1;
                }else if($g->gembok == "no-gembok"){
                    $kunciChart[3][1] += 1;
                }else{
                    $kunciChart[4][1] += 1;
                }
                //engsel
                if($g->engsel_odc == "on"){
                    $engselChart[1][1] += 1;
                }else{
                    $engselChart[2][1] += 1;
                }
                //odc
                if($g->kerapian_odc == "on"){
                    $odcChart[1][1] += 1;
                }else{
                    $odcChart[2][1] += 1;
                }
            }else{
                $kunciChart[0][1] += 1;
                $odcChart[0][1] += 1;
                $engselChart[0][1] += 1;
            }

        }
        return view('admin.dashboard', compact('dfz','dwtl','userChart','gembokChart','gembokCount','workChart','workCount','userCount','newUser','aksesLog','countnotakseslastmonth','countunscaning','kunciChart','odcChart','engselChart','unclose'));
    }
    public function ajaxGetUnakses($reg,$wtl,$owner)
    {
        $data = Gembok::getUnakses($reg,$wtl,$owner);
        return view('admin.ajaxHomeGembok', compact('data'));
    }
    public function instansiGet()
    {
        return DB::table('instansi')->select('instansi as id','instansi as text')->get();
    }
    public function instansiSave(Request $req)
    {
        $input = $req->only('instansi');
        DB::table('instansi')->insert($input);
        return $req->instansi;
    }

    public function library($idgembok)
    {
        if($this->isAdmin()){
            $gembok = Gembok::getById($idgembok);
            $lib = Work::getLibraryByGembok($idgembok, 4);
            return view('/admin/library', compact('lib', 'gembok'));
        }else{
            return view('401');
        }
    }
    public function getState($idgembok)
    {
        $gembok = Work::isOpen($idgembok);
        if($gembok){
            return 0;
        }else{
            return 1;
        }
    }
    public function getStatePID($pid,$state)
    {
        $gembok = Gembok::getByPID($pid);
        $open = Work::isOpen($gembok->id);
        Master::insertLOGAPI(["produk_id"=>$pid,"gembok_id"=>$gembok->id,"state"=>$state]);
        if($open){
            return 1;
        }else{
            return 0;
        }
    }
    private function isAdmin(){
        if(session('auth')->level==2 || session('auth')->level==3){
            return 1;
        }else{
            return null;
        }
    }
    public function live_gembok_open()
    {
        return view('admin.live_gembok');
    }
    public function stream_live_gembok_open(){
        return response()->stream(function () {
            while (true) {
                echo "event: ping\n";
                $curDate = date('Y-m-d H:i:s');
                echo 'data: {"time": "' . $curDate . '"}';
                echo "\n\n";
                $vehicles = Vehicles::getLastLocationAllVehicle();
                // dd($vehicles);
                if ($vehicles) {
                    echo 'data: '. json_encode($vehicles). "\n\n";
                }

                ob_flush();
                flush();

                // Break the loop if the client aborted the connection (closed the page)
                if (connection_aborted()) {break;}
                sleep(5);
            }
        }, 200, [
            'Cache-Control' => 'no-cache',
            'Content-Type' => 'text/event-stream',
        ]);
    }

    public function getdatabywasakaid($wasakaid)
    {
        $data = Work::getById($wasakaid);
        if($data){
            return view('admin.api', compact('data'));
        }else{
            return view('401');
        }
    }

    public function tiketList($reg,$wtl)
    {   
        $region = Master::getRegionalAll();
        $dwtl = Master::getWitelByRegional($reg);
        $tiket = Master::getTiket($reg,$wtl);
        // dd($tiket);
        return view('admin.tiket.list', compact('region','dwtl','tiket'));
    }
    public function tiketForm($id)
    {   
        $data = Master::getTiketByID($id);
        return view('admin.tiket.form', compact('data'));
    }
    public function tiketAjax($id)
    {   
        $data = Work::ajaxTiket($id);
        return view('admin.tiket.ajaxForm', compact('data'));
    }
    
    public function tiketUpdate($id, Request $req)
    {   
        $data = Work::ajaxTiket($req->work_id);
        // dd($data);
        $curr = $data['current'];$past = $data['past'];
        $user_id = 0;
        if($req->suspend_user == 'Current User'){
            $user_id = $curr->id_user;
        }else if($req->suspend_user == 'Past User'){
            $user_id = $past->id_user;
        }
        $input = ['past_work_id'=>$past->id,'regional'=>$curr->regional,'wtl'=>$curr->wtl,'work_id'=>$req->work_id,'issue'=>$req->issue,'status'=>$req->status,'suspend_user'=>$req->suspend_user,'user_id'=>$user_id,'created_by'=>session('auth')->id_user];
        // dd($add);
        if(Master::getTiketByID($id)){
            Master::updateTiket($id,$input);
            return redirect()->back()
                ->with('alertblock', [
                  ['type' => 'success', 'text' => '<strong>Berhasil</strong> Mengupdate User!']
                ]);
        }else{
            //insert
            if(Master::tiketExists($id)){
                return redirect()->back()
                    ->withInput($req->all())
                    ->with('alertblock', [
                      ['type' => 'danger', 'text' => '<strong>GAGAL</strong> Tiket Sudah Tersedia!']
                    ]);
            }else{
                Master::insertTiket($input);
                return redirect()->back()
                    ->with('alertblock', [
                      ['type' => 'success', 'text' => '<strong>Berhasil</strong> Menambahkan Tiket Baru!']
                    ]);
            }

        }
    }
}
