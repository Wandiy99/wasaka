<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DA\Gembok;
use App\DA\Work;
use App\DA\Dashboard;
use App\DA\Master;
use Telegram;
use Validator;
use DateTime;
use DB;
class DashboardController extends Controller
{
    public function index($priode, $witel)
    {   
        $auth = session('auth');
    	$query_witel = "";
    	if ($witel != "ALL")
    		$query_witel = ' witel = "'.$witel.'" and ';
        $odcs = Dashboard::top10odc($priode, $query_witel);
        $users = Dashboard::top10user($priode, $query_witel);
        $times = Dashboard::top10time($priode, $query_witel);
        $work = Dashboard::top10work($priode, $query_witel);

        $row = Gembok::listAll();
        $data = array();
        $witel = '';
        $kategori = '';
        $no=0;
        foreach ($row as $no => $m){
          if($witel == $m->witel){
            if($kategori == $m->kategori)
                $data[count($data)-1][$m->kategori] += 1;  
            else
                $data[count($data)-1][$m->kategori] = 1;
            $data[count($data)-1]["total"] += 1; 
            $no++;
          }else{
            $data[] = array("witel" => $m->witel, 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, "total" => 1);
            $data[count($data)-1][$m->kategori] += 1;
          }
          $witel = $m->witel;
          $kategori = $m->kategori;
        }
        $regional = Master::getRegionalByFiberzone($auth->fiberzone);
        $data = DB::select('SELECT mw.*, (select count(id) from manci_gembok where witel = mw.witel and kategori= 1) as ftm,
            (select count(id) from manci_gembok where witel = mw.witel and kategori= 2) as odc,
            (select count(id) from manci_gembok where witel = mw.witel and kategori= 3) as msan,
            (select count(id) from manci_gembok where witel = mw.witel and kategori= 4) as mdu,
            (select count(id) from manci_gembok where witel = mw.witel and kategori= 5) as onu,
            (select count(id) from manci_gembok where witel = mw.witel and kategori= 6) as olt,
            (select count(id) from manci_gembok where witel = mw.witel and kategori is not null) as total
            FROM manci_witel mw WHERE mw.regional = "'.$regional.'" order by total desc');
        $merkGraph = DB::select('SELECT COUNT( id ) AS  Rows ,  merk 
            FROM  manci_gembok WHERE witel = "'.$witel.'"
            GROUP BY  merk 
            ORDER BY  merk');
        return view('top10', compact('odcs', 'users', 'times', 'work', 'data', 'merkGraph'));
    }
}
