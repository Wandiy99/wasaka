<?php
 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DA\Gembok;
use App\DA\Work;
use App\DA\User;
use App\DA\Dashboard;
use App\DA\Master;
use DB;
class GembokController extends Controller
{
    public function index(Request $req)
    {
        // dd(session('auth'));
        $auth = session('auth');
        // dd($auth);$list = null;
        $perangkat = null;
        if ($req->has('q')) {
            $list = Gembok::search($req->input('q'));
        }else{
            
        }
        $workUnclosed = null;
        // dd();
        if ($auth->level != '2') {
            $workUnclosed = Work::getUnclosedByUser($auth->id_user);
        }
        return view('gembok/search', compact('list', 'workUnclosed'));
    }

    public function createForm()
    {
        // dd(session('auth'));
        $witel = User::getWitelByFiberzone(session('auth')->fiberzone);
        $workUnclosed = [];
        $workClosed = [];
        $merk = Gembok::merkList();
        return view('gembok/form', compact('workClosed', 'workUnclosed', 'witel', 'merk'));
    }

    public function create(Request $req)
    {
        $this->validate($req, [
            'kode' => 'required',
            'pin'  => 'required',
            'witel'  => 'required'
        ],[
            'kode.required' => 'Kode Perangkat harus diisi',
            'pin.required'  => 'Pin Gembok harus diisi',
            'witel.required'  => 'Witel harus diisi'
        ]);
        $data = Gembok::getByKode($req->kode);
        if($data){
            return redirect()->back()
                    ->withInput($req->input())->with('alerts', [
                ['type' => 'danger', 'text' => 'Kode gembok sudah ada silahkan input kode lain.']
            ]);
        }else{
            $input = $req->only(['kode', 'pin', 'type', 'kap', 'merk', 'witel', 'koordinat', 'kategori', 'fiberzone']);
            $id = Gembok::create($input);
            if($req->file('profile'))
                Gembok::storePhotoProfile($id, $req->file('profile'));
            return redirect("/gembok/{$id}")->with('alerts', [
                ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan data']
            ]);

        }
    }

    public function read($idGembok)
    {   

        $gembok = Gembok::getById($idGembok);
        $gembok->profile = Gembok::hasPhoto($idGembok);
        $workUnclosed = Work::getUnclosedByGembok($idGembok);
        $workClosed = Work::getClosedByGembok($idGembok, env('PAGESIZE_LIST_BYGEMBOK', 25));
        $witel = User::getWitel();
        $merk = Gembok::merkList();
        return view('gembok/form', compact('gembok', 'workUnclosed', 'workClosed', 'witel', 'merk'));
    }
    public function list($witel)
    {   
        $listGembok = Gembok::getListByWitel($witel);
        return view('gembok/list', compact('listGembok'));
    }
    public function update(Request $req, $idGembok)
    {
        // todo: validation
        $input = $req->only(['kode', 'pin', 'type', 'kap', 'merk', 'witel', 'koordinat', 'kategori']);
        $data = Gembok::getById($idGembok);
        if($data->pin != $req->pin){
            Gembok::updateLogPin(['gembok_id'=>$idGembok, 'user_id'=>session('auth')->id_user, 'pin_lama'=>$data->pin,'pin_baru'=>$req->pin]);
        }
        Gembok::updateById($idGembok, $input);
        if($req->file('profile'))
            Gembok::storePhotoProfile($idGembok, $req->file('profile'));
        return redirect('/gembok/' . $idGembok)->with('alerts', [
            ['type' => 'success', 'text' => '<strong>SUKSES</strong> merubah data']
        ]);
    }
    public function jsonGetOpen(){
        return json_encode(Work::getUnclosed());
    }
}
