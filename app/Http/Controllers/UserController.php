<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\DA\User;
use App\DA\Gembok;
use Telegram;
class UserController extends Controller
{
    public function index()
    {
        $list = User::getUser();
        return view('user.list', compact('list'));
    }
    public function acl(Request $req)
    {
        $auth = session('auth');
        $list = User::getRecordedUser($req);
        $fz = User::getFiberzone();
        $witel = User::getWitel();
        $req->flashOnly(['fiberzone', 'witel']);
        return view('user.aclList', compact('list', 'fz', 'witel'));
    }
    public function profile()
    {
        $auth = session('auth');
        $user = User::getProfile($auth->id_user);
        $fz = User::getFiberzone();
        if(isset($user->fiberzone))
            $witel = User::getWitelByFiberzone($user->fiberzone);
        else
            $witel = User::getWitel();
        return view('user.profile', compact('user', 'fz', 'witel'));
    }
    public function getWitelByFiberzone($id){
        return json_encode(User::getWitelByFiberzone($id));
    }
    public function getWitelByRegional($id){
        return json_encode(User::getWitelByRegional($id));
    }
    public function profileEdit($nik)
    {
        $user = User::getProfile($nik);
        $fz = User::getFiberzone();
        if(isset($user->fiberzone))
            $witel = User::getWitelByFiberzone($user->fiberzone);
        else
            $witel = User::getWitel();
        return view('user.profile', compact('user', 'n', 'fz', 'witel'));
    }

    public function updateProfile(Request $req)
    {
        $user = User::getProfile($req->nik);
        if($user){
            $input = ['wtl'=>$req->witel, 'fz'=>$req->fiberzone, 'level'=>$req->level, 'akses_perangkat'=>$req->akses_perangkat];
            User::updateProfile($req, $input);
            if(!$user->wtl && !$user->fz){
                //-200897635
                Telegram::sendMessage(["chat_id" => "-747040158",
                    "text" => "new user from ".$req->fiberzone.":".$req->witel." a/n:".$user->nama." need approval profile.\nlink :\nwasaka.telkomakses.co.id/admin/user/".$req->nik
                ]);
            }

        }else{
            $input = ['id_user'=>$req->nik, 'wtl'=>$req->witel, 'fz'=>$req->fiberzone, 'level'=>$req->level, 'akses_perangkat'=>$req->akses_perangkat];
            $usr = User::getUserById($req->nik);
            $input['nama_alt'] = $usr->nama;
            User::storeProfile($input);
        }
        return redirect('/');
    }

    public function userForm($id)
    {
        $user = User::getUserById($id);
        return view('user.update', compact('user'));
    }

    public function update(Request $req, $id)
    {
        $user = User::userExists($id);
        if(count($user)){
            // dd($user);
            User::userUpdate($req);
        }else{
            $data = User::userExists($req->id_user);
            if($data){
                // dd('asd');
                return redirect()->back()->withInput($req->input())->with('alerts', [
                    ['type' => 'danger', 'text' => 'Gagal : IDuser sudah ada silahkan input IDuser lain']
                ]);
            }else{
                User::userCreate($req);
            }
        }
        // DB::transaction(function() use($req, $id) {
            
            // $karyawan = User::karyawanExists($id);
            // if(count($karyawan)){
            //  User::karyawanUpdate($req);
            // }else{
            //  User::karyawanCreate($req);
            // }
        // });
        return redirect('/user');
    }
    public function saran()
    {
        $saran = DB::table('saran')->orderBy('id', 'desc')->get();
        return view('faq.saran', compact('saran'));
    }
    
    public function saranSave(Request $req)
    {
        $input = $req->only('saran');
        $input['user'] = session('auth')->id_user;
        DB::table('saran')->insert($input);
        return redirect()->back();
    }
    
}
