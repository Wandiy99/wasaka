<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\DA\MapModel;

class MapController extends Controller
{
    public function map_odp_keyless()
    {
        return view('map.odp_keyless');
    }

    public function ajax_odp_keyless()
    {
        $result = MapModel::odp_keyless();

        return response()->json($result);
    }

    public function ajax_info_odp_keyless()
    {
        $result = ["akses"=>MapModel::info_odp_keyless(),"alpro"=>MapModel::info_total_alpro()];

        // dd($result);
        return response()->json($result);
    }

    public function detail_odp_keyless($unit, $status)
    {
        $data = MapModel::detail_odp_keyless($unit, $status);

        return view('map.detail_odp_keyless', compact('unit', 'status', 'data'));
    }

    public function ajax_detail_odp_keyless($unit, $status)
    {
        $raw_data = MapModel::detail_odp_keyless($unit, $status);
        $no = 0;
        $final_data = new \stdClass();

        if(count($raw_data) )
        {
          foreach($raw_data as $k => $v)
          {
            $dteStart = new \DateTime($v->time_start);
            $dteEnd   = new \DateTime($v->time_close); 
            $dteDiff  = $dteStart->diff($dteEnd); 
            $durasi   = $dteDiff->format("%Hh %Im %Ss");
            $timezone = '';
            if($v->timezone)
            {
                $timezone = $v->timezone;
            }

            $final_data->data[$k] = new \stdClass();

            $final_data->data[$k]->no              = ++$no;
            $final_data->data[$k]->time_start      = $v->time_start;
            $final_data->data[$k]->time_close      = $v->time_close;
            $final_data->data[$k]->durasi          = $durasi;
            $final_data->data[$k]->kode            = $v->kode;
            $final_data->data[$k]->jenis_pekerjaan = $v->jenis_pekerjaan;
            $final_data->data[$k]->anggota         = $v->anggota;
            $final_data->data[$k]->uraian          = $v->uraian;
            $final_data->data[$k]->nama            = $v->nama;
          }
        }
        else
        {
          $final_data->data = [];
        }

        return response()->json($final_data);
    }

    public function logactivity_odp_keyless($id)
    {
        $data = MapModel::logactivity_odp_keyless($id);

        return view('map.logactivity_odp_keyless', compact('id', 'data'));
    }

    public function ajax_logactivity_odp_keyless($id)
    {
        $raw_data = MapModel::logactivity_odp_keyless($id);
        $no = 0;
        $final_data = new \stdClass();

        if(count($raw_data) )
        {
          foreach($raw_data as $k => $v)
          {
            $dteStart = new \DateTime($v->time_start);
            $dteEnd   = new \DateTime($v->time_close); 
            $dteDiff  = $dteStart->diff($dteEnd); 
            $durasi   = $dteDiff->format("%Hh %Im %Ss");
            
            $timezone = '';
            if($v->timezone)
            {
                $timezone = $v->timezone;
            }

            $final_data->data[$k] = new \stdClass();

            $final_data->data[$k]->no              = ++$no;
            $final_data->data[$k]->time_start      = $v->time_start;
            $final_data->data[$k]->time_close      = $v->time_close;
            $final_data->data[$k]->durasi          = $durasi;
            $final_data->data[$k]->kode            = $v->kode;
            $final_data->data[$k]->jenis_pekerjaan = $v->jenis_pekerjaan;
            $final_data->data[$k]->anggota         = $v->anggota;
            $final_data->data[$k]->uraian          = $v->uraian;
            $final_data->data[$k]->nama            = $v->nama;
          }
        }
        else
        {
          $final_data->data = [];
        }

        return response()->json($final_data);
    }
    
}
