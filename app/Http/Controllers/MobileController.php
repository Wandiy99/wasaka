<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Requests;
use App\DA\Mobile;
use App\DA\Master;
use App\DA\Gembok;
use App\DA\Work;
use App\DA\User;
use App\DA\Loker;
use Image;
use Imagick;
use DB;
use Telegram;
use Validator;
use DateTime;
DEFINE('UPLOAD_PATH2', public_path().'/upload/work/');
DEFINE('PIN_PATH2', public_path().'/upload/pin');
DEFINE('ACAK_PIN_PATH2', public_path().'/upload/pin/acak');
class MobileController extends Controller
{
  CONST PATH = 'asd';
  private $kelengkapan = ["unit_splicer","holder","battery","charger","stripper","cleaver"];
  private $files = [
    'before',
    'item-before',
    'work-progress',
    'item-after'
  ];
  public function form_start($idGembok)
  {   
    // dd(UPLOAD_PATH2);
    $userid = session('auth')->id_user;
    $gembok = Gembok::getById($idGembok);
    $havingUnclosed = Work::isHavingUnclosed($userid);
    $pekerjaan = Work::getPekerjaan();
    if($gembok->kategori!='9'){
      $prof = User::getProfile($userid);
      if(!str_contains($prof->akses_perangkat, [$gembok->kategori]) || !$prof->level){
        return redirect()->back()
                ->withInput()->with('alerts', [
                            'type' => 'Gagal',
                            'text' => 'Level akses tidak diizinkan! hubungi admin setempat!'
                        ]);
      }
      $flag = Work::isOpen($idGembok);
      // dd($pekerjaan,$gembok,$havingUnclosed,$flag);
      return view('mobile.start', compact('gembok', 'flag', 'pekerjaan', 'havingUnclosed'));
    }else{
      $kelengkapan = Loker::get_items($idGembok);
      // dd($kelengkapan);
      // $flag = Work::isOpen($idGembok);
      $info = Loker::get_loker_by_id($idGembok);
      // dd($info);
      return view('mobile.loker.start', compact('pekerjaan', 'havingUnclosed', 'kelengkapan', 'info'));
    }
      
  }
  public function save_start($idGembok, Request $req)
  {   

    $userid = session('auth')->id_user;
    $gembok = Gembok::getById($idGembok);
    $prof = User::getProfile($userid);
    if(!str_contains($prof->akses_perangkat, [$gembok->kategori]) || !$prof->level){
      return redirect()->back()
              ->withInput()->with('alerts', [
                          'type' => 'Gagal',
                          'text' => 'Level akses tidak diizinkan! hubungi admin setempat!'
                      ]);
    }
    $flag = Work::isOpen($idGembok);
    if(!$flag){
      $data = $gembok;
      $rules = array(
        'anggota' => 'required',
        'before' => 'required'
      );
      $messages = [
        'anggota.required' => 'Silahkan isi kolom anggota Bang!',
        'before.required' => 'Silahkan isi kolom before Bang!',
      ];
      $validator = Validator::make($req->all(), $rules, $messages);
      $input = $req->only(['jenis_pekerjaan_id', 'anggota']);
      if ($validator->fails()) {
        return redirect()->back()
            ->withInput($req->input())
                    ->withErrors($validator)->with('alerts', [
                        'type' => 'Gagal',
                        'text' => 'Isi semua Field yang ada!'
                    ]);
      }
      $np = str_pad(rand(0,9999), 4, "0", STR_PAD_LEFT);
      if($data->pin == $np){
        $np++;
      }
      $pa = rand(0,9);
      $pin_acak = $pa.$pa.$pa.$pa;
      $input['id_user'] = $req->session()->get('auth')->id_user;
      $input['id_gembok'] = $idGembok;
      $input['time_start'] = time();
      $input['nama_user'] = $req->session()->get('auth')->nama;
      $input['pin_lama'] = $data->pin;
      $input['pin_new'] = $np;
      $input['pin_acak'] = $pin_acak;
      $id = 0;
      DB::transaction(function() use($input,$req, &$id) {
        $id = Work::create($input);
        if ($req->before) {
          $ext = 'jpg';
          $fname = 'before';
          $path = UPLOAD_PATH2.$id;
          if (!file_exists($path)){
            if (!mkdir($path, 0770, true)){
              return redirect()->back()
                  ->withInput($req->input())->with('alerts', [
                          'type' => 'Gagal',
                          'text' => 'Gagal menyiapkan folder foto'
                      ]);
            }
          }
          file_put_contents("$path/$fname.$ext", base64_decode(explode(',',$req->before)[1]));
          $i = new \Imagick("$path/$fname.$ext");
          // self::autoRotateImage($i);
          $i->scaleImage(100,150, true);
          $i->writeImage("$path/$fname-th.$ext");
        }
      });
      if($id){
        exec('cd ..;php artisan sendOpened '.$id.' > /dev/null &', $out);
        return redirect("/mobile/work/{$id}/progress")->withInput()->with('alerts', ['type' => 'Berhasil', 'text' => 'Berhasil Request PIN.']);
      }else{
        $req->session()->flash('alerts', ['type' => 'danger', 'text' => 'Ada error hub admin']);
      }
      //self::sendReportTelegram($id, 0);
    }else{
        $req->session()->flash('alerts', ['type' => 'danger', 'text' => 'Tidak dapat request PIN karena pekerjaan sedang berlangsung.']);
    }
      
  }
    
  public function form_progress($idWork)
  {   
    $data = Work::getById($idWork);
    $pekerjaan = Work::getPekerjaan();
    if ($data->time_close != '0' && session('auth')->level != 2) {
      return redirect('mobile/work/'.$idWork);
    }
    $last = Work::getLastOpen($data->id_gembok);
    if(isset($last[2]))
      $last->pin_last = $last[2]->pin_new;
    else
      $last->pin_last = "NULL";
    if(isset($last[1]))
      $last->id_last = $last[1]->id;
    else
      $last->id_last = 0;
    $data->hasPhoto = [];
    foreach($this->files as $field) {
      $data->hasPhoto[$field] = Work::hasPhotoName($data->id, "$field.jpg");
    }
    // dd($data,$pekerjaan,$last);
    return view('mobile/progress', compact('data', 'last', 'pekerjaan'));
  }
    
  public function save_progress($idWork, Request $req)
  {   
    // dd($req->input());
    $data = Work::getById($idWork);
    if ($data->time_close != '0') {
      return redirect('mobile/work/'.$idWork);
    }

    $input = $req->only([
      'uraian'
    ]);
    $rules = array(
      'uraian'        => 'required',
      'item-before'   => 'required',
      'item-after'    => 'required',
      'work-progress' => 'required'
    );
    $messages = [
      'uraian.required' => 'Silahkan isi kolom "Uraian Pekerjaan" Bang!',
      'item-before.required' => 'Silahkan isi kolom "item-before" Bang!',
      'item-after.required' => 'Silahkan isi kolom "item-after" Bang!',
      'work-progress.required' => 'Silahkan isi kolom "work-progress" Bang!'
    ];
    $validator = Validator::make($req->all(), $rules, $messages);
    if ($validator->fails()) {
        // dd('validator fail');
            return redirect()->back()
                ->withInput($req->input())
                        ->withErrors($validator)->with('alerts', [
                            'type' => 'Gagal',
                            'text' => 'Isi semua Field yang ada!'
                        ]);
                        
    }
    // dd($req->has('work-progress'));
    foreach($this->files as $field) {
      if ($req->has($field)) {
        try {
          $ext = 'jpg';
          $path = UPLOAD_PATH2.$idWork;
          if (!file_exists($path)){
              if (!mkdir($path, 0770, true)){
                  throw new \Exception('Gagal menyiapkan folder foto');
              }
          }
          file_put_contents("$path/$field.$ext", base64_decode(explode(',',$req->input($field))[1]));
          $i = new \Imagick("$path/$field.$ext");
          // self::autoRotateImage($i);
          $i->scaleImage(100,150, true);
          $i->writeImage("$path/$field-th.$ext");
        }
        catch (\Exception $e) {
          return redirect()->back()
              ->withInput($req->input())->with('alerts', [
                      'type' => 'Gagal',
                      'text' => 'Gagal menyimpan file, hubungi admin. trace : '.$e->getMessage()
                  ]);
        }
      }
    }
    Work::updateById($idWork, $input);

    return redirect("mobile/work/{$idWork}/close")->withInput()->with('alerts', ['type' => 'Berhasil', 'text' => 'Berhasil menyimpan data progress.']);
  }
  public function form_close($idWork)
  {
    $data = Work::getById($idWork);
    if($data->time_close != 0){
      return redirect('/mobile')->with('alerts', ['type' => 'success', 'text' => 'Pekerjaan sudah close']);
    }
    $data->hasPhoto = [];
    foreach($this->files as $field) {
      $data->hasPhoto[$field] = Work::hasPhotoName($data->id, "$field.jpg");
    }

    $last = Work::getLastOpen($data->id_gembok);
    if(isset($last[2]))
      $last->pin_last = $last[2]->pin_new;
    else
      $last->pin_last = "NULL";
    if(isset($last[1]))
      $last->id_last = $last[1]->id;
    else
      $last->id_last = 0;
    // dd($data);
    return view('mobile/close', compact('data', 'last'));
  }
  public function save_close($idWork,Request $req)
  {
    // dd($req->input());
    $data = Work::getById($idWork);
    $gembok = Gembok::getById($data->id_gembok);
    if($data->produk_id){
      $rules = array();
    }else{
      $rules = array(
        'resetPin' => 'required',
        'acakPin' => 'required'
      );
    }
    if ($req->session()->get('auth')->id_user != $data->id_user) {
      return redirect('/')->with('alerts', ['type' => 'Gagal', 'text' => 'Hanya User yg bersangkutan.']);
    }
    // TODO: pin validation
    if($data->time_close != 0){
      return redirect('/')->with('alerts', ['type' => 'success', 'text' => 'Pekerjaan sudah close.']);
    }
    if($gembok->pin == $req->input('pin')){
      return redirect()->back()->with('alerts', ['type' => 'danger', 'text' => 'Gagal : PIN Lama harus beda dengan PIN Baru!']);
    }else if(strlen($req->input('pin'))!=4){
      return redirect()->back()->with('alerts', ['type' => 'danger', 'text' => 'Gagal : PIN Baru harus 4 Digit!']);
    }else{
        
          
      $messages = [
        'resetPin.required' => 'Silahkan isi kolom upload "Foto Pin Reset" Bang!',
        'acakPin.required' => 'Silahkan isi kolom upload "Foto Acak PIN" Bang!'
      ];
      $validator = Validator::make($req->all(), $rules, $messages);
      if ($validator->fails()) {
        return redirect()->back()
              ->withInput($req->input())
                      ->withErrors($validator)->with('alerts', [
                          'type' => 'Gagal',
                          'text' => 'Isi semua Field yang ada!'
                      ]);
      }
      try {
        // Work::storePhotoPin($idWork, $req->file('resetPin'));
        // Work::storePhotoAcakPin($idWork, $req->file('acakPin'));
        foreach($this->files as $field) {
          if ($req->has($field)) {
            try {
              $ext = 'jpg';
              $path = UPLOAD_PATH2.$idWork;
              if (!file_exists($path)){
                if (!mkdir($path, 0770, true)){
                  throw new \Exception('Gagal menyiapkan folder foto');
                }
              }
              file_put_contents("$path/$field.$ext", base64_decode(explode(',',$req->input($field))[1]));
              $i = new \Imagick("$path/$field.$ext");
              // self::autoRotateImage($i);
              $i->scaleImage(100,150, true);
              $i->writeImage("$path/$field-th.$ext");
            }
            catch (\Exception $e) {
              return redirect()->back()
                    ->withInput($req->input())->with('alerts', [
                            'type' => 'Gagal',
                            'text' => 'Gagal menyimpan file, hubungi admin. trace : '.$e->getMessage()
                        ]);
            }
          }
        }
        if ($req->has('resetPin')) {
          try {
            $ext = 'jpg';
            $path = PIN_PATH;
            if (!file_exists($path)){
              if (!mkdir($path, 0770, true)){
                throw new \Exception('Gagal menyiapkan folder foto');
              }
            }
            file_put_contents("$path/$idWork.$ext", base64_decode(explode(',',$req->input('resetPin'))[1]));
            $i = new \Imagick("$path/$idWork.$ext");
            // self::autoRotateImage($i);
            $i->scaleImage(100,150, true);
            $i->writeImage("$path/$idWork-th.$ext");
          }
          catch (\Exception $e) {
            return redirect()->back()
                  ->withInput($req->input())->with('alerts', [
                          'type' => 'Gagal',
                          'text' => 'Gagal menyimpan file, hubungi admin. trace : '.$e->getMessage()
                      ]);
          }
        }
        if ($req->has('acakPin')) {
          try {
            $ext = 'jpg';
            $path = ACAK_PIN_PATH2;
            if (!file_exists($path)){
              if (!mkdir($path, 0770, true)){
                throw new \Exception('Gagal menyiapkan folder foto');
              }
            }
            file_put_contents("$path/$idWork.$ext", base64_decode(explode(',',$req->input('acakPin'))[1]));
            $i = new \Imagick("$path/$idWork.$ext");
            // self::autoRotateImage($i);
            $i->scaleImage(100,150, true);
            $i->writeImage("$path/$idWork-th.$ext");
          }catch (\Exception $e) {
            return redirect()->back()
                  ->withInput($req->input())->with('alerts', [
                          'type' => 'Gagal',
                          'text' => 'Gagal menyimpan file, hubungi admin. trace : '.$e->getMessage()
                      ]);
          }
        }
      }
      catch (\Exception $e) {
        return redirect()->back()
                  ->withInput($req->input())->with('alerts', [
                          'type' => 'Gagal',
                          'text' => 'Gagal menyimpan file, hubungi admin. trace : '.$e->getMessage()
                      ]);
      }
      Work::close($idWork, $data->pin_new);
      //self::sendReportTelegram($idWork, 1);
      exec('cd ..;php artisan sendClosed '.$idWork.' > /dev/null &', $out);
      exec('cd ..;php artisan sendAdmin '.$idWork.' > /dev/null &', $out);
      return redirect('/')->withInput()->with('alerts', ['type' => 'Berhasil', 'text' => 'Sukses menyimpan data']);
    }  
  }
  public function scan()
  {   
    $userid = session('auth')->id_user;
    $prof = User::getProfile($userid);
    if(!$prof->level){
        return redirect('/')
                ->withInput()->with('alerts', [
                            'type' => 'Gagal',
                            'text' => 'Level akses tidak diizinkan! hubungi admin setempat!'
                        ]);
    }
    $ogp = Work::getUnclosedByUser(session('auth')->id_user);
    return view('mobile.scan',compact('ogp'));
  }
  public function search(Request $req)
  {   
    $userid = session('auth')->id_user;
    $prof = User::getProfile($userid);
    if(!$prof->level){
        return redirect('/')
                ->withInput()->with('alerts', [
                            'type' => 'Gagal',
                            'text' => 'Level akses tidak diizinkan! hubungi admin setempat!'
                        ]);
    }
    if(!session('auth')->loker){
        return redirect('/')
                ->withInput()->with('alerts', [
                    'type' => 'Gagal',
                    'text' => 'Silahkan Update Profile Terbaru!'
                ]);
    }

    $ogp = Work::getUnclosedByUser(session('auth')->id_user);
    if(count($ogp)){
        $list = new \stdClass;
    }else{
        $list = Gembok::search($req->input('q'));
    }
    // dd($ogp,session('auth'));
    return view('mobile.search',compact('ogp','list'));
  }
  public function profile()
  {   
      $profile = Master::getUserByID(session('auth')->id_user);
      $instansi = Master::getInstansi();
      $loker = Master::getLoker();
      $reg = Master::getRegionalAll();
      $wtl = new \stdClass;
      if($profile){
          if($profile->regional != '' || $profile->regional != null){
              $wtl = Master::getWitelByRegional($profile->regional);
          }
      }
      return view('mobile.profile',compact('profile','instansi','reg','wtl','loker'));
  }
  public function profileSave(Request $req)
  {   
    $update = $req->only('no_hp','instansi','regional','wtl','loker');
    $rules = array(
      'no_hp' => 'required',
      'instansi' => 'required',
      'regional' => 'required',
      'wtl' => 'required',
      'loker' => 'required'
    );
    $messages = [
      'no_hp.required' => 'Silahkan isi kolom anggota Bang!',
      'instansi.required' => 'Silahkan isi kolom instansi Bang!',
      'regional.required' => 'Silahkan isi kolom Regional Bang!',
      'wtl.required' => 'Silahkan isi kolom witel Bang!',
      'loker.required' => 'Silahkan isi kolom loker Bang!',
    ];
    $validator = Validator::make($update, $rules, $messages);
    if ($validator->fails()) {
      return redirect()->back()
          ->withInput($update)
          ->withErrors($validator)->with('alerts', [
              'type' => 'Gagal',
              'text' => 'Isi semua Field yang ada!'
          ]);
    }else{
      Master::profileSave(session('auth')->id_user, $update);
      return redirect()->back()->withInput()->with('alerts', [
        'type' => 'Success',
        'text' => 'Berhasil Submit'
      ]);

    }
  }
  public function ajaxHistory($gembok_id)
  {   
    $data=work::getLastOpen($gembok_id);
    return view('mobile.histAjax', compact('data'));
  }
    
}
