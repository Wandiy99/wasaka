<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link rel="apple-touch-icon" sizes="180x180" href="/app/icons/icon-192x192.png">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<link rel="manifest" href="/_manifest.json">
    <title>@yield('title')</title>

    <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css"> -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css">

    <?php
        // session('auth')->pixeltheme = 'frost';
        $auth = session('auth');
        //dd($auth);
        $path = Request::path();
        // dd(Request::path());
        $theme = 'mint-dark';
        if(isset($auth->pixeltheme)){
            if($auth->pixeltheme){
                $theme = $auth->pixeltheme;
            }
        }
        $color = '';
        if (str_contains($theme, 'dark')) {
            $color = '-dark';
        }
        // dd($theme,$color);
    ?>
    <link href="/pixeladmin/css/bootstrap{{ $color }}.min.css" rel="stylesheet" type="text/css">
    <link href="/pixeladmin/css/pixeladmin{{ $color }}.min.css" rel="stylesheet" type="text/css">
    <link href="/pixeladmin/css/widgets{{ $color }}.min.css" rel="stylesheet" type="text/css">
    <link href="/pixeladmin/css/themes/{{ $theme }}.min.css" rel="stylesheet" type="text/css">

    <script src="/pixeladmin/pace/pace.min.js"></script>
    @yield('css')
</head>
<?php
$admin = array('97150038', '17920262');
$isAdmin = (session('auth')->level==2 || session('auth')->level==3)?1:0;
?>
<body>
    <nav class="px-nav px-nav-left px-nav-fixed">
        <button type="button" class="px-nav-toggle" data-toggle="px-nav">
            <span class="px-nav-toggle-arrow"></span>
            <span class="navbar-toggle-icon"></span>
            <span class="px-nav-toggle-label font-size-11">HIDE MENU</span>
        </button>

        <ul class="px-nav-content">
            @if($isAdmin)
            @if(@$auth->regional)
                <li class="px-nav-item">
                    <a href="/home/{{$auth->regional}}/{{$auth->witel}}/0"><i class="px-nav-icon ion-pie-graph"></i><span class="px-nav-label">Dashboard</span></a>
                </li>
            @else
                <li class="px-nav-item">
                    <a href="/admin/user/{{ $auth->id_user }}"><i class="px-nav-icon ion-pie-graph"></i><span class="px-nav-label">Dashboard</span><span class="label label-round label-danger badge">Set Profile</span></a>
                    
                </li>
            @endif
            @if(@$auth->regional)
                <li class="px-nav-item px-nav-dropdown disable">
                    <a href="#"><i class="px-nav-icon ion-settings"></i><span class="px-nav-label">Admin</span></a>
                    <ul class="px-nav-dropdown-menu">
                        <li class="px-nav-item"><a href="/admin/gembok/{{$auth->regional}}/{{$auth->witel}}/0"><span class="px-nav-label">Kunci</span></a></li>
                        <li class="px-nav-item"><a href="/admin/user/{{$auth->regional}}/{{$auth->witel}}"><span class="px-nav-label">User & Akses</span></a></li>
                        <li class="px-nav-item"><a href="/admin/history/{{date('Y-m-d:Y-m-d')}}/{{$auth->regional}}/{{$auth->witel}}/0"><span class="px-nav-label">History</span></a></li>
                        <li class="px-nav-item"><a href="/admin/listTiket/{{$auth->regional}}/{{$auth->witel}}"><span class="px-nav-label">Tiketing QC</span></a></li>
                    </ul>
                </li>
                @if($auth->id_user == "wandiy99" || $auth->id_user == "16012851")
                <li class="px-nav-item px-nav-dropdown disable">
                    <a href="#"><i class="px-nav-icon ion-settings"></i><span class="px-nav-label">Admin MTEL</span></a>
                    <ul class="px-nav-dropdown-menu">
                        <li class="px-nav-item"><a href="/admin/gembok/{{$auth->regional}}/{{$auth->witel}}/1"><span class="px-nav-label">Kunci</span></a></li>
                        <li class="px-nav-item"><a href="/admin/history/{{date('Y-m-d:Y-m-d')}}/{{$auth->regional}}/{{$auth->witel}}/1"><span class="px-nav-label">History</span></a></li>
                    </ul>
                </li>
                @endif
            @else
                <li class="px-nav-item">
                    <a href="/admin/user/{{ $auth->id_user }}"><i class="px-nav-icon ion-settings"></i><span class="px-nav-label">Admin</span><span class="label label-round label-danger badge">Set Profile</span></a>
                    
                </li>
            @endif
            <!-- <li class="px-nav-item px-nav-dropdown">
                <a href="#"><i class="px-nav-icon ion-filing"></i><span class="px-nav-label">SOP</span></a>
                <ul class="px-nav-dropdown-menu">
                    <li class="px-nav-item"><a href="#"><span class="px-nav-label">For Admin</span></a></li>
                    <li class="px-nav-item"><a href="#"><span class="px-nav-label">For User</span></a></li>
                </ul>
            </li> -->
            @endif
            <li class="px-nav-item px-nav-dropdown">
                <a href="#"><i class="px-nav-icon ion-person"></i><span class="px-nav-label">{{ $auth->nama }} <code>{{ $auth->id_user }}</code></span></a>
                <ul class="px-nav-dropdown-menu">
                    <li class="px-nav-item"><a href="/lama"><span class="px-nav-label">Tampilan Lama</span></a></li>
                    <!-- <li class="px-nav-item"><a href="/"><span class="px-nav-label">Tampilan Mobile</span></a></li> -->
                    <li class="px-nav-item"><a href="/admin/user/{{ $auth->id_user }}"><span class="px-nav-label">Profile</span></a></li>
                    <li class="px-nav-item"><a href="/admin/theme"><span class="px-nav-label">Themes</span></a></li>
                    <li class="px-nav-item"><a href="/logout"><span class="px-nav-label">Logout</span></a></li>
                </ul>
            </li>
        </ul>
    </nav>

    <nav class="navbar px-navbar">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">WASAKA</a>
        </div>

        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#px-navbar-collapse" aria-expanded="false"><i class="navbar-toggle-icon"></i></button>

        <div class="collapse navbar-collapse" id="px-navbar-collapse">
            <ul class="nav navbar-nav">
                <li>
                  <a href="#">
                    <span class="font-weight-bold">@yield('title')</span>
                  </a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    <i class="px-navbar-icon ion ion-unlocked font-size-14"></i>
                    <span class="px-navbar-icon-label">Income messages</span>
                    <span class="px-navbar-label label label-danger">{{ count($unclosedCount['witel']) }}</span>
                  </a>
                  <div class="dropdown-menu p-a-0" style="width: 300px;">
                    <div id="navbar-messages" style="height: 280px; position: relative;">
                    @foreach($unclosedCount['witel'] as $open)
                      <div class="widget-messages-alt-item">
                        <?php
                          $path = "/upload/Depan_ODC/".$open->id_gembok;
                          $th   = "$path-th.jpg";
                        ?>
                        @if (file_exists(public_path().$th))
                          <img src="{{ $th }}" alt="" class="widget-messages-alt-avatar"/>
                        @else
                          <img src="/image/placeholder.gif" alt="" class="widget-messages-alt-avatar"/>
                        @endif
                        <a href="#" class="widget-messages-alt-subject text-truncate">{{ $open->kode }} : {{ $open->uraian }}</a>
                        <div class="widget-messages-alt-description">from <a href="#">{{ $open->nama_user }}</a></div>
                        <div class="widget-messages-alt-date">{{ $open->durasi }} ago</div>
                      </div>
                    @endforeach
                    </div>
                  </div> <!-- / .dropdown-menu -->
                </li>
                <li>
                    <form class="navbar-form" role="search" action="/adminsearch">
                        <div class="form-group">
                            <input type="text" name="q" class="form-control border-round" placeholder="Search Gembok" style="width: 240px;">
                        </div>
                    </form>
                </li>
            </ul>
        </div>
    </nav>

    <div class="px-content">
        <div id="block-alert-with-timer"></div>
        @yield('content')
    </div>

    <footer class="px-footer px-footer-bottom">
        <code>WA</code>dah <code>SA</code>gan <code>K</code>unci <code>A</code>kses.Powered by Laravel-Pixeladmin.
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="/pixeladmin/js/bootstrap.min.js"></script>
    <script src="/pixeladmin/js/pixeladmin.min.js"></script>
    <script type="text/javascript">
        $(function(){
            var file = window.location.pathname;
            $('body > .px-nav')
            .find('.px-nav-item > a[href="' + decodeURIComponent(file) + '"]')
            .parent()
            .addClass('active');
            
            $('body > .px-nav').pxNav();
            $('body > .px-footer').pxFooter();
            var alertExist = <?= json_encode(Session::has('alerts')); ?>;
            if(alertExist){
                toastr.options = {
                    "closeButton": true,
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "timeOut": "200000"
                }
                var msg = <?= session('alert')?json_encode(session('alerts')[0]):'0'; ?>;
                toastr.info(msg.text);
            }

            var alertBlock = <?= json_encode(Session::has('alertblock')); ?>;
            if(alertBlock){
                var $container = $('#block-alert-with-timer');
                var alrt = <?= session('alertblock')?json_encode(session('alertblock')[0]):'0'; ?>;
                $container.pxBlockAlert(alrt.text, { type: alrt.type, style: 'light', timer: 3 });
            }
            document.addEventListener('DOMContentLoaded', () => {
    'use strict'

    //Global Variables
    let isPWA = true;  // Enables or disables the service worker and PWA
    let isAJAX = false; // AJAX transitions. Requires local server or server
    var pwaName = "WasakaApp"; //Local Storage Names for PWA
    var pwaRemind = 1; //Days to re-remind to add to home
    var pwaNoCache = false; //Requires server and HTTPS/SSL. Will clear cache with each visit

    //Setting Service Worker Locations scope = folder | location = service worker js location
    var pwaScope = "/";
    var pwaLocation = "/_service-worker.js";

    //Place all your custom Javascript functions and plugin calls below this line
    function init_template(){
        //Caching Global Variables
 
        //Detecting Mobile OS
        let isMobile = {
            Android: function() {return navigator.userAgent.match(/Android/i);},
            iOS: function() {return navigator.userAgent.match(/iPhone|iPad|iPod/i);},
            any: function() {return (isMobile.Android() || isMobile.iOS());}
        };
        function iOSversion() {if (/iP(hone|od|ad)/.test(navigator.platform)) {var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/); return [parseInt(v[1], 10)];}}


        const androidDev = document.getElementsByClassName('show-android');
        const iOSDev = document.getElementsByClassName('show-ios');
        const noDev = document.getElementsByClassName('show-no-device');

        if(!isMobile.any()){
            for (let i = 0; i < iOSDev.length; i++) {iOSDev[i].classList.add('disabled');}
            for (let i = 0; i < androidDev.length; i++) {androidDev[i].classList.add('disabled');}
        }
        if(isMobile.iOS()){
            for (let i = 0; i < noDev.length; i++) {noDev[i].classList.add('disabled');}
            for (let i = 0; i < androidDev.length; i++) {androidDev[i].classList.add('disabled');}
            //Detect iOS 15 or Higher Version and Attach Classes
            var iOSVer = iOSversion();
            if(iOSVer > 15){
                //const tabBar = document.querySelectorAll('.iosTabBar')[0];
                //if(!tabBar){document.querySelectorAll('#footer-bar')[0].classList.add('iosTabBar');}
            }
        }
        if(isMobile.Android()){
            for (let i = 0; i < iOSDev.length; i++) {iOSDev[i].classList.add('disabled');}
            for (let i = 0; i < noDev.length; i++) {noDev[i].classList.add('disabled');}
        }

        //Adding is-on-homescreen class to be targeted when used as PWA.
        function ath(){
            (function(a, b, c) {
                if (c in b && b[c]) {
                    var d, e = a.location,
                        f = /^(a|html)$/i;
                    a.addEventListener("click", function(a) {
                        d = a.target;
                        while (!f.test(d.nodeName)) d = d.parentNode;
                        "href" in d && (d.href.indexOf("http") || ~d.href.indexOf(e.host)) && (a.preventDefault(), e.href = d.href)
                    }, !1);
                    document.querySelectorAll('.page-content')[0].classList.add('is-on-homescreen');
                    setTimeout(function(){document.querySelectorAll('#footer-bar')[0].classList.remove('iosTabBar');},50)
                }
            })(document, window.navigator, "standalone")
        }
        ath();

        //PWA Settings
        if(isPWA === true){
            //Defining PWA Windows
            var iOS_PWA = document.querySelectorAll('#menu-install-pwa-ios')[0];
            if(iOS_PWA){var iOS_Window = new bootstrap.Offcanvas(iOS_PWA)}
            var Android_PWA = document.querySelectorAll('#menu-install-pwa-android')[0];
            if(Android_PWA){var Android_Window = new bootstrap.Offcanvas(Android_PWA)}

            var checkPWA = document.getElementsByTagName('html')[0];
            if(!checkPWA.classList.contains('isPWA')){
                if ('serviceWorker' in navigator) {
                  window.addEventListener('load', function() {
                    navigator.serviceWorker.register(pwaLocation, {scope: pwaScope}).then(function(registration){registration.update();})
                  });
                }

                //Setting Timeout Before Prompt Shows Again if Dismissed
                var hours = pwaRemind * 24; // Reset when storage is more than 24hours
                var now = Date.now();
                var setupTime = localStorage.getItem(pwaName+'-PWA-Timeout-Value');
                if (setupTime == null) {
                    localStorage.setItem(pwaName+'-PWA-Timeout-Value', now);
                } else if (now - setupTime > hours*60*60*1000) {
                    localStorage.removeItem(pwaName+'-PWA-Prompt')
                    localStorage.setItem(pwaName+'-PWA-Timeout-Value', now);
                }


                const pwaClose = document.querySelectorAll('.pwa-dismiss');
                pwaClose.forEach(el => el.addEventListener('click',e =>{
                    const pwaWindows = document.querySelectorAll('#menu-install-pwa-android, #menu-install-pwa-ios');
                    for(let i=0; i < pwaWindows.length; i++){pwaWindows[i].classList.remove('menu-active');}
                    localStorage.setItem(pwaName+'-PWA-Timeout-Value', now);
                    localStorage.setItem(pwaName+'-PWA-Prompt', 'install-rejected');
                    console.log('PWA Install Rejected. Will Show Again in '+ (pwaRemind)+' Days')
                }));

                //Trigger Install Prompt for Android
                const pwaWindows = document.querySelectorAll('#menu-install-pwa-android, #menu-install-pwa-ios');
                if(pwaWindows.length){
                    if (isMobile.Android()) {
                        if (localStorage.getItem(pwaName+'-PWA-Prompt') != "install-rejected") {
                            function showInstallPrompt() {
                                setTimeout(function(){
                                    if (!window.matchMedia('(display-mode: fullscreen)').matches) {
                                        console.log('Triggering PWA Window for Android')
                                        Android_Window.show()
                                    }
                                },3500);
                            }
                            var deferredPrompt;
                            window.addEventListener('beforeinstallprompt', (e) => {
                                e.preventDefault();
                                deferredPrompt = e;
                                showInstallPrompt();
                            });
                        }
                        const pwaInstall = document.querySelectorAll('.pwa-install');
                        pwaInstall.forEach(el => el.addEventListener('click', e => {
                            deferredPrompt.prompt();
                            deferredPrompt.userChoice
                                .then((choiceResult) => {
                                    if (choiceResult.outcome === 'accepted') {
                                        console.log('Added');
                                    } else {
                                        localStorage.setItem(pwaName+'-PWA-Timeout-Value', now);
                                        localStorage.setItem(pwaName+'-PWA-Prompt', 'install-rejected');
                                        setTimeout(function(){
                                            if (!window.matchMedia('(display-mode: fullscreen)').matches) {
                                                Android_Window.show()
                                            }
                                        },50);
                                    }
                                    deferredPrompt = null;
                                });
                        }));
                        window.addEventListener('appinstalled', (evt) => {
                            Android_Window.hide()
                        });
                    }
                    //Trigger Install Guide iOS
                    if (isMobile.iOS()) {
                        if (localStorage.getItem(pwaName+'-PWA-Prompt') != "install-rejected") {
                            setTimeout(function(){
                                if (!window.matchMedia('(display-mode: fullscreen)').matches) {
                                    console.log('Triggering PWA Window for iOS');
                                    iOS_Window.show()
                                }
                            },3500);
                        }
                    }
                }
            }
            checkPWA.setAttribute('class','isPWA');
        }
    }
    init_template();
});
            
        });
    </script>
    @yield('js')
</body>
</html>
