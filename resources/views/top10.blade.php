@extends('layout')

@section('head')
    <style type="text/css">
        .container {
            width: 100%;
            margin: 0;
            padding: 0;
        }
        .table-condensed{
          font-size: 10px;
        }
    </style>
@endsection
@section('content')
<div class="rows col-sm-3">
    <div class="panel panel-default">
        <div class="panel-heading">Top 10 Akses ODC</div>
        <div class="panel-body">
            <table class="table table-bordered table-striped table-condensed">
                <tr>
                    <th>#</th>
                    <th>Nama ODC</th>
                    <th>Times</th>
                </tr>
                @foreach($odcs as $no => $odc)
                    <tr>
                        <td>{{ ++$no }}</td>
                        <td>{{ $odc->kode }}</td>
                        <td>{{ $odc->Baris }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
<div class="rows col-sm-3">
    <div class="panel panel-default">
        <div class="panel-heading">Top 10 Akses Technician</div>
        <div class="panel-body">
            <table class="table table-bordered table-striped table-condensed">
                <tr>
                    <th>#</th>
                    <th>Nama Technician</th>
                    <th>Times</th>
                </tr>
                @foreach($users as $no => $user)
                    <tr>
                        <td>{{ ++$no }}</td>
                        <td>{{ $user->nama_user }}</td>
                        <td>{{ $user->Baris }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
<div class="rows col-sm-3">
    <div class="panel panel-default">
        <div class="panel-heading">Top 10 Waktu Akses ODC</div>
        <div class="panel-body">
            <table class="table table-bordered table-striped table-condensed">
                <tr>
                    <th>#</th>
                    <th>Nama ODC</th>
                    <th>Durasi(SUM)</th>
                </tr>
                @foreach($times as $no => $time)
                    <tr>
                        <td>{{ ++$no }}</td>
                        <td>{{ $time->kode }}</td>
                        <td>{{ $time->Baris }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
<div class="rows col-sm-3">
    <div class="panel panel-default">
        <div class="panel-heading">Top 10 Pekerjaan ODC</div>
        <div class="panel-body">
            <table class="table table-bordered table-striped table-condensed">
                <tr>
                    <th>#</th>
                    <th>Pekerjaan</th>
                    <th>Times</th>
                    <th>Durasi(AVG)</th>
                </tr>
                @foreach($work as $no => $w)
                    <tr>
                        <td>{{ ++$no }}</td>
                        <td>{{ $w->jenis_pekerjaan }}</td>
                        <td>{{ $w->Baris }}</td>
                        <td>{{ $w->average }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-md-12 mx-2">
        <div id="chartdiv" style="height:250px;background-color:white;-webkit-border-radius: 10px;
        -moz-border-radius: 10px;
        border-radius: 10px;"></div>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-md-12">
        <div id="chartdiv1" style="height:250px;background-color:white;-webkit-border-radius: 10px;
        -moz-border-radius: 10px;
        border-radius: 10px;"></div>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-md-6">
        <div id="chart1div" style="height:250px;background-color:white;-webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;"></div>
    </div>
    <div class="col-md-6">
        <div id="chart2div" style="height:250px;background-color:white;-webkit-border-radius: 10px;
        -moz-border-radius: 10px;
        border-radius: 10px;"></div>
    </div>
</div>
@endsection
@section('script')
    <script src="/bower_components/amcharts3/amcharts/amcharts.js" type="text/javascript"></script>
    <script src="/bower_components/amcharts3/amcharts/serial.js" type="text/javascript"></script>
    <script src="/bower_components/amcharts3/amcharts/themes/light.js" type="text/javascript"></script>
    <script type="text/javascript">

    $(function() {
        var data = <?= json_encode($data) ?>;
        AmCharts.makeChart("chartdiv", {
            "type": "serial",
            "theme": "light",
            "legend": {
                "horizontalGap": 10,
                "maxColumns": 1,
                "position": "right",
                "useGraphSettings": true,
                "markerSize": 10
            },
            "dataProvider": data,
            "urlField": "url",
            "valueAxes": [{
                "stackType": "regular",
                "axisAlpha": 0.3,
                "gridAlpha": 0

            }],
            "graphs": [{
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "FTM",
                "type": "column",
                "valueField": "ftm",
                "urlField": "url"
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "ODC",
                "type": "column",
                "valueField": "odc",
                "urlField": "url"
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "MSAN",
                "type": "column",
                "valueField": "msan",
                "urlField": "url"
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "MDU",
                "type": "column",
                "valueField": "mdu",
                "urlField": "url"
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "ONU",
                "type": "column",
                "valueField": "onu",
                "urlField": "url"
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "OLT",
                "type": "column",
                "valueField": "olt",
                "urlField": "url"
            }],
            "categoryField": "witel",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "gridAlpha": 0,
                "position": "left",
                "title": "Count installed Gembok Per witel"
            }
        });
        var mg = <?= json_encode($merkGraph) ?>;
         AmCharts.makeChart("chartdiv1", {
            "theme": "light",
            "type": "serial",
            "dataProvider": mg,
            "valueAxes": [{
                "title": "Top10 akses ODC"
            }],
            "graphs": [{
                "balloonText": "Unit [[category]]:[[value]]",
                "fillAlphas": 1,
                "lineAlpha": 0.2,
                "title": "Income",
                "type": "column",
                "valueField": "Rows"
            }],
            "depth3D": 20,
            "angle": 30,
            "rotate": true,
            "categoryField": "merk",
            "categoryAxis": {
                "gridPosition": "start",
                "fillAlpha": 0.05,
                "position": "left"
            }
        });
        var odc = <?= json_encode($odcs) ?>;
        AmCharts.makeChart("chart1div", {
            "theme": "light",
            "type": "serial",
            "dataProvider": odc,
            "valueAxes": [{
                "title": "Top10 akses ODC"
            }],
            "graphs": [{
                "balloonText": "ODC [[category]]:[[value]]",
                "fillAlphas": 1,
                "lineAlpha": 0.2,
                "title": "Income",
                "type": "column",
                "valueField": "Baris"
            }],
            "depth3D": 20,
            "angle": 30,
            "rotate": true,
            "categoryField": "kode",
            "categoryAxis": {
                "gridPosition": "start",
                "fillAlpha": 0.05,
                "position": "left"
            }
        });
        var work = <?= json_encode($work) ?>;
        AmCharts.makeChart("chart2div", {
            "theme": "light",
            "type": "serial",
            "dataProvider": work,
            "valueAxes": [{
                "title": "Top10 Kategori Pekerjaan"
            }],
            "graphs": [{
                "balloonText": "Kategori [[category]]:[[value]]",
                "fillAlphas": 1,
                "lineAlpha": 0.2,
                "title": "Income",
                "type": "column",
                "valueField": "Baris"
            }],
            "depth3D": 20,
            "angle": 30,
            "rotate": true,
            "categoryField": "jenis_pekerjaan",
            "categoryAxis": {
                "gridPosition": "start",
                "fillAlpha": 0.05,
                "position": "left"
            }
        });
    });
</script>
@endsection