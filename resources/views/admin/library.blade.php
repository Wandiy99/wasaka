@extends('layoutadmin')
@section('css')
<style type="text/css">

</style>
@endsection
@section('title', 'Library Foto')
@section('content')
<div class="panel m-t-2">
  <div class="panel-body">
    <div class="table-responsive table-primary m-t-2">
      <table class="table" id="datatables">
        <thead>
          <tr>
            <th>#</th>
            <th>Informasi</th>
            <th>Kondisi Awal</th>
            <th>Sebelum Kegiatan</th>
            <th>Proses Kegiatan</th>
            <th>Setelah Kegiatan</th>
        </tr>
        </thead>
        <tbody>
          @foreach($lib as $no => $log)
            <?php
            $context = stream_context_create( [
                'ssl' => [
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                ],
            ]);
            $dteStart = new \DateTime(date('Y-m-d H:i:s', $log->time_start)); 
            $dteEnd   = new \DateTime(date('Y-m-d H:i:s', $log->time_close)); 
            $dteDiff  = $dteStart->diff($dteEnd); 
            $durasi   = $dteDiff->format("%Hh %Im %Ss");
            $path="/upload/work/".$log->id;
            $path2="https://marina.bjm.tomman.app/backupwasaka/work/".$log->id;
            // dd(get_headers($path2)[0]);
            $path3="https://marina.bjm.tomman.app/backupwasaka2/work/".$log->id;
            if(file_exists(public_path().$path."/before.jpg")){
                $path = $path;
            }else if(stripos(get_headers($path2."/before.jpg", 0, $context)[0],"200 OK")){
                $path = $path2;
            }else if(stripos(get_headers($path3."/before.jpg", 0, $context)[0],"200 OK")){
                $path = $path3;
            }else{
                $path = '/image/placeholder.gif';
            }
            ?>
            <tr>
                <td>{{ ++$no }}</td>
                <td>
                    <span class="label label-info">Tgl : {{ date('d/m/Y', $log->time_start) }}</span><br/>
                    <span class="label label-info">Durasi : {{ $durasi }}</span><br/>
                    <span class="label label-info">Pekerjaan :{{ $log->jenis_pekerjaan_id == 15 ? $log->pekerjaan : $log->jenis_pekerjaan }}</span><br/>
                    <span class="label label-info">User : 
                    @if($log->nama)
                        {{ $log->nama }}
                    @elseif($log->nama_user)
                        {{ $log->nama_user }}
                    @endif</span>
                </td>
                <td>
                    <a href="{{ $path }}/before.jpg">
                        <img src="{{ $path }}/before-th.jpg" alt="">
                    </a>
                </td>
                <td>
                    <a href="{{ $path }}/item-before.jpg">
                        <img src="{{ $path }}/item-before-th.jpg" alt="">
                    </a>
                </td>
                <td>
                    <a href="{{ $path }}/work-progress.jpg">
                        <img src="{{ $path }}/work-progress-th.jpg" alt="">
                    </a>
                </td>
                <td>
                    <a href="{{ $path }}/item-after.jpg">
                        <img src="{{ $path }}/item-after-th.jpg" alt="">
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
      </table>
    </div>
</div></div>
@if ($lib->total() > $lib->perPage())
<div class="panel-footer panel-pager text-center">
<nav>
  <ul class="pagination">
    {{ $lib->links() }}
  </ul>
</nav>
</div>
@endif
@endsection
@section('js')
<script type="text/javascript">
</script>
@endsection