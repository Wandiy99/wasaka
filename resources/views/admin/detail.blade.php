@extends('layoutadmin')
@section('css')
<style type="text/css">
  .input-photos img {
        width: 100px;
        height: 150px;
        margin-bottom: 5px;
    }
</style>
@endsection
@section('title', 'Detail')
@section('content')
<?php
    $context = stream_context_create( [
        'ssl' => [
            'verify_peer' => false,
            'verify_peer_name' => false,
        ],
    ]);
    $path="/upload/work/".$data->id;
    $path2="https://marina.bjm.tomman.app/backupwasaka/work/".$data->id;
    $path3="https://marina.bjm.tomman.app/backupwasaka2/work/".$data->id;
    $pin="/upload/pin/".$data->id;
    $pin2="https://marina.bjm.tomman.app/backupwasaka/pin/".$data->id;
    $pin3="https://marina.bjm.tomman.app/backupwasaka2/pin/".$data->id;
    $pinacak="/upload/pin/acak/".$data->id;
    $pinacak2="https://marina.bjm.tomman.app/backupwasaka/pin/acak/".$data->id;
    $pinacak3="https://marina.bjm.tomman.app/backupwasaka2/pin/acak/".$data->id;
    if(file_exists(public_path().$path."/before.jpg")){
        $path=$path;
    }else if(stripos(get_headers($path2."/before.jpg", 0, $context)[0],"200 OK")){
        $path = $path2;
    }else if(stripos(get_headers($path3."/before.jpg", 0, $context)[0],"200 OK")){
        $path = $path3;
    }else{
        $path = '/image/placeholder.gif';
    }
    if(file_exists(public_path().$pin.".jpg")){
        $pin=$pin;
    }else if(stripos(get_headers($pin2.".jpg", 0, $context)[0],"200 OK")){
        $pin = $pin2;
    }else if(stripos(get_headers($pin3.".jpg", 0, $context)[0],"200 OK")){
        $pin = $pin3;
    }else{
        $pin = '/image/placeholder.gif';
    }
    if(file_exists(public_path().$pinacak.".jpg")){
        $pinacak=$pinacak;
    }else if(stripos(get_headers($pinacak2.".jpg", 0, $context)[0],"200 OK")){
        $pinacak = $pinacak2;
    }else if(stripos(get_headers($pinacak3.".jpg", 0, $context)[0],"200 OK")){
        $pinacak = $pinacak3;
    }else{
        $pinacak = '/image/placeholder.gif';
    }
    // $pin = str_replace('backupwasaka/work', 'backupwasaka2/pin', $path);
    // dd($pin);

    $timezone = '';
      if($data->timezone){
        $timezone = $data->timezone;
      }
?>
<div class="box">
    <span class="box-cell col-xs-5 p-a-3 valign-top">
        <h4 class="m-y-1 font-weight-normal"><i class="ion ion-document-text"></i>&nbsp;&nbsp;DETAIL LOG</h4>
      <ul class="list-group m-x-0 m-t-3 m-b-0">
        <li class="list-group-item b-x-0 b-b-0 b-t-0">
          ID Wasaka
          <span class="label pull-right">{{ $data->id }}</span>
        </li>
        <li class="list-group-item b-x-0 b-b-0 b-t-0">
          User
          <span class="label pull-right">[{{ $data->id_user }}] {{ $data->nama_user }}</span>
        </li>
        <li class="list-group-item b-x-0 b-b-0">
          Kode Perangkat
          <span class="label pull-right">{{ $data->kode }}</span>
        </li>
        <li class="list-group-item b-x-0 b-b-0">
          Jenis Pekerjaan
          <span class="label pull-right">{{ $data->jenis_pekerjaan }}</span>
        </li>
        <li class="list-group-item b-x-0 b-b-0">
          Jam Open
          <span class="label pull-right">{{ date('d/m/Y H:i', strtotime($data->date_start.$timezone)) }}</span>
        </li>
        <li class="list-group-item b-x-0 b-b-0">
          Jam Close
          <span class="label pull-right">{{ date('d/m/Y H:i', strtotime($data->date_close.$timezone)) }}</span>
        </li>
        <li class="list-group-item b-x-0 b-b-0">
          Durasi
          <span class="label pull-right">{{ $data->durasi }}</span>
        </li>
        <li class="list-group-item b-x-0 b-b-0">
          Uraian
          <span class="label pull-right">{{ $data->uraian }}</span>
        </li>
        <li class="list-group-item b-x-0 b-b-0">
          Anggota
          <span class="label pull-right">{{ $data->anggota }}</span>
        </li>
      </ul>
      <a href="#" onclick="history.back()"><i class="ion ion-arrow-left-a"></i></a>
    </span>

  <div class="box-cell">
    <div class="box-container">
      <div class="box-row">
        <span href="#" class="box-cell p-a-1 valign-middle text-xs-center">
            <a href="{{ $path }}/before.jpg">
                <img src="{{ $path }}/before-th.jpg" alt="">
            </a>
            <p>Depan Alpro</p>
        </span>
      </div>
      <div class="box-row">
        <span href="#" class="box-cell p-a-1 valign-middle text-xs-center">
            <a href="{{ $path }}/work-progress.jpg">
                <img src="{{ $path }}/work-progress-th.jpg" alt="">
            </a>
            <p>Proses Pengerjaan</p>
        </span>
      </div>
    </div> <!-- / .box-container -->
  </div> <!-- / .box-cell -->
  <div class="box-cell">
    <div class="box-container">
      <div class="box-row">
        <span href="#" class="box-cell p-a-1 valign-middle text-xs-center">
            <a href="{{ $path }}/item-before.jpg">
                <img src="{{ $path }}/item-before-th.jpg" alt="">
            </a>
            <p>Sebelum Pekerjaan</p>
        </span>
      </div>
      <div class="box-row">
        <span href="#" class="box-cell p-a-1 valign-middle text-xs-center">
            <a href="{{ $path }}/item-after.jpg">
                <img src="{{ $path }}/item-after-th.jpg" alt="">
            </a>
            <p>Setelah Pekerjaan</p>
        </span>
      </div>
    </div> <!-- / .box-container -->
  </div> <!-- / .box-cell -->
  <div class="box-cell">
    <div class="box-container">
      <div class="box-row">
        <span href="#" class="box-cell p-a-1 valign-middle text-xs-center input-photos">
            <a href="{{ $pin }}.jpg">
                <img src="{{ $pin }}-th.jpg" alt="">
            </a>
            <p>PIN</p>
        </span>
      </div>
      <div class="box-row">
        <span href="#" class="box-cell p-a-1 valign-middle text-xs-center input-photos">
            <a href="{{ $pinacak }}.jpg">
                <img src="{{ $pinacak }}-th.jpg" alt="">
            </a>
            <p>PIN Acak</p>
        </span>
      </div>
    </div> <!-- / .box-container -->
  </div> <!-- / .box-cell -->
</div>
@endsection
@section('js')
<script type="text/javascript">
</script>
@endsection