<?php
  $timezone = '';
  if($data->timezone){
    $timezone = $data->timezone;
  }
?>
<table>
  <tbody>
    <tr>
      <td>[{{ $data->id_user }}] {{ $data->nama_user }}</td>
      <td>{{ $data->kode }}</td>
      <td>{{ $data->jenis_pekerjaan }}</td>
      <td>{{ date('d/m/Y H:i', strtotime($data->date_start.$timezone)) }}</td>
      <td>{{ date('d/m/Y H:i', strtotime($data->date_close.$timezone)) }}</td>
      <td>{{ $data->durasi }}</td>
      <td>{{ $data->uraian }}</td>
      <td>{{ $data->anggota }}</td>
    </tr>
  </tbody>
</table>