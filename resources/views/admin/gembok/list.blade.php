@extends('layoutadmin')
@section('css')
<style type="text/css">

</style>
@endsection
@section('title', 'Kunci')
@section('content')
<div class="row">
  <div class="col-md-7">
    <a href="/admin/gembok/new" class="btn btn-info btn-outline btn-rounded"><i class="ion ion-plus"></i>&nbsp;&nbsp;Create Kunci Baru</a>
    <a href="/downloadGembok/{{ Request::segment(3) }}/{{ Request::segment(4) }}/{{ Request::segment(5) }}" target="_BLANK" class="btn btn-info btn-rounded"><i class="ion ion-ios-cloud-download"></i>&nbsp;&nbsp;Download</a>
  </div>
  <div class="col-md-2 pull-right">
    <input type="text" name="witel" value="{{ Request::segment(4) }}" class="border-round pull-right" id="witel"/>
  </div>
  <div class="col-md-3 pull-right">
    <input type="text" name="regional" value="{{ Request::segment(3) }}" class="border-round pull-right" id="regional"/>
  </div>
</div>

<div class="panel m-t-2">
  <div class="panel-body">
<div class="table-responsive table-primary m-t-2">
  <table class="table" id="datatables">
    
  </table>
</div>
</div></div>
@endsection
@section('js')
<script type="text/javascript">
  $(function() {
    $('#datatables').dataTable({
      processing: true,
      serverSide: true,
      ajax: "/ajx/gembokdata/{{ Request::segment(3) }}/{{ Request::segment(4) }}/{{ Request::segment(5) }}",
      columns:
      [
         { "data": "kode", "name": "kode", "title": "Kode"},
         { "data": "witel", "name": "witel", "title": "Witel" },
         { "data": "pin", "name": "pin", "title": "PIN" },
         { "data": "jenis_alpro", "name": "jenis_alpro", "title": "Kategori" },
         { "data": "koordinat", "name": "koordinat", "title": "Koordinat" },
         { "data": "kap", "name": "kap", "title": "Kap" },
         { "data": "merk", "name": "merk", "title": "Merk" },
         { "data": "last_akses", "name": "last_akses", "title": "Last Akses" },
         { "data": "akses_count", "name": "akses_count", "title": "Akses Count" },
         { "data": "isHapus", "name": "isHapus", "title": "isHapus" },
         { "data": "scaningadmin", "name": "scaningadmin", "title": "Scaningadmin" },
         { "data": "id", "name": "id", "title": "ACT", "render": render }
      ],
      responsive: true,
      "order": [[ 0, "desc" ]]
    });
    function render(data, type, row, meta){
      if(type === 'display'){
          data = '<a href="/admin/lib/' + row.id + '" class="btn btn-xs btn-success"><i class="ion ion-images"></i>&nbsp;&nbsp;</a><a href="/admin/gembok/' + row.id + '" class="btn btn-xs btn-success m-x-1"><i class="ion ion-compose"></i>&nbsp;&nbsp;</a>';
          if(row.jenis_alpro==='ODC'){
            data += '<a href="/mc/' + row.id + '" class="btn btn-xs btn-success pull-right" ><i class="ion ion-shuffle"></i> Layout</a>'
          }
      }

      return data;
    }
    $('#regional').select2({
      data:$.merge([{'id':'all','text':'Semua Regional'}],<?= json_encode($region); ?>),
      placeholder:'Select Regional',
      containerCssClass :'border-round'

    }).change(function(){
      var segments = $(location).attr('href').split( '/' );
      var owner = segments[7];
      var wtl = segments[6];
      var tgl = segments[4];
      window.location.href = document.location.origin+"/admin/gembok/"+this.value+"/all/"+owner;
    });
    $('#witel').select2({
      data:$.merge([{'id':'all','text':'Semua Witel'}],<?= json_encode($dwtl); ?>),
      placeholder:'Select Witel',
      containerCssClass :'border-round'

    }).change(function(){
      var segments = $(location).attr('href').split( '/' );
      var owner = segments[7];
      var fz = segments[5];
      var tgl = segments[4];
      // alert(tgl)
      window.location.href = document.location.origin+"/admin/gembok/"+fz+"/"+this.value+"/"+owner;
    });
  });
</script>
@endsection