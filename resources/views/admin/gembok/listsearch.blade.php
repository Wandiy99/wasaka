@extends('layoutadmin')
@section('css')
<style type="text/css">

</style>
@endsection
@section('title', 'Search')
@section('content')
<div class="panel m-t-2">
  <div class="panel-body">
<div class="table-responsive table-primary m-t-2">
  <table class="table" id="datatables">
    <thead>
      <tr>
        <th>#</th>
        <th>Kode</th>
        <th>Witel</th>
        <th>PIN</th>
        <th>Kategori</th>
        <th>Koordinat</th>
        <th>Kap</th>
        <th>Merk</th>
        <th>Last Akses</th>
        <th>Akses Count</th>
        <th>Status</th>
        <th>Scanning</th>
        <th>Aksi</th>
      </tr>
    </thead>
    <tbody>
      @foreach($data as $no => $d)
      <tr>
        <td>{{ ++$no }}</td>
        <td>{{ $d->kode }}</td>
        <td>{{ $d->witel }}</td>
        <td>{{ $d->pin }}</td>
        <td>{{ $d->jenis_alpro }}</td>
        <td>{{ substr($d->koordinat,0,20) }}</td>
        <td>{{ $d->kap }}</td>
        <td>{{ $d->merk }}</td>
        <td>{{ $d->last_akses }}</td>
        <td>{{ $d->akses_count }}</td>
        <td>{!! $d->isHapus?'<span class="label label-danger">Deleted</span>':'<span class="label label-success">Aktf</span>' !!}</td>
        <td>{!! $d->scaningadmin?'<span class="label label-success">Done</span>':'<span class="label label-danger">Not Yet</span>' !!}</td>
        <td><a href="/admin/lib/{{ $d->id }}"><i class="ion ion-images"></i>&nbsp;&nbsp;</a><a href="/admin/gembok/{{ $d->id }}"><i class="ion ion-compose pull-right"></i>&nbsp;&nbsp;</a></td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
</div></div>
@endsection
@section('js')
<script type="text/javascript">
  $(function() {
    $('#datatables').dataTable();
  });
</script>
@endsection