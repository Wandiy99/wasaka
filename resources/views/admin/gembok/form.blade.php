@extends('layoutadmin')
@section('css')
<style type="text/css">
  .input-photos img {
        width: 100px;
        height: 150px;
        margin-bottom: 5px;
    }
</style>
@endsection
@section('title', 'Gembok')
@section('content')
<form method="post" id="gembokform" enctype="multipart/form-data">
<div class="panel box">
  <div class="box-row">
    <div class="box-container">
      <div class="box-cell p-a-1">
        <div class="row">
            <div class="col-md-2 form-group form-message-dark">
                <label for="jenis_gembok">Jenis Gembok</label>
                <input type="text" name="gembok" id="jenis_gembok" class="form-control border-round m-b-2" required value="{{ $data->gembok or old('gembok') }}"/>
            </div>
            <div class="col-md-3 form-group form-message-dark">
                <label for="kode">Kode Alpro</label>
                <input type="text" name="kode" id="kode" class="form-control border-round" required autocomplete="off" placeholder="Input Kode" value="{{ $data->kode or old('kode') }}"/>
            </div>
            
            <div class="col-md-3 form-group form-message-dark div_pin">
                <label for="pin">PIN</label>
                <input type="text" name="pin" id="pin" class="form-control border-round" autocomplete="off" placeholder="Input PIN" value="{{ $data->pin or old('pin') }}"/>
            </div>
            <div class="col-md-3 form-group form-message-dark div_pid">
                <label for="produk_id">Produk ID</label>
                <input type="text" name="produk_id" id="produk_id" class="form-control border-round" autocomplete="off" value="{{ $data->produk_id or old('produk_id') }}" placeholder="Enter Produk ID" />
            </div>
            <div class="col-md-2 form-group form-message-dark">
                <label for="kategori">Kategori</label>
                <input type="text" name="kategori" id="kategori" class="form-control border-round" autocomplete="off" required value="{{ $data->kategori or old('kategori') }}"/>
            </div>
            <div class="col-md-2 form-group form-message-dark">
              <label for="merk">Merk</label>
              <input type="text" name="merk" id="merk" class="form-control border-round" autocomplete="off" required value="{{ $data->merk or old('merk') }}"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 form-group form-message-dark">
              <label for="koordinat">Koordinat</label>
              <input type="text" name="koordinat" id="koordinat" class="form-control border-round" autocomplete="off" placeholder="Input Koordinat" required value="{{ $data->koordinat or old('koordinat') }}"/>
            </div>
            <div class="col-md-2 form-group form-message-dark">
              <label for="kap">Kap</label>
              <input type="text" name="kap" id="kap" class="form-control border-round" autocomplete="off" required value="{{ $data->kap or old('kap') }}"/>
            </div>
            <div class="col-md-3 form-group form-message-dark">
              <label for="regional">Regional</label>
              <input type="text" name="regional" id="regional" class="form-control border-round" autocomplete="off" required value="{{ $data->regional or old('regional') }}"/>
            </div>
            <div class="col-md-2 form-group form-message-dark">
              <label for="witel">Witel</label>
              <input type="text" name="witel" id="witel" class="form-control border-round" autocomplete="off" required value="{{ $data->witel or old('witel') }}"/>
            </div>
            <div class="col-md-2 form-group form-message-dark">
              <label for="status">Status</label>
              <input type="text" name="isHapus" id="isHapus" class="form-control border-round m-b-2" autocomplete="off" required value="{{ $data->isHapus or old('isHapus') }}"/>
            </div>
        </div>
      </div>
    </div> <!-- / .box-container -->
  </div>
  <div class="box-row">
    <div class="box-container valign-top">
      <div class="box-cell p-x-1 p-y-1">
        <input type="hidden" name="scaningadmin" value="1"/>
        <label for="engsel" class="switcher switcher-success switcher-lg m-b-2">
          <input type="checkbox" id="engsel" name="engsel_odc" {{ @$data->engsel_odc?'checked':'' }}>
          <div class="switcher-indicator">
            <div class="switcher-yes">Yes</div>
            <div class="switcher-no">No</div>
          </div>
          Engsel Pintu ODC Berfungsi?
        </label>
        <label for="odc_rapi" class="switcher switcher-success switcher-lg m-b-2">
          <input type="checkbox" id="odc_rapi" name="kerapian_odc" {{ @$data->kerapian_odc?'checked':'' }}>
          <div class="switcher-indicator">
            <div class="switcher-yes">Yes</div>
            <div class="switcher-no">No</div>
          </div>
          Isi ODC Rapi?
        </label>

        
      </div>
      <div class="box-cell p-x-1 p-y-1">
        <?php
            $segment = Request::segment(3);
            $url = "https://wasaka.telkomakses.co.id/work/start/".$segment;
            if($segment == 'new'){
              $url = false;
            }
        ?>
        <div class="col-sm-12">
          @if($url)
        <a href="https://api.qrserver.com/v1/create-qr-code/?data={{ $url }}&#38;size=200x200">

            <img src="https://api.qrserver.com/v1/create-qr-code/?data={{ $url }}&#38;size=200x200"/>
        </a>
          @else
            <img src="/image/placeholder.gif"/>
          @endif
      </div>
      </div>
      <div class="box-cell p-x-1 p-y-1 text-xs-center">
        <?php
          $foto = ["Depan_ODC", "Splitter"];
        ?>
        @foreach($foto as $input)
          <div class="col-sm-6 text-xs-center input-photos">
            <?php
              $path = "/upload/".$input."/".@$data->id;
              $th   = "$path-th.jpg";
              $img  = "$path.jpg";
            ?>
            @if (file_exists(public_path().$th))
              <a href="{{ $img }}">
                <img src="{{ $th }}" alt="1" class="photo" />
              </a>
            @else
              <img src="/image/placeholder.gif" alt="" />
            @endif
            <input type="file" class="hidden" name="photo-{{ $input }}" accept="image/jpeg" />
            <p>{{ str_replace('_',' ',$input) }}</p>
            <button type="button" class="btn btn-sm btn-info">
              <i class="ion ion-camera"></i>
            </button>
          </div>
        @endforeach
      </div>
    </div> <!-- / .box-container -->
  </div>
  <button type="submit" class="btn btn-primary form-control border-round m-y-2" id="submit_button"><i class="ion-soup-can"></i> Simpan</button>
</div>
</form>
@if(count($work))
<div class="panel m-t-2">
  <div class="panel-heading">
    <span class="panel-title"><i class="panel-title-icon ion ion-soup-can"></i> LOG {{count($work)}} Rows</span>
  </div>
  <div class="panel-body">
<div class="table-responsive table-primary m-t-2">
  <table class="table" id="datatables">
    <thead>
      <tr>
        <th>#</th>
        <th>Start</th>
        <th>Close</th>
        <th>Durasi</th>
        <th>Kode</th>
        <th>Pekerjaan</th>
        <th>User</th>
        @if (session('auth')->level == '2')
          <th>PIN Baru</th>
          <th>PIN Acak</th>
        @endif
        <th>Aksi</th>
      </tr>
    </thead>
    <tbody>
      @foreach($work as $no => $log)
        <?php
          $dteStart = new \DateTime(date('Y-m-d H:i:s', $log->time_start)); 
          $dteEnd   = new \DateTime(date('Y-m-d H:i:s', $log->time_close)); 
          $dteDiff  = $dteStart->diff($dteEnd); 
          $durasi   = $dteDiff->format("%Hh %Im %Ss");
          $timezone = '';
          if($log->timezone){
            $timezone = $log->timezone;
          }
        ?>
        <tr>
          <td>{{ ++$no }}</td>
          <td>{{ date('d/m/Y H:i', strtotime($log->date_start.$timezone)) }}</td>
          <td>{{ date('d/m/Y H:i', strtotime($log->date_close.$timezone)) }}</td>
          <td>{{ $durasi }}</td>
          <td>
          <a href="/admin/gembok/{{ $log->id_gembok }}">{{ $log->kode }}</a>
          
          </td>
          <td>{{ $log->jenis_pekerjaan_id == 15 ? $log->pekerjaan : $log->jenis_pekerjaan }}</td>
          <td>
              <!-- <a href="/workUser/{{ $log->id_user }}"> -->
              {{ $log->id_user }}
              @if($log->nama)
                  {{ $log->nama }}
              @elseif($log->nama_user)
                  {{ $log->nama_user }}
              @endif
              <!-- </a> -->
          </td>
          @if (session('auth')->level == '2')
            <td>{{ $log->pin_new }}</td>
            <td>{{ $log->pin_acak }}</td>
          @endif
          <td><a href="/admin/detail/{{$log->id}}"><i class="ion ion-information pull-right"></i>&nbsp;&nbsp;</a></td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
</div></div>
@endif
@endsection
@section('js')
<script type="text/javascript">
    $(function() {
      $('.div_pid, .div_pin').hide();
      var merk = <?= json_encode($merk) ?>;
      $('#merk').select2({
          data: merk,
          placeholder:'Pilih Merk',
          containerCssClass :'border-round'
      });
      $('#jenis_gembok').select2({
          data: [{"id":"no-gembok","text":"Tidak Ada Gembok"},{"id":"gembok-universal","text":"Gembok Universal"},{"id":"versi-lama","text":"Gembok Master Key"},{"id":"versi-Baru","text":"Gembok Wasaka"},{"id":"kunci-elektrik","text":"Kunci Elektrik ( Keyless )"}],
          placeholder:'Pilih Gembok',
          containerCssClass :'border-round'
      }).change(function(){
        if($(this).val()=="kunci-elektrik"){
          $('#pin').removeAttr("required");
          $('#produk_id').attr("required","true");
          $('.div_pid').show();
          $('.div_pin').hide();
        }else{
          $('.div_pin').show();
          $('.div_pid').hide();
          $('#pin').attr("required","true");
          $('#produk_id').removeAttr("required");
        }
      }).trigger('change');
      $('#isHapus').select2({
          data: [{"id":0,"text":"Aktif"},{"id":1,"text":"Deleted"}],
          placeholder:'Pilih Status',
          containerCssClass :'border-round'
      });
      $('#kap').select2({
          data: [{"id":8,"text":8},{"id":16,"text":16},{"id":48,"text":48},{"id":144,"text":144},{"id":288,"text":288}],
          placeholder:'Pilih Kapasitas',
          containerCssClass :'border-round'
      });
      var data = <?= json_encode($reg) ?>;
      var regional = $('#regional').select2({
          data: data,
          placeholder:'Pilih Regional',
          containerCssClass :'border-round'
      });

      var data = <?= json_encode($wtl) ?>;
      var witel = $('#witel').select2({
          data: $.merge([{'id':'all','text':'Semua Witel'}],data),
          placeholder:'Pilih Witel',
          containerCssClass :'border-round'
      });
      var kat = [{"id":1, "text":"FTM"},{"id":2, "text":"ODC"},{"id":3, "text":"MSAN"},{"id":4, "text":"MDU"},{"id":5, "text":"ONU"},{"id":6, "text":"OLT"},{"id":7, "text":"Custom Akses"},{"id":8, "text":"ODP"},{"id":9, "text":"LEMARI"}];

      $('#kategori').select2({
          data: kat,
          placeholder:'Pilih Kategori',
          containerCssClass :'border-round'
      });
      $("#regional").change(function(){
          $.getJSON("/getWitelByRegional/"+$("#regional").val(), function(data){
              witel.select2({
                data:$.merge([{'id':'all','text':'Semua Witel'}],data),
                placeholder:'Pilih Witel',
                containerCssClass :'border-round'
              });
          });
      });
      $('#gembokform').pxValidate();
      $('.input-photos').on('click', 'button', function() {
        $(this).parent().find('input[type=file]').click();
      });
      $('input[type=file]').change(function() {
        var inputEl = this;
        if (inputEl.files && inputEl.files[0]) {
          $(inputEl).parent().find('input[type=text]').val(1);
          var reader = new FileReader();
          reader.onload = function(e) {
            $(inputEl).parent().find('img').attr('src', e.target.result);

          }
          reader.readAsDataURL(inputEl.files[0]);
        }
      });
  });
</script>
@endsection