@extends('layoutadmin')
@section('css')
<style type="text/css">
</style>
@endsection
@section('title', 'Dashboard')
@section('content')
<?php
  $auth=session('auth');
?>
<div class="row">
  <div class="col-md-7">
    <!-- <button type="submit" class="btn btn-primary border-round"><i class="ion-android-archive"></i> Download</button> -->
  </div>
  <div class="col-md-2 pull-right">
    <input type="text" name="witel" value="{{ Request::segment(3) }}" class="border-round pull-right" id="witel"/>
  </div>
  <div class="col-md-3 pull-right">
    <input type="text" name="fiberzone" value="{{ Request::segment(2) }}" class="border-round pull-right" id="fiberzone"/>
  </div>
</div>

<div class="row m-t-2">
  <!-- Stats -->
  <div class="col-md-2">
    <a href="/admin/user/{{ Request::segment(2) }}/{{ Request::segment(3) }}">
      <div class="box bg-info darken border-round">
        <div class="box-cell p-x-3 p-y-1">
          <div class="font-weight-semibold font-size-12">USERS</div>
          <div class="font-weight-bold font-size-20">{{ $userCount }}</div>
          <i class="box-bg-icon middle right font-size-52 ion-ios-people"></i>
        </div>
      </div>
    </a>
  </div>

  <div class="col-md-2">
    <a href="/admin/gembok/{{ Request::segment(2) }}/{{ Request::segment(3) }}/0">
      <div class="box bg-danger darken border-round">
        <div class="box-cell p-x-3 p-y-1">
          <div class="font-weight-semibold font-size-12">Gembok Registered</div>
          <div class="font-weight-bold font-size-20">{{ $gembokCount }}</div>
          <i class="box-bg-icon middle right font-size-52 ion-ios-locked-outline"></i>
        </div>
      </div>
    </a>
  </div>

  <div class="col-md-2">
    <a href="#" id="ajaxUnakses">
      <div class="box bg-danger darken border-round">
        <div class="box-cell p-x-3 p-y-1">
          <div class="font-weight-semibold font-size-12">Unaccess ODC > 30D</div>
          <div class="font-weight-bold font-size-20">{{ $countnotakseslastmonth[0]->akses }}</div>
          <i class="box-bg-icon middle right font-size-52 ion-ios-locked-outline"></i>
        </div>
      </div>
    </a>
  </div>

  <div class="col-md-2">
    <a href="/admin/gembok/{{ Request::segment(2) }}/{{ Request::segment(3) }}/0">
      <div class="box bg-danger darken border-round">
        <div class="box-cell p-x-3 p-y-1">
          <div class="font-weight-semibold font-size-12">Belum Scanning ODC</div>
          <div class="font-weight-bold font-size-20">{{ $countunscaning[0]->akses }}</div>
          <i class="box-bg-icon middle right font-size-52 ion-ios-locked-outline"></i>
        </div>
      </div>
    </a>
  </div>

  <div class="col-md-2">
    <!-- <a href="/admin/history/{{date('Y-m-d:Y-m-d')}}/{{@$auth->fz}}/{{@$auth->witel}}"> -->
    <a href="#" data-toggle="modal" data-target="#modal-base">
      <div class="box bg-warning darken border-round">
        <div class="box-cell p-x-3 p-y-1">
          <div class="font-weight-semibold font-size-12">GEMBOK OPEN</div>
          <div class="font-weight-bold font-size-20">{{ count($unclose) }}</div>
          <i class="box-bg-icon middle right font-size-52 ion-ios-book-outline"></i>
        </div>
      </div>
    </a>
  </div>

  <div class="col-md-2">
    <a href="/admin/user/{{ Request::segment(2) }}/{{ Request::segment(3) }}">
      <div class="box bg-success darken border-round">
        <div class="box-cell p-x-3 p-y-1">
          <div class="font-weight-semibold font-size-12">NEW USER</div>
          <div class="font-weight-bold font-size-20">{{ $newUser }}</div>
          <i class="box-bg-icon middle right font-size-52 ion-person"></i>
        </div>
      </div>
    </a>
  </div>
  <!-- / Stats -->
</div>
<div class="panel box">
  <div class="box-row">
    <div class="box-container valign-middle text-xs-center">
      <div class="box-cell p-y-1">
        <div class="font-size-17"><strong>Gembok</strong></div>
        <div id="kunciChart" style="height: 200px"></div>
      </div>
      <div class="box-cell p-y-1">
        <div class="font-size-17"><strong>Kerapian Odc</strong></div>
        <div id="odcChart" style="height: 200px"></div>
      </div>
      <div class="box-cell p-y-1">
        <div class="font-size-17"><strong>Engsel Odc</strong></div>
        <div id="engselChart" style="height: 200px"></div>
      </div>
    </div> <!-- / .box-container -->
  </div>
  <div class="box-row">
    <div class="box-container">
      <div class="box-cell p-a-1">
        <div class="font-size-17 text-xs-center"><strong>Akses Log Past 30 Days</strong></div>
        <div id="overview-chart" style="height: 250px"></div>
      </div>
      <div class="box-cell p-a-1 text-xs-center col-md-3">
        <div class="font-size-17"><strong>Users</strong></div>
        <div id="user-chart" style="height: 200px"></div>
      </div>
    </div> <!-- / .box-container -->
  </div>
  <div class="box-row">
    <div class="box-container valign-middle text-xs-center">
      <div class="box-cell p-y-1">
        <div class="font-size-17"><strong>Alpro Merk</strong></div>
        <div id="gembok-chart" style="height: 320px"></div>
      </div>
      <div class="box-cell p-y-1">
        <div class="font-size-17"><strong>Today Work</strong></div>
        <div id="work-chart" style="height: 320px"></div>
      </div>
    </div> <!-- / .box-container -->
  </div>
  <div class="box-row">
    <div class="box-container valign-middle text-xs-center">
      
    </div> <!-- / .box-container -->
  </div>
</div>

<div class="modal fade" id="modal-base" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title" id="myModalLabel">List Gembok Open</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th>#</th>
                <th>Kode</th>
                <th>Witel</th>
                <th>Pekerjaan</th>
                <th>User</th>
                <th>Date Open</th>
                <th>Durasi</th>
              </tr>
            </thead>
            <tbody>
              @foreach($unclose as $no => $uc)
              <?php
                $dteStart = new \DateTime(date('Y-m-d H:i:s', $uc->time_start)); 
                $dteEnd   = new \DateTime(date('Y-m-d H:i:s')); 
                $dteDiff  = $dteStart->diff($dteEnd); 
                $durasi   = $dteDiff->format("%ad %Hh %Im %Ss");
              ?>
              <tr>
                <th scope="row">{{ ++$no }}</th>
                <td>{{ $uc->kode }}</td>
                <td>{{ $uc->wtl }}</td>
                <td>{{ $uc->jenis_pekerjaan }}</td>
                <td>{{ $uc->nama_user }}</td>
                <td>{{ $uc->date_start }}</td>
                <td>{{ $durasi }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_gembok" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title" id="modal_tittle">Tittle</h4>
      </div>
      <div class="modal-body" id="modal_body">
        
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
    $(function() {
        var segments = $(location).attr('href').split( '/' );
        var fz = segments[4];
        var wtl = segments[5];
        var owner = segments[6];
        $('#fiberzone').select2({
            data:<?= json_encode($dfz); ?>,
            placeholder:'Select Fiberzone',
            containerCssClass :'border-round'

        }).change(function(){
            window.location.href = document.location.origin+"/home/"+this.value+"/all/"+owner;
        });
        $('#witel').select2({
            data:$.merge([{'id':'all','text':'Semua Witel'}],<?= json_encode($dwtl); ?>),
            placeholder:'Select Witel',
            containerCssClass :'border-round'
        }).change(function(){
            window.location.href = document.location.origin+"/home/"+fz+"/"+this.value+"/"+owner;
        });
        $('#ajaxUnakses').click(function(){
          $.get('/ajaxUnakses/'+fz+'/'+wtl+'/'+owner, function(data){
            $('#modal_body').html(data);
            $('#modal_tittle').text('List Unaccess > 30 Day');
            $('#modal_gembok').modal('show');
          });
        });
        
        new Morris.Area({
          element:        'overview-chart',
          data:           <?= json_encode($aksesLog); ?>,
          xkey:           'ItemDate',
          ykeys:          ['baris'],
          hideHover:      'auto',
          fillOpacity:    0.1,
          pointSize:      0,
          grid:false,
        });

        var data = {
          columns: <?= json_encode($userChart); ?>,
          type : 'pie',
        };

        c3.generate({
          bindto: '#user-chart',
          data:   data,
          legend: { position: 'right' },
          pie: {
            label: {
              format: function(value, ratio, id) {
                return value;
              }
            }
          }
        });

        c3.generate({
          bindto: '#kunciChart',
          data:   {
            columns: <?= json_encode($kunciChart); ?>,
            type : 'pie',
          },
          legend: { position: 'right' },
          pie: {
            label: {
              format: function(value, ratio, id) {
                return value;
              }
            }
          }
        });
        c3.generate({
          bindto: '#engselChart',
          data:   {
            columns: <?= json_encode($engselChart); ?>,
            type : 'pie',
          },
          legend: { position: 'right' },
          pie: {
            label: {
              format: function(value, ratio, id) {
                return value;
              }
            }
          }
        });
        c3.generate({
          bindto: '#odcChart',
          data:   {
            columns: <?= json_encode($odcChart); ?>,
            type : 'pie',
          },
          legend: { position: 'right' },
          pie: {
            label: {
              format: function(value, ratio, id) {
                return value;
              }
            }
          }
        });

        var data = {
          columns: <?= json_encode($gembokChart); ?>,
          type : 'pie',
        };

        c3.generate({
          bindto: '#gembok-chart',
          data:   data,
          legend: { position: 'right' },
          pie: {
            label: {
              format: function(value, ratio, id) {
                return value;
              }
            }
          }, tooltip: {
                 format: {
                     title: function (d) {
                         return d;
                     }
                 }
             }
        });

        var data = {
          columns: <?= json_encode($workChart); ?>,
          type : 'pie',
        };

        c3.generate({
          bindto: '#work-chart',
          data:   data,
          legend: { position: 'right' },
          pie: {
            label: {
              format: function(value, ratio, id) {
                return value;
              }
            }
          }
        });
    });
</script>
@endsection