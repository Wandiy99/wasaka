@extends('layoutadmin')
@section('css')
<style type="text/css">

</style>
@endsection
@section('title', 'History')
@section('content')
<div class="row">
  <div class="col-md-4">
    <a href="/download/history/{{ Request::segment(3) }}/{{ Request::segment(4) }}/{{ Request::segment(5) }}/{{ Request::segment(6) }}"><button type="button" class="btn btn-primary border-round"><i class="ion-android-archive"></i> Download</button></a>
  </div>
  <div class="col-md-3">
    <button id="daterange-4" type="button" class="btn dropdown-toggle border-round pull-right"></button>
  </div>
  <div class="col-md-2 pull-right">
    <input type="text" name="witel" value="{{ Request::segment(5) }}" class="border-round pull-right" id="witel"/>
  </div>
  <div class="col-md-3 pull-right">
    <input type="text" name="regional" value="{{ Request::segment(4) }}" class="border-round pull-right" id="regional"/>
  </div>
</div>
<div class="panel m-t-2">
  <div class="panel-body">
<div class="table-responsive table-primary m-t-2">
  <table class="table" id="datatables">
    <thead>
      <tr>
        <th>#</th>
        <th>Start</th>
        <th>Close</th>
        <th>Durasi</th>
        <th>Kode</th>
        <th>Pekerjaan</th>
        <th>User</th>
        @if (session('auth')->level == '2')
          <th>PIN Baru</th>
          <th>PIN Acak</th>
        @endif
        <th>Aksi</th>
      </tr>
    </thead>
    <tbody>
      @foreach($data as $no => $log)
        <?php
          $dteStart = new \DateTime(date('Y-m-d H:i:s', $log->time_start)); 
          $dteEnd   = new \DateTime(date('Y-m-d H:i:s', $log->time_close)); 
          $dteDiff  = $dteStart->diff($dteEnd); 
          $durasi   = $dteDiff->format("%Hh %Im %Ss");
          // dd(date('d/m/Y H:i', strtotime($log->date_start.$log->timezone)));
          $timezone = '';
          if($log->timezone){
            $timezone = $log->timezone;
          }
        ?>
        <tr>
          <td>{{ ++$no }}</td>
          <td>{{ date('d/m/Y H:i', strtotime($log->date_start.$timezone)) }}</td>
          <td>{{ date('d/m/Y H:i', strtotime($log->date_close.$timezone)) }}</td>
          <td>{{ $durasi }}</td>
          <td>
          <a href="/admin/gembok/{{ $log->id_gembok }}">{{ $log->kode }}</a>
          
          </td>
          <td>{{ $log->jenis_pekerjaan_id == 15 ? $log->pekerjaan : $log->jenis_pekerjaan }}</td>
          <td>
              <!-- <a href="/workUser/{{ $log->id_user }}"> -->
              {{ $log->id_user }}
              @if($log->nama)
                  {{ $log->nama }}
              @elseif($log->nama_user)
                  {{ $log->nama_user }}
              @endif
              <!-- </a> -->
          </td>
          @if (session('auth')->level == '2')
            <td>{{ $log->pin_new }}</td>
            <td>{{ $log->pin_acak }}</td>
          @endif
          <td><a href="/admin/detail/{{$log->id}}"><i class="ion ion-information pull-right"></i>&nbsp;&nbsp;</a></td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
</div></div>
@endsection
@section('js')
<script type="text/javascript">
  $(function() {
    var start = moment($('#start').val());
    var end = moment($('#end').val());

    function cb(start, end) {
      $('#daterange-4').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#daterange-4').daterangepicker({
      startDate: start,
      endDate: end,
      ranges: {
       'Hari Ini': [moment(), moment()],
       'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
       '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
       '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
       'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
       'Bulan Sebelumnya': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
    }, cb);

    cb(start, end);
    $('#daterange-4').on('apply.daterangepicker', function(ev, picker) {
        var s = picker.startDate.format('YYYY-MM-DD');
        var e = picker.endDate.format('YYYY-MM-DD');
        var segments = $(location).attr('href').split( '/' );
        var owner = segments[8];
        var wtl = segments[7];
        var fz = segments[6];
        window.location.href = document.location.origin+"/admin/history/"+s+":"+e+"/"+fz+"/"+wtl+"/"+owner;
    });
    $('#datatables').dataTable();
    $('#regional').select2({
      data:$.merge([{'id':'all','text':'Semua Regional'}],<?= json_encode($region); ?>),
      placeholder:'Select Regional',
      containerCssClass :'border-round'

    }).change(function(){
      var segments = $(location).attr('href').split( '/' );
      var owner = segments[8];
      var wtl = segments[7];
      var tgl = segments[5];
      window.location.href = document.location.origin+"/admin/history/"+tgl+"/"+this.value+"/all/"+owner;
    });
    $('#witel').select2({
      data:$.merge([{'id':'all','text':'Semua Witel'}],<?= json_encode($dwtl); ?>),
      placeholder:'Select Witel',
      containerCssClass :'border-round'

    }).change(function(){
      var segments = $(location).attr('href').split( '/' );
      var owner = segments[8];
      var fz = segments[6];
      var tgl = segments[5];
      window.location.href = document.location.origin+"/admin/history/"+tgl+"/"+fz+"/"+this.value+"/"+owner;
    });
  });
</script>
@endsection