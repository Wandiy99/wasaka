@extends('layoutadmin')
@section('css')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<style>
    html, body {
        height: 100%;
        margin: 0;
    }
    .leaflet-container {
        height: 500px;
        /* width: 1000px; */
        max-width: 100%;
        max-height: 100%;
    }
    #map-container {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
    }
    #map {
        margin-top: 50px;
        height: 92%;
        width: 100%;
    }
    .custom-popup {
    background-color: #fff;
    border: 1px solid #ccc;
    padding: 10px;
    border-radius: 5px;
  }
  tr, th, td {
        text-align: center;
    }
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
      padding: 2px 1px;
    }
</style>
@endsection
@section('title', 'Live Gembok')
@section('content')
    <div id="map-container">
        <div id="map"></div>
    </div>
@endsection
@section('js')
<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="/leaflet.movingRotatedMarker.js"></script>
<script type="text/javascript">
    var datenow = {!! json_encode($date_now) !!};

    var map = L.map('map').setView([-3.0926, 115.2838], 8);

    var streetLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      attribution: 'OpenStreetMap'
    });

    var satelliteLayer = L.tileLayer('https://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}', {
      maxZoom: 20,
      subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
      attribution: 'Google Satellite'
    });

    var hybridLayer = L.layerGroup([streetLayer, satelliteLayer]);

    var baseLayers = {
      "Jalan": streetLayer,
      "Satelit": satelliteLayer,
      "Jalan & Satelit": hybridLayer
    };

    L.control.layers(baseLayers).addTo(map);

    streetLayer.addTo(map);

    var marker = [];
    var gembok = {!! json_encode($gembok) !!};
    var markers = [];
    gembok.forEach(function(point) {
      var gps = JSON.parse(point.last_fmb_data)[0];
      var crash_detection = gps.element_247?"<span class='badge badge-danger'>Detected</span>":"<span class='badge badge-success'>Not Detected</span>";
      var ignition = gps.element_239?"<span class='badge badge-danger'>Detected</span>":"<span class='badge badge-success'>Not Detected</span>";
      var movement = gps.element_240?"<span class='badge badge-danger'>Detected</span>":"<span class='badge badge-success'>Not Detected</span>";
      var idling = gps.element_251?"<span class='badge badge-danger'>Detected</span>":"<span class='badge badge-success'>Not Detected</span>";
      var unplug = gps.element_252?"<span class='badge badge-danger'>Detected</span>":"<span class='badge badge-success'>Not Detected</span>";
      var over_speeding = gps.element_255?"<span class='badge badge-danger'>Detected</span>":"<span class='badge badge-success'>Not Detected</span>";
      var ts = JSON.parse(point.last_fmb_data).datetime;
      var popupContent = `
          <div class="custom-popup table-responsive">
              <table class="table table-sm">
                  <tbody>
                      <tr>
                          <td style="font-size: 10px">Kode</td>
                          <td style="font-size: 10px">:</td>
                          <td style="font-size: 10px">${point.driver}</td>
                      </tr>
                      <tr>
                          <td style="font-size: 10px">Status</td>
                          <td style="font-size: 10px">:</td>
                          <td style="font-size: 10px">${point.vehicle_number} (${point.vehicle_regnumber})</td>
                      </tr>
                  </tbody>
              </table>
          </div>
      `;
      var alpro = "";
      if (point.kategori == 2)
      {
        alpro = "odc";
      }
      else if (point.kategori == 8)
      {
        alpro = "odp";
      }
      var icon_truck = L.icon({
          iconUrl: '/images/point/'+alpro+'_black.png',
          iconSize: [32, 32],
          iconAnchor: [16, 32]
      });
      if (gps.longitude)
      {
          markers[point.vehicle_regnumber] = L.marker([gps.latitude, gps.longitude], { icon: icon_truck,
              rotationAngle: gps.angle }).addTo(map).bindPopup(popupContent);
          markers[point.vehicle_regnumber].addEventListener("click", function (e){
              map.setView(this.getLatLng(), 17);
          });
      }
    });

    const eventSource = new EventSource('/stream');

    eventSource.onmessage = function(event) {
        const data = JSON.parse(event.data);
        data.forEach(function(currentData){
            if(currentData.last_fmb_data){
                var gps = JSON.parse(currentData.last_fmb_data)[0];
                markers[currentData.vehicle_regnumber].slideTo([gps.latitude, gps.longitude], {
                    duration: 3000,
                    rotationAngle: gps.angle
                });
                var gps_fuel_consumption = Math.round(currentData.gps_fuel_consumption * 100) / 100;

                var crash_detection = gps.element_247?"<span class='badge badge-danger'>Detected</span>":"<span class='badge badge-success'>Not Detected</span>";
                var ignition = gps.element_239?"<span class='badge badge-danger'>Detected</span>":"<span class='badge badge-success'>Not Detected</span>";
                var movement = gps.element_240?"<span class='badge badge-danger'>Detected</span>":"<span class='badge badge-success'>Not Detected</span>";
                var idling = gps.element_251?"<span class='badge badge-danger'>Detected</span>":"<span class='badge badge-success'>Not Detected</span>";
                var unplug = gps.element_252?"<span class='badge badge-danger'>Detected</span>":"<span class='badge badge-success'>Not Detected</span>";
                var over_speeding = gps.element_255?"<span class='badge badge-danger'>Detected</span>":"<span class='badge badge-success'>Not Detected</span>";
                var ts = gps.datetime;
                var popupContent = `
                    <div class="custom-popup table-responsive">
                        <table class="table table-sm">
                            <tbody>
                                <tr>
                                    <td style="font-size: 10px">Driver</td>
                                    <td style="font-size: 10px">:</td>
                                    <td style="font-size: 10px">${currentData.driver}</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 10px">Vehicle Number</td>
                                    <td style="font-size: 10px">:</td>
                                    <td style="font-size: 10px">${currentData.vehicle_number} (${currentData.vehicle_regnumber})</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 10px">Vehicle Type</td>
                                    <td style="font-size: 10px">:</td>
                                    <td style="font-size: 10px">${currentData.vehicle_type}</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 10px">Status</td>
                                    <td style="font-size: 10px">:</td>
                                    <td style="font-size: 10px"><span class="badge badge-success">${currentData.status}</span></td>
                                </tr>
                                <tr>
                                    <td style="font-size: 10px">Sub Status</td>
                                    <td style="font-size: 10px">:</td>
                                    <td style="font-size: 10px"><span class="badge badge-danger">${currentData.sub_status}</span></td>
                                </tr>
                                <tr>
                                    <td style="font-size: 10px">Timestamp</td>
                                    <td style="font-size: 10px">:</td>
                                    <td style="font-size: 10px">${ts}</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 10px">Fuel Level Realtime</td>
                                    <td style="font-size: 10px">:</td>
                                    <td style="font-size: 10px">${gps.element_9} mv</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 10px">External Voltage</td>
                                    <td style="font-size: 10px">:</td>
                                    <td style="font-size: 10px">${currentData.external_voltage}</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 10px">Ignition</td>
                                    <td style="font-size: 10px">:</td>
                                    <td style="font-size: 10px">${ignition}</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 10px">Movement</td>
                                    <td style="font-size: 10px">:</td>
                                    <td style="font-size: 10px">${movement}</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 10px">Idling</td>
                                    <td style="font-size: 10px">:</td>
                                    <td style="font-size: 10px">${idling}</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 10px">Unplug</td>
                                    <td style="font-size: 10px">:</td>
                                    <td style="font-size: 10px">${unplug}</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 10px">Over Speeding</td>
                                    <td style="font-size: 10px">:</td>
                                    <td style="font-size: 10px">${over_speeding}</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 10px">Crash Detection</td>
                                    <td style="font-size: 10px">:</td>
                                    <td style="font-size: 10px">${crash_detection}</td>
                                </tr>

                            </tbody>
                        </table>
                        <a href="/dashboard/vehicles-detil/${currentData.vehicle_regnumber}/${datenow}" class="btn btn-sm btn-block btn-primary" type="button"><i class="bi bi-cursor-fill fs-2"></i> Detail</a>
                    </div>
                    `;
                markers[currentData.vehicle_regnumber].getPopup().setContent(popupContent);
                markers[currentData.vehicle_regnumber].getPopup().update();
            }
        });
    }
</script>
@endsection