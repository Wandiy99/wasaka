@extends('layoutadmin')
@section('css')
<style type="text/css">

</style>
@endsection
@section('title', 'Theme')
@section('content')
<?php
    $theme = array('asphalt','purple-hills','adminflare','dust','frost','fresh','silver','clean','white','candy-black','candy-blue','candy-red','candy-orange','candy-green','candy-purple','candy-cyan','mint-dark','dark-blue','dark-red','dark-orange','dark-green','dark-purple','dark-cyan','darklight-blue','darklight-red','darklight-orange','darklight-green','darklight-purple','darklight-cyan');
  ?>
  <form class="form-horizontal" method="post" id="form-theme" action="/admin/theme">
    <div class="px-demo-themes-list clearfix bg-primary">
      @foreach($theme as $t)
      <?php
        $img = '/themes/'.$t.'.png';
      ?>
      <label class="px-demo-themes-item">
        <input type="radio" class="px-demo-themes-toggler itemtheme" name="theme" value="{{ $t }}">
        <img src="{{ $img }}" class="px-demo-themes-thumbnail">
        <div class="px-demo-themes-title font-weight-semibold">
          <span class="text-white">{{$t}}</span>
          <div class="bg-primary">
          </div>
        </div>
      </label>
      @endforeach
    </div>
  </form>
@endsection
@section('js')
<script type="text/javascript">
  $('.itemtheme').change(function(){

    $('#form-theme').submit();
  });
</script>
@endsection