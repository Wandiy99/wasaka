<div class="table-responsive">
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>Kode</th>
        <th>Witel</th>
        <th>Engsel ODC</th>
        <th>Kerapian ODC</th>
        <th>Count Akses</th>
        <th>Last Akses</th>
        <th>Act</th>
      </tr>
    </thead>
    <tbody>
      @foreach($data as $no => $d)
      <tr>
        <th scope="row">{{ ++$no }}</th>
        <td>{{ $d->kode }}</td>
        <td>{{ $d->witel }}</td>
        <td>{{ $d->engsel_odc }}</td>
        <td>{{ $d->kerapian_odc }}</td>
        <td>{{ $d->akses_count }}</td>
        <td>{{ $d->last_akses }}</td>
        <td><a target="_BLANK" href="/admin/gembok/{{ $d->id }}"><i class="ion ion-compose pull-right"></i></a></td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>