@extends('layoutadmin')
@section('css')
<style type="text/css">
  .input-photos img {
    width: 100px;
    height: 150px;
    margin-bottom: 5px;
  }
  .no-search .select2-search {
    display:none
  }
</style>
@endsection
@section('title', 'Kabinet SO')
@section('content')
<div class="row">
  <div class="col-md-8">
    <div class="panel">
      <div class="panel-heading">
        <span class="panel-title">List Kabinet</span>
        <div class="panel-heading-controls">
          <a href="/admin/lemari/new" class="btn btn-info btn-outline btn-rounded btn-xs"><i class="ion ion-plus"></i>&nbsp;&nbsp;Create Kabinet Baru</a>
        </div>
      </div>
      <div class="panel-body">
        <div class="table-responsive table-primary">
          <table class="table table-hover" id="datatables">
            <thead>
              <tr>
                <th>#</th>
                <th>SO</th>
                <th>Regional</th>
                <th>Witel</th>
                <th>Loker Count</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($data as $no => $d)
              <tr>
                <td>{{ ++$no }}</td>
                <td>{{ $d->so }}</td>
                <td>{{ $d->regional }}</td>
                <td>{{ $d->witel }}</td>
                <td>{{ $d->loker_count }}</td>
                <td><a href="/admin/lemari/{{ $d->id }}"><i class="ion ion-compose pull-right"></i>&nbsp;&nbsp;</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="panel">
      <div class="panel-heading text-center">
        <span class="panel-title">List Approval</span>
      </div>
      <div class="panel-body">
        <div class="table-responsive table-primary">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>#</th>
                <th>Loker</th>
                <th>User</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($approval as $no => $a)
                <tr>
                  <th scope="row">{{ ++$no }}</th>
                  <td>{{ $a->kode }} ({{ $a->produk_id }})</td>
                  <td>{{ $a->id_user }} ({{ $a->nama_user }})</td>
                  <td>
                    @if($a->step==1)
                      <button class="btn btn-xs btn-success btn-outline border-round" data-toggle="modal" data-target="#modal_approval_pinjam" data-work_id="{{ $a->id }}">Approval Pinjam</button>
                    @else
                      <button class="btn btn-xs btn-info btn-outline border-round" data-toggle="modal" data-target="#modal_approval_return" data-work_id="{{ $a->id }}">Approval Return</button>
                    @endif
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_approval_pinjam" tabindex="-1">
  <div class="modal-dialog modal-l">
    <div class="modal-content">
      <form method="post" action="/loker/approvePinjam">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Form Approve Pinjam</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="work_id" id="work_id_pinjam" value=""/>
        <div class="content_pinjam">
          
        </div>
        <div class="form-group form-message-dark m-t-2">
          <label for="status1">Status</label>
          <input type="text" name="status" id="status1" class="form-control border-round status" autocomplete="off" required value="{{ $data->status or old('status') }}"/>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary form-control border-round m-b-1"><i class="ion-soup-can"></i> Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_approval_return" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form method="post" action="/loker/approveReturn">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Form Approve Return</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="work_id" id="work_id_return" value=""/>
        <div class="content_return"></div>
        <div class="form-group form-message-dark">
          <label for="status">Status</label>
          <input type="text" name="status" id="status" class="form-control border-round status" autocomplete="off" required value="{{ $data->status or old('status') }}"/>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary form-control border-round m-b-1"><i class="ion-soup-can"></i> Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
  $(function() {
    $('#status1').select2({
      data: [{"id":2,"text":"Reject"},{"id":3,"text":"Approve"}],
      placeholder:'Pilih Status',
      containerCssClass :'border-round',
      dropdownCssClass : 'no-search',
      dropdownParent: $("#modal_approval_pinjam .modal-content")
    });
    $('#status').select2({
      data: [{"id":5,"text":"Reject"},{"id":6,"text":"Approve"}], 
      placeholder:'Pilih Status',
      containerCssClass :'border-round',
      dropdownCssClass : 'no-search',
      dropdownParent: $("#modal_approval_return .modal-content")
    });
    $('#modal_approval_pinjam').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget);
      var work_id = button.data('work_id');
      $('#work_id_pinjam').val(work_id);
      $('.content_pinjam').html();
      $.get('/admin/loker/ajax/'+work_id, function(data){
        $('.content_pinjam').html(data);
      });
    });
    $('#modal_approval_return').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget);
      var work_id = button.data('work_id');
      $('#work_id_return').val(work_id);
      $('.content_return').html();
      $.get('/admin/loker/ajax/'+work_id, function(data){
        $('.content_return').html(data);
      });
    });
  });
</script>
@endsection