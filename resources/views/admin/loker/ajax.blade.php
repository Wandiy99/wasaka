<style type="text/css">
  .input-photos img {
        width: 100px;
        height: 150px;
        margin-bottom: 5px;
    }
</style>
<?php
  $context = stream_context_create( [
      'ssl' => [
          'verify_peer' => false,
          'verify_peer_name' => false,
      ],
  ]);
  $path="/upload/loker/".$data->id;
  $path2="https://marina.bjm.tomman.app/backupwasaka/loker/".$data->id;
  $path3="https://marina.bjm.tomman.app/backupwasaka2/loker/".$data->id;
  $nf=0;
  if(file_exists(public_path().$path."/depan_so.jpg")){
      $path=$path;
  }else if(stripos(get_headers($path2."/depan_so.jpg", 0, $context)[0],"200 OK")){
      $path = $path2;
  }else if(stripos(get_headers($path3."/depan_so.jpg", 0, $context)[0],"200 OK")){
      $path = $path3;
  }else{
      $path = '/image/placeholder.gif';
      $nf=1;
  }
?>
@if($data->step==1)
<div class="row">
  <div class="col-md-8">
    <span class="text-muted">Loker</span><br/>
    <span class="font-size-16">{{ $data->kode }} ({{ $data->so }})</span><br/>
    <span class="text-muted">User</span><br/>
    <span class="font-size-16"><span class="text-danger">{{ $data->id_user }}</span> / {{ $data->nama_user }}</span><br/>
    <span class="text-muted">Splicer</span><br/>
    <span class="font-size-16">{{ $data->merk }} ({{ $data->sn_splicer }})</span><br/>
    <span class="text-muted">Keperluan</span><br/>
    <span class="font-size-16">{{ $data->pekerjaan }}</span><br/>
    <span class="text-muted">Keyless</span><br/>
    <span class="font-size-16"><span class="text-success">Online</span> ({{ $data->produk_id }})</span><br/>
  </div>
  <div class="col-md-4 text-xs-center">
    <div class="text-muted">Items</div>
    @foreach($items as $i)
      <div class="col-md-12"><span class="font-size-16">{{ ucwords(str_replace('_',' ',$i->item)) }}</span></div>
    @endforeach
    <div href="#" class="valign-middle">
      @if($nf)
        <a href="{{ $path }}" target="_BLANK">
            <img src="{{ $path }}" alt="" />
        </a>
      @else
        <a href="{{ $path }}/depan_so.jpg" target="_BLANK">
            <img src="{{ $path }}/depan_so-th.jpg" alt="" />
        </a>
      @endif
        <p>Depan SO</p>
    </div>
  </div>
</div>
@else
<div class="box-row">
  <div class="box-cell">
    <div class="box-container">
      <div class="box-cell p-x-0">
        <span class="text-muted">Loker</span><br/>
        <span class="font-size-16">{{ $data->kode }} ({{ $data->so }})</span><br/>
        <span class="text-muted">User</span><br/>
        <span class="font-size-16"><span class="text-info">{{ $data->id_user }}</span> / {{ $data->nama_user }}</span><br/>
        <span class="text-muted">Splicer</span><br/>
        <span class="font-size-16">{{ $data->merk }} ({{ $data->sn_splicer }})</span><br/>
        <span class="text-muted">Keperluan</span><br/>
        <span class="font-size-16">{{ $data->pekerjaan }}</span><br/>
        <span class="text-muted">Keyless</span><br/>
        <span class="font-size-16">
          @if($data->last_online_menit<5)
            <span class="text-success">Online</span>
          @else
            <span class="text-danger">Offline</span>
          @endif
          ({{ $data->produk_id }})<br/>{{ $data->last_online }}</span><br/>
        <span class="text-muted">Arc</span><br/>
        <span class="font-size-16">
          <!-- <span class="text-info">Sebelum</span>  -->
        ({{ $data->arc_count }})</span><br/>
        <span class="text-muted">Catatan</span><br/>
        <span class="font-size-16">{{ $data->catatan }}</span><br/>
      </div>
      <div class="box-cell p-x-0 text-center">
        <span class="text-muted">Items</span><br>
        @foreach($items as $i)
          <span class="font-size-16">{{ ucwords(str_replace('_',' ',$i->item)) }}
            @if($i->feedback == 'good')
              <span class="text-success">({{ $i->feedback }})</span>
            @else
              <span class="text-danger">({{ $i->feedback }})</span>
            @endif
          </span><br/>
        @endforeach

        <div href="#" class="valign-middle">
          @if($nf)
            <a href="{{ $path }}">
                <img src="{{ $path }}" alt="" target="_BLANK" />
            </a>
          @else
            <a href="{{ $path }}/depan_so.jpg">
                <img src="{{ $path }}/depan_so-th.jpg" alt="" target="_BLANK" />
            </a>
          @endif
            <p>Depan SO</p>
        </div>
      </div>
      <div class="box-cell p-x-0 text-center">
        <span class="text-muted">Evidence</span><br>
        <div href="#" class="valign-middle">
          @if($nf)
            <a href="{{ $path }}">
                <img src="{{ $path }}" alt="" />
            </a>
          @else
            <a href="{{ $path }}/evidence_arc.jpg">
                <img src="{{ $path }}/evidence_arc-th.jpg" alt="" target="_BLANK" />
            </a>
          @endif
            <p>Arc</p>
        </div>
        <div href="#" class="valign-middle">
          @if($nf)
            <a href="{{ $path }}">
                <img src="{{ $path }}" alt="" />
            </a>
          @else
            <a href="{{ $path }}/evidence_penyerahan.jpg">
                <img src="{{ $path }}/evidence_penyerahan-th.jpg" alt="" target="_BLANK" />
            </a>
          @endif
            <p>Penyerahan</p>
        </div>
      </div>
    </div>
  </div>
</div>
@endif