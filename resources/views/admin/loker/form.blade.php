@extends('layoutadmin')
@section('css')
<style type="text/css">
  .input-photos img {
    width: 100px;
    height: 150px;
    margin-bottom: 5px;
  }
  .no-search .select2-search {
    display:none
  }
</style>
@endsection
@section('title', 'Kabinet')
@section('content')
<?php
  $isAdmin = (session('auth')->level==2 || session('auth')->level==3)?1:0;
?>
<div class="panel" id="div_form_lemari">
  <div class="panel-body">
  <form method="post">
    <div class="row">
      <div class="col-md-3 form-group form-message-dark">
        <label for="so">Site Operation</label>
        <input type="text" name="so" id="so" class="form-control border-round" autocomplete="off" placeholder="Input SO" value="{{ $data->so or old('so') }}" required />
      </div>
      <div class="col-md-3 form-group form-message-dark">
        <label for="regional">Regional</label>
        <input type="text" name="regional" id="regional" class="form-control border-round regional" autocomplete="off" required value="{{ $data->regional or old('regional') }}"/>
      </div>
      <div class="col-md-3 form-group form-message-dark">
        <label for="witel">Witel</label>
        <input type="text" name="witel" id="witel" class="form-control border-round witel" autocomplete="off" required value="{{ $data->witel or old('witel') }}"/>
      </div>
      <div class="col-md-3 form-group form-message-dark">
        <label for="loker_count">Loker Count</label>
        <input type="text" name="loker_count" id="loker_count" class="form-control border-round" autocomplete="off" required placeholder="Isi Jumlah Loker" value="{{ $data->loker_count or old('loker_count') }}" required/>
      </div>
    </div>
    <div class=" form-group form-message-dark">
        <button type="submit" class="btn btn-primary form-control border-round m-t-1"><i class="ion-soup-can"></i> Simpan Kabinet</button>
      
    </div>
  </form>
  </div>
</div>
@if(Request::segment(3)!='new')
<div class="panel">
  <div class="panel-heading">
    <span class="panel-title">List Loker</span>
    <div class="panel-heading-controls">
      <button class="btn btn-xs btn-success btn-outline border-round" data-toggle="modal" data-target="#modal_loker" data-gembok="new"><i class="ion ion-plus"></i> Tambah Loker</button>
    </div>
  </div>
  <div class="panel-body">
    <div id="ajaxcontent" class="row">
      @foreach($list as $k => $l)
        <div class="col-sm-2">
          <div class="panel box">
            <div class="box-row">
              <div class="box-cell p-x-2 p-y-1 font-size-11 font-weight-semibold">
                <i class="ion ion-social-windows"></i>&nbsp;&nbsp;{{$l->kode}} / {{$k+1}}
                <button class="btn btn-xs btn-info pull-right btn-outline border-round" data-toggle="modal" data-target="#modal_loker" data-gembok="{{ $k }}">
                  <i class="ion ion-android-create"></i> Edit
                </button>
              </div>
            </div>
            <div class="box-row">
              <div class="box-cell p-y-2 text-xs-center">
                <?php
                  $segment = $l->gembok_id;
                  $url = "https://wasaka.telkomakses.co.id/mobile/work/start/".$segment;
                  if($segment == 'new'){
                    $url = false;
                  }
                ?>
                @if($url)
                  <a href="https://api.qrserver.com/v1/create-qr-code/?data={{ $url }}&#38;size=150x150">

                    <img src="https://api.qrserver.com/v1/create-qr-code/?data={{ $url }}&#38;size=150x150"/>
                  </a>
                @else
                  <img src="/image/placeholder.gif"/>
                @endif
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
</div>
@endif
<div class="modal fade" id="modal_loker" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form method="post" action="/admin/lemari/{{ Request::segment(3) }}/loker">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Form add loker</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="gembok" id="jenis_gembok" class="form-control border-round m-b-2" value="kunci-elektrik"/>
        <input type="hidden" name="pin" id="pin" class="form-control border-round" autocomplete="off" placeholder="Input PIN" value="0000"/>
        <input type="hidden" name="kategori" id="kategori" class="form-control border-round" autocomplete="off" value="9"/>
        <input type="hidden" name="witel" id="witels" class="form-control border-round" autocomplete="off" required value="{{ $data->witel or '' }}"/>
        <input type="hidden" name="regional" id="regionals" class="form-control border-round" autocomplete="off" required value="{{ $data->regional or '' }}"/>
        <input type="hidden" name="gembok_id" value=""/>
        <div class="row">
          <div class="col-md-3 m-b-5">
            <label for="kode">Items</label>
            @foreach($kelengkapan as $k)
              <label class="custom-control custom-checkbox has-success m-b-1" for="{{ $k }}">
                <input type="checkbox" name="items[{{ $k }}]" id="{{ $k }}" class="custom-control-input items">
                <span class="custom-control-indicator"></span>
                {{ ucwords(str_replace('_',' ',$k)) }}
              </label>
            @endforeach
          </div>
          <div class="col-md-9">
            <div class="row">
              <div class="form-group form-message-dark col-md-6">
                <label for="kode">Kode Loker</label>
                <input type="text" name="kode" id="kode" class="form-control border-round" required autocomplete="off" placeholder="Input Kode" value="{{ $data->kode or old('kode') }}"/>
              </div>
              <div class="form-group form-message-dark col-md-6">
                <label for="sn_splicer">SN Splicer</label>
                <input type="text" name="sn_splicer" id="sn_splicer" class="form-control border-round" required autocomplete="off" placeholder="Input SN Splicer" value="{{ $data->sn_splicer or old('sn_splicer') }}"/>
              </div>
            </div>
            <div class="form-group form-message-dark div_pid">
              <label for="produk_id">Produk ID (SN Keyless)</label>
              <input type="text" name="produk_id" id="produk_id" class="form-control border-round" autocomplete="off" value="{{ $data->produk_id or old('produk_id') }}" placeholder="Enter Produk ID" />
            </div>
            <div class="form-group form-message-dark">
              <label for="status">Status</label>
              <input type="text" name="isHapus" id="isHapus" class="form-control border-round" autocomplete="off" required value="{{ $data->isHapus or old('isHapus') }}"/>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary form-control border-round m-b-1"><i class="ion-soup-can"></i> Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
  $(function() {
    $('#div_form_loker').hide();
    $('#suspend_user').select2({
      data: [{"id":"Current User", "text":"Current User"},{"id":"Past User", "text":"Past User"}],
      placeholder:'Pilih Suspend User',
      containerCssClass :'border-round',
      dropdownCssClass : 'no-search'
    });
    $('#status').select2({
      data: [{"id":1, "text":"Open"},{"id":2, "text":"Closed"},{"id":3, "text":"Deleted"}],
      placeholder:'Pilih Status',
      containerCssClass :'border-round',
      dropdownCssClass : 'no-search'
    });
    $('#isHapus').select2({
      data: [{"id":0,"text":"Aktif"},{"id":1,"text":"Deleted"}],
      placeholder:'Pilih Status',
      containerCssClass :'border-round',
      dropdownCssClass : 'no-search',
      dropdownParent: $("#modal_loker .modal-content")
    });

    var data = <?= json_encode($reg) ?>;
    var regional = $('.regional').select2({
      data: data,
      placeholder:'Pilih Regional',
      containerCssClass :'border-round',
      dropdownCssClass : 'no-search'
    });

    var data = <?= json_encode($wtl) ?>;
    var witel = $('.witel').select2({
      data: $.merge([{'id':'all','text':'Semua Witel'}],data),
      placeholder:'Pilih Witel',
      containerCssClass :'border-round'
    });
    $("#regional").change(function(){
      $.getJSON("/getWitelByRegional/"+$("#regional").val(), function(data){
        witel.select2({
          data:$.merge([{'id':'all','text':'Semua Witel'}],data),
          placeholder:'Pilih Witel',
          containerCssClass :'border-round'
        });
      });
    });
    var listkabinet = <?= json_encode($list) ?>;
    $('#modal_loker').on('show.bs.modal', function (event) {
      $('input[name=kode]').val('');
      $('input[name=produk_id]').val('');
      $('input[name=sn_splicer]').val('');
      $('input[name=isHapus]').val('');
      $('.items').prop('checked', false);
      var button = $(event.relatedTarget);
      var gembok_id = button.data('gembok');
      if(gembok_id=='new'){
        $('.items').prop('checked', true);
        $('input[name=gembok_id]').val(gembok_id);
      }else{
        $('input[name=gembok_id]').val(listkabinet[gembok_id].gembok_id);
        $.getJSON('/admin/loker/items/'+listkabinet[gembok_id].gembok_id, function(data){
          $.each( data, function( key, val ) {
            $('#'+val.item).prop('checked', true);
          });
        });
        $('input[name=kode]').val(listkabinet[gembok_id].kode);
        $('input[name=sn_splicer]').val(listkabinet[gembok_id].sn_splicer);
        $('input[name=produk_id]').val(listkabinet[gembok_id].produk_id);
        $('input[name=isHapus]').val(listkabinet[gembok_id].isHapus).trigger("change");
      }
    });
    $('#userform').pxValidate();
  });
</script>
@endsection