@extends('layoutadmin')
@section('css')
<style type="text/css">

</style>
@endsection
@section('title', 'Tiket')
@section('content')
<div class="row">
  <div class="col-md-7">
    <a href="/admin/tiket/new" class="btn btn-info btn-outline btn-rounded"><i class="ion ion-plus"></i>&nbsp;&nbsp;Create Tiket Baru</a>
    <a href="/downloadTiket/{{ Request::segment(3) }}/{{ Request::segment(4) }}" target="_BLANK" class="btn btn-info btn-rounded"><i class="ion ion-ios-cloud-download"></i>&nbsp;&nbsp;Download</a>
  </div>
  <div class="col-md-2 pull-right">
    <input type="text" name="witel" value="{{ Request::segment(4) }}" class="border-round pull-right" id="witel"/>
  </div>
  <div class="col-md-3 pull-right">
    <input type="text" name="regional" value="{{ Request::segment(3) }}" class="border-round pull-right" id="regional"/>
  </div>
</div>
<div class="panel m-t-2">
  <div class="panel-body">
    <div class="table-responsive table-primary m-t-2">
      <table class="table" id="datatables">
        <thead>
          <tr>
            <th>#</th>
            <th>ID Wasaka</th>
            <th>Issue</th>
            <th>Suspend User</th>
            <th>User</th>
            <th>Status</th>
            <th>Created By</th>
            <th>Created At</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          @foreach($tiket as $no => $d)
          <tr>
            <td>{{ ++$no }}</td>
            <td>{{ $d->work_id }}</td>
            <td>{{ $d->issue }}</td>
            <td>{{ $d->suspend_user }}</td>
            <td>{{ $d->user_id }}</td>
            <td>{{ $d->status }}</td>
            <td>{{ $d->created_by }}</td>
            <td>{{ $d->created_at }}</td>
            <td><a href="/admin/tiket/{{ $d->id_tiket }}"><i class="ion ion-compose pull-right"></i>&nbsp;&nbsp;</a></td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>

  </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
  $(function() {
    $('.table').dataTable();
    $('#regional').select2({
      data:$.merge([{'id':'all','text':'Semua Regional'}],<?= json_encode($region); ?>),
      placeholder:'Select Regional',
      containerCssClass :'border-round'

    }).change(function(){
      var segments = $(location).attr('href').split( '/' );
      var wtl = segments[6];
      var tgl = segments[4];
      window.location.href = document.location.origin+"/admin/listTiket/"+this.value+"/all";
    });
    $('#witel').select2({
      data:$.merge([{'id':'all','text':'Semua Witel'}],<?= json_encode($dwtl); ?>),
      placeholder:'Select Witel',
      containerCssClass :'border-round'

    }).change(function(){
      var segments = $(location).attr('href').split( '/' );
      var fz = segments[5];
      var tgl = segments[4];
      // alert(tgl)
      window.location.href = document.location.origin+"/admin/listTiket/"+fz+"/"+this.value;
    });
  });
</script>
@endsection