@extends('layoutadmin')
@section('css')
<style type="text/css">

</style>
@endsection
@section('title', 'User')
@section('content')
<?php
  $jenis_alpro = [1=>'FTM','ODC','MSAN','MDU','ONU','OLT','Custom Akses','ODP'];
  $level = [0=>'View',1=>'User',2=>'Admin Witel',3=>'Admin Regional',4=>'Superadmin',99=>'Mitra'];
?>
<div class="row">
  <div class="col-md-7">
    <a href="/admin/user/new" class="btn btn-info btn-outline btn-rounded"><i class="ion ion-plus"></i>&nbsp;&nbsp;Create User Baru</a>
    <a href="/downloadUser/{{ Request::segment(3) }}/{{ Request::segment(4) }}" target="_BLANK" class="btn btn-info btn-rounded"><i class="ion ion-ios-cloud-download"></i>&nbsp;&nbsp;Download</a>
  </div>
  <div class="col-md-2 pull-right">
    <input type="text" name="witel" value="{{ Request::segment(4) }}" class="border-round pull-right" id="witel"/>
  </div>
  <div class="col-md-3 pull-right">
    <input type="text" name="regional" value="{{ Request::segment(3) }}" class="border-round pull-right" id="regional"/>
  </div>
</div>

<div class="panel m-t-2">
  <div class="panel-body">
    <ul class="nav nav-tabs nav-xs" id="tab-resize-xs">
      @foreach($user as $no => $u)
        <li class="{{ $no?'':'active' }}"><a href="#{{ $u->id }}" data-toggle="tab">{{ $u->nama }}<span class="label label-pill label-danger pull-right">{{ count($u->data) }}</span></a></li>
      @endforeach
    </ul>
    <div class="tab-content">
      @foreach($user as $no => $u)
        <div class="tab-pane {{ $no?'':'active' }}" id="{{ $u->id }}">
          <div class="table-responsive table-primary m-t-2">
            <table class="table" id="datatables">
              <thead>
                <tr>
                  <th>#</th>
                  <th>ID</th>
                  <th>Nama</th>
                  <th>Instansi</th>
                  <th>Type</th>
                  <th>Level</th>
                  <th>Akses</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                @foreach($u->data as $no => $d)
                <tr>
                  <td>{{ ++$no }}</td>
                  <td>{{ $d->id_user }}</td>
                  <td>{{ $d->nama }}</td>
                  <td>{{ $d->instansi }}</td>
                  <td>{!! $d->login_type==1?'<span class="label label-info"/>Wasaka</span>':'<span class="label label-success"/>SSO</span>' !!}</td>

                  <td>{{ @$level[$d->level] }}</td>
                  <td>
                    @foreach(explode(',',$d->akses_perangkat) as $ap)
                    @if($ap)
                      <span class="label label-secondary m-b-1">{{ $jenis_alpro[$ap] }}</span>
                    @endif
                    @endforeach
                    </td>
                  <td>{!! $d->status?'<span class="label label-danger"/>Non-Aktif</span>':'<span class="label label-primary">Aktif</span>' !!}</td>
                  <td><a href="/admin/user/{{ $d->id_user }}"><i class="ion ion-compose pull-right"></i>&nbsp;&nbsp;</a></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      @endforeach
    </div>

</div></div>
@endsection
@section('js')
<script type="text/javascript">
  $(function() {
    $('#tab-resize-xs').pxTabResize();
    $('.table').dataTable();
    $('#regional').select2({
      data:$.merge([{'id':'all','text':'Semua Regional'}],<?= json_encode($region); ?>),
      placeholder:'Select Regional',
      containerCssClass :'border-round'

    }).change(function(){
      var segments = $(location).attr('href').split( '/' );
      var wtl = segments[6];
      var tgl = segments[4];
      window.location.href = document.location.origin+"/admin/user/"+this.value+"/all";
    });
    $('#witel').select2({
      data:$.merge([{'id':'all','text':'Semua Witel'}],<?= json_encode($dwtl); ?>),
      placeholder:'Select Witel',
      containerCssClass :'border-round'

    }).change(function(){
      var segments = $(location).attr('href').split( '/' );
      var fz = segments[5];
      var tgl = segments[4];
      // alert(tgl)
      window.location.href = document.location.origin+"/admin/user/"+fz+"/"+this.value;
    });
  });
</script>
@endsection