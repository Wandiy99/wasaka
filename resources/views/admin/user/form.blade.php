@extends('layoutadmin')
@section('css')
<style type="text/css">

</style>
@endsection
@section('title', 'User')
@section('content')
<?php
  $isAdmin = (session('auth')->level==2 || session('auth')->level==3)?1:0;
  // dd($isAdmin,session('auth'));
?>
<div class="panel">
  <div class="panel-body">
  <form method="post" id="userform" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-4 form-group form-message-dark">
            <label for="id_user">ID User</label>
            <input type="text" name="id_user" id="id_user" class="form-control border-round" autocomplete="off" placeholder="Input ID User" value="{{ $data->id_user or old('id_user') }}" {{ $isAdmin?'required':'readonly' }}/>
        </div>
        <div class="col-md-4 form-group form-message-dark">
            <label for="no_hp">No. HP</label>
            <input type="text" name="no_hp" id="no_hp" class="form-control border-round" autocomplete="off" required placeholder="Input No Handphone" value="{{ $data->no_hp or old('no_hp') }}"/>
        </div>
        <div class="col-md-4 form-group form-message-dark">
            <label for="nama">Nama</label>
            <input type="text" name="nama" id="nama" class="form-control border-round" autocomplete="off" required placeholder="Input Nama" value="{{ $data->nama or old('nama') }}"/>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 form-group form-message-dark">
          <label for="level">Level</label>
          @if($isAdmin)
          <input type="text" name="level" id="level" value="{{ $data->level or old('level') }}" class="form-control border-round" autocomplete="off" required/>
          @else
          <input type="text" value="{{ $data->level or old('level') }}" class="form-control border-round" readonly/>
          @endif
        </div>
        <div class="col-md-4 form-group form-message-dark">
          <label for="status">Status</label>
          <input type="text" name="status" id="{{ $isAdmin?'status':'statusUser' }}" value="{{ $data->status or old('status') }}" class="form-control border-round" autocomplete="off" {{ $isAdmin?'required':'readonly' }}/>
        </div>
        <div class="col-md-4 form-group form-message-dark">
          <label for="password">Password</label>
          @if(@$data->login_type==2)
            <input class="form-control border-round" autocomplete="off" placeholder="Password SSO" disabled="true" />
          @else
            <input type="password" name="password" id="password" class="form-control border-round" autocomplete="off" {{ @$data->password?"":"required" }} placeholder="Input Password" />
          @endif
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 form-group form-message-dark">
        <label for="regional">Regional</label>
            <input type="text" name="regional" id="regional" value="{{ $data->regional or old('regional') }}" class="form-control border-round" autocomplete="off" required />
        </div>
        <div class="col-md-6 form-group form-message-dark">
        <label for="witel">Witel</label>
            <input type="text" name="wtl" id="witel" value="{{ $data->wtl or old('wtl') }}" class="form-control border-round" autocomplete="off" required />
        </div>
    </div>
    <div class="row">
      <div class="col-md-6 form-group form-message-dark">
        <label for="instansi">Instansi</label>
        <div class="input-group">
          <input type="text" name="instansi" id="instansi" value="{{ $data->instansi or old('instansi') }}" class="form-control border-round" required/>
          <div class="input-group-btn">
            <button type="button" class="btn btn-success border-round" data-toggle="modal" data-target="#modal-instansi"><i class="ion-plus"></i> Register Instansi</button>
          </div>
        </div>
      </div>
      <div class="col-md-4 form-group form-message-dark">
        <label for="akses">Akses</label>
        <input type="text" name="akses_perangkat" id="{{ $isAdmin?'akses':'AksesUser' }}" value="{{ $data->akses_perangkat or old('akses_perangkat') }}" class="form-control border-round" {{ $isAdmin?'required':'readonly' }} />
      </div>
      <div class="col-md-2 form-group form-message-dark">
        <label for="a">&nbsp;</label>
        <button type="submit" class="btn btn-primary form-control border-round"><i class="ion-soup-can"></i> Simpan</button>
      </div>
    </div>
  </form>
</div>
</div>
@if(count($work))
<div class="panel m-t-2">
  <div class="panel-heading">
    <span class="panel-title"><i class="panel-title-icon ion ion-soup-can"></i> LOG</span>
  </div>
  <div class="panel-body">
<div class="table-responsive table-primary m-t-2">
  <table class="table" id="datatables">
    <thead>
      <tr>
        <th>#</th>
        <th>Start</th>
        <th>Close</th>
        <th>Durasi</th>
        <th>Kode</th>
        <th>Pekerjaan</th>
        <th>User</th>
        @if (session('auth')->level == '2')
          <th>PIN Baru</th>
          <th>PIN Acak</th>
        @endif
        <th>Aksi</th>
      </tr>
    </thead>
    <tbody>
      @foreach($work as $no => $log)
        <?php
          $dteStart = new \DateTime(date('Y-m-d H:i:s', $log->time_start)); 
          $dteEnd   = new \DateTime(date('Y-m-d H:i:s', $log->time_close)); 
          $dteDiff  = $dteStart->diff($dteEnd); 
          $durasi   = $dteDiff->format("%Hh %Im %Ss");
          $timezone = '';
          if($log->timezone){
            $timezone = $log->timezone;
          }
        ?>
        <tr>
          <td>{{ ++$no }}</td>
          <td>{{ date('d/m/Y H:i', strtotime($log->date_start.$timezone)) }}</td>
          <td>{{ date('d/m/Y H:i', strtotime($log->date_close.$timezone)) }}</td>
          <td>{{ $durasi }}</td>
          <td>
          <a href="/admin/gembok/{{ $log->id_gembok }}">{{ $log->kode }}</a>
          
          </td>
          <td>{{ $log->jenis_pekerjaan_id == 15 ? $log->pekerjaan : $log->jenis_pekerjaan }}</td>
          <td>
              <!-- <a href="/workUser/{{ $log->id_user }}"> -->
              {{ $log->id_user }}
              @if($log->nama)
                  {{ $log->nama }}
              @elseif($log->nama_user)
                  {{ $log->nama_user }}
              @endif
              <!-- </a> -->
          </td>
          @if (session('auth')->level == '2')
            <td>{{ $log->pin_new }}</td>
            <td>{{ $log->pin_acak }}</td>
          @endif
          <td><a href="/admin/detail/{{$log->id}}"><i class="ion ion-information pull-right"></i>&nbsp;&nbsp;</a></td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
</div></div>
@endif
<div id="modal-instansi" class="modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="formInstansi">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Form Register Instansi</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-10 form-group form-message-dark">
            <label for="instansi">Nama Instansi</label>
            <input type="text" name="instansi" placeholder="Input Nama Instansi" id="instansicreate" class="form-control border-round" autocomplete="off" required/>
          </div>
          <div class="col-md-2 form-group form-message-dark">
            <label for="a">&nbsp;</label>
            <button type="submit" id="saveInstansi" class="btn btn-primary form-control border-round"><i class="ion-soup-can"></i> Simpan</button>
          </div>
        </div>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
  $(function() {
    var data = <?= json_encode($reg) ?>;
    var fiberzone = $('#regional').select2({
      data: data,
      allowClear:true,
      placeholder:'Pilih Regional',
      containerCssClass :'border-round'
    });
    var data = <?= json_encode($wtl) ?>;
    var witel = $('#witel').select2({
      data: $.merge([{'id':'all','text':'Semua Witel'}],data),
      allowClear:true,
      placeholder:'Pilih Witel',
      containerCssClass :'border-round'
    });
    var data = [{"id":0,"text":"VIEW"},{"id":1,"text":"USER"},{"id":2,"text":"ADMIN WITEL"},{"id":3,"text":"ADMIN REGIONAL"},{"id":99,"text":"MITRA"}];
    var level = $('#level').select2({
      data: data,
      placeholder:'Pilih Level',
      containerCssClass :'border-round'
    });
    var kat = [{"id":1, "text":"FTM"},{"id":2, "text":"ODC"},{"id":3, "text":"MSAN"},{"id":4, "text":"MDU"},{"id":5, "text":"ONU"},{"id":6, "text":"OLT"},{"id":7, "text":"Custom Akses"},{"id":8, "text":"ODP"}];

    var akses = $('#akses').select2({
      data: kat,
      multiple:true,
      placeholder:'Pilih Alpro',
      containerCssClass :'border-round'
    });
    $('#status').select2({
      data: [{"id":0, "text":"Aktif"},{"id":1, "text":"Non-Aktif"},{"id":2, "text":"Delete"}],
      placeholder:'Pilih Status',
      containerCssClass :'border-round'
    });
    var instansi = $('#instansi').select2({
      data: <?= json_encode($instansi) ?>,
      placeholder:'Pilih Instansi',
      containerCssClass :'border-round'
    });
    $("#regional").change(function(){
      $.getJSON("/getWitelByRegional/"+$("#regional").val(), function(data){
          witel.select2({
            data:$.merge([{'id':'all','text':'Semua Witel'}],data),
            placeholder:'Pilih Witel',
            allowClear:true,
            containerCssClass :'border-round'
          });
      });
    });
    $('#userform').pxValidate();
    $("#formInstansi").submit(function(e){
      e.preventDefault();
      var formdata = new FormData(this);
      $.ajax({
        url: "/admin/insertInstansiNew",
        type: "POST",
        data: formdata,
        contentType: false,
        cache: false,
        processData: false,
        success: function(e){
          $.getJSON("/admin/getInstansi", function(data){
            instansi.select2({
              data: data,
              placeholder:'Pilih Instansi',
              containerCssClass :'border-round'
            });
          });
          $('#instansi').val(e).change();
          $('#modal-instansi').modal('toggle');
        },error: function(){
          alert("okey");
        }
      });
    });
  });
</script>
@endsection