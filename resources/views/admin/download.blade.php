@extends('layoutadmin')
@section('heading')
<h1><span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Form Download</span></h1>
@endsection
@section('css')
<style type="text/css">

</style>
@endsection
@section('title', 'Download')
@section('content')
<div class="panel">
  <div class="panel-body">
  <form method="post" id="userform" enctype="multipart/form-data">
    <input type="hidden" name="start" id="start" value="{{ date('Y-m-d') }}">
    <input type="hidden" name="end" id="end" value="{{ date('Y-m-d') }}">
    <div class="row">
      <div class="btn-group">
          <button type="button" id="daterange-4" class="btn dropdown-toggle"></button>
          <button type="submit" class="btn btn-primary"><i class="ion-android-archive"></i> Download</button>
      </div>
    </div>
  </form>
</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
    $(function() {
      var start = moment($('#start').val());
      var end = moment($('#end').val());

      function cb(start, end) {
        $('#daterange-4').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }

      $('#daterange-4').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
         'Hari Ini': [moment(), moment()],
         'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
         '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
         '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
         'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
         'Bulan Sebelumnya': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
      }, cb);

      cb(start, end);
    });
</script>
@endsection