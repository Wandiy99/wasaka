<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link rel="apple-touch-icon" sizes="180x180" href="/app/icons/icon-192x192.png">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
  
  <title>Login Page</title>


  <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">

<!-- Core stylesheets -->
  <link href="/pixeladmin/css/bootstrap-dark.min.css" rel="stylesheet" type="text/css">
  <link href="/pixeladmin/css/pixeladmin-dark.min.css" rel="stylesheet" type="text/css">
  <link href="/pixeladmin/css/widgets-dark.min.css" rel="stylesheet" type="text/css">

  <!-- Theme -->
  <link href="/pixeladmin/css/themes/mint-dark.min.css" rel="stylesheet" type="text/css">

  <!-- Pace.js -->
  <script src="/pixeladmin/pace/pace.min.js"></script>

  <!-- Custom styling -->
  <style>
    .page-signin-modal {
      position: relative;
      top: auto;
      right: auto;
      bottom: auto;
      left: auto;
      z-index: 1;
      display: block;
    }

    .page-signin-form-group { position: relative; }

    .page-signin-icon {
      position: absolute;
      line-height: 21px;
      width: 36px;
      border-color: rgba(0, 0, 0, .14);
      border-right-width: 1px;
      border-right-style: solid;
      left: 1px;
      top: 9px;
      text-align: center;
      font-size: 15px;
    }

    html[dir="rtl"] .page-signin-icon {
      border-right: 0;
      border-left-width: 1px;
      border-left-style: solid;
      left: auto;
      right: 1px;
    }

    html:not([dir="rtl"]) .page-signin-icon + .page-signin-form-control { padding-left: 50px; }
    html[dir="rtl"] .page-signin-icon + .page-signin-form-control { padding-right: 50px; }

    #page-signin-forgot-form {
      display: none;
    }

    /* Margins */

    .page-signin-modal > .modal-dialog { margin: 30px 10px; }

    @media (min-width: 544px) {
      .page-signin-modal > .modal-dialog { margin: 60px auto; }
    }
  </style>
  <!-- / Custom styling -->
</head>
<body>
  
<div class="page-signin-modal modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="box m-a-0">
          <div class="box-row">

            <div class="box-cell col-md-5 bg-primary p-a-4">
              <div class="text-xs-center text-md-left">
                <a class="px-demo-brand px-demo-brand-lg" href="index.html"><span class="px-demo-logo bg-primary m-t-0"><span class="px-demo-logo-1"></span><span class="px-demo-logo-2"></span><span class="px-demo-logo-3"></span><span class="px-demo-logo-4"></span><span class="px-demo-logo-5"></span><span class="px-demo-logo-6"></span><span class="px-demo-logo-7"></span><span class="px-demo-logo-8"></span><span class="px-demo-logo-9"></span></span><span class="font-size-20 line-height-1">WASAKA</span></a>
                <div class="font-size-15 m-t-1 line-height-1">WASAKA</div>
              </div>
              <ul class="list-group m-t-3 m-b-0 visible-md visible-lg visible-xl">
                <!--
                <li class="list-group-item p-x-0 p-b-0 b-a-0"><i class="list-group-icon fa fa-sitemap text-white"></i> Flexible modular structure</li>
                <li class="list-group-item p-x-0 p-b-0 b-a-0"><i class="list-group-icon fa fa-file-text-o text-white"></i> SCSS source files</li>
                <li class="list-group-item p-x-0 p-b-0 b-a-0"><i class="list-group-icon fa fa-outdent text-white"></i> RTL direction support</li>
              -->
                <li class="list-group-item p-x-0 p-b-0 b-a-0"><i class="list-group-icon fa fa-heart text-white"></i> WAdah SAgan Kunci Akses</li>
              </ul>
            </div>

            <div class="box-cell col-md-7">

              <!-- Sign In form -->

              <form method="post" class="p-a-4" id="form-login">
                <div id="block-alert-with-timer" class="m-b-1"></div>
                <h4 class="m-t-0 m-b-4 text-xs-center font-weight-semibold">LOG IN</h4>

                <fieldset class="page-signin-form-group form-group form-group-lg form-message-dark">
                  <div class="page-signin-icon text-muted"><i class="ion-person"></i></div>
                  <input type="text" class="page-signin-form-control form-control" placeholder="username" name="login" required>
                </fieldset>

                <fieldset class="page-signin-form-group form-group form-group-lg form-message-dark">
                  <div class="page-signin-icon text-muted"><i class="ion-asterisk"></i></div>
                  <input type="password" class="page-signin-form-control form-control" placeholder="password" name="password" required>
                </fieldset>

                <div class="clearfix">
                  <!-- <label class="custom-control custom-checkbox pull-xs-left">
                    <input type="checkbox" class="custom-control-input">
                    <span class="custom-control-indicator"></span>
                    Remember me
                  </label>
                  <a href="#" class="font-size-12 text-muted pull-xs-right" id="page-signin-forgot-link">Forgot your password?</a>
 -->                </div>

                <button type="submit" class="btn btn-block btn-lg btn-primary m-t-3">Log In!</button>
              </form>


              <!-- / Sign In form -->

              <!-- Reset form -->

              <!-- <form action="index.html" class="p-a-4" id="page-signin-forgot-form">
                <h4 class="m-t-0 m-b-4 text-xs-center font-weight-semibold">Password reset</h4>

                <fieldset class="page-signin-form-group form-group form-group-lg">
                  <div class="page-signin-icon text-muted"><i class="ion-at"></i></div>
                  <input type="email" class="page-signin-form-control form-control" placeholder="Your Email">
                </fieldset>

                <button type="submit" class="btn btn-block btn-lg btn-primary m-t-3">Send password reset link</button>
                <div class="m-t-2 text-muted">
                  <a href="#" id="page-signin-forgot-back">&larr; Back</a>
                </div>
              </form> -->

              <!-- / Reset form -->

            </div>
          </div>
        </div>
      </div>

      <!--<div class="text-xs-center m-t-2 font-weight-bold font-size-14 text-white" id="px-demo-signup-link">
        Not a member? <a href="/register" class="text-white"><u>Sign Up now</u></a>
      </div>-->
    </div>
  </div>
  <div class="px-responsive-bg"><div class="px-responsive-bg-overlay" style="background: rgb(0, 0, 0); opacity: 0.2;"></div><img alt="" src="/logistik.jpg" style="width: 100%; height: 1067px; top: -137px; left: 0px;"></div>
  <!-- / Sign In form -->

  <!-- ==============================================================================
  |
  |  SCRIPTS
  |
  =============================================================================== -->

  <!-- Load jQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <!-- Core scripts -->
  <script src="/pixeladmin/js/bootstrap.min.js"></script>
  <script src="/pixeladmin/js/pixeladmin.min.js"></script>

  <!-- Your scripts -->
  <script type="text/javascript">
    $(function() {
      $('#form-login').pxValidate();
      $('#responsive-bg-example').pxResponsiveBg();
      var alertBlock = <?= json_encode(Session::has('alertblock')); ?>;
      if(alertBlock){
        var $container = $('#block-alert-with-timer');
        var alrt = <?= json_encode(session('alertblock')); ?>;
        $container.pxBlockAlert(alrt.text, { type: alrt.type, style: 'light', timer: 6 });
      }
    });
  </script>
</body>
</html>
