<?php
  $current = $data['current'];
  $past = $data['past'];
  $context = stream_context_create( [
      'ssl' => [
          'verify_peer' => false,
          'verify_peer_name' => false,
      ],
  ]);
  $c_path="/upload/work/".$current->id;
  $c_path2="https://marina.bjm.tomman.app/backupwasaka/work/".$current->id;
  $c_path3="https://marina.bjm.tomman.app/backupwasaka2/work/".$current->id;
  $c_pin="/upload/pin/".$current->id;
  $c_pin2="https://marina.bjm.tomman.app/backupwasaka/pin/".$current->id;
  $c_pin3="https://marina.bjm.tomman.app/backupwasaka2/pin/".$current->id;
  $c_pinacak="/upload/pin/acak/".$current->id;
  $c_pinacak2="https://marina.bjm.tomman.app/backupwasaka/pin/acak/".$current->id;
  $c_pinacak3="https://marina.bjm.tomman.app/backupwasaka2/pin/acak/".$current->id;
  if(file_exists(public_path().$c_path."/before.jpg")){
      $c_path=$c_path;
  }else if(stripos(get_headers($c_path2."/before.jpg", 0, $context)[0],"200 OK")){
      $c_path = $c_path2;
  }else if(stripos(get_headers($c_path3."/before.jpg", 0, $context)[0],"200 OK")){
      $c_path = $c_path3;
  }else{
      $c_path = '/image/placeholder.gif';
  }
  if(file_exists(public_path().$c_pin.".jpg")){
      $c_pin=$c_pin;
  }else if(stripos(get_headers($c_pin2.".jpg", 0, $context)[0],"200 OK")){
      $c_pin = $c_pin2;
  }else if(stripos(get_headers($c_pin3.".jpg", 0, $context)[0],"200 OK")){
      $c_pin = $c_pin3;
  }else{
      $c_pin = '/image/placeholder.gif';
  }
  if(file_exists(public_path().$c_pinacak.".jpg")){
      $c_pinacak=$c_pinacak;
  }else if(stripos(get_headers($c_pinacak2.".jpg", 0, $context)[0],"200 OK")){
      $c_pinacak = $c_pinacak2;
  }else if(stripos(get_headers($c_pinacak3.".jpg", 0, $context)[0],"200 OK")){
      $c_pinacak = $c_pinacak3;
  }else{
      $c_pinacak = '/image/placeholder.gif';
  }

  $p_path="/upload/work/".$past->id;
  $p_path2="https://marina.bjm.tomman.app/backupwasaka/work/".$past->id;
  $p_path3="https://marina.bjm.tomman.app/backupwasaka2/work/".$past->id;
  $p_pin="/upload/pin/".$past->id;
  $p_pin2="https://marina.bjm.tomman.app/backupwasaka/pin/".$past->id;
  $p_pin3="https://marina.bjm.tomman.app/backupwasaka2/pin/".$past->id;
  $p_pinacak="/upload/pin/acak/".$past->id;
  $p_pinacak2="https://marina.bjm.tomman.app/backupwasaka/pin/acak/".$past->id;
  $p_pinacak3="https://marina.bjm.tomman.app/backupwasaka2/pin/acak/".$past->id;
  if(file_exists(public_path().$p_path."/before.jpg")){
      $p_path=$p_path;
  }else if(stripos(get_headers($p_path2."/before.jpg", 0, $context)[0],"200 OK")){
      $p_path = $p_path2;
  }else if(stripos(get_headers($p_path3."/before.jpg", 0, $context)[0],"200 OK")){
      $p_path = $p_path3;
  }else{
      $p_path = '/image/placeholder.gif';
  }
  if(file_exists(public_path().$p_pin.".jpg")){
      $p_pin=$p_pin;
  }else if(stripos(get_headers($p_pin2.".jpg", 0, $context)[0],"200 OK")){
      $p_pin = $p_pin2;
  }else if(stripos(get_headers($p_pin3.".jpg", 0, $context)[0],"200 OK")){
      $p_pin = $p_pin3;
  }else{
      $p_pin = '/image/placeholder.gif';
  }
  if(file_exists(public_path().$p_pinacak.".jpg")){
      $p_pinacak=$p_pinacak;
  }else if(stripos(get_headers($p_pinacak2.".jpg", 0, $context)[0],"200 OK")){
      $p_pinacak = $p_pinacak2;
  }else if(stripos(get_headers($p_pinacak3.".jpg", 0, $context)[0],"200 OK")){
      $p_pinacak = $p_pinacak3;
  }else{
      $p_pinacak = '/image/placeholder.gif';
  }
?>

<div class="col-md-6">
  <div class="panel">
    <div class="panel-heading"><span class="panel-title">Current Work</span></div>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-6">
          <ul class="list-group m-x-0 m-b-0">
            <li class="list-group-item b-x-0 b-b-0">
              ID Wasaka
              <span class="label pull-right"> {{ $current->id }} </span>
            </li>
            <li class="list-group-item b-x-0 b-b-0">
              User
              <span class="label pull-right">[ {{ $current->id_user }} ]  {{ $current->nama_user }} </span>
            </li>
            <li class="list-group-item b-x-0 b-b-0">
              Kode Perangkat
              <span class="label pull-right"> {{ $current->kode }} </span>
            </li>
            <li class="list-group-item b-x-0 b-b-0">
              Jenis Pekerjaan
              <span class="label pull-right"> {{ $current->jenis_pekerjaan }}</span>
            </li>
            <li class="list-group-item b-x-0 b-b-0">
              Jam Open
              <span class="label pull-right"> {{ $current->date_start }} {{ $current->timezone }} </span>
            </li>
            <li class="list-group-item b-x-0 b-b-0">
              Jam Close
              <span class="label pull-right"> {{ $current->date_close }} {{ $current->timezone }} </span>
            </li>
            <li class="list-group-item b-x-0 b-b-0">
              Durasi
              <span class="label pull-right"> {{ $current->durasi }} </span>
            </li>
            <li class="list-group-item b-x-0 b-b-0">
              Uraian
              <span class="label pull-right"> {{ $current->uraian }} </span>
            </li>
            <li class="list-group-item b-x-0 b-b-1">
              Anggota
              <span class="label pull-right"> {{ $current->anggota }} </span>
            </li>
          </ul>
        </div>
        <div class="col-md-6">
          <div class="box-cell">
            <div class="box-container">
              <div class="box-row">
                <span href="#" class="box-cell p-a-1 valign-middle text-xs-center">
                    <a href="{{ $c_path }}/before.jpg">
                        <img src="{{ $c_path }}/before-th.jpg" alt="">
                    </a>
                    <p>Depan Alpro</p>
                </span>
              </div>
              <div class="box-row">
                <span href="#" class="box-cell p-a-1 valign-middle text-xs-center">
                    <a href="{{ $c_path }}/work-progress.jpg">
                        <img src="{{ $c_path }}/work-progress-th.jpg" alt="">
                    </a>
                    <p>Proses Pengerjaan</p>
                </span>
              </div>
            </div> <!-- / .box-container -->
          </div> <!-- / .box-cell -->
          <div class="box-cell">
            <div class="box-container">
              <div class="box-row">
                <span href="#" class="box-cell p-a-1 valign-middle text-xs-center">
                    <a href="{{ $c_path }}/item-before.jpg">
                        <img src="{{ $c_path }}/item-before-th.jpg" alt="">
                    </a>
                    <p>Sebelum Pekerjaan</p>
                </span>
              </div>
              <div class="box-row">
                <span href="#" class="box-cell p-a-1 valign-middle text-xs-center">
                    <a href="{{ $c_path }}/item-after.jpg">
                        <img src="{{ $c_path }}/item-after-th.jpg" alt="">
                    </a>
                    <p>Setelah Pekerjaan</p>
                </span>
              </div>
            </div> <!-- / .box-container -->
          </div> <!-- / .box-cell -->
          <div class="box-cell">
            <div class="box-container">
              <div class="box-row">
                <span href="#" class="box-cell p-a-1 valign-middle text-xs-center input-photos">
                    <a href="{{ $c_pin }}.jpg">
                        <img src="{{ $c_pin }}-th.jpg" alt="">
                    </a>
                    <p>PIN</p>
                </span>
              </div>
              <div class="box-row">
                <span href="#" class="box-cell p-a-1 valign-middle text-xs-center input-photos">
                    <a href="{{ $c_pinacak }}.jpg">
                        <img src="{{ $c_pinacak }}-th.jpg" alt="">
                    </a>
                    <p>PIN Acak</p>
                </span>
              </div>
            </div> <!-- / .box-container -->
          </div> <!-- / .box-cell -->
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-md-6">
  <div class="panel">
    <div class="panel-heading"><span class="panel-title">Past Work</span></div>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-6">
          <ul class="list-group m-x-0 m-b-0">
            <li class="list-group-item b-x-0 b-b-0">
              ID Wasaka
              <span class="label pull-right"> {{ $past->id }} </span>
            </li>
            <li class="list-group-item b-x-0 b-b-0">
              User
              <span class="label pull-right">[ {{ $past->id_user }} ]  {{ $past->nama_user }} </span>
            </li>
            <li class="list-group-item b-x-0 b-b-0">
              Kode Perangkat
              <span class="label pull-right"> {{ $past->kode }} </span>
            </li>
            <li class="list-group-item b-x-0 b-b-0">
              Jenis Pekerjaan
              <span class="label pull-right"> {{ $past->jenis_pekerjaan }}</span>
            </li>
            <li class="list-group-item b-x-0 b-b-0">
              Jam Open
              <span class="label pull-right"> {{ $past->date_start }} {{ $past->timezone }} </span>
            </li>
            <li class="list-group-item b-x-0 b-b-0">
              Jam Close
              <span class="label pull-right"> {{ $past->date_close }} {{ $past->timezone }} </span>
            </li>
            <li class="list-group-item b-x-0 b-b-0">
              Durasi
              <span class="label pull-right"> {{ $past->durasi }} </span>
            </li>
            <li class="list-group-item b-x-0 b-b-0">
              Uraian
              <span class="label pull-right"> {{ $past->uraian }} </span>
            </li>
            <li class="list-group-item b-x-0 b-b-1">
              Anggota
              <span class="label pull-right"> {{ $past->anggota }} </span>
            </li>
          </ul>
        </div>
        <div class="col-md-6">
          <div class="box-cell">
            <div class="box-container">
              <div class="box-row">
                <span href="#" class="box-cell p-a-1 valign-middle text-xs-center">
                    <a href="{{ $p_path }}/before.jpg">
                        <img src="{{ $p_path }}/before-th.jpg" alt="">
                    </a>
                    <p>Depan Alpro</p>
                </span>
              </div>
              <div class="box-row">
                <span href="#" class="box-cell p-a-1 valign-middle text-xs-center">
                    <a href="{{ $p_path }}/work-progress.jpg">
                        <img src="{{ $p_path }}/work-progress-th.jpg" alt="">
                    </a>
                    <p>Proses Pengerjaan</p>
                </span>
              </div>
            </div> <!-- / .box-container -->
          </div> <!-- / .box-cell -->
          <div class="box-cell">
            <div class="box-container">
              <div class="box-row">
                <span href="#" class="box-cell p-a-1 valign-middle text-xs-center">
                    <a href="{{ $p_path }}/item-before.jpg">
                        <img src="{{ $p_path }}/item-before-th.jpg" alt="">
                    </a>
                    <p>Sebelum Pekerjaan</p>
                </span>
              </div>
              <div class="box-row">
                <span href="#" class="box-cell p-a-1 valign-middle text-xs-center">
                    <a href="{{ $p_path }}/item-after.jpg">
                        <img src="{{ $p_path }}/item-after-th.jpg" alt="">
                    </a>
                    <p>Setelah Pekerjaan</p>
                </span>
              </div>
            </div> <!-- / .box-container -->
          </div> <!-- / .box-cell -->
          <div class="box-cell">
            <div class="box-container">
              <div class="box-row">
                <span href="#" class="box-cell p-a-1 valign-middle text-xs-center input-photos">
                    <a href="{{ $p_pin }}.jpg">
                        <img src="{{ $p_pin }}-th.jpg" alt="">
                    </a>
                    <p>PIN</p>
                </span>
              </div>
              <div class="box-row">
                <span href="#" class="box-cell p-a-1 valign-middle text-xs-center input-photos">
                    <a href="{{ $p_pinacak }}.jpg">
                        <img src="{{ $p_pinacak }}-th.jpg" alt="">
                    </a>
                    <p>PIN Acak</p>
                </span>
              </div>
            </div> <!-- / .box-container -->
          </div> <!-- / .box-cell -->
        </div>
      </div>
    </div>
  </div>
</div>