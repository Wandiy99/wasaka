<div class="panel panel-default" id="info">
  <div class="panel-heading">List</div>
  <div id="fixed-table-container-demo" class="fixed-table-container">
    <table class="table table-bordered table-fixed">
      <tr>
        <th>#</th>
        <th>ID Wasaka</th>
        <th>Issue</th>
        <th>Suspend User</th>
        <th>User</th>
        <th>Status</th>
        <th>Created By</th>
        <th>Created At</th>
      </tr>
      @foreach($data as $no => $d) 
        <tr>
          <td>{{ ++$no }}</td>
          <td>{{ $d->work_id }}</td>
          <td>{{ $d->issue }}</td>
          <td>{{ $d->suspend_user }}</td>
          <td>{{ $d->user_id }}</td>
          <td>{{ $d->status }}</td>
          <td>{{ $d->created_by }}</td>
          <td>{{ $d->created_at }}</td>
        </tr>
      @endforeach
    </table>
  </div>
</div>