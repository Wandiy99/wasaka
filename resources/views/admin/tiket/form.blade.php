@extends('layoutadmin')
@section('css')
<style type="text/css">
  .input-photos img {
    width: 100px;
    height: 150px;
    margin-bottom: 5px;
  }
  .no-search .select2-search {
    display:none
  }
</style>
@endsection
@section('title', 'Tiket')
@section('content')
<?php
  $isAdmin = (session('auth')->level==2 || session('auth')->level==3)?1:0;
?>
<div class="panel">
  <div class="panel-body">
  <form method="post" id="userform" enctype="multipart/form-data">
    <div class="row">
      <div class="col-md-2 form-group form-message-dark">
        <label for="id_wasaka">ID Wasaka</label>
        <input type="text" name="work_id" id="id_wasaka" class="form-control border-round" autocomplete="off" placeholder="Input ID Wasaka" value="{{ $data->work_id or old('work_id') }}" required />
      </div>
      <div class="col-md-4 form-group form-message-dark">
        <label for="issue">Issue</label>
        <input type="text" name="issue" id="issue" class="form-control border-round" autocomplete="off" required placeholder="Input Issue" value="{{ $data->issue or old('issue') }}" required/>
      </div>
      <div class="col-md-3 form-group form-message-dark">
        <label for="status">Status</label>
        <input type="text" name="status" id="status" class="form-control border-round" autocomplete="off" required placeholder="Pilih status" value="{{ $data->status or old('status') }}" required/>
      </div>
      <div class="col-md-3 form-group form-message-dark">
        <label for="suspend_user">Suspend User</label>
        <input type="text" name="suspend_user" id="suspend_user" class="form-control border-round" autocomplete="off" required placeholder="Pilih Suspend User" value="{{ $data->suspend_user or old('suspend_user') }}" required/>
      </div>
    </div>
    <div class=" form-group form-message-dark">
      <label for="a">&nbsp;</label>
      <button type="submit" class="btn btn-primary form-control border-round"><i class="ion-soup-can"></i> Simpan</button>
    </div>
  </form>
  </div>
</div>
<div id="ajaxcontent" class="row">
  
</div>

@endsection
@section('js')
<script type="text/javascript">
  $(function() {
    $('#suspend_user').select2({
      data: [{"id":"Current User", "text":"Current User"},{"id":"Past User", "text":"Past User"}],
      placeholder:'Pilih Suspend User',
      containerCssClass :'border-round',
      dropdownCssClass : 'no-search'
    });
    $('#status').select2({
      data: [{"id":1, "text":"Open"},{"id":2, "text":"Closed"},{"id":3, "text":"Deleted"}],
      placeholder:'Pilih Status',
      containerCssClass :'border-round',
      dropdownCssClass : 'no-search'
    });
    $("#id_wasaka").on('paste', function () {
      setTimeout(function () {
        $("#issue").focus();
      }, 100);
    });
    $("#id_wasaka").blur(function(){
      $.get("/admin/ajaxTiket/"+$("#id_wasaka").val(), function(data){
        $("#ajaxcontent").html(data);
      });
    });
    $('#userform').pxValidate();
  });
</script>
@endsection