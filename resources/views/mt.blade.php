<!doctype html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>WASAKA</title>

    <link rel="stylesheet" href="/bower_components/bootswatch-dist/css/bootstrap.min.css" />
    <!-- <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css" /> -->
    <link rel="stylesheet" href="/style.css" />

    <style>
        div.clearfix {
            border-radius: 15px;
            box-shadow: 0 0 8px rgba(0, 0, 0, 0.4);
            padding: 5px;
            width: 400px;
            height: 200px;
            background: white;
            color:black;
            text-align: center;
            margin: auto;
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }
        .img1 {
            float: right;
        }
        .clearfix {
            overflow: auto;
        }
        .img2 {
            float: right;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="clearfix">
            <img class="img2" src="/img/mt.gif" width="170" height="170">
            <h1 style="margin-bottom:0;">WASAKA</h1>
            <a href="#">WA</a>dah <a href="#">SA</a>gan <a href="#">K</a>unci <a href="#">A</a>kses<br/><br/>
            Saat ini Server sedang Maintenance<br/>Silahkan kembali lagi nanti.</a>
        </div>
    </div>
</body>
</html>