@extends('layout')

@section('content')
    @if (session('auth')->level == '2')
    <div class="panel panel-default">
        <div class="panel-heading">
            Kotak Saran
        </div>
        <div class="panel-body">
            <form method="post" class="form-horizontal">
                <div class="form-group {{ $errors->has('saran') ? 'has-error' : '' }}">
                    <label for="saran" class="col-sm-3 col-md-2 control-label">Saran</label>
                    <div class="col-sm-9 col-md-10">
                        <textarea name="saran" id="saran" class="form-control"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-md-offset-2 col-sm-2">
                        <button class="btn btn-primary" type="submit">
                            <span class="glyphicon glyphicon-floppy-disk"></span>
                            <span>Simpan</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @endif
    <div class="panel panel-default">
            <div class="panel-heading">List </div>
            <div class="panel-body table-responsive">
                <table class="table table-bordered table-striped">
                    <tr>
                        <th>#</th>
                        <th>Saran</th>
                        <th>User</th>
                        <th>Date</th>
                    </tr>
                    @foreach($saran as $no => $s)
                        <tr>
                            <td>{{ ++$no }}</td>
                            <td>{{ $s->saran }}</td>
                            <td>{{ $s->user }}</td>
                            <td>{{ $s->ts }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
@endsection