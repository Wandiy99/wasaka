@extends('layout')

@section('content')
<h2>SOP wasaka</h2>
	<p>
		Berikut ini adalah penjelasan menu dan cara penggunaan wasaka
	</p>
	
	<p>
		<ol>
			<li>
				Pada tampilan awal ketika memasukkan alamat url <u>wasaka.telkomakses.co.id</u> maka akan diarahkan kemenu login.
				<br><img src="/img/1.png" height="300" width="500">
				<br>Untuk bisa mengakses atau masuk kedalam menu utama gunakan username dan password yang 		terdaftar di SSO pada portal telkomakses.co.id.
				<br>Jika sudah berhasil login, selanjutnya akan diarahkan ketampilan utama seperti berikut
				<br><img src="/img/3.1.png" height="300" width="700">
				<br>Pada tampilan utama terdapat 5 pilihan menu yaitu <u>Search</u>, <u>History</u>, <u>MyHistory</u>, <u>Map</u> dan <u>Dashboard</u>. 
				<br>Untuk keluar dari sistem klik tombol <u>Logout</u> yang terdapat pada pojok kanan bawah.
			</li>

			<li>
				<p>
					Pada menu search akan dijelaskan beberapa fungsi atau kegunaan dari pada menu ini.
					<ol type="a">
						<li>Request PIN</li>
							Untuk melakukan Request Pin hal pertama yang dilakukan adalah melakukan pencarian perangkat yang akan dilakukan pengerjaan, dengan cara menuliskan nama perangkatnya seperti contoh berikut 
							<br><img src="/img/4.png" height="300" width="700">
							<br>Setelah itu pilih perangkat yang akan dilakukan pengerjaan, kemudian klik maka akan ada 2 opsi pilihan yang akan diberikan. <u>Request PIN</u> dan <u>Library Foto</u>
							<br><img src="/img/5.png" height="300" width="700">
							<br>dikarenakan kita akan melakukan request pin maka kita lakukan klik pada pilihan request pin, selanjutnya akan diarahkan pada tampilan berikut.
							<br><img src="/img/tes.jpg" height="300" width="700">
							<br>Untuk melakukan request pin, isi semua inputan yang ada, inputkan jenis pekerjaan, nama-nama anggota yang terlibat dan juga foto perangkat sebelum dilakukan pengerjaan. Setelah semua isian diinput lakukan request pin, jika request pin berhasil maka akan diarahakan ke tampilan seperti berikut berikut
							<br><img src="/img/7.1.png" height="300" width="700">
							<br>4 digit angka yang ada diatas adalah pin yang diberikan oleh admin untuk membuka kunci perangkat. kunci pin ini bersifat random dan tidak tetap oleh karenanya setiap dilakukan pembukaan kunci maka kunci akan selalu direset. Harap untuk memoto atau mengcapture pin yang diberikan karena akan digunakan kembali saat closing pekerjaan.
						<p>	
						<li>Panic Button</li>
							Masih berhubungan dengan Request Pin, jika Request Pin gagal dilakukan maka kita bisa melakukan klik pada panic button, disini akan diberikan 2 opsi pilihan yaitu <u>Jaringan Lelet</u> dan <u>Pin Tidak Sesuai</u> seperti tampilan berikut.
							<br><img src="/img/9.png" height="200" width="500">
							<br>Jika Pin tidak muncul maka kita bisa memilih opsi pertama yaitu jaringan lelet dan jika Pin yang diberikan tidak bisa digunakan pada kunci perangkat maka kita bisa gunakan opsi yang kedua, pada pilihan opsi yang kedua akan diarahkan ketampilan seperti berikut
							<br><img src="/img/10.png" height="450" width="400">
							<br>Selanjutnya masukkan salah satu dari 2 pin diatas untuk dimasukkan kembali kedalam kunci perangkat.
						<p>
						<li>Progres</li>
							Setelah mendapatkan pin kunci selanjutkan isikan semua inputan progres yang ada dibagian bawah. Isikan uraian pekerjaan yang dilakukan kemudian upload foto perangkat sebelum dilakukan pekerjaan,  proses perbaikan dan foto perangkat setelah pekerjaan dilakukan kemudian klik simpan
							<br><img src="/img/7.2.png" width="800" height="300">
						<p>
						<li>Closing</li> 
							Selanjutnya adalah closing pekerjaan perangkat. Uploadkan foto pin yang sudah didapat pada request pin pada inputan foto pin reset, selanjutnya klik simpan
							<br><img src="/img/8.png" height="150" width="550">
					</ol>
				</p>
			</li>

			<li>Menu History</li>
				Menu ini digunakan untuk mengecek pekerjaan yang sedang berlangsung dan juga digunakan untuk mengecek pekerjaan perangkat yang sudah dikerjakan.
				<br><img src="/img/12.png" height="300" width="800">

			<p>
			<li>Menu MyHistory</li>
				Menu MyHistory dari segi tampilan sama dengan menu history bedanya di menu ini kita hanya bisa melihat pekerjaan yang pernah kita kerjakan.
		</ol>
	</p>
@endsection