@extends('layout')

@section('content')
<h2>SOP wasaka untuk admin.</h2>
	<p>
		Berikut ini adalah penjelasan menu dan cara penggunaan
	</p>
	
	<p>
		<ol>
			<li>
				Pada tampilan awal ketika memasukkan alamat url <u>wasaka.telkomakses.co.id</u> maka akan diarahkan kemenu login.
				<br><img src="/img/1.png" height="300" width="500">
				<br>Untuk bisa mengakses atau masuk kedalam menu utama gunakan username dan password yang 		terdaftar di SSO pada portal telkomakses.co.id.
				<br>Jika sudah berhasil login, selanjutnya akan diarahkan ketampilan utama seperti berikut
				<br><img src="/img/a_1.png" height="300" width="700">
				<br>Pada tampilan utama terdapat 5 pilihan menu yaitu <u>Search</u>, <u>History</u>, <u>Manage User</u>, <u>Map</u> dan <u>Dashboard</u>. 
				<br>Untuk keluar dari sistem klik tombol <u>Logout</u> yang terdapat pada pojok kanan bawah.
			</li>

			<li>
				<p>
					Pada menu search akan dijelaskan beberapa fungsi atau kegunaan dari pada menu ini.
					<ol type="a">
						<li>Input Gembok</li>
							Input gembok digunakan untuk menambah perangkat yang ada, cara menambahkannya dengan cara klik input gembok selanjutnya akan diarahkan ke tampilan sebagai berikut
						<br><img src="/img/a_2.png" width="800" height="400">
						<br>Isikan semua inputan yang ada pada form tersebut, inputkan kode perangkat, pin, witel, type ODC, merk, kapasitas, kategori, koordinat dan foto profile kemudian simpan data perangkat tersebut dengan cara klik simpan
							
						<li>Request PIN</li>
							Untuk melakukan Request Pin hal pertama yang dilakukan adalah melakukan pencarian perangkat yang akan dilakukan pengerjaan, dengan cara menuliskan nama perangkatnya seperti contoh berikut 
							<br><img src="/img/4.png" height="300" width="700">
							<br>Setelah itu pilih perangkat yang akan dilakukan pengerjaan, kemudian klik maka akan ada 3 opsi pilihan yang akan diberikan. <u>Request PIN</u>, <u>Edit Gembok</u> dan <u>Library Foto</u>
							<br><img src="/img/a_5.png" height="300" width="700">
							<br>dikarenakan kita akan melakukan request pin maka kita lakukan klik pada pilihan request pin, selanjutnya akan diarahkan pada tampilan berikut.
							<br><img src="/img/tes.jpg" height="300" width="700">
							<br>Untuk melakukan request pin, isi semua inputan yang ada, inputkan jenis pekerjaan, nama-nama anggota yang terlibat dan juga foto perangkat sebelum dilakukan pengerjaan. Setelah semua isian diinput lakukan request pin, jika request pin berhasil maka akan diarahakan ke tampilan seperti berikut berikut
							<br><img src="/img/7.1.png" height="300" width="700">
							<br>4 digit angka yang ada diatas adalah pin yang diberikan oleh admin untuk membuka kunci perangkat. kunci pin ini bersifat random dan tidak tetap oleh karenanya setiap dilakukan pembukaan kunci maka kunci akan selalu direset. Harap untuk memoto atau mengcapture pin yang diberikan karena akan digunakan kembali saat closing pekerjaan.
						<p>	
						<li>Panic Button</li>
							Masih berhubungan dengan Request Pin, jika Request Pin gagal dilakukan maka kita bisa melakukan klik pada panic button, disini akan diberikan 2 opsi pilihan yaitu <u>Jaringan Lelet</u> dan <u>Pin Tidak Sesuai</u> seperti tampilan berikut.
							<br><img src="/img/9.png" height="200" width="500">
							<br>Jika Pin tidak muncul maka kita bisa memilih opsi pertama yaitu jaringan lelet dan jika Pin yang diberikan tidak bisa digunakan pada kunci perangkat maka kita bisa gunakan opsi yang kedua, pada pilihan opsi yang kedua akan diarahkan ketampilan seperti berikut
							<br><img src="/img/10.png" height="450" width="400">
							<br>Selanjutnya masukkan salah satu dari 2 pin diatas untuk dimasukkan kembali kedalam kunci perangkat.
						<p>
						<li>Progres</li>
							Setelah mendapatkan pin kunci selanjutkan isikan semua inputan progres yang ada dibagian bawah. Isikan uraian pekerjaan yang dilakukan kemudian upload foto perangkat sebelum dilakukan pekerjaan,  proses perbaikan dan foto perangkat setelah pekerjaan dilakukan kemudian klik simpan
							<br><img src="/img/7.2.png" width="800" height="300">
						<p>
						<li>Closing</li> 
							Selanjutnya adalah closing pekerjaan perangkat. Uploadkan foto pin yang sudah didapat pada request pin pada inputan foto pin reset, selanjutnya klik simpan
							<br><img src="/img/8.png" height="150" width="550">
						<p>
						<li>Edit Gembok Perangkat</li>
							Untuk melakukan pengeditan gembok perangkat sama caranya dengan cara melakukan request pin pilih perangkat yang akan diedit kemudiam lakukan klik pilih opsi kedua yaitu Edit Gembok, selanjutnya akan muncul tampilan sebagai berikut
							<br><img src="/img/a_6.png" width="800" height="400">
							<br>Untuk melakukan perubahan data sama caranya dengan melakukan penyimpanan gembok baru, isikan semua inputan yang ada kemudian ubah semua data lama dengan data yang baru kemudian simpan data dengan cara klik simpan.
					</ol>
				</p>
			</li>

			<li>Menu History</li>
				Menu ini digunakan untuk mengecek pekerjaan yang sedang berlangsung dan juga digunakan untuk mengecek pekerjaan perangkat yang sudah dikerjakan.
				<br><img src="/img/12.png" height="300" width="800">

			<p>
			<li>Menu Manage Useer</li>
				Menu Manage User digunakan untuk menambah user baru dan juga untuk mengedit data user lama.
			<br><img src="/img/a_3.png" width="800" height="400">
			<br> Untuk melakukan penambahan user klik tombol <u>Create User</u> maka akan diarahkan ketampilan sebagai berikut
			<br><img src="/img/a_4.png" width="700" height="400">
			<br>Selanjutnya isikan semua inputan yang ada pada form tersebut, inputkan nama user, nama perusahaan, IDuser/NIK, password dan status setelah itu lakukan penyimpanan dengan cara klik simpan.
			<br>Untuk melakukan pengeditan pada data user dilakukan dengan cara pilih user yang akan dilakukan pengeditan data kemudian lakukan klik pada nama tersebut maka akan diarahkan ke tampilan sebagai berikut
			<br><img src="/img/a_7.png" width="700" height="400">
			<br>Ubah data yang lama dengan data yang baru dengan cara yang sama pada saat melakukan penambahan user baru, jika sudah dilakukan perubahan pada data tersebut simpan data dengan cara klik simpan.
		</ol>
	</p>
@endsection