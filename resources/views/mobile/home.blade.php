@extends('mobile.layout')
@section('title')
  Home
@endsection
@section('content')
  <?php
    $auth = session('auth');
    $jenis_alpro = [1=>'FTM','ODC','MSAN','MDU','ONU','OLT','Custom Akses','ODP'];
    $level = [0=>'View',1=>'User',2=>'Admin',3=>'View',4=>'Superadmin',99=>'Mitra'];
  ?>
  <!-- Page Title-->
  <div class="pt-3">
    <div class="page-title d-flex">
      <div class="align-self-center me-auto">
        <p class="color-highlight header-date"></p>
        <h1>Welcome {{ session('auth')->nama }}</h1>
      </div>
      <div class="align-self-center ms-auto">
        <a href="#"
        data-bs-toggle="dropdown"
        class="icon gradient-blue color-white shadow-bg shadow-bg-s rounded-m">
           <i class="bi bi-menu-button-fill font-17"></i>
        </a>
        <!-- Page Title Dropdown Menu-->
        <div class="dropdown-menu">
          <div class="card card-style shadow-m mt-1 me-1">
            <div class="list-group list-custom list-group-s list-group-flush rounded-xs px-3 py-1">
              <!-- <a href="/lama" class="list-group-item">
                <i class="has-bg gradient-yellow shadow-bg shadow-bg-xs color-white rounded-xs bi bi-person-circle"></i>
                <strong class="font-13">Web Lama</strong>
              </a> -->
              <!-- <a href="#" class="list-group-item">
                <i class="has-bg gradient-yellow shadow-bg shadow-bg-xs color-white rounded-xs bi bi-person-circle"></i>
                <strong class="font-13">User</strong>
              </a> -->
              <a href="/logout" class="list-group-item">
                <i class="has-bg gradient-red shadow-bg shadow-bg-xs color-white rounded-xs bi bi-power"></i>
                <strong class="font-13">Log Out</strong>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Account Activity Title-->
  <div class="content my-0 mt-n2 px-1">
    <div class="d-flex">
      <div class="align-self-center">
        <h3 class="font-16 mb-2">Account Info</h3>
      </div>
    </div>
  </div>
  <?php
    $warna = "blue";$status = "Aktif";
    if($auth->status==0){
      $warna = "blue";$status = "Aktif";
    }else if($auth->status==1){
      $warna = "red";$status = "Non-Aktif(Call Admin)";
    }else if($auth->status==3){
      $warna = "orange";$status = "Suspended(Selesaikan Responsibility)";
    }
  ?>
  <!--Account Activity Notification-->
  <div class="card card-style gradient-{{ $warna }} shadow-bg shadow-bg-s">
    <div class="content">
      <a href="/mobile/profile" class="d-flex">
        <div class="align-self-center pt-1">
          <h5 class="pt-1 mb-n2 color-white">Regional</h5>
          <h5 class="pt-1 mb-n2 color-white">Witel</h5>
          <h5 class="pt-1 mb-n2 color-white">Level</h5>
          <h5 class="pt-1 mb-n2 color-white">Status</h5>
          <h5 class="pt-1 mb-n2 color-white">Akses Alpro</h5>
        </div>
        <div class="align-self-center ms-auto text-end">
          <h4 class="pt-1 mb-n2 color-white">{{ $auth->regional?:'Not Set' }}</h4>
          <h4 class="pt-1 mb-n2 color-white">{{ $auth->wtl?:'Not Set' }}</h4>
          <h4 class="pt-1 mb-n2 color-white">{{ $auth->level?$level[$auth->level]:'Not Set' }}</h4>
          <h4 class="pt-1 mb-n2 color-white">{{ $status }}</h4>
          <h4 class="pt-1 mb-n2 color-white">
            @if($auth->akses_perangkat)
              @foreach(explode(',',$auth->akses_perangkat) as $ap)
                @if($ap)
                  <span class="label label-secondary m-b-1">{{ $jenis_alpro[$ap] }}</span>
                @endif
              @endforeach
            @else
              <span class="label label-secondary m-b-1">Not Set</span>
            @endif
          </h4>
        </div>
      </a>
    </div>
  </div>
  
  @if(count($ogp))
    <!-- Account Activity Title-->
    <div class="content my-0 mt-n2 px-1">
      <div class="d-flex">
        <div class="align-self-center">
          <h3 class="font-16 mb-2">OnGoing Activity</h3>
        </div>
      </div>
    </div>

    <!--Account Activity Notification-->
    @foreach($ogp as $o)
      @if($o->step)
        <div class="card card-style gradient-orange shadow-bg shadow-bg-s">
          <div class="content">
            <a href="/loker/close/{{$o->id}}" class="d-flex py-1">
              <div class="align-self-center">
                <span class="icon rounded-s me-2 gradient-yellow shadow-bg shadow-bg-s color-white">{{ $o->id }}</span>
              </div>
              <div class="align-self-center ms-auto text-end">
                <h5 class="pt-1 mb-n1 color-white">{{ $o->kode }}</h5>
                <p class="mb-0 font-11 color-white">{{ date("d F Y H:i:s", strtotime($o->date_start)) }} <b class="mb-0 font-11">({{ $o->durasi }})</b></p>
              </div>
            </a>
            <p class="mb-0 font-11 ms-auto text-end color-white">{{ $o->jenis_pekerjaan }}</p>
          </div>
        </div>
      @else
        <div class="card card-style gradient-orange shadow-bg shadow-bg-s">
          <div class="content">
            <a href="/mobile/work/{{$o->id}}/close" class="d-flex py-1">
              <div class="align-self-center">
                <span class="icon rounded-s me-2 gradient-yellow shadow-bg shadow-bg-s color-white">{{ $o->id }}</span>
              </div>
              <div class="align-self-center ms-auto text-end">
                <h5 class="pt-1 mb-n1 color-white">{{ $o->kode }}</h5>
                <p class="mb-0 font-11 color-white">{{ date("d F Y H:i:s", strtotime($o->date_start)) }} <b class="mb-0 font-11">({{ $o->durasi }})</b></p>
              </div>
            </a>
            <p class="mb-0 font-11 ms-auto text-end color-white">{{ $o->jenis_pekerjaan }}</p>
          </div>
        </div>
      @endif
    @endforeach
  @endif
  @if(count($work))
    <!-- Recent Activity Title-->
    <div class="content my-0 mt-n2 px-1">
      <div class="d-flex">
        <div class="align-self-center">
          <h3 class="font-16 mb-2">Recent Activity</h3>
        </div>
      </div>
    </div>

    <!-- Recent Activity Cards-->
    <div class="card card-style">
      <div class="content">
        @foreach($work as $no => $w)
          @if($no)
            <div class="divider my-2"></div>
          @endif
          <a href="#" class="d-flex py-1">
            <div class="align-self-center">
              <span class="icon rounded-s me-2 gradient-green shadow-bg shadow-bg-s color-white">{{ $item->jenis_alpro or '-' }}</span>
            </div>
            <div class="align-self-center ms-auto text-end">
              <h5 class="pt-1 mb-n1">{{ $w->kode }}</h5>
              <p class="mb-0 font-11 opacity-50">{{ date("d F Y H:i:s", strtotime($w->date_start)) }} <b class="mb-0 font-11">({{ $w->durasi }})</b></p>
            </div>
          </a>
          <p class="mb-0 font-11 ms-auto text-end">{{ $w->jenis_pekerjaan }}</p>
        @endforeach
      </div>
    </div>
  @endif
@endsection