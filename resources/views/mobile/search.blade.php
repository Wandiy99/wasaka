@extends('mobile.layout')
@section('title')
    Search Gembok
@endsection
@section('menu')
  <div id="detailGembok" class="offcanvas offcanvas-bottom offcanvas-detached rounded-m">
    <div class="card" style="">
      <div class="d-flex mx-3 mt-3 py-1">
        <div class="align-self-center">
          <h1 class="mb-0 title" id="tittle">Kode</h1>
        </div>
        <div class="align-self-center ms-auto">
          <a href="#" class="ps-4 shadow-0 me-n2" data-bs-dismiss="offcanvas">
            <i class="bi bi-x color-red-dark font-26 line-height-xl"></i>
          </a>
        </div>
      </div>
      <div class="card card-style">
        <div class="content mt-3">
          <div class="tabs tabs-box" id="tab-group-1">
            <div class="tab-controls rounded-s border-highlight">
              <a class="font-13 color-highlight" data-bs-toggle="collapse" href="#tab-1" aria-expanded="true">Detail</a>
              <a class="font-13 color-highlight" data-bs-toggle="collapse" href="#tab-2" aria-expanded="false">History</a>
              <a class="font-13 color-highlight linkrequest" href="/">Request PIN</a>
            </div>
            <div class="mt-3"></div>
            <div class="collapse show" id="tab-1" data-bs-parent="#tab-group-1">
              <div class="row">
                <strong class="col-5 color-theme">Gembok Version</strong>
                <strong class="col-7 text-end" id="versi_gembok">versi</strong>
                <div class="col-12 mt-2 mb-2"><div class="divider my-0"></div></div>
                <strong class="col-5 color-theme">Kerapian ODC</strong>
                <strong class="col-7 text-end color-highlight" id="kerapian_odc">Yess</strong>
                <div class="col-12 mt-2 mb-2"><div class="divider my-0"></div></div>
                <strong class="col-5 color-theme">Engsel ODC</strong>
                <strong class="col-7 text-end color-highlight" id="engsel_odc">Bagus</strong>
                <div class="col-12 mt-2 mb-2"><div class="divider my-0"></div></div>
                <strong class="col-5 color-theme">Last Akses</strong>
                <strong class="col-7 text-end" id="last_akses">2022-06-06 09:31:57</strong>
                <div class="col-12 mt-2 mb-2"><div class="divider my-0"></div></div>
                <strong class="col-5 color-theme">Count Akses</strong>
                <strong class="col-7 text-end" id="akses_count">267</strong>
                <div class="col-12 mt-2 mb-2"><div class="divider my-0"></div></div>
            </div>
            </div>
            <div class="collapse" id="tab-2" data-bs-parent="#tab-group-1"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('content')
  <!-- Page Title-->
  <div class="pt-3">
    <div class="page-title d-flex">
      <div class="align-self-center me-auto">
        <p class="color-highlight header-date"></p>
        <h1>Search Alpro</h1>
      </div>
      <div class="align-self-center ms-auto">
          
      </div>
    </div>
  </div>
  @if(count($ogp))
    <!-- Account Activity Title-->
    <div class="content my-0 mt-n2 px-1">
      <div class="d-flex">
        <div class="align-self-center">
          <h3 class="font-16 mb-2">OnGoing Activity</h3>
        </div>
      </div>
    </div>

    <!--Account Activity Notification-->
    @foreach($ogp as $o)
      <div class="card card-style gradient-orange shadow-bg shadow-bg-s">
        <div class="content">
          <a href="/mobile/work/{{$o->id}}/close" class="d-flex py-1">
            <div class="align-self-center">
              <span class="icon rounded-s me-2 gradient-yellow shadow-bg shadow-bg-s color-white">{{ $o->id }}</span>
            </div>
            <div class="align-self-center ms-auto text-end">
              <h5 class="pt-1 mb-n1 color-white">{{ $o->kode }}</h5>
              <p class="mb-0 font-11 color-white">{{ date("d F Y H:i:s", strtotime($o->date_start)) }} <b class="mb-0 font-11">({{ $o->durasi }})</b></p>
            </div>
          </a>
          <p class="mb-0 font-11 ms-auto text-end color-white">{{ $o->jenis_pekerjaan }}</p>
        </div>
      </div>
    @endforeach
  @else
    <!-- Recent Activity Title-->
    <div class="content my-0 mt-n2 px-1">
      <div class="d-flex">
        <div class="align-self-center">
          <h3 class="font-16 mb-2">Alpro Match</h3>
        </div>
      </div>
    </div>


    <!-- Recent Activity Cards-->
    <div class="card card-style">
      <div class="content">
        @foreach($list as $no => $item)
          <?php
            $scan = $item->scaningadmin;
          ?>
          @if($no)
            <div class="divider my-2"></div>
          @endif
          @if(session('auth')->level==2)
            <a href="#" class="d-flex py-1" data-bs-toggle="offcanvas" data-bs-target="#detailGembok" data-bs-kode="{{ $item->kode }}" data-bs-idgembok="{{ $item->id }}" data-bs-gembok="{{ $scan?$item->gembok:'Blm Scanning' }}" data-bs-last_akses="{{ $item->last_akses }}" data-bs-kerapian_odc="{{ $scan?$item->kerapian_odc:'Blm Scanning' }}" data-bs-engsel_odc="{{ $scan?$item->engsel_odc:'Blm Scanning' }}" data-bs-akses_count="{{ $item->akses_count }}">
              <div class="align-self-center">
                <span class="icon rounded-s me-2 gradient-green shadow-bg shadow-bg-s color-white">{{ $item->jenis_alpro or '-' }}</span>
              </div>
              <div class="align-self-center ms-auto text-end">
                <h5 class="pt-1 mb-n1">{{ $item->kode }}</h5>
                <p class="mb-0 font-11 opacity-50">{{ date("d F Y H:i:s", strtotime($item->ts)) }} <b class="mb-0 font-11">(Last Update)</b></p>
              </div>
            </a>
          @else
            <a href="/mobile/work/start/{{ $item->id }}" class="d-flex py-1">
              <div class="align-self-center">
                <span class="icon rounded-s me-2 gradient-green shadow-bg shadow-bg-s color-white">{{ $item->jenis_alpro or '-' }}</span>
              </div>
              <div class="align-self-center ms-auto text-end">
                <h5 class="pt-1 mb-n1">{{ $item->kode }}</h5>
                <p class="mb-0 font-11 opacity-50">{{ date("d F Y H:i:s", strtotime($item->ts)) }} <b class="mb-0 font-11">(Last Update)</b></p>
              </div>
            </a>
          @endif
        @endforeach
      </div>
    </div>
  @endif
@endsection
@section('js')
<script type="text/javascript">
  var modal = document.getElementById('detailGembok')
  modal.addEventListener('show.bs.offcanvas', function (event) {
    var button = event.relatedTarget
    modal.querySelector('#tittle').textContent = button.getAttribute('data-bs-kode')
    modal.querySelector('#versi_gembok').textContent = button.getAttribute('data-bs-gembok')
    modal.querySelector('#kerapian_odc').textContent = button.getAttribute('data-bs-kerapian_odc')
    modal.querySelector('#engsel_odc').textContent = button.getAttribute('data-bs-engsel_odc')
    modal.querySelector('#last_akses').textContent = button.getAttribute('data-bs-last_akses')
    modal.querySelector('#akses_count').textContent = button.getAttribute('data-bs-akses_count')
    modal.querySelector('.linkrequest').href = "/mobile/work/start/"+button.getAttribute('data-bs-idgembok')
    fetch('/mobile/ajaxHistory/'+button.getAttribute('data-bs-idgembok')).then(function(response) {
      return response.text().then(function(text) {
        modal.querySelector('#tab-2').innerHTML  = text
      })
    })
  })
</script>
@endsection