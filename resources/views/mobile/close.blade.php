@extends('mobile.layout')
@section('title')
    Form Closing
@endsection

@section('css')
  <link rel="stylesheet" type="text/css" href="/camerastyle.css" />
  <style type="text/css">
    .input-photos img {
      width: 100px;
      height: 150px;
      margin-bottom: 5px;
    }
  </style>
@endsection
@section('content')
<div class="pt-3">
  <div class="page-title d-flex">
    <div class="align-self-center me-auto">
      <p class="color-highlight header-date"></p>
      <h1>{{ $data->kode }}</h1>
    </div>
  </div>
</div>
@if(!$data->produk_id)
  <div class="card card-style gradient-blue shadow-bg shadow-bg-s">
    <a data-bs-toggle="offcanvas" data-bs-target="#menu-top-detached" href="#">
      <div class="content row text-center">
        <div class="col-3">
          <span class="icon rounded-s gradient-yellow shadow-bg shadow-bg-s color-white">{{ substr($data->pin,0,1) }}</span>
        </div>
        <div class="col-3">
          <span class="icon rounded-s gradient-yellow shadow-bg shadow-bg-s color-white">{{ substr($data->pin,1,1) }}</span>
        </div>
        <div class="col-3">
          <span class="icon rounded-s gradient-yellow shadow-bg shadow-bg-s color-white">{{ substr($data->pin,2,1) }}</span>
        </div>
        <div class="col-3">
          <span class="icon rounded-s gradient-yellow shadow-bg shadow-bg-s color-white">{{ substr($data->pin,3,1) }}</span>
        </div>
      </div>
    </a>
  </div>
@endif
<canvas id="canvas" hidden></canvas>
<form id="closeform" class="form form-horizontal" method="post" enctype="multipart/form-data">
  <input type="hidden" name="pin" value="{{ $data->pin_new }}">
  <div class="card card-style shadow-bg shadow-bg-s">
    <div class="content">
      <h1 class="text-center">Progress</h1>
      <div class="divider divider-margins"></div>

      <div class="form-custom form-label form-icon mx-3">
        <i class="bi bi-pencil-fill color-blue-dark font-14"></i>
        <textarea class="form-control rounded-xs" id="c43" require>{{ $data->uraian }}</textarea>
        <label for="c43" class="form-label-always-active color-blue-dark font-11">Uraian Pekerjaan</label>
      </div>
      <div class="row text-center">
        <div class="col-6">
          <div class="form-custom text-center input-photos">
            @if ($data->hasPhoto['before'])
              <img id="before-img" src="/upload/work/{{ $data->id }}/before-th.jpg" class="rounded-l" />
            @else
              <img id="before-img" src="/image/placeholder.gif" class="rounded-l" />
            @endif
            <p>Foto Depan Alpro</p>
          </div>
        </div>
        <div class="col-6 mb-n2">
          <div class="form-custom text-center input-photos">
            @if ($data->hasPhoto['item-before'])
              <img id="item-before-img" src="/upload/work/{{ $data->id }}/item-before-th.jpg" class="rounded-l" />
            @else
              <img id="item-before-img" src="/image/placeholder.gif" class="rounded-l" />
            @endif
            <textarea id="item-before-text" name="item-before" hidden></textarea>
            <p>Sebelum Progress</p>
          </div>
          <a data-bs-toggle="offcanvas" onclick="startCamera('item-before');" data-bs-target="#menu-bottom-full" href="#" class="btn gradient-blue shadow-bg shadow-bg-s mb-4 btn-camera"><i class="bi-camera-fill"></i></a>
        </div>
      </div>
      <div class="row text-center">
        <div class="col-6">
          <div class="form-custom text-center input-photos">
            @if ($data->hasPhoto['work-progress'])
              <img id="work-progress-img" src="/upload/work/{{ $data->id }}/work-progress-th.jpg" class="rounded-l" />
            @else
              <img id="work-progress-img" src="/image/placeholder.gif" class="rounded-l" />
            @endif
            <textarea id="work-progress-text" name="work-progress" hidden></textarea>
            <p>Progress</p>
          </div>
          
          <a data-bs-toggle="offcanvas" onclick="startCamera('work-progress');" data-bs-target="#menu-bottom-full" href="#" class="btn gradient-blue shadow-bg shadow-bg-s btn-camera"><i class="bi-camera-fill"></i></a>
        </div>
        <div class="col-6">
          <div class="form-custom text-center input-photos">
            @if ($data->hasPhoto['item-after'])
              <img id="item-after-img" src="/upload/work/{{ $data->id }}/item-after-th.jpg" class="rounded-l" />
            @else
              <img id="item-after-img" src="/image/placeholder.gif" class="rounded-l" />
            @endif
            <textarea id="item-after-text" name="item-after" hidden></textarea>
            <p>Sesudah Progress</p>
          </div>
          <a data-bs-toggle="offcanvas" onclick="startCamera('item-after');" data-bs-target="#menu-bottom-full" href="#" class="btn gradient-blue shadow-bg shadow-bg-s mb-4 btn-camera"><i class="bi-camera-fill"></i></a>
        </div>
      </div>
    </div>
  </div>

  <div class="card card-style shadow-bg shadow-bg-s">
    @if(!$data->produk_id)
      <div class="content">
        <h1 class="text-center">Form Closing</h1>
        <div class="divider divider-margins"></div>
        <div class="row text-center">
          <div class="col-6">
            <div class="form-custom text-center input-photos">
              <img id="pin-img" src="{{ old('resetPin') ?: '/image/placeholder.gif' }}" class="rounded-l" />
              <textarea id="pin-text" name="resetPin" hidden>{{ old('resetPin') ?: ''}}</textarea>
              <p>PIN baru <i class="bi bi-unlock-fill"></i> <b>{{$data->pin_new}}</b></p>
            </div>
            
            <a data-bs-toggle="offcanvas" onclick="startCamera('pin');" data-bs-target="#menu-bottom-full" href="#" class="btn gradient-blue shadow-bg shadow-bg-s btn-camera"><i class="bi-camera-fill"></i></a>
          </div>
          <div class="col-6">
            <div class="form-custom text-center input-photos">
              <img id="pin_acak-img" src="{{ old('acakPin') ?: '/image/placeholder.gif' }}" class="rounded-l" />
              <textarea id="pin_acak-text" name="acakPin" hidden>{{ old('acakPin') ?: ''}}</textarea>
              <p>PIN Acak <i class="bi bi-lock-fill"></i> <b>{{$data->pin_acak}}</b></p>
            </div>
            <a data-bs-toggle="offcanvas" onclick="startCamera('pin_acak');" data-bs-target="#menu-bottom-full" href="#" class="btn gradient-blue shadow-bg shadow-bg-s mb-4 btn-camera"><i class="bi-camera-fill"></i></a>
          </div>
        </div>
      </div>
    @endif
    <button type="submit" data-bs-dismiss="offcanvas" class="mx-3 btn btn-full gradient-blue shadow-bg shadow-bg-s mb-3">Submit</button>
  </div>
</form>
@endsection

@section('menu')
<div id="menu-bottom-full" style="height:100%;" class="offcanvas offcanvas-bottom">
  <div class="d-flex m-3">
    <button class="icon icon-xs me-n2" data-bs-dismiss="offcanvas">
      <i class="bi bi-x-circle-fill color-red-dark font-16"></i>
    </button>
  </div>
  <div class="content mt-0">
    <div id="container">
      <div id="vid_container">
        <video id="video" autoplay playsinline></video>
        <div id="video_overlay"></div>
      </div>
      <div id="gui_controls">
        <button
          id="switchCameraButton"
          name="switch Camera"
          type="button"
          aria-pressed="false"></button>
        <button id="takePhotoButton" name="take Photo" type="button" data-bs-dismiss="offcanvas"></button>
        <button
          id="toggleFullScreenButton"
          name="toggle FullScreen"
          type="button"
          aria-pressed="false"></button>
      </div>
    </div>
  </div>
</div>

<div id="menu-top-detached" class="offcanvas offcanvas-top rounded-m offcanvas-detached">
  <div class="d-flex m-3">
    <div class="align-self-center">
      <h2 class="font-700 mb-0">PIN tidak sesuai? Coba PIN dibawah:</h2>
    </div>
    <div class="align-self-center ms-auto">
      <a href="#" class="icon icon-xs me-n2" data-bs-dismiss="offcanvas">
        <i class="bi bi-x-circle-fill color-red-dark font-16"></i>
      </a>
    </div>
  </div>
  <div class="content mt-0">
    @foreach($last as $no => $l)
      @if($no)
      <div class="card card-style gradient-blue shadow-bg shadow-bg-s">
        <div class="content row text-center">
          <div class="col-3">
            <span class="icon rounded-s gradient-yellow shadow-bg shadow-bg-s color-white">{{ substr($l->pin_lama,0,1) }}</span>
          </div>
          <div class="col-3">
            <span class="icon rounded-s gradient-yellow shadow-bg shadow-bg-s color-white">{{ substr($l->pin_lama,1,1) }}</span>
          </div>
          <div class="col-3">
            <span class="icon rounded-s gradient-yellow shadow-bg shadow-bg-s color-white">{{ substr($l->pin_lama,2,1) }}</span>
          </div>
          <div class="col-3">
            <span class="icon rounded-s gradient-yellow shadow-bg shadow-bg-s color-white">{{ substr($l->pin_lama,3,1) }}</span>
          </div>
        </div>
      </div>
      @endif
    @endforeach

    <div class="row text-center">
      <div class="col-6">
        <div class="form-custom text-center input-photos">
          @if (file_exists(public_path()."/upload/pin/".@$last[1]->id.".jpg"))
            <a href="/upload/pin/{{ @$last[1]->id }}.jpg">
              <img id="pin1-img" src="/upload/pin/{{ $last[1]->id }}-th.jpg" class="rounded-l"/>
            </a>
          @else
            <img id="pin1-img" src="/image/placeholder.gif" class="rounded-l"/>
          @endif
          <p>Evidence PIN 1</p>
        </div>
      </div>
      <div class="col-6 mb-n2">
        <div class="form-custom text-center input-photos">
          @if (file_exists(public_path()."/upload/pin/".@$last[2]->id.".jpg"))
            <a href="/upload/pin/{{ @$last[2]->id }}.jpg">
              <img id="pin2-img" src="/upload/pin/{{ $last[2]->id }}-th.jpg" class="rounded-l"/>
            </a>
          @else
            <img id="pin2-img" src="/image/placeholder.gif" class="rounded-l"/>
          @endif
          <p>Evidence PIN 2</p>
        </div>
      </div>
    </div>
    <a href="#" data-bs-dismiss="offcanvas" class="btn btn-full gradient-highlight shadow-bg shadow-bg-xs mt-2">Oke!</a>
  </div>
</div>
@endsection
@section('js')
  <script src="/js/adapter.min.js"></script>
  <script src="/js/screenfull.min.js"></script>
  <script src="/js/howler.core.min.js"></script>
  <script type="text/javascript">

/*

>> kasperkamperman.com - 2018-04-18
>> kasperkamperman.com - 2020-05-17
>> https://www.kasperkamperman.com/blog/camera-template/

*/

var takeSnapshotUI = createClickFeedbackUI();
let canvas = document.querySelector("#canvas");
var video;
var takePhotoButton;
var toggleFullScreenButton;
var texttextarea;
var imgshow;
var settingcamera;
var toggleFullScreenButton;
var switchCameraButton;
var amountOfCameras = 0;
var currentFacingMode = 'environment';

// this function counts the amount of video inputs
// it replaces DetectRTC that was previously implemented.
function deviceCount() {
  return new Promise(function (resolve) {
    var videoInCount = 0;

    navigator.mediaDevices
      .enumerateDevices()
      .then(function (devices) {
        devices.forEach(function (device) {
          if (device.kind === 'video') {
            device.kind = 'videoinput';
          }

          if (device.kind === 'videoinput') {
            videoInCount++;
            console.log('videocam: ' + device.label);
          }
        });

        resolve(videoInCount);
      })
      .catch(function (err) {
        console.log(err.name + ': ' + err.message);
        resolve(0);
      });
  });
}

// document.addEventListener('DOMContentLoaded', function (event) {
function startCamera(output){
  // check if mediaDevices is supported
  if (
    navigator.mediaDevices &&
    navigator.mediaDevices.getUserMedia &&
    navigator.mediaDevices.enumerateDevices
  ) {
    // first we call getUserMedia to trigger permissions
    // we need this before deviceCount, otherwise Safari doesn't return all the cameras
    // we need to have the number in order to display the switch front/back button
    navigator.mediaDevices
      .getUserMedia({
        audio: false,
        video: true,
      })
      .then(function (stream) {
        stream.getTracks().forEach(function (track) {
          track.stop();
        });

        deviceCount().then(function (deviceCount) {
          amountOfCameras = deviceCount;

          // init the UI and the camera stream
          initCameraUI(output);
          initCameraStream();
        });
      })
      .catch(function (error) {
        //https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia
        if (error === 'PermissionDeniedError') {
          alert('Permission denied. Please refresh and give permission.');
        }

        console.error('getUserMedia() error: ', error);
      });
  } else {
    alert(
      'Mobile camera is not supported by browser, or there is no camera detected/connected '+output,
    );
  }
}

function initCameraUI(output) {
  video = document.getElementById('video');
  texttextarea = document.getElementById(output+'-text');
  imgshow = document.getElementById(output+'-img');
  
  toggleFullScreenButton = document.getElementById('toggleFullScreenButton');
  switchCameraButton = document.getElementById('switchCameraButton');


  // https://developer.mozilla.org/nl/docs/Web/HTML/Element/button
  // https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques/Using_the_button_role

  
  
  // -- fullscreen part

  function fullScreenChange() {
    if (screenfull.isFullscreen) {
      toggleFullScreenButton.setAttribute('aria-pressed', true);
    } else {
      toggleFullScreenButton.setAttribute('aria-pressed', false);
    }
  }

  if (screenfull.isEnabled) {
    screenfull.on('change', fullScreenChange);

    toggleFullScreenButton.style.display = 'block';

    // set init values
    fullScreenChange();

    toggleFullScreenButton.addEventListener('click', function () {
      screenfull.toggle(document.getElementById('container')).then(function () {
        console.log(
          'Fullscreen mode: ' +
            (screenfull.isFullscreen ? 'enabled' : 'disabled'),
        );
      });
    });
  } else {
    console.log("iOS doesn't support fullscreen (yet)");
  }

  // -- switch camera part
  if (amountOfCameras > 1) {
    switchCameraButton.style.display = 'block';

    switchCameraButton.addEventListener('click', function () {
      if (currentFacingMode === 'environment') currentFacingMode = 'user';
      else currentFacingMode = 'environment';

      initCameraStream();
    });
  }

  // Listen for orientation changes to make sure buttons stay at the side of the
  // physical (and virtual) buttons (opposite of camera) most of the layout change is done by CSS media queries
  // https://www.sitepoint.com/introducing-screen-orientation-api/
  // https://developer.mozilla.org/en-US/docs/Web/API/Screen/orientation
  window.addEventListener(
    'orientationchange',
    function () {
      // iOS doesn't have screen.orientation, so fallback to window.orientation.
      // screen.orientation will
      if (screen.orientation) angle = screen.orientation.angle;
      else angle = window.orientation;

      var guiControls = document.getElementById('gui_controls').classList;
      var vidContainer = document.getElementById('vid_container').classList;

      if (angle == 270 || angle == -90) {
        guiControls.add('left');
        vidContainer.add('left');
      } else {
        if (guiControls.contains('left')) guiControls.remove('left');
        if (vidContainer.contains('left')) vidContainer.remove('left');
      }

      //0   portrait-primary
      //180 portrait-secondary device is down under
      //90  landscape-primary  buttons at the right
      //270 landscape-secondary buttons at the left
    },
    false,
  );
}

// https://github.com/webrtc/samples/blob/gh-pages/src/content/devices/input-output/js/main.js
function initCameraStream() {
  // stop any active streams in the window
  if (window.stream) {
    window.stream.getTracks().forEach(function (track) {
      console.log(track);
      track.stop();
    });
  }

  // we ask for a square resolution, it will cropped on top (landscape)
  // or cropped at the sides (landscape)
  var size = 960;
  // var size = 1280;

  var constraints = {
    audio: false,
    video: {
      // width: { ideal: size },
      // height: { ideal: size },
      width: { min: 1024, ideal: window.innerWidth, max: 1920 },
      height: { min: 776, ideal: window.innerHeight, max: 1080 },
      facingMode: currentFacingMode,
    },
  };

  navigator.mediaDevices
    .getUserMedia(constraints)
    .then(handleSuccess)
    .catch(handleError);

  function handleSuccess(stream) {
    window.stream = stream; // make stream available to browser console
    video.srcObject = stream;

    if (constraints.video.facingMode) {
      if (constraints.video.facingMode === 'environment') {
        switchCameraButton.setAttribute('aria-pressed', true);
      } else {
        switchCameraButton.setAttribute('aria-pressed', false);
      }
    }
    console.log(window.stream.getVideoTracks().length+'ada sesini');
    const track = window.stream.getVideoTracks()[0];
    const settings = track.getSettings();
    str = JSON.stringify(settings, null, 4);
    settingcamera = str;
    console.log('settings ' + str);
  }

  function handleError(error) {
    console.error('getUserMedia() error: ', error);
  }
}

function takeSnapshot() {
  // if you'd like to show the canvas add it to the DOM
  // var canvas = document.createElement('canvas');
      var ctx = canvas.getContext("2d");
      // var width = video.videoWidth;
      // var height = video.videoHeight;
      var width = JSON.parse(settingcamera).width;
      var height = JSON.parse(settingcamera).height;
      // console.log(width);
      // console.log(height);
      // JSON.parse(asd).height
      canvas.width = width;
      canvas.height = height;
      ctx.drawImage(video, 0, 0, width, height);

      // ctx.font = "40px Verdana";
      // // Create gradient
      // var gradient = ctx.createLinearGradient(0, 0, width, 0);
      // gradient.addColorStop("0"," magenta");
      // gradient.addColorStop("0.5", "blue");
      // gradient.addColorStop("1.0", "red");
      // // Fill with gradient
      // ctx.fillStyle = gradient;
      ctx.fillText("WASAKA", 10, 90);

      let image_data_url = canvas.toDataURL('image/jpeg');

      imgshow.src = image_data_url;
      texttextarea.value = image_data_url;

      console.log('b');
      if (window.stream) {
        window.stream.getTracks().forEach(function (track) {
          console.log(track);
          track.stop();
        });
      }
  // polyfil if needed https://github.com/blueimp/JavaScript-Canvas-to-Blob

  // https://developers.google.com/web/fundamentals/primers/promises
  // https://stackoverflow.com/questions/42458849/access-blob-value-outside-of-canvas-toblob-async-function
  // function getCanvasBlob(canvas) {
  //   return new Promise(function (resolve, reject) {
  //     canvas.toBlob(function (blob) {
  //       resolve(blob);
  //     }, 'image/jpeg');
  //   });
  // }

  // some API's (like Azure Custom Vision) need a blob with image data
  // getCanvasBlob(canvas).then(function (blob) {
  //   // do something with the image blob

  // });
}

// https://hackernoon.com/how-to-use-javascript-closures-with-confidence-85cd1f841a6b
// closure; store this in a variable and call the variable as function
// eg. var takeSnapshotUI = createClickFeedbackUI();
// takeSnapshotUI();

function createClickFeedbackUI() {
  // in order to give feedback that we actually pressed a button.
  // we trigger a almost black overlay
  var overlay = document.getElementById('video_overlay'); //.style.display;

  // sound feedback
  var sndClick = new Howl({ src: ['/snd/click.mp3'] });

  var overlayVisibility = false;
  var timeOut = 80;

  function setFalseAgain() {
    overlayVisibility = false;
    overlay.style.display = 'none';
  }

  return function () {
    if (overlayVisibility == false) {
      sndClick.play();
      overlayVisibility = true;
      overlay.style.display = 'block';
      setTimeout(setFalseAgain, timeOut);
    }
  };
}
document.getElementById('takePhotoButton').addEventListener('click', function () {
    takeSnapshotUI();
    takeSnapshot();
    
  });
    </script>
@endsection