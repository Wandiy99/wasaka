@extends('mobile.layout')
@section('title')
    Permohonan Pinjam
@endsection
@section('css')
  <link rel="stylesheet" type="text/css" href="/camerastyle.css" />
  <style type="text/css">
    .input-photos img {
      width: 100px;
      height: 150px;
      margin-bottom: 5px;
    }
  </style>
@endsection
@section('content')
  <div class="pt-3">
    <div class="page-title d-flex">
      <div class="align-self-center">
        <a href="#"
        data-back-button
        class="me-3 ms-0 icon icon-xxs bg-theme rounded-s shadow-m">
          <i class="bi bi-chevron-left color-theme font-14"></i>
        </a>
      </div>
      <div class="align-self-center me-auto">
        <h1 class="color-theme mb-0 font-18">Form Pinjam Splicer</h1>
      </div>
      <div class="align-self-center ms-auto">
        <a href="#"
        data-bs-toggle="dropdown"
        class="icon gradient-blue color-white shadow-bg shadow-bg-s rounded-m">
           <i class="bi bi-menu-button-fill font-17"></i>
        </a>
        <!-- Page Title Dropdown Menu-->
        <div class="dropdown-menu">
          <div class="card card-style shadow-m mt-1 me-1">
            <div class="list-group list-custom list-group-s list-group-flush rounded-xs px-3 py-1">
              <a href="/loker/start" class="list-group-item">
                <i class="has-bg gradient-red shadow-bg shadow-bg-xs color-white rounded-xs bi bi-power"></i>
                <strong class="font-13">Start</strong>
              </a>
              <a href="/loker/close" class="list-group-item">
                <i class="has-bg gradient-red shadow-bg shadow-bg-xs color-white rounded-xs bi bi-power"></i>
                <strong class="font-13">Close</strong>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card card-style gradient-magenta shadow-card shadow-card-l">
    <div class="content">
      <div class="row">
        <div class="col-6 color-white fw-bold">SO</div>
        <div class="col-6 color-white fw-bold text-end">{{ $info->so }}</div>
      </div>
      <div class="row">
        <div class="col-6 color-white fw-bold">Loker Number</div>
        <div class="col-6 color-white fw-bold text-end">{{ $info->kode }}</div>
      </div>
      <div class="row">
        <div class="col-6 color-white fw-bold">Splicer SN</div>
        <div class="col-6 color-white fw-bold text-end">{{ $info->sn_splicer }}</div>
      </div>
      <div class="row">
        <div class="col-6 color-white fw-bold">Splicer Merk</div>
        <div class="col-6 color-white fw-bold text-end">{{ $info->merk }}</div>
      </div>
    </div>
  </div>

      <!-- <div class="align-self-center">
        <h5 class="pt-1 mb-n2 color-white">SO</h5>
        <h5 class="pt-1 mb-n2 color-white">Loker Number</h5>
        <h5 class="pt-1 mb-n2 color-white">Splicer SN</h5>
        <h5 class="pt-1 mb-n2 color-white">Splicer Merk</h5>
      </div>
      <div class="align-self-center ms-auto text-end">
        <h4 class="pt-1 mb-n2 color-white">{{ $info->so }}</h4>
        <h4 class="pt-1 mb-n2 color-white">{{ $info->kode  }}</h4>
        <h4 class="pt-1 mb-n2 color-white">{{ $info->sn_splicer  }}</h4>
        <h4 class="pt-1 mb-n2 color-white">{{ $info->merk  }}</h4>
      </div> -->
  <div class="card card-style">
    <div class="content mb-0">

        <!-- Tab Wrapper-->
      <div class="tabs tabs-pill" id="tab-group-2">
        <!-- Tab Controls -->
        <div class="tab-controls rounded-m p-1">
          <a class="font-13 rounded-m" data-bs-toggle="collapse" href="#tab-4" aria-expanded="true">Request</a>
          <a class="font-13 rounded-m" data-bs-toggle="collapse" href="#tab-5" aria-expanded="false">History</a>
        </div>

        <!-- Tab 1 -->
        <div class="mt-3"></div>
        <div class="collapse show" id="tab-4" data-bs-parent="#tab-group-2">
          <form id="startform" class="form form-horizontal" method="post" enctype="multipart/form-data" action="/loker/start/{{ Request::segment(4) }}">
            <div class="form-custom form-label form-icon">
              <i class="bi bi-rulers font-14"></i>
              <select name="jenis_pekerjaan_id" class="form-select rounded-xs" id="jenis_pekerjaan_id" aria-label="Floating label select example">
                <option value="">Pilih Pekerjaan</option>
                @foreach($pekerjaan as $p)
                  <option value="{{ $p->id }}" {{ old('jenis_pekerjaan_id')==$p->id ?'selected': '' }}>{{ $p->jenis_pekerjaan }}</option>
                @endforeach
              </select>
              <label for="jenis_pekerjaan_id" class="form-label-always-active color-highlight font-11">Jenis Pekerjaan</label>
            </div>
            <div class="row">
              <?php
                $state = old('items');
              ?>
              <div class="list-group list-custom list-group-m list-group-flush rounded-xs col-8 ps-3">
                @foreach($kelengkapan as $k)
                  <a href="#" class="list-group-item" data-trigger-switch="{{ $k->item }}">
                    <div><strong> {{ ucwords(str_replace('_',' ',$k->item)) }}</strong></div>
                    <div class="form-switch ios-switch switch-green switch-s">
                      
                      <input name="items[{{ $k->item }}]" type="checkbox" class="ios-input" id="{{ $k->item }}" value="{{ $k->id }}" {{ isset($state[$k->item])?'checked': '' }} />
                      <label class="custom-control-label" for="{{ $k->item }}"></label>
                    </div>
                  </a>
                @endforeach
              </div>
              <div class="col-4 text-center mt-3">
                <div class="form-custom input-photos">
                  <img id="depan_so-img" src="{{ old('depan_so') ?: '/image/placeholder.gif' }}" class="rounded-l" />
                  <textarea id="depan_so-text" name="depan_so" hidden>{{ old('depan_so') ?: '' }}</textarea>
                  <p>Foto Depan SO</p>
                </div>
                <a data-bs-toggle="offcanvas" onclick="startCamera('depan_so');" data-bs-target="#menu-bottom-full" href="#" class="btn gradient-blue shadow-bg shadow-bg-s mb-4 btn-camera"><i class="bi-camera-fill"></i></a>
              </div>
            </div>
            
            <a href="#" id="button_submit" class="btn btn-full gradient-highlight shadow-bg shadow-bg-s mt-3 mb-3">Submit Request</a>
            <a href="#" id="button_proses" class="btn btn-full gradient-green shadow-bg shadow-bg-s mt-3 mb-3 d-none">Processing Request</a>
            <canvas id="canvas" hidden></canvas>
          </form>
        </div>

        <!-- Tab 2-->
        <div class="collapse" id="tab-5" data-bs-parent="#tab-group-2">
          <div class="list-group list-custom list-group-m list-group-flush rounded-xs">
            <a href="#" class="list-group-item">
              <i class="has-bg gradient-green color-white rounded-xs bi bi-cash-coin"></i>
              <div><strong>User Name</strong><span>Date Start</span></div>
              <span class="badge bg-transparent color-theme text-end font-15">
               Duration<br>
               <em class="fst-normal font-12 opacity-30">Date Finish</em>
              </span>
            </a>
            <a href="#" class="list-group-item">
              <i class="has-bg gradient-yellow color-white rounded-xs bi bi-droplet"></i>
              <div><strong>User Name</strong><span>Date Start</span></div>
              <span class="badge bg-transparent color-theme text-end font-15">
               Duration<br>
               <em class="fst-normal font-12 opacity-30">Date Finish</em>
              </span>
            </a>
            <a href="#" class="list-group-item">
              <i class="has-bg gradient-blue color-white rounded-xs bi bi-bag"></i>
              <div><strong>User Name</strong><span>Date Start</span></div>
              <span class="badge bg-transparent color-theme text-end font-15">
               Duration<br>
               <em class="fst-normal font-12 opacity-30">Date Finish</em>
              </span>
            </a>
            <a href="#" class="list-group-item">
              <i class="has-bg gradient-red color-white rounded-xs bi bi-gear"></i>
              <div><strong>User Name</strong><span>Date Start</span></div>
              <span class="badge bg-transparent color-theme text-end font-15">
               Duration<br>
               <em class="fst-normal font-12 opacity-30">Date Finish</em>
              </span>
            </a>
            <a href="#" class="list-group-item">
              <i class="has-bg gradient-magenta color-white rounded-xs bi bi-shuffle"></i>
              <div><strong>User Name</strong><span>Date Start</span></div>
              <span class="badge bg-transparent color-theme text-end font-15">
               Duration<br>
               <em class="fst-normal font-12 opacity-30">Date Finish</em>
              </span>
            </a>
          </div>
        </div>
        <!-- End of Tabs-->
      </div>
      <!-- End of Tab Wrapper-->
    </div>
  </div>
  
@endsection
@section('menu')
  <div id="menu-bottom-full" style="height:100%;" class="offcanvas offcanvas-bottom">
    <div class="d-flex m-3">
      <button class="icon icon-xs me-n2" data-bs-dismiss="offcanvas">
        <i class="bi bi-x-circle-fill color-red-dark font-16"></i>
      </button>
    </div>
    <div class="content mt-0">
      <div id="container">
        <div id="vid_container">
          <video id="video" autoplay playsinline></video>
          <div id="video_overlay"></div>
        </div>
        <div id="gui_controls">
          <button
            id="switchCameraButton"
            name="switch Camera"
            type="button"
            aria-pressed="false"
          ></button>
          <button id="takePhotoButton" name="take Photo" type="button" data-bs-dismiss="offcanvas"></button>

        </div>
      </div>
    </div>
  </div>
  <div id="preview" style="height:100%;" class="offcanvas offcanvas-bottom">
    <div class="d-flex m-3">
      <button class="icon icon-xs me-n2" data-bs-dismiss="offcanvas">
        <i class="bi bi-x-circle-fill color-red-dark font-16"></i>
      </button>
    </div>
    <div class="content mt-0">
      <div id="container" class="input-photos">
        <img src="/image/placeholder.gif" />
      </div>
    </div>
  </div>
  
@endsection
@section('js')
  <script>
    document.getElementById('button_submit').onclick = function(){
        document.getElementById('button_submit').classList.add('d-none');
        document.getElementById('button_proses').classList.remove('d-none');
        document.getElementById('startform').submit();
    }
  </script>
  <script src="/js/adapter.min.js"></script>
  <script src="/js/screenfull.min.js"></script>
  <script src="/js/howler.core.min.js"></script>
  <script type="text/javascript">

/*

>> kasperkamperman.com - 2018-04-18
>> kasperkamperman.com - 2020-05-17
>> https://www.kasperkamperman.com/blog/camera-template/

*/

var takeSnapshotUI = createClickFeedbackUI();
let canvas = document.querySelector("#canvas");
let imgurl = document.querySelector("#imgurl");
var video;
var takePhotoButton;
var texttextarea;
var imgshow;
var settingcamera;
var toggleFullScreenButton;
var switchCameraButton;
var amountOfCameras = 0;
var currentFacingMode = 'environment';

// this function counts the amount of video inputs
// it replaces DetectRTC that was previously implemented.
function deviceCount() {
  return new Promise(function (resolve) {
    var videoInCount = 0;

    navigator.mediaDevices
      .enumerateDevices()
      .then(function (devices) {
        devices.forEach(function (device) {
          if (device.kind === 'video') {
            device.kind = 'videoinput';
          }

          if (device.kind === 'videoinput') {
            videoInCount++;
            console.log('videocam: ' + device.label);
          }
        });

        resolve(videoInCount);
      })
      .catch(function (err) {
        console.log(err.name + ': ' + err.message);
        resolve(0);
      });
  });
}

function startCamera(output){
  // check if mediaDevices is supported
  if (
    navigator.mediaDevices &&
    navigator.mediaDevices.getUserMedia &&
    navigator.mediaDevices.enumerateDevices
  ) {
    // first we call getUserMedia to trigger permissions
    // we need this before deviceCount, otherwise Safari doesn't return all the cameras
    // we need to have the number in order to display the switch front/back button
    navigator.mediaDevices
      .getUserMedia({
        audio: false,
        video: true,
      })
      .then(function (stream) {
        stream.getTracks().forEach(function (track) {
          track.stop();
        });

        deviceCount().then(function (deviceCount) {
          amountOfCameras = deviceCount;

          // init the UI and the camera stream
          initCameraUI(output);
          initCameraStream();
        });
      })
      .catch(function (error) {
        //https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia
        if (error === 'PermissionDeniedError') {
          alert('Permission denied. Please refresh and give permission.');
        }

        console.error('getUserMedia() error: ', error);
      });
  } else {
    alert(
      'Mobile camera is not supported by browser, or there is no camera detected/connected',
    );
  }
}

function initCameraUI(output) {
  video = document.getElementById('video');

  texttextarea = document.getElementById(output+'-text');
  imgshow = document.getElementById(output+'-img');
  takePhotoButton = document.getElementById('takePhotoButton');
  switchCameraButton = document.getElementById('switchCameraButton');

  // https://developer.mozilla.org/nl/docs/Web/HTML/Element/button
  // https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques/Using_the_button_role

  // -- fullscreen part





  // -- switch camera part
  if (amountOfCameras > 1) {
    switchCameraButton.style.display = 'block';

    switchCameraButton.addEventListener('click', function () {
      if (currentFacingMode === 'environment') currentFacingMode = 'user';
      else currentFacingMode = 'environment';

      initCameraStream();
    });
  }

  window.addEventListener(
    'orientationchange',
    function () {
      // iOS doesn't have screen.orientation, so fallback to window.orientation.
      // screen.orientation will
      if (screen.orientation) angle = screen.orientation.angle;
      else angle = window.orientation;

      var guiControls = document.getElementById('gui_controls').classList;
      var vidContainer = document.getElementById('vid_container').classList;

      if (angle == 270 || angle == -90) {
        guiControls.add('left');
        vidContainer.add('left');
      } else {
        if (guiControls.contains('left')) guiControls.remove('left');
        if (vidContainer.contains('left')) vidContainer.remove('left');
      }
      //0   portrait-primary
      //180 portrait-secondary device is down under
      //90  landscape-primary  buttons at the right
      //270 landscape-secondary buttons at the left
    },
    false,
  );
}

// https://github.com/webrtc/samples/blob/gh-pages/src/content/devices/input-output/js/main.js
function initCameraStream() {
  // stop any active streams in the window
  if (window.stream) {
    window.stream.getTracks().forEach(function (track) {
      console.log(track);
      track.stop();
    });
  }

  // we ask for a square resolution, it will cropped on top (landscape)
  // or cropped at the sides (landscape)
  
  var size = 960;
  // var size = 1280;

  var constraints = {
    audio: false,
    video: {
      // width: { ideal: size },
      // height: { ideal: size },
      width: { min: 1024, ideal: window.innerWidth, max: 1920 },
      height: { min: 776, ideal: window.innerHeight, max: 1080 },
      facingMode: currentFacingMode,
    },
  };

  navigator.mediaDevices
    .getUserMedia(constraints)
    .then(handleSuccess)
    .catch(handleError);

  function handleSuccess(stream) {
    window.stream = stream;
    video.srcObject = stream;

    if (constraints.video.facingMode) {
      if (constraints.video.facingMode === 'environment') {
        switchCameraButton.setAttribute('aria-pressed', true);
      } else {
        switchCameraButton.setAttribute('aria-pressed', false);
      }
    }

    const track = window.stream.getVideoTracks()[0];
    const settings = track.getSettings();
    str = JSON.stringify(settings, null, 4);
    settingcamera = str;
    console.log('settings ' + str);
  }

  function handleError(error) {
    alert('Resolusi Kamera Tidak Support!');
    console.error('getUserMedia() error: ', error);
  }
}

function takeSnapshot() {
  var ctx = canvas.getContext("2d");
  // var width = video.videoWidth;
  // var height = video.videoHeight;
  var width = JSON.parse(settingcamera).width;
  var height = JSON.parse(settingcamera).height;
  canvas.width = width;
  canvas.height = height;
  ctx.drawImage(video, 0, 0, width, height);

  ctx.font = "40px Verdana";
  ctx.fillText("WASAKA", 10, 90);
  let image_data_url = canvas.toDataURL('image/jpeg');
  
  imgshow.src = image_data_url;
  texttextarea.value = image_data_url;

  if (window.stream) {
    window.stream.getTracks().forEach(function (track) {
      console.log(track);
      track.stop();
    });
  }
}
function createClickFeedbackUI() {
  var overlay = document.getElementById('video_overlay'); //.style.display;
  var sndClick = new Howl({ src: ['/snd/click.mp3'] });
  var overlayVisibility = false;
  var timeOut = 80;

  function setFalseAgain() {
    overlayVisibility = false;
    overlay.style.display = 'none';
  }

  return function () {
    if (overlayVisibility == false) {
      sndClick.play();
      overlayVisibility = true;
      overlay.style.display = 'block';
      setTimeout(setFalseAgain, timeOut);
    }
  };
}

document.getElementById('takePhotoButton').addEventListener('click', function () {
  takeSnapshotUI();
  takeSnapshot();
  
});
    </script>
@endsection