@extends('mobile.layout')
@section('title')
    Form Closing
@endsection
@section('css')
  <link rel="stylesheet" type="text/css" href="/camerastyle.css" />
  <style type="text/css">
    .input-photos img {
      width: 100px;
      height: 150px;
      margin-bottom: 5px;
    }
  </style>
@endsection
@section('content')
<div class="pt-3">
  <div class="page-title d-flex">
    <div class="align-self-center">
      <a href="#"
      data-back-button
      class="me-3 ms-0 icon icon-xxs bg-theme rounded-s shadow-m">
          <i class="bi bi-chevron-left color-theme font-14"></i>
      </a>
    </div>
    <div class="align-self-center me-auto">
      <h1 class="color-theme mb-0 font-18">Form Return Splicer</h1>
    </div>
    <div class="align-self-center ms-auto">
      <a href="#"
      data-bs-toggle="dropdown"
      class="icon gradient-blue color-white shadow-bg shadow-bg-s rounded-m">
         <i class="bi bi-menu-button-fill font-17"></i>
      </a>
      <!-- Page Title Dropdown Menu-->
      <div class="dropdown-menu">
        <div class="card card-style shadow-m mt-1 me-1">
          <div class="list-group list-custom list-group-s list-group-flush rounded-xs px-3 py-1">
            <a href="/loker/start" class="list-group-item">
              <i class="has-bg gradient-red shadow-bg shadow-bg-xs color-white rounded-xs bi bi-power"></i>
              <strong class="font-13">Start</strong>
            </a>
            <a href="/loker/close" class="list-group-item">
              <i class="has-bg gradient-red shadow-bg shadow-bg-xs color-white rounded-xs bi bi-power"></i>
              <strong class="font-13">Close</strong>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<canvas id="canvas" hidden></canvas>

<div class="card card-style gradient-magenta shadow-card shadow-card-l">
  <div class="content">
    <div class="row">
      <div class="col-6 color-white fw-bold">SO</div>
      <div class="col-6 color-white fw-bold text-end">{{ $info->so }}</div>
    </div>
    <div class="row">
      <div class="col-6 color-white fw-bold">Loker Number</div>
      <div class="col-6 color-white fw-bold text-end">{{ $info->kode }}</div>
    </div>
    <div class="row">
      <div class="col-6 color-white fw-bold">Splicer SN</div>
      <div class="col-6 color-white fw-bold text-end">{{ $info->sn_splicer }}</div>
    </div>
    <div class="row">
      <div class="col-6 color-white fw-bold">Splicer Merk</div>
      <div class="col-6 color-white fw-bold text-end">{{ $info->merk }}</div>
    </div>
    <div class="row">
      <div class="col-6 color-white fw-bold">Status</div>
      <?php
        switch ($info->step) {
        case "1":
          $status = "Permohonan Pinjam";
          break;
        case "2":
          $status = "Reject Pinjam";
          break;
        case "3":
          $status = "Dipinjam";
          break;
        case "4":
          $status = "Permohonan Return";
          break;
        case "5":
          $status = "Reject Return";
          break;
        case "6":
          $status = "Returned";
          break;
        default:
          $status = "Not Found";
      }
      ?>
      <div class="col-6 color-white fw-bold text-end">{{ $status }}</div>
    </div>
  </div>
</div>
@if($info->step == 3 || $info->step == 5)
<form id="closeform" class="form form-horizontal" method="post" enctype="multipart/form-data">
  <div class="card card-style shadow-bg shadow-bg-s">
    <div class="content">
      <div class="form-custom form-label form-icon">
        <i class="bi bi-pencil-fill color-blue-dark font-14"></i>
        <input type="text" class="form-control rounded-xs" id="arc" placeholder="Isi Arc Count" name="arc" value="{{ old('arc') ?: '' }}" autocomplete="off" />
        <label for="arc" class="form-label-always-active color-highlight font-11">Arc</label>
      </div>
      <div class="row text-center">
        <div class="col-6">
          <div class="form-custom text-center input-photos">
            <img id="evidence_arc-img" src="{{ old('evidence_arc') ?: '/image/placeholder.gif' }}" class="rounded-l" />
            <textarea id="evidence_arc-text" name="evidence_arc" hidden>{{ old('evidence_arc') ?: '' }}</textarea>
            <p>Arc</p>
          </div>
          <a data-bs-toggle="offcanvas" onclick="startCamera('evidence_arc');" data-bs-target="#menu-bottom-full" href="#" class="btn gradient-blue shadow-bg shadow-bg-s btn-camera"><i class="bi-camera-fill"></i></a>
        </div>
        <div class="col-6">
          <div class="form-custom text-center input-photos">
            <img id="evidence_penyerahan-img" src="{{ old('evidence_penyerahan') ?: '/image/placeholder.gif' }}" class="rounded-l" />
            <textarea id="evidence_penyerahan-text" name="evidence_penyerahan" hidden>{{ old('evidence_penyerahan') ?: '' }}</textarea>
            <p>Penyerahan</p>
          </div>
          <a data-bs-toggle="offcanvas" onclick="startCamera('evidence_penyerahan');" data-bs-target="#menu-bottom-full" href="#" class="btn gradient-blue shadow-bg shadow-bg-s mb-4 btn-camera"><i class="bi-camera-fill"></i></a>
        </div>
      </div>
      <div class="form-custom form-label form-icon">
        <i class="bi bi-code-square font-14 pull-left"></i>
        <textarea name="catatan" class="form-control rounded-xs" id="catatan" placeholder="Isi Catatan">{{ old('catatan') ?: '' }}</textarea>
        <label for="catatan" class="form-label-always-active color-highlight font-11">Catatan</label>
      </div>
      <div class="row text-center">
        <?php
          $state = old('ratings');
        ?>
        @foreach($kelengkapan as $k)
          <div class="col-6 mb-3">
            <h5 class="card-title">{{ ucwords(str_replace('_',' ',$k->item)) }}</h5>
            <input type="radio" class="btn-check" name="ratings[{{ $k->id }}]" id="{{ $k->item }}_good" value="good" autocomplete="off"  {{ isset($state[$k->id])?($state[$k->id]=='good' ?'checked': ''):'' }} />
            <label class="btn btn-outline-success color-black btn-xs" for="{{ $k->item }}_good">
              <span class="icon icon-xs rounded-m gradient-yellow shadow-bg shadow-bg-xs"><i class="bi bi-emoji-smile"></i></span>
              <br/>Good
            </label>
            <input type="radio" class="btn-check" name="ratings[{{ $k->id }}]" id="{{ $k->item }}_bad" value="bad" autocomplete="off" {{ isset($state[$k->id])?($state[$k->id]=='bad' ?'checked': ''):'' }} />
            <label class="btn btn-outline-danger color-black btn-xs" for="{{ $k->item }}_bad">
              <span class="icon icon-xs rounded-m gradient-yellow shadow-bg shadow-bg-xs"><i class="bi bi-emoji-frown"></i></span>
              <br/>Bad
            </label>
          </div>
        @endforeach
      </div>
      <a href="#" id="button_submit" class="btn btn-full gradient-highlight shadow-bg shadow-bg-s mt-3 mb-3">Submit Request</a>
      <a href="#" id="button_proses" class="btn btn-full gradient-green shadow-bg shadow-bg-s mt-3 mb-3 d-none">Processing Request</a>
    </div>
  </div>
</form>
@endif
@endsection

@section('menu')
<div id="menu-bottom-full" style="height:100%;" class="offcanvas offcanvas-bottom">
  <div class="d-flex m-3">
    <button class="icon icon-xs me-n2" data-bs-dismiss="offcanvas">
      <i class="bi bi-x-circle-fill color-red-dark font-16"></i>
    </button>
  </div>
  <div class="content mt-0">
    <div id="container">
      <div id="vid_container">
        <video id="video" autoplay playsinline></video>
        <div id="video_overlay"></div>
      </div>
      <div id="gui_controls">
        <button
            id="switchCameraButton"
            name="switch Camera"
            type="button"
            aria-pressed="false"
          ></button>
        <button id="takePhotoButton" name="take Photo" type="button" data-bs-dismiss="offcanvas"></button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
  <script>
    document.getElementById('button_submit').onclick = function(){
      document.getElementById('button_submit').classList.add('d-none');
      document.getElementById('button_proses').classList.remove('d-none');
      document.getElementById('closeform').submit();
    }
  </script>
  <script src="/js/adapter.min.js"></script>
  <script src="/js/screenfull.min.js"></script>
  <script src="/js/howler.core.min.js"></script>
  <script type="text/javascript">
var takeSnapshotUI = createClickFeedbackUI();
let canvas = document.querySelector("#canvas");
let imgurl = document.querySelector("#imgurl");
var video;
var takePhotoButton;
var texttextarea;
var imgshow;
var settingcamera;
var toggleFullScreenButton;
var switchCameraButton;
var amountOfCameras = 0;
var currentFacingMode = 'environment';

// this function counts the amount of video inputs
// it replaces DetectRTC that was previously implemented.
function deviceCount() {
  return new Promise(function (resolve) {
    var videoInCount = 0;

    navigator.mediaDevices
      .enumerateDevices()
      .then(function (devices) {
        devices.forEach(function (device) {
          if (device.kind === 'video') {
            device.kind = 'videoinput';
          }

          if (device.kind === 'videoinput') {
            videoInCount++;
            console.log('videocam: ' + device.label);
          }
        });

        resolve(videoInCount);
      })
      .catch(function (err) {
        console.log(err.name + ': ' + err.message);
        resolve(0);
      });
  });
}

function startCamera(output){
  // check if mediaDevices is supported
  if (
    navigator.mediaDevices &&
    navigator.mediaDevices.getUserMedia &&
    navigator.mediaDevices.enumerateDevices
  ) {
    // first we call getUserMedia to trigger permissions
    // we need this before deviceCount, otherwise Safari doesn't return all the cameras
    // we need to have the number in order to display the switch front/back button
    navigator.mediaDevices
      .getUserMedia({
        audio: false,
        video: true,
      })
      .then(function (stream) {
        stream.getTracks().forEach(function (track) {
          track.stop();
        });

        deviceCount().then(function (deviceCount) {
          amountOfCameras = deviceCount;

          // init the UI and the camera stream
          initCameraUI(output);
          initCameraStream();
        });
      })
      .catch(function (error) {
        //https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia
        if (error === 'PermissionDeniedError') {
          alert('Permission denied. Please refresh and give permission.');
        }

        console.error('getUserMedia() error: ', error);
      });
  } else {
    alert(
      'Mobile camera is not supported by browser, or there is no camera detected/connected',
    );
  }
}

function initCameraUI(output) {
  video = document.getElementById('video');

  texttextarea = document.getElementById(output+'-text');
  imgshow = document.getElementById(output+'-img');
  takePhotoButton = document.getElementById('takePhotoButton');
  switchCameraButton = document.getElementById('switchCameraButton');

  // https://developer.mozilla.org/nl/docs/Web/HTML/Element/button
  // https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques/Using_the_button_role

  // -- fullscreen part





  // -- switch camera part
  if (amountOfCameras > 1) {
    switchCameraButton.style.display = 'block';

    switchCameraButton.addEventListener('click', function () {
      if (currentFacingMode === 'environment') currentFacingMode = 'user';
      else currentFacingMode = 'environment';

      initCameraStream();
    });
  }

  window.addEventListener(
    'orientationchange',
    function () {
      // iOS doesn't have screen.orientation, so fallback to window.orientation.
      // screen.orientation will
      if (screen.orientation) angle = screen.orientation.angle;
      else angle = window.orientation;

      var guiControls = document.getElementById('gui_controls').classList;
      var vidContainer = document.getElementById('vid_container').classList;

      if (angle == 270 || angle == -90) {
        guiControls.add('left');
        vidContainer.add('left');
      } else {
        if (guiControls.contains('left')) guiControls.remove('left');
        if (vidContainer.contains('left')) vidContainer.remove('left');
      }
      //0   portrait-primary
      //180 portrait-secondary device is down under
      //90  landscape-primary  buttons at the right
      //270 landscape-secondary buttons at the left
    },
    false,
  );
}

// https://github.com/webrtc/samples/blob/gh-pages/src/content/devices/input-output/js/main.js
function initCameraStream() {
  // stop any active streams in the window
  if (window.stream) {
    window.stream.getTracks().forEach(function (track) {
      console.log(track);
      track.stop();
    });
  }

  // we ask for a square resolution, it will cropped on top (landscape)
  // or cropped at the sides (landscape)
  
  var size = 960;
  // var size = 1280;

  var constraints = {
    audio: false,
    video: {
      // width: { ideal: size },
      // height: { ideal: size },
      width: { min: 1024, ideal: window.innerWidth, max: 1920 },
      height: { min: 776, ideal: window.innerHeight, max: 1080 },
      facingMode: currentFacingMode,
    },
  };

  navigator.mediaDevices
    .getUserMedia(constraints)
    .then(handleSuccess)
    .catch(handleError);

  function handleSuccess(stream) {
    window.stream = stream;
    video.srcObject = stream;

    if (constraints.video.facingMode) {
      if (constraints.video.facingMode === 'environment') {
        switchCameraButton.setAttribute('aria-pressed', true);
      } else {
        switchCameraButton.setAttribute('aria-pressed', false);
      }
    }

    const track = window.stream.getVideoTracks()[0];
    const settings = track.getSettings();
    str = JSON.stringify(settings, null, 4);
    settingcamera = str;
    console.log('settings ' + str);
  }

  function handleError(error) {
    alert('Resolusi Kamera Tidak Support!');
    console.error('getUserMedia() error: ', error);
  }
}

function takeSnapshot() {
  var ctx = canvas.getContext("2d");
  // var width = video.videoWidth;
  // var height = video.videoHeight;
  var width = JSON.parse(settingcamera).width;
  var height = JSON.parse(settingcamera).height;
  canvas.width = width;
  canvas.height = height;
  ctx.drawImage(video, 0, 0, width, height);

  ctx.font = "40px Verdana";
  ctx.fillText("WASAKA", 10, 90);
  let image_data_url = canvas.toDataURL('image/jpeg');
  
  imgshow.src = image_data_url;
  texttextarea.value = image_data_url;

  if (window.stream) {
    window.stream.getTracks().forEach(function (track) {
      console.log(track);
      track.stop();
    });
  }
}
function createClickFeedbackUI() {
  var overlay = document.getElementById('video_overlay'); //.style.display;
  var sndClick = new Howl({ src: ['/snd/click.mp3'] });
  var overlayVisibility = false;
  var timeOut = 80;

  function setFalseAgain() {
    overlayVisibility = false;
    overlay.style.display = 'none';
  }

  return function () {
    if (overlayVisibility == false) {
      sndClick.play();
      overlayVisibility = true;
      overlay.style.display = 'block';
      setTimeout(setFalseAgain, timeOut);
    }
  };
}

document.getElementById('takePhotoButton').addEventListener('click', function () {
  takeSnapshotUI();
  takeSnapshot();
  
});
    </script>
@endsection