@foreach($data as $o)
  <div class="card shadow-bg shadow-bg-s mb-2">
    <div class="content">
      <a href="#" class="d-flex py-0">
        <div class="align-self-center">
          <span class="icon rounded-s me-2 gradient-yellow shadow-bg shadow-bg-s color-white">{{ $o->id }}</span>
        </div>
        <div class="align-self-center ms-auto text-end">
          <h5 class="pt-1 mb-n1">{{ $o->nama }}</h5>
          <p class="mb-0 font-11">{{ date("d F Y H:i:s", strtotime($o->date_start)) }} <b class="mb-0 font-11">({{ $o->durasi }})</b></p>
        </div>
      </a>
      <p class="mb-0 font-11 ms-auto text-end">{{ $o->jenis_pekerjaan }}</p>
    </div>
  </div>
@endforeach