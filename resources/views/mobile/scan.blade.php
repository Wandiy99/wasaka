@extends('mobile.layout')
@section('title')
  Scan
@endsection
@section('content')
  <!-- Page Title-->
  <div class="pt-3">
    <div class="page-title d-flex">
      <div class="align-self-center me-auto">
        <p class="color-highlight header-date"></p>
        <h1>Scan Alpro</h1>
      </div>
      <div class="align-self-center ms-auto">
          
      </div>
    </div>
  </div>
  @if(count($ogp))
    <!-- Account Activity Title-->
    <div class="content my-0 mt-n2 px-1">
      <div class="d-flex">
        <div class="align-self-center">
          <h3 class="font-16 mb-2">Silahkan Close Aktifitas dibawah ini</h3>
        </div>
      </div>
    </div>

    <!--Account Activity Notification-->
    @foreach($ogp as $o)
      @if($o->step)
        <div class="card card-style gradient-orange shadow-bg shadow-bg-s">
          <div class="content">
            <a href="/loker/close/{{$o->id}}" class="d-flex py-1">
              <div class="align-self-center">
                <span class="icon rounded-s me-2 gradient-yellow shadow-bg shadow-bg-s color-white">{{ $o->id }}</span>
              </div>
              <div class="align-self-center ms-auto text-end">
                <h5 class="pt-1 mb-n1 color-white">{{ $o->kode }}</h5>
                <p class="mb-0 font-11 color-white">{{ date("d F Y H:i:s", strtotime($o->date_start)) }} <b class="mb-0 font-11">({{ $o->durasi }})</b></p>
              </div>
            </a>
            <p class="mb-0 font-11 ms-auto text-end color-white">{{ $o->jenis_pekerjaan }}</p>
          </div>
        </div>
      @else
        <div class="card card-style gradient-orange shadow-bg shadow-bg-s">
          <div class="content">
            <a href="/mobile/work/{{$o->id}}/close" class="d-flex py-1">
              <div class="align-self-center">
                <span class="icon rounded-s me-2 gradient-yellow shadow-bg shadow-bg-s color-white">{{ $o->id }}</span>
              </div>
              <div class="align-self-center ms-auto text-end">
                <h5 class="pt-1 mb-n1 color-white">{{ $o->kode }}</h5>
                <p class="mb-0 font-11 color-white">{{ date("d F Y H:i:s", strtotime($o->date_start)) }} <b class="mb-0 font-11">({{ $o->durasi }})</b></p>
              </div>
            </a>
            <p class="mb-0 font-11 ms-auto text-end color-white">{{ $o->jenis_pekerjaan }}</p>
          </div>
        </div>
      @endif
    @endforeach
  @else
    <!--Account Activity Notification-->
    <div class="card card-style gradient-orange shadow-bg shadow-bg-s">
      <div class="content">
        <div id="qr-reader"></div>
      </div>
    </div>
  @endif
@endsection
@section('js')
<script src="/scripts/html5-qrcode.min.js"></script>
<script type="text/javascript">
  function onScanSuccess(decodedText, decodedResult) {
    // handle the scanned code as you like, for example:

    console.log(`Code matched = ${decodedText}`, decodedResult);
    window.location.href = document.location.origin+"/mobile/work/start/"+decodedText.split('/')[6];
    alert('gotolink');
  }

  function onScanFailure(error) {
    // handle scan failure, usually better to ignore and keep scanning.
    // for example:
    console.warn(`Code scan error = ${error}`);
  }

  let html5QrcodeScanner = new Html5QrcodeScanner(
    "qr-reader",
    { fps: 10, qrbox: {width: 250, height: 250} },
    /* verbose= */ false);
  html5QrcodeScanner.render(onScanSuccess, onScanFailure);

</script>
@endsection