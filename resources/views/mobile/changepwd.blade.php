<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title>Login</title>
<link rel="stylesheet" type="text/css" href="/styles/bootstrap.css">
<link rel="stylesheet" type="text/css" href="/fonts/bootstrap-icons.css">
<link rel="stylesheet" type="text/css" href="/styles/style.css">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@500;600;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
<link rel="manifest" href="/_manifest.json">
<meta id="theme-check" name="theme-color" content="#FFFFFF">
<link rel="apple-touch-icon" sizes="180x180" href="/app/icons/icon-192x192.png"></head>
<style>
.error { color: red; }
</style>
<body class="theme-light">

<div id="preloader"><div class="spinner-border color-highlight" role="status"></div></div>

<!-- Page Wrapper-->
<div id="page">

  <!-- Footer Bar -->

  <!-- Page Content - Only Page Elements Here-->
  <div class="page-content footer-clear">
    <div class="card bg-5 card-fixed">
      <div class="card-center mx-3 px-4 py-4 bg-white rounded-m">
        <form id="form_change" class="form form-horizontal" method="post" novalidate>
          <input type="hidden" name="nik" value="{{ $user->id_user }}">
          <h1 class="font-30 font-800 mb-0">Wasaka</h1>
          <p>Create New Password</p>
          <div class="form-custom form-label form-border form-icon mb-3 bg-transparent">
              <i class="bi bi-asterisk font-13"></i>
              <input name="password" type="text" class="form-control rounded-xs" id="c2" placeholder="Choose Password" required>
              <label for="c2" class="color-theme">Choose Password</label>
              <span>(required)</span>
          </div>
          <a id="button_submit" href="/register" class="btn btn-full gradient-highlight shadow-bg shadow-bg-s mt-4">Create Password</a>
        </form>
      </div>
      <div class="card-overlay rounded-0 m-0 bg-black opacity-70"></div>
  </div>

  </div>
    <!-- End of Page Content-->

    <!-- Off Canvas and Menu Elements-->
    <!-- Always outside the Page Content-->

    <!-- Main Sidebar Menu -->
    <!-- <div id="menu-sidebar"
        data-menu-active="nav-pages"
        data-menu-load="menu-sidebar.html"
        class="offcanvas offcanvas-start offcanvas-detached rounded-m">
    </div> -->
    
    <!-- Highlights Menu -->
    <!-- <div id="menu-highlights"
        data-menu-load="menu-highlights.html"
        class="offcanvas offcanvas-bottom offcanvas-detached rounded-m">
    </div> -->
  @if(Session::has('alertblock'))
    <?php
      $type = 0;
      if(session('alertblock')['type']=='success'){
        $type = 1;
      }
    ?>
    <div id="menu-top-detached" class="offcanvas offcanvas-top rounded-m offcanvas-detached alert-auto-activate">
      <div class="d-flex m-3">
        <div class="align-self-center">
          <h2 class="font-700 mb-0">{{ session('alertblock')['type'] }}</h2>
        </div>
        <div class="align-self-center ms-auto">
          <a href="#" class="icon icon-xs me-n2" data-bs-dismiss="offcanvas">
              <i class="bi bi-x-circle-fill color-{{ $type?'green':'red' }}-dark font-16"></i>
          </a>
        </div>
      </div>
      <div class="content mt-0">
        <h4 class="pt-0 mb-4">{{ session('alertblock')['text'] }}</h4>
        <a href="#" data-bs-dismiss="offcanvas" class="btn btn-full gradient-{{ $type?'green':'red' }} shadow-bg shadow-bg-xs">Oke!</a>
      </div>
    </div>
  @endif
</div>
<!-- End of Page ID-->
<script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js"></script>
<script src="/scripts/bootstrap.min.js"></script>
<script src="/scripts/custom.js"></script>
@if(Session::has('alertblock'))
  <script type="text/javascript">
    var autoActivates = new bootstrap.Offcanvas(document.getElementsByClassName('alert-auto-activate')[0])
    autoActivates.show();
  </script>
@endif
<script type="text/javascript">
  $("#form_change").validate({
      errorElement: "div",
      errorContainer: $(".invalid-feedback"),
      success: function(label) {
        label.text("ok!").addClass("d-none");
      }
  });
  var button_submit = document.querySelector("#button_submit");
  button_submit.addEventListener("click", function(e) {
    e.preventDefault();
    $("#form_change").submit();
  });
</script>
</body>