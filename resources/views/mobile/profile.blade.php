@extends('mobile.layout')
@section('title')
  Profile
@endsection
@section('css')
  <link rel="stylesheet" type="text/css" href="/camerastyle.css" />
  <style type="text/css">
    .input-photos img {
      width: 100px;
      height: 150px;
      margin-bottom: 5px;
    }
  </style>
@endsection
@section('content')
  <div class="pt-3">
    <div class="page-title d-flex">
      <div class="align-self-center me-auto">
        <p class="color-highlight">{{ session('auth')->id_user }}<h1></h1></p>
        <h1>{{ session('auth')->nama }}</h1>
      </div>
    </div>
  </div>
  <form id="profileform" class="form form-horizontal" method="post" enctype="multipart/form-data">
    <div class="card card-style shadow-bg shadow-bg-s">
      <div class="content">
        <div class="form-custom form-label form-icon">
          <i class="bi bi-rulers font-14"></i>
          <input type="text" name="no_hp" value="{{ $profile->no_hp }}" class="form-control rounded-xs" id="no_hp" aria-label="Floating label select example">
          <label for="no_hp" class="form-label-always-active color-highlight font-11">Nomor Handphone</label>
        </div>
        <div class="form-custom form-label form-icon">
          <i class="bi bi-rulers font-14"></i>
          <select name="instansi" class="form-select rounded-xs" id="instansi" aria-label="Floating label select example">
            @foreach($instansi as $i)
              <option value="{{ $i->text }}" {{ $profile->instansi==$i->text?'selected':'' }}>{{ $i->text }}</option>
            @endforeach
          </select>
          <label for="wtl" class="form-label-always-active color-highlight font-11">Instansi</label>
        </div>
        <div class="form-custom form-label form-icon">
          <i class="bi bi-rulers font-14"></i>
          <select name="regional" class="form-select rounded-xs" id="regional" aria-label="Floating label select example">
            @foreach($reg as $f)
              <option value="{{ $f->text }}" {{ $profile->regional==$f->text?'selected':'' }}>{{ $f->text }}</option>
            @endforeach
          </select>
          <label for="regional" class="form-label-always-active color-highlight font-11">Regional</label>
        </div>
        <div class="form-custom form-label form-icon">
          <i class="bi bi-rulers font-14"></i>
          <select name="wtl" class="form-select rounded-xs" id="wtl" aria-label="Floating label select example">
            @foreach($wtl as $f)
              <option value="{{ $f->text }}" {{ $profile->wtl==$f->text?'selected':'' }}>{{ $f->text }}</option>
            @endforeach
          </select>
          <label for="wtl" class="form-label-always-active color-highlight font-11">Witel</label>
        </div>
        <div class="form-custom form-label form-icon">
          <i class="bi bi-rulers font-14"></i>
          <select name="loker" class="form-select rounded-xs" id="loker" aria-label="Floating label select example">
            <option value="">Pilih Loker</option>
            @foreach($loker as $f)
              <option value="{{ $f->text }}" {{ $profile->loker==$f->text?'selected':'' }}>{{ $f->text }}</option>
            @endforeach
          </select>
          <label for="loker" class="form-label-always-active color-highlight font-11">Loker</label>
        </div>
      </div>
      <button class="mx-3 btn btn-full gradient-red shadow-bg shadow-bg-s mb-3">Submit</button>
    </div>
  </form>
@endsection
@section('js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $("#regional").change(function(){
      $.getJSON("/getWitelByRegional/"+$("#regional").val(), function(data){
        var opt = "";
        $.each(data, function(key, value){
          opt += '<option value="'+value.id+'">'+value.text+'</option>';
        });
        $('#wtl').html(opt);
      });
    });
  });
</script>
@endsection