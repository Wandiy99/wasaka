<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title>@yield('title')</title>
<link rel="stylesheet" type="text/css" href="/styles/bootstrap.css">
<link rel="stylesheet" type="text/css" href="/fonts/bootstrap-icons.css">
<link rel="stylesheet" type="text/css" href="/styles/style.css">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@500;600;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
<link rel="manifest" href="/_manifest.json">
<meta id="theme-check" name="theme-color" content="#FFFFFF">
<link rel="apple-touch-icon" sizes="180x180" href="/app/icons/icon-192x192.png">
@yield('css')
</head>

<body class="theme-light">

<div id="preloader"><div class="spinner-border color-highlight" role="status"></div></div>

<!-- Page Wrapper-->
<div id="page">
  <!-- Footer Bar -->
  <div id="footer-bar" class="footer-bar footer-bar-1 footer-bar-detached">
    <a href="/" class="{{Request::path()=='/'?'active-nav':''}}"><i class="bi bi-house"></i><span>Home</span></a>
    <a href="#" data-bs-toggle="offcanvas" data-bs-target="#menu-transfer"><i class="bi bi-search"></i><span>Search</span></a>
    <a href="/mobile/scan" class="circle-nav-2 {{Request::path()=='mobile/scan'?'active-nav':''}}"><i class="bi bi-upc-scan"></i><span>Scan</span></a>
    <a href="/mobile/profile" class="{{Request::path()=='mobile/profile'?'active-nav':''}}"><i class="bi bi-file-earmark-person"></i><span>Profile</span></a>
    <a href="/logout"><i class="bi bi-door-open"></i><span>Logout</span></a>
  </div>

  <!-- Page Content - Only Page Elements Here-->
  <div class="page-content footer-clear">
    @yield('content')
  </div>
  @yield('menu')
  <div id="menu-transfer" class="offcanvas offcanvas-bottom offcanvas-detached rounded-m">
    <!-- menu-size will be the dimension of your menu. If you set it to smaller than your content it will scroll-->
    <form action="/mobile/search" method="post" id="searchform">
      <div class="card" style="height:260px;">
        <div class="d-flex mx-3 mt-3 py-1">
          <div class="align-self-center">
            <h1 class="mb-0">Search Alpro</h1>
          </div>
          <div class="align-self-center ms-auto">
            <a href="#" class="ps-4 shadow-0 me-n2" data-bs-dismiss="offcanvas">
              <i class="bi bi-x color-red-dark font-26 line-height-xl"></i>
            </a>
          </div>
        </div>
        <div class="divider divider-margins mt-3"></div>
        <div class="content mt-0">
          <div class="form-custom form-label form-icon">
            <i class="bi bi-code-square font-14"></i>
            <input type="text" class="form-control rounded-xs" id="c43" placeholder="Nama Alpro" name="q" autocomplete="off" />
            <label for="c43" class="form-label-always-active color-highlight font-11">Alpro</label>
            <span class="font-10">( ODC-AAA-AAA )</span>
          </div>
        </div>
        <button type="submit" data-bs-dismiss="offcanvas" class="mx-3 btn btn-full gradient-blue shadow-bg shadow-bg-s mb-3">Cari</button>
      </div>
    </form>
  </div>
        <!-- menu-size will be the dimension of your menu. If you set it to smaller than your content it will scroll-->
    <!-- <div id="menu-profile" class="offcanvas offcanvas-bottom offcanvas-detached rounded-m">
        <div class="menu-size" style="height:370px;">
            <div class="d-flex mx-3 mt-0 py-0">
                <div class="align-self-center">
                    <h1 class="mb-0">Profile</h1>
                </div>
                <div class="align-self-center ms-auto">
                    <a href="#" class="ps-1 shadow-0 me-n1" data-bs-dismiss="offcanvas">
                        <i class="bi bi-x color-red-dark font-26 line-height-xl"></i>
                    </a>
                </div>
            </div>
            <div class="divider divider-margins mt-0"></div>
            <div class="content mt-0">
                <div class="form-custom form-label form-icon">
                    <i class="bi bi-rulers font-14"></i>
                    <select class="form-select rounded-xs" id="fz" aria-label="Floating label select example" name="fz">
                        <option value="All" selected >All Fiberzone</option>
                        
                    </select>
                    <label for="fz" class="form-label-always-active color-highlight font-11">Choose Fiberzone</label>
                </div>
                <div class="form-custom form-label form-icon">
                    <i class="bi bi-puzzle font-14"></i>
                    <select class="form-select rounded-xs" id="c6a" aria-label="Floating label select example">
                        <option value="All" selected >All Witel</option>
                        <option value="WITEL 1">WITEL 1</option>
                        <option value="WITEL 2">WITEL 2</option>
                    </select>
                    <label for="c6" class="form-label-always-active color-highlight font-11">Choose Witel</label>
                </div>
                <div class="form-custom form-label form-icon">
                    <i class="bi bi-person-circle font-14"></i>
                    <input type="text" class="form-control rounded-xs" id="c32" placeholder="Null, Call Admin" disabled="true" />
                    <label for="c32" class="form-label-always-active color-highlight font-11">Account Level</label>
                </div>
                <div class="form-custom form-label form-icon">
                    <i class="bi bi-code-square font-14"></i>
                    <input type="number" class="form-control rounded-xs" id="c43" placeholder="Null, Call Admin"/>
                    <label for="c43" class="form-label-always-active color-highlight font-11">Accessable</label>
                </div>
            </div>
            <a href="#" data-bs-dismiss="offcanvas" class="mx-3 btn btn-full gradient-blue shadow-bg shadow-bg-s">Submit</a>
        </div>

    </div> -->
  <div class="offcanvas offcanvas-bottom rounded-m offcanvas-detached" id="menu-install-pwa-android">
    <div class="content">
      <img src="/app/icons/icon-128x128.png" alt="img" width="80" class="rounded-m mx-auto my-4">
      <h1 class="text-center">Install WasakaAPP</h1>
      <p class="boxed-text-l">
        Install Aplikasi Wasaka.
      </p>
      <a href="#" class="pwa-install btn btn-m rounded-s text-uppercase font-900 gradient-highlight shadow-bg shadow-bg-s btn-full">Install</a><br>
      <a href="#" data-bs-dismiss="offcanvas" class="pwa-dismiss close-menu color-theme text-uppercase font-900 opacity-60 font-11 text-center d-block mt-n1">Nanti</a>
    </div>
  </div>
  @if (Session::has('alerts'))
    <?php
      $type = 0;
      if(session('alerts')['type']=='Berhasil'){
        $type = 1;
      }
    ?>
    <div id="menu-top-detached" class="offcanvas offcanvas-top rounded-m offcanvas-detached alert-auto-activate">
      <div class="d-flex m-3">
        <div class="align-self-center">
          <h2 class="font-700 mb-0">{{ session('alerts')['type'] }}</h2>
        </div>
        <div class="align-self-center ms-auto">
          <a href="#" class="icon icon-xs me-n2" data-bs-dismiss="offcanvas">
            <i class="bi bi-x-circle-fill color-{{ $type?'green':'red' }}-dark font-16"></i>
          </a>
        </div>
      </div>
      <div class="content mt-0">
        <h4 class="pt-0 mb-4">{{ session('alerts')['text'] }}</h4>
        <a href="#" data-bs-dismiss="offcanvas" class="btn btn-full gradient-{{ $type?'green':'red' }} shadow-bg shadow-bg-xs">Oke!</a>
      </div>
    </div>
  @endif
</div>
<!-- End of Page ID-->

<script src="/scripts/bootstrap.min.js"></script>
<script src="/scripts/custom.js"></script>
<script type="text/javascript">
  window.addEventListener("beforeunload", (event) => {
    var preloader = document.getElementById('preloader').classList.remove('preloader-hide');
  });
</script>
@if (Session::has('alerts'))
<script type="text/javascript">
  var autoActivates = new bootstrap.Offcanvas(document.getElementsByClassName('alert-auto-activate')[0])
  autoActivates.show();
</script>
@endif
@yield('js')
</body>