@extends('layoutadmin')
@section('content')
<div class="page-500-bg bg-danger text-center">
  <h1 class="page-500-error-code"><strong>401</strong></h1>
  <h2 class="page-500-subheader">OUCH!</h2>
  <h3 class="page-500-text">
    UNAUTHORIZE REQUEST
    <br>
    <br>
    <span class="font-size-16 font-weight-normal">Please upgrade user level.</span>
  </h3>
</div>		
@endsection
