@extends('layout')
@section('head')
    <style>
        .navbar {
            margin: 0;
        }
        .container {
            width: 100%;
            margin: 0;
            padding: 0;
        }
        #map {
            height: 100%;
        }
        .gm-style-iw ul {
            padding-left: 20px;
            padding-top: 10px;
        }
        .gm-style-iw li {
            margin-bottom: 5px;
        }
        .gm-style-iw img.info-image {
            margin-top: -20px;
        }

        #layerView {
            position: absolute;
            top: 81px;
            left: 115px;
        }
        #layerView .checkbox {
            padding-left: 10px;
            margin: 0;
        }
        #layerView .checkbox:hover {
            background-color: #0092db;
            color: white;
        }
        #layerView label {
            display: block;
            padding: 6px 12px 6px 25px;
        }

        #filterView, #filterResultView {
            position: absolute;
            right: 46px;
            top: 61px;
            width: 300px;
        }
        #filterResultView {
            top: 105px;
            height: 420px;
            overflow-y: auto;
            background-color: rgba(255,255,255, .5);
        }
        #filterResultView:hover {
            background-color: white;
        }
        #filterResultView .panel-body {
            padding: 0;
        }
        #filterResultView ul {
            list-style-type: none;
            padding: 0;
        }
        #filterResultView li {
            padding: 6px 8px 10px;
        }
        #filterResultView li:hover {
            background-color: #0092db;
            color: white;
            cursor: pointer;
        }

        #testMarkerView {
            position: absolute;
            top: 61px;
            width: 335px;
        }
        #testMarkerView input {
            width: 117px;
        }
    </style>
@endsection
@section('content')

<div id="map" style="height:540px;"></div>
 <div id="layerView" class="dropdown" style="display: none">
        <button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
            Layer
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" style="width:200px">
            <li>
                <div class="checkbox">
                    <label>
                        <input id="layerStoCheck" name="sto" type="checkbox" checked/>
                        ODC
                    </label>
                </div>
            </li>
            <li>
                <div class="checkbox">
                    <label>
                        <input id="layerOdcCheck" name="odc" type="checkbox" checked/>
                        FTM
                    </label>
                </div>
            </li>
            <li>
                <div class="checkbox">
                    <label>
                        <input id="layerCustCheck" name="cust" type="checkbox" checked/>
                        MSAN
                    </label>
                </div>
            </li>
            <li>
                <div class="checkbox">
                    <label>
                        <input id="layerPoiCheck" name="poi" type="checkbox" checked/>
                        MDU
                    </label>
                </div>
            </li>
            <li class="dropdown-header">Witel</li>
            <li>
                <div class="checkbox">
                    <label>
                        <input id="layerOdpCheck_0" name="witel_0" type="checkbox" checked/>
                        KALSEL
                    </label>
                </div>
            </li>
            <li>
                <div class="checkbox">
                    <label>
                        <input id="layerOdpCheck_1" name="witel_1" type="checkbox" checked/>
                        KALTENG
                    </label>
                </div>
            </li>
            <li>
                <div class="checkbox">
                    <label>
                        <input id="layerOdpCheck_2" name="witel_2" type="checkbox" checked/>
                        KALTARA
                    </label>
                </div>
            </li>
            <li>
                <div class="checkbox">
                    <label>
                        <input id="layerOdpCheck_3" name="witel_3" type="checkbox" checked/>
                        KALBAR
                    </label>
                </div>
            </li>
            <li>
                <div class="checkbox">
                    <label>
                        <input id="layerOdpCheck_4" name="witel_4" type="checkbox" checked/>
                        Samarinda
                    </label>
                </div>
            </li>
            <li>
                <div class="checkbox">
                    <label>
                        <input id="layerOdpCheck_5" name="witel_5" type="checkbox" checked/>
                        Balikpapan
                    </label>
                </div>
            </li>
        </ul>


</div>
    
@endsection
@section('footerS')
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCSn96DCIJdATC6AHuV3sLF3ddwdaIsW10"></script>
<script type="text/javascript">

(function() {
    var $layerView = $('#layerView');
            var layerVisibilityCheck = {
                sto: document.getElementById('layerStoCheck'),
                odc: document.getElementById('layerOdcCheck'),
                //odp: document.getElementById('layerOdpCheck'),
                odp_0: document.getElementById('layerOdpCheck_0'),
                odp_1: document.getElementById('layerOdpCheck_1'),
                odp_2: document.getElementById('layerOdpCheck_2'),
                odp_3: document.getElementById('layerOdpCheck_3'),
                cust: document.getElementById('layerCustCheck'),
                //cable: document.getElementById('layerCableCheck'),
                poi: document.getElementById('layerPoiCheck')
            };
            $layerView.show();
    var a = new Array();
    a['city'] = "Portland";
    a['state'] = "Oregon";
    a['country'] = "United States";
    delete a['country'];
    console.log(Object.keys(a));
    $.each( Object.keys(a), function( key, latLng ) {
        console.log(a[latLng]);
    });
    
    
    var ghostEl = document.createElement('div');
            ghostEl.style.position = 'absolute';
            ghostEl.style.top = '-500px';
            document.body.appendChild(ghostEl);
            var infoWindow = new google.maps.InfoWindow({ maxWidth: 600 });
    var map = null, marker = null, koormulti = [], markers = [], allMarker = [], renderClose=[], renderOpen=[];

    var showMarker = function() {
        $.each( Object.keys(allMarker), function( key, latLng ) {
            var koor = allMarker[latLng].koordinat;
            var myArr = koor.split(",");
            var point = {
                lat:  Number(myArr[0]),
                lng:  Number(myArr[1])
            }
            var marker = new google.maps.Marker({
                position: point,
                map: map,
                icon: "http://maps.google.com/mapfiles/marker_grey.png",
                draggable: true,
                kode: allMarker[latLng].kode
            });
            allMarker[latLng] = marker;
            google.maps.event.addListener(marker, 'click', function() {
                showInfoMarker(marker);
            }); 
        });
    };
    var renderOpenedMarker = function() {
        $.each( Object.keys(renderOpen), function( key, latLng ) {
            console.log("render new open");
            
                var koor = renderOpen[latLng].koordinat;
                console.log(renderOpen[latLng].koordinat);
                var myArr = koor.split(",");
                var point = {
                    lat:  Number(myArr[0]),
                    lng:  Number(myArr[1])
                }
                setMapOnAll(renderOpen[latLng].kode);
                var marker = new google.maps.Marker({
                    position: point,
                    map: map,
                    draggable: true,
                    kode: renderOpen[latLng].kode
                });
                
                markers[renderOpen[latLng].kode] = marker;
                allMarker[renderOpen[latLng].kode] = marker;
                google.maps.event.addListener(marker, 'click', function() {
                    showInfoMarker(marker);
                }); 
            
        });
    };
    var renderClosedMarker = function() {
        $.each( Object.keys(renderClose), function( key, latLng ) {
            console.log("render new close");
            if(key){
                var koor = renderClose[latLng].koordinat;
                var myArr = koor.split(",");
                var point = {
                    lat:  Number(myArr[0]),
                    lng:  Number(myArr[1])
                }
                setMapOnAll(renderClose[latLng].kode);
                var marker = new google.maps.Marker({
                    position: point,
                    map: map,
                    draggable: true,
                    kode: renderClose[latLng].kode
                });
                delete markers[renderClose[latLng].kode];
                allMarker[renderClose[latLng].kode] = marker;
                google.maps.event.addListener(marker, 'click', function() {
                    showInfoMarker(marker);
                }); 
            }
            
        });
    };
    function setMapOnAll(kode) {
        console.log(allMarker[kode]);
        allMarker[kode].setMap(null);
    }
    function checkNewClosed() {
        $.each( Object.keys(markers), function( key, value ) {
            if(koormulti[markers[value].kode] === undefined){
                renderClose[value] = markers[value];
                console.log("loop add close");
            }
        });
        renderClosedMarker();
    }
    function checkNewOpen() {
        $.each( Object.keys(koormulti), function( key, value ) {
            if(markers[koormulti[value].kode] === undefined){
                renderOpen[value] = koormulti[value];
                
            }
        });
        renderOpenedMarker();
    }
    function showInfoMarker(marker){
                var bbox = ghostEl.getBoundingClientRect(),
                width = Math.ceil(bbox.right - bbox.left),
                height = Math.ceil(bbox.bottom - bbox.top);
                console.log(marker.kode);
                var content = '<span class="label label-info">'+marker.kode+'</span><br/><span class="label label-info">'+marker.kode+'</span>';
                infoWindow.setContent(content);
                infoWindow.open(map, marker);
        }
    var initMap = function() {
        var center = {
                lat:  -2.9924130809467746,
                lng:  115.477294921875
            },
            zoom = 9;
        if (!map) {
            map = new google.maps.Map(
                document.getElementById('map'),
                {
                    center: center,
                    zoom: zoom,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }
            );
        }
        else {
            map.setZoom(zoom);
        }
        $.get('/allGembok', function(data) {
            $.each(JSON.parse(data), function(key, gembok) {
                allMarker[gembok.kode]=gembok;
            });
            showMarker();
        });
    };
    initMap();
    refresh();
    var timer = setInterval(refresh, 10000);
    function refresh() {
        koormulti=[];
        renderOpen=[];
        renderClose=[];
        $.get('/unclose', function(data) {
            $.each(JSON.parse(data), function(key, gembok) {
                koormulti[gembok.kode]=gembok;
            });
            checkNewClosed();
            checkNewOpen();
        });
    };
})();
</script>
@endsection