@extends('layout')
@section('head')
    <link rel="stylesheet" href="/bower_components/select2/dist/css/select2.css" />
@endsection
@section('content')
    <?php $authLevel = Session::get('auth')->level ?>
    <form class="row" style="margin-bottom: 20px;">
       
            <div class="col-sm-6">
                <input type="text" name="fiberzone" id="fzz" class="form-control" value="{{ old('fiberzone') }}"/>
            </div>
            <div class="col-sm-5">
                <input type="text" name="witel" id="witel" class="form-control" value="{{ old('witel') }}"/>
            </div>
            <div class="col-sm-1">
            <button class="btn btn-default btn-xs" type="submit">
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
            </div>

    </form>
    @if (isset($list))
        <?php $url = '/profile/' ?>
        <ul class="list-blocks">
            @foreach($list as $item)
                <li>
                    <a class="text-left btn-primary" href="{{ $url . $item->nik }}">
                        {{ $item->nik }}<br/>
                        {{ $item->nama or "-"}}<br/>
                        <span class="label label-success">{{ $item->fiberzone }}</span><br><span class="label label-info">{{ $item->witel }}</span><br/>
                        @if($item->level == 2)
                            <span class="label label-success">ADMIN</span>
                        @elseif($item->level == 99)
                            <span class="label label-success">MITRA</span>
                        @elseif(empty($item->level))
                            <span class="label label-default">VIEW</span>
                        @else
                            <span class="label label-warning">USER</span>
                        @endif
                    </a>
                </li>
            @endforeach
        </ul>
    @endif
@endsection
@section('script')
    <script src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/bower_components/select2/dist/js/select2.full.js"></script>
    <script>
        $(function() {
            var data = <?= json_encode($fz) ?>;
            var fiberzone = $('#fzz').select2({
                data: data,
                placeholder: "select fiberzone",
                allowClear:true,
                formatResult: function(data) {
                        return    '<span class="label label-default">'+data.text+'</span>'+
                                '<strong style="margin-left:5px">'+data.text+'&'+data.id+'</strong>';
                    }
            });
            var data = <?= json_encode($witel) ?>;
            var witel = $('#witel').select2({
                data: data,
                allowClear:true,
                placeholder: "select witel",
                formatResult: function(data) {
                        return    '<span class="label label-default">'+data.text+'</span>'+
                                '<strong style="margin-left:5px">'+data.text+'&'+data.id+'</strong>';
                    }
            });
            $("#fzz").change(function(){
                $.getJSON("/getWitelByFiberzone/"+$("#fzz").val(), function(data){
                    witel.select2({data:data});
                });
            });
        });
    </script>
@endsection