@extends('layout')

@section('content')
    @if (session('auth')->level == '2')
    <div class="panel panel-default">
        <div class="panel-heading">
            @if (isset($gembok))
                Edit User {{ $user->nama }}
            @else
                Input User Baru
            @endif
        </div>
        <div class="panel-body">
            <form method="post" class="form-horizontal">
                <input name="hid" type="hidden" value="{{ @$user->id_user }}"/>
                <div class="form-group {{ $errors->has('nama') ? 'has-error' : '' }}">
                    <label for="nama" class="col-sm-3 col-md-2 control-label">Nama</label>
                    <div class="col-sm-5">
                        <input name="nama" type="text" id="nama" class="form-control" value="{{ old('nama') ?: @$user->nama }}">
                        @foreach($errors->get('nama') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>

                <div class="form-group {{ $errors->has('nama_instansi') ? 'has-error' : '' }}">
                    <label for="nama" class="col-sm-3 col-md-2 control-label">Nama Perusahaan</label>
                    <div class="col-sm-5">
                        <input name="nama_instansi" type="text" id="nama_instansi" class="form-control" value="{{ old('nama_instansi') ?: @$user->nama_instansi }}">
                        @foreach($errors->get('nama_instansi') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>

                <div class="form-group {{ $errors->has('id_user') ? 'has-error' : '' }}">
                    <label for="id_user" class="col-sm-3 col-md-2 control-label">IDUser/NIK</label>
                    <div class="col-sm-2">
                        <input name="id_user" type="text" id="id_user" class="form-control"
                               value="{{ old('id_user') ?: @$user->id_user }}"/>
                        @foreach($errors->get('id_user') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>

                <div class="form-group">
                    <label for="pwd" class="col-sm-3 col-md-2 control-label">password</label>
                    <div class="col-sm-2">
                        <input name="pwd" type="text" id="pwd" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="stat" class="col-sm-3 col-md-2 control-label">Status</label>
                    <div class="col-sm-2">
                        <select class="form-control" id="stat" name="status">
                            <option value="0" {{ @$user->status=="0" ? "selected" : "" }}>ACTIVE</option>
                            <option value="1" {{ @$user->status=="1" ? "selected" : "" }}>NON-ACTIVE</option>
                            <option value="2" {{ @$user->status=="2" ? "selected" : "" }}>DELETE</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-md-offset-2 col-sm-2">
                        <button class="btn btn-primary" type="submit">
                            <span class="glyphicon glyphicon-floppy-disk"></span>
                            <span>Simpan</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @endif
@endsection