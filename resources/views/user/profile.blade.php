@extends('layout')
@section('head')
    <link rel="stylesheet" href="/bower_components/select2/dist/css/select2.css" />
@endsection
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Profile [{{ @$user->nik }}] {{ @$user->nama_alt }}
        </div>
        <div class="panel-body">
            <form method="post" class="form-horizontal">
                <input name="nik" type="hidden" value="{{ Request::segment(2) ?: session('auth')->id_user }}"/>
                <input name="nama" type="hidden" value="{{ session('auth')->nama }}"/>
                <div class="form-group">
                    <label for="stat" class="col-sm-3 col-md-2 control-label">Fiberzone</label>
                    <div class="col-sm-2">
                        <input type="text" name="fiberzone" id="fzz" class="form-control input-lg" value="{{ $user->fiberzone or '' }}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="stat" class="col-sm-3 col-md-2 control-label">Witel</label>
                    <div class="col-sm-2">
                        <input type="text" name="witel" id="witel" class="form-control input-lg" value="{{ $user->witel or '' }}"/>
                    </div>
                </div>
                @if(session('auth')->level == 2)
                <div class="form-group">
                    <label for="stat" class="col-sm-3 col-md-2 control-label">Level</label>
                    <div class="col-sm-2">
                        <input type="text" name="level" id="level" class="form-control input-lg" value="{{ $user->level or '' }}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="stat" class="col-sm-3 col-md-2 control-label">Boleh Akses</label>
                    <div class="col-sm-2">
                        <input type="text" name="akses_perangkat" id="akses_perangkat" class="form-control input-lg" value="{{ $user->akses_perangkat or '' }}"/>
                    </div>
                </div>

                @else
                    <input type="hidden" name="level" value="{{ $user->level or '0' }}" />
                    <input type="hidden" name="akses_perangkat" value="{{ $user->akses_perangkat or null }}" />
                @endif
                <div class="form-group">
                    <div class="col-sm-offset-3 col-md-offset-2 col-sm-2">
                        <button class="btn btn-primary" type="submit">
                            <span class="glyphicon glyphicon-floppy-disk"></span>
                            <span>Simpan</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/bower_components/select2/dist/js/select2.full.js"></script>
    <script>
        $(function() {
            var data = <?= json_encode($fz) ?>;
            var fiberzone = $('#fzz').select2({
                data: data,
                formatResult: function(data) {
                        return    '<span class="label label-default">'+data.text+'</span>'+
                                '<strong style="margin-left:5px">'+data.text+'&'+data.id+'</strong>';
                    }
            });
            var data = <?= json_encode($witel) ?>;
            var witel = $('#witel').select2({
                data: data,
                allowClear:true,
                formatResult: function(data) {
                        return    '<span class="label label-default">'+data.text+'</span>'+
                                '<strong style="margin-left:5px">'+data.text+'&'+data.id+'</strong>';
                    }
            });
            var data = [{"id":0,"text":"VIEW"},{"id":1,"text":"USER"},{"id":2,"text":"ADMIN"},{"id":99,"text":"MITRA"}];
            var level = $('#level').select2({
                data: data,
                formatResult: function(data) {
                        return    '<span class="label label-default">'+data.text+'</span>'+
                                '<strong style="margin-left:5px">'+data.text+'&'+data.id+'</strong>';
                    }
            });
            var kat = [{"id":1, "text":"FTM"},{"id":2, "text":"ODC"},{"id":3, "text":"MSAN"},{"id":4, "text":"MDU"},{"id":5, "text":"ONU"},{"id":6, "text":"OLT"}];
            var akses = $('#akses_perangkat').select2({
                data: kat,
                multiple:true,
                formatResult: function(data) {
                    return    '<span class="label label-default">'+data.text+'</span>'+
                            '<strong style="margin-left:5px">'+data.text+'&'+data.id+'</strong>';
                }
            });
            $("#fzz").change(function(){
                $.getJSON("/getWitelByFiberzone/"+$("#fzz").val(), function(data){
                    witel.select2({data:data});
                });
            });
        });
    </script>
@endsection