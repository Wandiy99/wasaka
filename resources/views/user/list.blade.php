@extends('layout')

@section('content')
    <?php $authLevel = Session::get('auth')->level ?>

    @if ($authLevel == '2')
        <a href="/user/create" class="btn btn-info" style="margin: 0 -15px;">
            <span class="glyphicon glyphicon-plus"></span>
            <span>Create User</span>
        </a>
    @endif
    @if (isset($list))
        <?php $url = '/user/' ?>
        <ul class="list-blocks">
            @foreach($list as $item)
                @if ($item->login_type == '1')
                <li>
                    <a class="text-left {{ $item->status ? 'btn-warning':'btn-primary' }}" href="{{ $url . $item->id_user }}">{{ $item->id_user }} <br/>{{ $item->nama }} <br/>{{ $item->nama_instansi }}</a>
                </li>
                @endif
            @endforeach
        </ul>
    @endif
@endsection