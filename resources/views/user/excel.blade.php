<?php
  $jenis_alpro = [1=>'FTM','ODC','MSAN','MDU','ONU','OLT','Custom Akses','ODP'];
  $level = [0=>'View',1=>'User',2=>'Admin Witel',3=>'Admin Regional',4=>'Superadmin',99=>'Mitra'];
?>
<div class="panel panel-default" id="info">
  <div class="panel-heading">List</div>
  <div id="fixed-table-container-demo" class="fixed-table-container">
    <table class="table table-bordered table-fixed">
      <tr>
        <th>id_user</th>
        <th>nama</th>
        <th>instansi</th>
        <th>loker</th>
        <th>no_hp</th>
        <th>level</th>
        <th>regional</th>
        <th>wtl</th>
        <th>login_type</th>
        <th>akses_perangkat</th>
        <th>status</th>
      </tr>
      @foreach($data as $no => $list) 

        <tr>
          <td>{{ $list->id_user }}</td>
          <td>{{ $list->nama }}</td>
          <td>{{ $list->instansi }}</td>
          <td>{{ $list->loker }}</td>
          <td>{{ $list->no_hp }}</td>
          <td>{{ @$level[$list->level] }}</td>
          <td>{{ $list->regional }}</td>
          <td>{{ $list->wtl }}</td>
          <td>{{ ($list->login_type==1?'WASAKA':'SSO TA') }}</td>
          <td>
            @foreach(explode(',',$list->akses_perangkat) as $ap)
              @if($ap)
                <span class="label label-secondary m-b-1">{{ $jenis_alpro[$ap] }}</span>
              @endif
            @endforeach
          </td>
          <td>{{ ($list->status ? ($list->status=='1'?'Non-Aktif':'Deleted'):'Aktif') }}</td>
        </tr>
      @endforeach
    </table>
  </div>
</div>