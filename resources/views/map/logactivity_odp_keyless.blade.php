@extends('layoutadmin')
@section('css')
<style type="text/css">

</style>
@endsection
@section('title', 'Log Activity ODP')
@section('content')

<div class="panel m-t-2">
    <div class="panel-body">
        <div class="table-responsive table-primary m-t-2">
            <table class="table" id="datatables">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Start</th>
                        <th>Close</th>
                        <th>Durasi</th>
                        <th>Kode</th>
                        <th>Pekerjaan</th>
                        <th>Anggota</th>
                        <th>Uraian</th>
                        <th>User</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $k => $v)
                    @php
                        $dteStart = new \DateTime($v->time_start);
                        $dteEnd   = new \DateTime($v->time_close); 
                        $dteDiff  = $dteStart->diff($dteEnd); 
                        $durasi   = $dteDiff->format("%Hh %Im %Ss");
                        $timezone = '';
                        if($v->timezone)
                        {
                            $timezone = $v->timezone;
                        }
                    @endphp
                    <tr>
                        <td>{{ ++$k }}</td>
                        <td>{{ $v->time_start }}</td>
                        <td>{{ $v->time_close }}</td>
                        <td>{{ $durasi }}</td>
                        <td>{{ $v->kode }}</td>
                        <td>{{ $v->jenis_pekerjaan }}</td>
                        <td>{{ $v->anggota }}</td>
                        <td>{{ $v->uraian }}</td>
                        <td>{{ $v->id_user }} {{ $v->nama }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
  $(function() {
    
    var is_id = {!! json_encode($id) !!};
    
    $('#datatables').DataTable({
        ajax: {
            type: 'GET',
            url: `https://wasaka.telkomakses.co.id/map/ajax/log-activity/odp-keyless/${is_id}`,
        },
        columns: [
            { data: 'no', title: '#' },
            { data: 'time_start', title: 'Start Time' },
            { data: 'time_close', title: 'Close Time' },
            { data: 'durasi', title: 'Durasi', defaultContent: '-' },
            { data: 'kode', title: 'Kode' },
            { data: 'jenis_pekerjaan', title: 'Jenis Pekerjaan', defaultContent: '-' },
            { data: 'anggota', title: 'Anggota' },
            { data: 'uraian', title: 'Uraian' },
            { data: 'nama', title: 'Nama', defaultContent: '-' }
        ]
    });

  });
</script>
@endsection