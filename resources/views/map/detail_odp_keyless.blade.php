@extends('layoutadmin')
@section('css')
<style type="text/css">

</style>
@endsection
@section('title', 'Detail Activity')
@section('content')

<div class="panel m-t-2">
    <div class="panel-body">
        <div class="table-responsive table-primary m-t-2">
            <table class="table" id="datatables">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Start</th>
                        <th>Close</th>
                        <th>Durasi</th>
                        <th>Kode</th>
                        <th>Pekerjaan</th>
                        <th>Anggota</th>
                        <th>Uraian</th>
                        <th>User</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
  $(function() {

    var is_unit = {!! json_encode($unit) !!},
    is_status = {!! json_encode($status) !!};
    
    $('#datatables').DataTable({
        ajax: {
            type: 'GET',
            url: `https://wasaka.telkomakses.co.id/map/ajax/detail/odp-keyless/${is_unit}/${is_status}`,
        },
        columns: [
            { data: 'no', title: '#' },
            { data: 'time_start', title: 'Start Time' },
            { data: 'time_close', title: 'Close Time' },
            { data: 'durasi', title: 'Durasi', defaultContent: '-' },
            { data: 'kode', title: 'Kode' },
            { data: 'jenis_pekerjaan', title: 'Jenis Pekerjaan', defaultContent: '-' },
            { data: 'anggota', title: 'Anggota' },
            { data: 'uraian', title: 'Uraian' },
            { data: 'nama', title: 'Nama', defaultContent: '-' }
        ]
    });

  });
</script>
@endsection