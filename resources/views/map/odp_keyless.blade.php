<!DOCTYPE html>
<html>
    <head>
        <!-- Basic Page Info -->
        <meta charset="utf-8">
        <title>WASAKA</title>

        <!-- Mobile Specific Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
        <!-- CSS -->
        @php
            $auth = session('auth');
            $path = Request::path();
            $theme = 'mint-dark';
            
            if (isset($auth->pixeltheme))
            {
                if ($auth->pixeltheme)
                {
                    $theme = $auth->pixeltheme;
                }
            }

            $color = '';
            
            if (str_contains($theme, 'dark'))
            {
                $color = '-dark';
            }
        @endphp
        <link href="/pixeladmin/css/bootstrap{{ $color }}.min.css" rel="stylesheet" type="text/css">
        <link href="/pixeladmin/css/pixeladmin{{ $color }}.min.css" rel="stylesheet" type="text/css">
        <link href="/pixeladmin/css/widgets{{ $color }}.min.css" rel="stylesheet" type="text/css">
        <link href="/pixeladmin/css/themes/{{ $theme }}.min.css" rel="stylesheet" type="text/css">

        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="https://cdn.datatables.net/1.13.7/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css">

        <style>
            body,
            html {
                height: 100%;
                margin: 0;
                padding: 0;
                overflow: hidden;
            }
    
            #map-container {
                position: absolute;
                top: 0;
                bottom: 0;
                left: 0;
                right: 0;
            }
    
            #map {
                height: 100%;
                width: 100%;
            }

            #info {
                position:absolute;
                right:0;
                width: 20em;
                height: 12em;
                background: rgba(255,255,255,.5);
                z-index:1;
                font: 12px Sans;
            }

            .middle-center {
                text-align: center;
                vertical-align: middle
            }

            .custom-popup {
                background-color: #fff;
                border: 1px solid #ccc;
                padding: 10px;
                border-radius: 5px;
            }

            tr, th, td {
                text-align: center;
            }

            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
                padding: 4px 2px;
            }

            .leaflet-control-logo a {
                display: inline-block;
                margin-right: 5px;
                line-height: 10px;
                vertical-align: middle;
                text-decoration: none;
            }

            table {
                width: 100%;
                border-collapse: collapse;
            }

            .info {
                padding: 6px 8px;
                font: 14px/16px Arial, Helvetica, sans-serif;
                background: white;
                background: rgba(255, 255, 255, 0.8);
                box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);
                border-radius: 5px;
            }
            .info h4 {
                margin: 0 0 5px;
                color: #777;
            }
        </style>
    </head>

    <body>
        <div id="map-container">
            <div id="map"></div>
        </div>
    </body>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.7/js/dataTables.bootstrap.min.js"></script>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var map = L.map("map-container", { zoom: 50 }).setView( [-3.3305863251218573, 114.6106770052837], 50 );

            function onMarkerClick(e) {
                map.setView(e.latlng, map.getMaxZoom());
            }

            var logoControl = L.Control.extend({
                onAdd: function(map) {
                    var logoContainer = L.DomUtil.create('div', 'leaflet-control');
                    var logoLink = L.DomUtil.create('a', 'leaflet-control-logo', logoContainer);

                    logoLink.href = '#';
                    logoLink.title = 'ODP Keyless by WASAKA';
                    logoLink.innerHTML = '<img src="/img/odp-keyless-logo-2.png" alt="Logo" style="width="70" height="70"">';

                    return logoContainer;
                },
            });

            map.addControl(new logoControl({ position: 'topleft' }));

            // var blackIcon = L.icon({
            //     iconUrl: `/img/marker-icon-black.png`,
            //     iconSize: [25, 41],
            //     iconAnchor: [12, 41],
            //     popupAnchor: [1, -34],
            // });


            // var greenIcon = L.icon({
            //     iconUrl: `/img/marker-icon-green.png`,
            //     iconSize: [25, 41],
            //     iconAnchor: [12, 41],
            //     popupAnchor: [1, -34],
            // });

            var markers = {};

            function updateMarkerLocation(marker, lat, lng) {
                marker.setLatLng([lat, lng]).update();
            }

            function loadOdpData() {
                $.ajax({
                    url: `/map/ajax/odp-keyless`,
                    dataType: "json",
                    success: function (data) {
                        data.forEach(function (item) {
                        if (item.latitude != null) {
                            if(item.kategori=='8'){
                                var kat = "odp";
                            }
                            else if(item.kategori=='2'){
                                var kat = "odc";
                            }
                            if (item.time_start != 0 && item.time_close == 0)
                            {
                                var customIcon = kat+'_red.png';
                            }
                            else if (item.time_start != 0 && item.time_close != 0)
                            {
                                var customIcon = kat+'_green.png';
                            }
                            var iconnya = L.icon({
                                iconUrl: `/img/`+customIcon,
                                iconSize: [50, 50],
                                iconAnchor: [50, 50],
                                popupAnchor: [-24, -47],
                            });
                            if (!markers[item.kode]) {
                                
                                markers[item.kode] = L.marker([item.latitude, item.longitude],
                                    { 
                                        icon: iconnya
                                    }
                                )
                                .addTo(map)
                                .bindPopup(
                                    `<div class="custom-popup table-responsive">
                                        <h5 style="font-size: 12px; text-align: center; vertical-align: middle">${item.kode}</h5>
                                        <table class="table table-sm">
                                            <tbody>
                                                <tr>
                                                    <td style="font-size: 10px">Witel / Regional</td>
                                                    <td style="font-size: 10px">:</td>
                                                    <td style="font-size: 10px">${item.witel} / ${item.regional}</td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 10px">Latitude</td>
                                                    <td style="font-size: 10px">:</td>
                                                    <td style="font-size: 10px">${item.latitude}</td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 10px">Longitude</td>
                                                    <td style="font-size: 10px">:</td>
                                                    <td style="font-size: 10px">${item.longitude}</td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 10px">Last Open</td>
                                                    <td style="font-size: 10px">:</td>
                                                    <td style="font-size: 10px">${item.timestamp}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <br>
                                        <a href="/map/log-activity/odp-keyless/${item.gembok_id}" class="btn btn-sm btn-block btn-primary" type="button" style="color: white !important" target="_blank">Log Activity</a>
                                    </div>`
                                );
                            } else {
                                updateMarkerLocation(
                                    markers[item.kode],
                                    item.latitude,
                                    item.longitude
                                );
                                markers[item.kode].setIcon(iconnya);
                            }

                            Object.values(markers).forEach(function (marker) {
                                marker.on('click', onMarkerClick);
                            });
                        }
                        });
                    },
                });

                setTimeout(loadOdpData, 5000);
            };

            loadOdpData();

            var streets = L.tileLayer(
                "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                {
                attribution:
                    '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
                }
            );

            var satellite = L.tileLayer(
                "https://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}",
                {
                maxZoom: 20,
                subdomains: ["mt0", "mt1", "mt2", "mt3"],
                attribution:
                    'Map data © <a href="https://www.google.com/maps">Google Maps</a>',
                }
            );

            var streetsAndSatellite = L.layerGroup([streets, satellite]);

            streets.addTo(map);
            var baseMaps = {
                "Jalan": streets,
                "Satelit": satellite,
                "Jalan dan Satelit": streetsAndSatellite,
            };

            L.control.layers(baseMaps).addTo(map);

            var info = L.control();

            info.onAdd = function (map) {
                this._div = L.DomUtil.create('div', 'info');
                this.update();
                return this._div;
            };

            info.update = function () {
                $.ajax({
                    url: `/map/ajax/info-odp-keyless`,
                    dataType: 'json',
                    success: function(data) {
                        var html = `
                                <h4><b class="pull-right">Today Activity</b></h4>
                                <table class="table table-bordered middle-center" style="margin-bottom: 1px;" id="today-activity-table">
                                    <thead>
                                        <tr>
                                            <th style="border: 1px solid;">Unit</th>
                                            <th style="border: 1px solid;">Open</th>
                                            <th style="border: 1px solid;">Close</th>
                                            <th style="border: 1px solid;">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                        `;

                        var totalOpen = 0;
                        var totalClose = 0;
                        var totalTotal = 0;

                        data.akses.forEach(function (item) {
                            var open = parseInt(item.is_open);
                            var close = parseInt(item.is_close);
                            var total = open + close;

                            totalOpen += open;
                            totalClose += close;
                            totalTotal += total;

                            if (item.unit == '')
                            {
                                var ini_unit = 'OTHER';
                            }
                            else
                            {
                                var ini_unit = item.unit;
                            }

                            html += `
                                <tr>
                                    <td style="border: 1px solid;">${ini_unit}</td>
                                    <td style="border: 1px solid;">
                                        <a href="/map/detail/odp-keyless/${ini_unit}/is_open" target="_blank" style="color: black">
                                            ${open}
                                        </a>
                                    </td>
                                    <td style="border: 1px solid;">
                                        <a href="/map/detail/odp-keyless/${ini_unit}/is_close" target="_blank" style="color: black">
                                            ${close}
                                        </a>
                                    </td>
                                    <td style="border: 1px solid;"><b>${total}</b></td>
                                </tr>
                            `;
                        });

                        html += `
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th style="border: 1px solid;">Total</th>
                                            <th style="border: 1px solid;">
                                                <a href="/map/detail/odp-keyless/all/is_open" target="_blank" style="color: black">
                                                    ${totalOpen}
                                                </a>
                                            </th>
                                            <th style="border: 1px solid;">
                                                <a href="/map/detail/odp-keyless/all/is_close" target="_blank" style="color: black">
                                                    ${totalClose}
                                                </a>
                                            </th>
                                            <th style="border: 1px solid;">${totalTotal}</th>
                                        </tr>
                                    </tfoot>
                                </table>`;


                        html += `<br>
                                <h4><b class="pull-right">Jumlah Perangkat</b></h4>
                                <table class="table table-bordered middle-center" style="margin-bottom: 1px;" id="today-activity-table">
                                    <thead>
                                        <tr>
                                            <th style="border: 1px solid;">Alpro</th>
                                            <th style="border: 1px solid;">Live</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                        `;
                        var totalAlpro = 0;
                        data.alpro.forEach(function (item) {
                            var total_alpro = parseInt(item.live);
                            var jenis_alpro = item.jenis_alpro;
                            totalAlpro += total_alpro;
                            html += `
                                <tr>
                                    <td style="border: 1px solid;">${jenis_alpro}</td>
                                    <td style="border: 1px solid;">${total_alpro}</td>
                                </tr>
                            `;
                        });
                        html += `
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th style="border: 1px solid;">Total</th>
                                            <th style="border: 1px solid;">${totalAlpro}</th>
                                        </tr>
                                    </tfoot>
                                </table>`;
                        info._div.innerHTML = html;
                    },
                    error: function(xhr, status, error) {
                        console.error(status, error);
                    }
                });
            };

            info.addTo(map);
        });
    </script>
</html>