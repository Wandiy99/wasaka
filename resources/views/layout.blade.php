<!doctype html>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>

    <link rel="stylesheet" href="/bower_components/bootswatch-dist/css/bootstrap.min.css"/>
    {{-- <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css" /> --}}
    <link rel="stylesheet" href="/style.css"/>
    <style type="text/css">
        .select2-results__option[aria-selected] {
            cursor: pointer;
            background-color: black;
        }
    </style>
    <title>WASAKA</title>

    @yield('head')
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a href="/" class="navbar-brand">
                <span class="glyphicon glyphicon-lock"></span>
                <span>PIN</span>
                @if ($unclosedCount['all'] > 0)
                    <span class="badge text-danger visible-xs-inline">
                        {{ $unclosedCount['all'] }}
                    </span>
                @endif
                @if ($unclosedCount['byUser'] > 0)
                    <span class="badge text-danger visible-xs-inline">
                        {{ $unclosedCount['byUser'] }}
                    </span>
                @endif
            </a>
        </div>

        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="{{ Request::path() == '/' ? 'active' : '' }}">
                    <a href="/">Search</a>
                </li>
                <li class="{{ Request::path() == 'work' ? 'active' : '' }}">
                    <a href="/work">
                        History
                        @if ($unclosedCount['all'] > 0)
                            <span class="badge text-danger">
                                {{ $unclosedCount['all'] }}
                            </span>
                        @endif
                    </a>
                    
                </li>
                @if (session('auth')->level != 19 && session('auth')->level != 2 )
                <li class="{{ Request::path() == 'mywork' ? 'active' : '' }}">
                    <a href="/mywork">
                        MyHistory
                        @if ($unclosedCount['byUser'] > 0)
                            <span class="badge text-danger">
                                {{ $unclosedCount['byUser'] }}
                            </span>
                        @endif
                    </a>
                </li>
                @endif
                @if (session('auth')->level == 2 )
                    <li class="{{ Request::path() == 'user' ? 'active' : '' }}">
                        <a href="/user">
                            Manage User
                        </a>
                    </li>
                    <li class="{{ Request::path() == 'acl' ? 'active' : '' }}">
                        <a href="/acl">
                            Access Control List
                        </a>
                    </li>
                @endif
                <!-- <li class="{{ Request::path() == 'dashboard' ? 'active' : '' }}">
                    <a href="/dashboard">
                        Map
                        @if ($unclosedCount['all'] > 0)
                            <span class="badge text-danger">
                                {{ $unclosedCount['all'] }}
                            </span>
                        @endif
                    </a>
                </li> -->
                <li class="{{ Request::path() == 'top10' ? 'active' : '' }}">
                    <a href="/top10/{{ date('Y-m') }}/{{ session('auth')->witel }}">
                        Dashboard
                    </a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">SOP<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="/sop-user">For User</a></li>
                    <li><a href="/sop-admin">For Admin</a></li>
                  </ul>
                </li>
                <li class="{{ Request::path() == '/work/download' ? 'active' : '' }}">
                    <a href="/work/download">Download</a>
                </li>
                @if (session('auth')->level == 2 )

                <li>
                    <a href="/home/{{@session('auth')->fz}}/{{@session('auth')->witel}}">Admin Style</a>
                </li>
                
                @endif
            </ul>

            {{-- <form action="" class="navbar-form navbar-right">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search" />
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit">
                    <span class="glyphicon glyphicon-search"></span>
                  </button>
                </span>
              </div>
            </form> --}}
        </div>
    </div>
</nav>
<div class="container">
    @include('partial.alert')

    @yield('content')
</div>

<div class="footer">
    <a href="/logout" style="margin-top:10px" class="btn btn-sm btn-warning pull-right">Logout</a>
    <span>[{{ session('auth')->fiberzone }}] {{ session('auth')->witel }}</span>
    <br/>
    <strong>[{{ session('auth')->id_user }}] {{ session('auth')->nama }}</strong>
</div>

<div class="wait-indicator">
    <span class="glyphicon glyphicon-refresh gly-spin"></span>
</div>

<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/bootswatch-dist/js/bootstrap.min.js"></script>

<script>
    $('form').submit(function () {
        $('.wait-indicator').show()
    })
</script>

@yield('script')
</body>
</html>