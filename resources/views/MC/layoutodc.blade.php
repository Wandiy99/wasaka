@extends('layoutadmin')
@section('css')
<style type="text/css">
  .popover {
    max-width: 400px;
  }
  .the-legend {
    font-size: 14px;
    line-height: 20px;
    margin-bottom: 0;
    width: auto;
    padding: 0 10px;
    border: 1px solid;
  }
  .the-fieldset {
    border: 1px solid;
    padding: 10px;
  }
</style>
@endsection
@section('title')
  LAYOUT || {{ $odc->kode }}
@endsection
@section('content')
<div class="row">
  <div class="col-md-3">
    <div class="panel panel-warning">
      <div class="panel-body">

        <div style="margin-top: 5px;" class="col-sm-4 col-md-4 col-lg-4 col-xl-4 col-xs-4 text-center b-a-2 border-rounded border-primary KBL" role="button" data-toggle="modal" data-target="#modal-kabel" data-kblid="0">
          TAMBAH<br/>
          KABEL
        </div>
        @foreach($kblarray as $key => $k)
          <div style="margin-top: 5px;" class="col-sm-4 col-md-4 col-lg-4 col-xl-4 col-xs-4 text-center b-a-2 border-round border-info KBL" role="button" data-toggle="modal" data-target="#modal-kabel" data-kblid="{{ $key }}">
            {{ $k->nama_kabel }}<br/>
            K:{{ $k->kap }} - R:{{ $k->reti_count }}
          </div>
        @endforeach
      </div>
    </div>
  </div>
  <div class="col-md-9">
    <div class="panel panel-info">
      <div class="panel-body">
        @foreach($splarray as $splid => $s)
          <?php
            $style_spl="";
            if($s['bg_color']){
              $style_spl .= "background-color:".$s['bg_color'].";";
            }
            if($s['text_color']){
              $style_spl .= "color:".$s['text_color'].";";
            }
            $content = "";
            foreach($s['link'] as $key => $l){
              if($l['status']=="in_use"){
                $label = "<span class='label label-xs label-danger pull-right'>B=".$l['basetray']."-P=".$l['port']."</span>";
              }else if($l['status']=="book"){
                $label = "<span class='label label-xs label-warning pull-right'>Booked</label>";
              }else{
                $label = "<span class='label label-info'>Free</span><a href='#' class='btn btn-xs btn-primary pull-right' data-toggle='modal' data-target='#modal-booking-spl' data-splid='".$splid."' data-splnumber='".$key."'><i class='ion ion-ios-book'></i> Book</a>";
              }
              if($key){
                $content .= "<div class='m-b-1'>OUT-".$key.":".$label."</div>";
              }else{
                $content .= "<div class='m-b-1'>IN:".$label."</div>";
              }
            }
          ?>
          <div style="{{$style_spl}}margin-top: 5px;" class="port col-sm-1 col-md-1 col-lg-1 col-xl-1 col-xs-1 text-center b-a-2 border-round border-default SPL SPL-{{ $s['urutan'] }} m-t-1" data-highligth="{{ $s['urutan'] }}" tabindex="0" data-placement="bottom" role="button" data-state="primary" data-html="true" data-toggle="popover" data-trigger="focus" title="SPL-{{$s['urutan']}} DETAIL LINK & ACTION" data-content="{{$content}}">
            SPL-{{ $s['urutan'] }}<br>
            U:{{ $s['used']?:'0' }};E:{{ $s['free'] }}
          </div>
        @endforeach
      </div>
    </div>
  </div>
</div> 

<div class="box-row">
  <div class="box-row">
      <div class="box-cell col-xs-1 col-sm-1 col-md-1 col-lg-1 col-xl-1 text-center valign-middle b-a-2 border-rounded border-default">
        <span class="btn btn-xs" data-toggle="modal" data-target="#modal-input-basetray" data-odc_id="{{ Request::segment(2) }}"><i class="ion ion-plus"></i> BASETRAY</span>
      </div>
      <div class="box-cell col-xs-11 col-sm-11 col-md-11 col-lg-11 col-xl-11">
        <div class="box-container">
          <div class="box-row">
            @for($i=1;$i<=$odc->basetray_port;$i++)
              <div class="port col-sm-1 col-md-1 col-lg-1 col-xl-1 col-xs-1 text-center b-a-2 border-rounded border-default">
                PORT-{{ $i }}
              </div>
            @endfor
          </div>
        </div>
      </div>
    </div>
  @foreach($basetray as $j => $bt)
    <?php
      $j++;
    ?>
    <div class="box-row">
      <div class="box-cell col-xs-1 col-sm-1 col-md-1 col-lg-1 col-xl-1 text-center valign-middle b-a-2 border-rounded border-default">
        <span class="btn btn-xs" data-toggle="modal" data-target="#modal-update-basetray" data-basetray_id="{{ $bt->id }}"><i class="ion ion-compose"></i> BASETRAY-{{$j}}</span><br>
        {!!$bt->label?'<span class="label label-xs label-warning">'.$bt->label.'</span>':''!!}
      </div>
      <div class="box-cell col-xs-11 col-sm-11 col-md-11 col-lg-11 col-xl-11">
        <div class="box-container">
          <div class="box-row font-size-11">
            @for($i=1;$i<=$odc->basetray_port;$i++)
              <?php
                $spl=$warna_port=$back_link_var=$status_port_label=null;
                $style_port = "";
                
                $front_text="<i class='ion ion-arrow-up-b'></i>";
                $back_text ="<i class='ion ion-arrow-down-b'></i>";
                $content = "<div>
                                  <fieldset class='the-fieldset'>
                                    <legend class='the-legend'>PORT</legend>
                                    <div><a href='#' class='btn btn-xs btn-default' data-toggle='modal' data-target='#modal-base' data-basetray='".$j."' data-port='".$i."' data-splid='0' data-portid='0' data-backlink='0' data-frontlink='0'><i class='ion ion-log-in'></i> Set Link</a></div>
                                    <div>Status : Not Set</div>
                                  </fieldset>
                                </div>";
                if(array_key_exists($j, $portarray) && array_key_exists($i, $portarray[$j])){
                  $port = $portarray[$j][$i];
                  $content = "<div>
                                  <fieldset class='the-fieldset'>
                                    <legend class='the-legend'>PORT</legend>
                                    <div><a href='#' class='btn btn-xs btn-default' data-toggle='modal' data-target='#modal-base' data-basetray='".$j."' data-port='".$i."' data-splid='".$port['spl_id']."' data-portid='".$port['port_id']."'  data-backlink='".$port['back_link']."' data-frontlink='".$port['front_link']."'><i class='ion ion-log-in'></i> Set Link</a>
                  <a href='#' class='btn btn-xs btn-primary pull-right' data-toggle='modal' data-target='#modal-status-port' data-basetray='".$j."' data-port='".$i."' data-portid='".$port['port_id']."'><i class='ion ion-ios-book'></i> Booking Port</a></div>
                                    <div>STATUS : ".($port['status_port']?$port['status_port']:'Not Set')." ".($port['status_port_user']?' By '.$$port['status_port_user']:'')."</div>
                                    <div>STATUS DATE : ".($port['status_port_ts']?:'Not Set')."</div>
                                  </fieldset>
                                </div>";
                  
                  if($port['back_link']){
                    $back_text = $port['nama_kabel']." C:".$port['core'];
                    if($port['back_link_var']){
                      $back_text = $port['back_link_var'];
                    }
                    $content .= "<div class='m-t-1'>
                                  <fieldset class='the-fieldset'>
                                    <legend class='the-legend'>BACK LINK</legend>
                                    <div>LABEL : ".($port['back_link_var']?:'Not Set')."</div>
                                    <div>VIA : KABEL-".$port['nama_kabel']." CORE-".$port['core']."</div>
                                    <div>STATUS : ".($port['status_core']?:'Not Set')."<a href='#' class='btn btn-xs btn-default pull-right' data-toggle='modal' data-target='#modal-set-redaman' data-back_link='".$port['back_link']."'><i class='ion ion-ios-speedometer-outline'></i> Set Status</a></div>
                                    <div>REDAMAN : ".($port['redaman']?:'Not Set')."</div>
                                    <div class='text-xs-center'></div>
                                  </fieldset>
                                </div>";
                  }
                  $spl = $port['urutan'];
                  if($port['bg_color']){
                    $style_port .= "background-color:".$port['bg_color'].";";
                  }
                  if($port['text_color']){
                    $style_port .= "color:".$port['text_color'].";";
                  }
                    $style_label = $style_port?:'color:black;';
                  if($port['status_port']=="book"){
                    $status_port_label='<span class="widget-activity-icon" style="'.$style_label.'">B</span>';
                    // dd($port,$status_port_label);
                  }
                  if($port['status_core']=="reti"){
                    $status_port_label='<span class="widget-activity-icon" style="'.$style_label.'">R</span>';
                  }
                  $content .= "<div class='m-t-1'>
                                  <fieldset class='the-fieldset'>
                                    <legend class='the-legend'>FRONT LINK</legend>";
                  if($port['front_link']){
                    if($port['spl_number']){
                      $front_text = 'SPL-'.$spl.'/OUT-'.$port['spl_number'];
                      $content .= "<div>VIA : SPL-".$port['urutan']."/OUT-".$port['spl_number']."</div>";
                    }else{
                      $front_text = 'SPL-'.$spl.'/IN';
                      $content .= "<div>VIA : SPL-".$spl."/IN</div>";

                    }
                    $content .= "<div><a href='#' class='btn btn-xs btn-warning' data-toggle='modal' data-target='#modal-change-port' data-basetray='".$j."' data-port='".$i."' data-portid='".$port['port_id']."' data-front_link='".$port['front_link']."'><i class='ion ion-shuffle'> Change Port</i></a>
                    <a href='#' class='btn btn-xs btn-danger pull-right' data-toggle='modal' data-target='#modal-lepas-spl' data-front_link='".$port['front_link']."'><i class='ion ion-log-out'> Lepas Konektor</i></a>
                    </div>";
                  }else{
                    $content .= "<div>VIA : Not Set</div>";
                  }
                  $content .= "</fieldset>
                                </div>";
                }
              ?>
              <div style="{{$style_port}}margin-top: 5px" class="port col-sm-1 col-md-1 col-lg-1 col-xl-1 col-xs-1 text-center b-a-2 border-round border-default SPL SPL-{{ $spl }} " tabindex="0" data-placement="bottom" role="button" data-state="primary" data-highligth="{{ $spl }}" data-html="true" data-toggle="popover" data-container="body" data-trigger="focus" title="BASETRAY-{{$j}} PORT-{{$i}} DETAIL & ACTION" data-content="{{ $content }}">
                {!! $status_port_label !!}
                {!! $front_text !!}<br/>
                {!! $back_text !!}
              </div>
            @endfor

          </div>
        </div>
      </div>
    </div>
  @endforeach
</div>
<div class="table-light m-t-1">
  <div class="table-header">
    <div class="table-caption">
      Logs
    </div>
  </div>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>#</th>
        <th>Keterangan</th>
        <th>User</th>
        <th>Date</th>
      </tr>
    </thead>
    <tbody>
      @foreach($logs as $no => $l)
        <tr>
          <td>{{ ++$no }}</td>
          <td>{{ $l->ket }}</td>
          <td>{{ $l->user }}</td>
          <td>{{ $l->ts }}</td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
<!-- modal -->
<div class="modal fade" id="modal-base" tabindex="-1">
  <form method="post" action="/mc/saveLink">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Modal title</h4>
        </div>
        <div class="modal-body">
          
          <input type="hidden" name="odc_id" id="hidden_odc_id" value="{{ Request::segment(2) }}"/>
          <input type="hidden" name="basetray" id="hidden_basetray"/>
          <input type="hidden" name="port" id="hidden_port"/>
          <input type="hidden" name="portid" id="hidden_portid"/>

          <input type="hidden" name="old_nama_kabel" id="old_nama_kabel"/>
          <input type="hidden" name="old_core" id="old_core"/>
          <input type="hidden" name="old_select_splitter" id="old_select_splitter"/>
          <input type="hidden" name="old_spl_link" id="old_spl_link"/>
          <div class="row">
            <div class="col-md-6">

            
              <div class="panel panel-info panel-dark">
                <div class="panel-heading">
                  <span class="panel-title">Back Link</span>
                  <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
                </div>
                <div class="panel-body form-horizontal">
                  
                  <div class="form-group notbook existing_kbl">
                    <label for="select_kabel" class="col-md-3 control-label">Pilih Kabel</label>
                    <div class="col-md-9">
                      <select class="custom-select form-control" name="select_kabel" id="select_kabel">
                        <option value="0" data-text="Not selected" data-detil="-" selected>Not selected</option>
                        @foreach($kblarray as $key => $k)
                          <option value="{{ $key }}" data-text="{{$k->nama_kabel}}" data-detil="RETI:{{ $k->reti_count }}">{{$k->nama_kabel}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group notbook select_core">
                    <label for="select_core" class="col-md-3 control-label">Pilih Core</label>
                    <div class="col-md-9">
                      <input type="hidden" class="form-control" id="select_core" name="select_core"/>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="back_link_var" class="col-md-3 control-label">Label Port</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" id="back_link_var" name="back_link_var" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="panel panel-primary">
                <div class="panel-heading">
                  <span class="panel-title">Front Link</span>
                  <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
                </div>
                <div class="panel-body form-horizontal">
                  <div class="form-group">
                    <label class="col-md-3 control-label">Splitter</label>
                    <div class="col-md-9">
                      <label class="custom-control custom-radio radio-inline" for="splitter_1">
                        <input type="radio" id="splitter_1" name="splitter" class="custom-control-input" value="new">
                        <span class="custom-control-indicator"></span>
                        New 
                      </label>
                      <label class="custom-control custom-radio radio-inline" for="splitter_2">
                        <input type="radio" id="splitter_2" name="splitter" class="custom-control-input" value="existing" checked>
                        <span class="custom-control-indicator"></span>
                        Existing
                      </label>
                    </div>
                  </div>

                  <div class="form-group existing_spl">
                    <label for="select_splitter" class="col-md-3 control-label">Pilih Splitter</label>
                    <div class="col-md-9">
                      <select class="cform-control" id="select_splitter" name="select_splitter">
                        <option value="0" data-text="Not selected" data-detil="-"></option>
                        @foreach($splarray as $key=> $s)
                          <option value="{{ $key }}" data-text="SPL-{{ $s['urutan'] }}" data-detil="Used:{{ $s['used'] }}/Empty:{{ $s['free'] }}"></option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group new_spl">
                    <label for="new_splitter" class="col-md-3 control-label">New Splitter</label>
                    <div class="col-md-9">
                      <select class="custom-select form-control" id="new_splitter" name="new_splitter">
                        <option value="0" selected>Not selected</option>
                        <option value="2">1:2</option>
                        <option value="4">1:4</option>
                        <option value="8">1:8</option>
                        <option value="16">1:16</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Link Splitter</label>
                    <div class="col-md-9 link-spl">
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </form>
</div>

<div class="modal fade" id="modal-kabel" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form method="post" action="/mc/saveKabel">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Form Input Kabel</h4>
        </div>
        <div class="modal-body">
          
          <input type="hidden" name="odc_id" value="{{ Request::segment(2) }}"/>
          <input type="hidden" name="kabel_id" id="kabel_id" value="0"/>

          <input type="hidden" name="kapasitas_old" id="kapasitas_old"/>
          <input type="hidden" name="nama_kabel_old" id="nama_kabel_old"/>
          <input type="hidden" name="jenis_kabel_old" id="jenis_kabel_old"/>
          <div class="panel panel-info">
            <div class="panel-body form-horizontal">
              <div class="form-group">
                <label for="jenis_kabel" class="col-md-3 control-label">Jenis Kabel</label>
                <div class="col-md-9">
                  <select class="custom-select form-control" id="jenis_kabel" name="jenis_kabel">
                    <option value="0">Feeder</option>
                    <option value="1">Distribusi</option>
                  </select>
                </div>
              </div>
              <div class="form-group notbook existing_kbl">
                <label for="kapasitas" class="col-md-3 control-label">Kapasitas</label>
                <div class="col-md-9">
                  <select class="custom-select form-control" name="kapasitas" id="kapasitas">
                    <option value="12">12 Core</option>
                    <option value="24">24 Core</option>
                    <option value="48">48 Core</option>
                    <option value="96">96 Core</option>
                    <option value="144">144 Core</option>
                    <option value="288">288 Core</option>
                  </select>
                </div>
              </div>
              <div class="form-group notbook">
                <label for="nama_kabel" class="col-md-3 control-label">Nama Kabel</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" id="nama_kabel" name="nama_kabel" maxlength="12" />
                </div>
              </div>
              <div class="form-group rt">
                <label for="rt" class="col-md-3 control-label">Reti Count</label>
                <div class="col-md-9">
                  <span class="label label-danger reti_count"></span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </form>
  </div>
</div>
<div class="modal fade" id="modal-status-port" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form method="post" action="/mc/saveStatusPort">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Form Status Port</h4>
        </div>
        <div class="modal-body">
          
          <input type="hidden" name="odc_id" id="sp_odc_id" value="{{ Request::segment(2) }}"/>
          <input type="hidden" name="basetray" id="sp_basetray"/>
          <input type="hidden" name="port" id="sp_port"/>
          <input type="hidden" name="portid" id="sp_portid"/>
          <div class="panel panel-info">
            <div class="panel-body form-horizontal">
              <div class="form-group">
                <label for="status_port" class="col-md-3 control-label">Status Port</label>
                <div class="col-md-9">
                  <select class="custom-select form-control" id="status_port" name="status_port">
                    <option value="0">Not Selected</option>
                    <option value="book">Book</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="catatan" class="col-md-3 control-label">Catatan</label>
                <div class="col-md-9">
                  <textarea class="form-control" id="catatan" name="catatan"></textarea>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </form>
  </div>
</div>
<div class="modal fade" id="modal-lepas-spl" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form method="post" action="/mc/saveLepasSPL">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Form Lepas SPL</h4>
        </div>
        <div class="modal-body">
          <input type="hidden" name="ls_front_link" id="ls_front_link"/>
          <div class="panel panel-info">
            <div class="panel-body form-horizontal">
              <div class="form-group">
                <label for="catatan" class="col-md-3 control-label">Catatan</label>
                <div class="col-md-9">
                  <textarea class="form-control" id="catatan" name="catatan"></textarea>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </form>
  </div>
</div>
<div class="modal fade" id="modal-booking-spl" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form method="post" action="/mc/saveBookingSPL">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Form Booking SPL</h4>
        </div>
        <div class="modal-body">
          <input type="hidden" name="bs_spl_id" id="bs_spl_id"/>
          <input type="hidden" name="bs_spl_number" id="bs_spl_number"/>
          <input type="hidden" name="odc_id" id="sp_odc_id" value="{{ Request::segment(2) }}"/>
          <div class="panel panel-info">
            <div class="panel-body form-horizontal">
              <div class="form-group">
                <label for="catatan" class="col-md-3 control-label">Catatan</label>
                <div class="col-md-9">
                  <textarea class="form-control" id="catatan" name="catatan"></textarea>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </form>
  </div>
</div>
<div class="modal fade" id="modal-set-redaman" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form method="post" action="/mc/saveSetRedaman">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Form Set Redaman</h4>
        </div>
        <div class="modal-body">
          
          <input type="hidden" name="odc_id" id="sr_odc_id" value="{{ Request::segment(2) }}"/>
          <input type="hidden" name="basetray" id="sr_basetray"/>
          <input type="hidden" name="port" id="sr_port"/>
          <input type="hidden" name="portid" id="sr_portid"/>
          <input type="hidden" name="back_link" id="sr_back_link"/>
          <div class="panel panel-info">
            <div class="panel-body form-horizontal">
              <div class="form-group">
                <label for="status_back_link" class="col-md-3 control-label">Status Back Link</label>
                <div class="col-md-9">
                  <select class="custom-select form-control" id="status_back_link" name="status_back_link">
                    <option value="0">Not Selected</option>
                    <option value="reti">Reti</option>
                    <option value="good">Good</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="redaman" class="col-md-3 control-label">Redaman</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" id="redaman" name="redaman"/>
                </div>
              </div>
              <div class="form-group">
                <label for="catatan" class="col-md-3 control-label">Catatan</label>
                <div class="col-md-9">
                  <textarea class="form-control" id="catatan" name="catatan"></textarea>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </form>
  </div>
</div>
<div class="modal fade" id="modal-input-basetray" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form method="post" action="/mc/saveBasetrayAdd">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Form Input Basetray</h4>
        </div>
        <div class="modal-body">
          <input type="hidden" name="odc_id" id="sr_odc_id" value="{{ Request::segment(2) }}"/>
          <div class="panel panel-info">
            <div class="panel-body form-horizontal">
              <div class="form-group">
                <label for="jumlah_basetray" class="col-md-3 control-label">Tambah Basetray</label>
                <div class="col-md-9">
                  <input type="number" class="form-control" id="jumlah_basetray" name="jumlah_basetray"/>
                </div>
              </div>
              <div class="form-group">
                <label for="jumlah_port" class="col-md-3 control-label">Jumlah Port</label>
                <div class="col-md-9">
                  <input type="number" class="form-control" id="jumlah_port" name="jumlah_port"/>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </form>
  </div>
</div>
<div class="modal fade" id="modal-update-basetray" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form method="post" action="/mc/saveBasetray">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Form Edit Basetray</h4>
        </div>
        <div class="modal-body">
          <input type="hidden" name="odc_id" id="ub_odc_id" value="{{ Request::segment(2) }}"/>
          <input type="hidden" name="basetray_id" id="ub_basetray_id"/>
          <div class="panel panel-info">
            <div class="panel-body form-horizontal">
              <div class="form-group">
                <label for="label_basetray" class="col-md-3 control-label">Label</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" id="label_basetray" name="label_basetray"/>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </form>
  </div>
</div>
<div class="modal fade" id="modal-change-port" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form method="post" action="/mc/saveChangePort">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Form Change Port</h4>
        </div>
        <div class="modal-body">
          <input type="hidden" name="odc_id" id="cp_odc_id" value="{{ Request::segment(2) }}"/>
          <input type="hidden" name="portid_old" id="cp_port_id_old"/>
          <input type="hidden" name="basetray_old" id="cp_basetray_old"/>
          <input type="hidden" name="port_old" id="cp_port_old"/>
          <input type="hidden" name="front_link" id="cp_front_link"/>
          
          <div class="panel panel-info">
            <div class="panel-body form-horizontal">
              <div class="form-group">
                <label for="cp_basetray" class="col-md-3 control-label">Basetray</label>
                <div class="col-md-9">
                  <select class="custom-select form-control" id="cp_basetray" name="basetray">
                    <option value="0" data-basetray="Select" data-detil="Select">Not Selected</option>
                    @foreach($basetray as $key => $bt)
                      <option value="{{ ++$key }}" data-basetray="{{ $key }}" data-detil="{{ $bt->free_count }}">{{ $key }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="cp_port" class="col-md-3 control-label">Port</label>
                <div class="col-md-9">
                  <input type="hidden" class="form-control" id="cp_port" name="port_id_new"/>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
  $(document).ready(function() {

    function formatPort(item) {
      if(item.status==='Free'){
        return 'Port '+item.text+' <span class="label label-primary">' + item.status + '</span> <span class="label label-info">'+ item.link+'</span>';
      }else{
        return 'Port '+item.text+' <span class="label label-danger">' + item.status + '</span> <span class="label label-warning">'+ item.link+'</span>';
      }
    }
    function formatBasetray(item) {
      var originalOption = item.element;
      return $(originalOption).data('basetray')+' <span class="label label-info pull-right"> Free ' + $(originalOption).data('detil') + '</span> ';
    }
    function format(item) {
      var originalOption = item.element;
      return $(originalOption).data('text')+' <span class="label label-info pull-right">' + $(originalOption).data('detil') + '</span> ';
    }
    function formatCore(item) {
      var originalOption = item.element;
      if(item.basetray){
        return 'Core : '+item.id+' <span class="label label-primary">'+item.basetray+'/'+item.port+'</span> TO <span class="label label-danger pull-right">' + item.back_link_var + '</span>';
      }else{
        return 'Core : '+item.id+' <span class="label label-info pull-right">' + item.back_link_var + '</span>';
      }
      
    }
    var splarray = <?= json_encode($splarray); ?>;
    var kblarray = <?= json_encode($kblarray); ?>;
    var portarray = <?= json_encode($portarray); ?>;
    $('[data-toggle="popover"]').popover();
    $('.SPL').hover(function(e){
      if(e.target.dataset.highligth){
        $('.SPL-'+e.target.dataset.highligth).addClass('border-success') 
      }
    });
    $('.SPL').mouseleave(function(e){ 
      if(e.target.dataset.highligth){
        $('.SPL-'+e.target.dataset.highligth).removeClass('border-success') 
      }
    });
    var spl = $('#select_splitter');
    spl.select2({
      templateResult: format,
      templateSelection: format,
      escapeMarkup: function(m) { return m; },
      dropdownParent: $('#modal-base')
    }).change(function(e){
      
      var content = '';
      if($(this).val()!=0){
        $.each(splarray[$(this).val()].link, function(index, v) {
          var options=v.status?"disabled":"";
          if($('#hidden_basetray').val()==v.basetray && $('#hidden_port').val()==v.port){
            options=' checked';
          }
          content += '<label class="custom-control custom-radio" for="spl_link_'+index+'"><input type="radio" id="spl_link_'+index+'" name="spl_link" class="custom-control-input" '+options+' value="'+index+'"><span class="custom-control-indicator"></span>';
          if(index){
            content += 'OUT-'+index+':';
          }else{
            content += 'IN:';
          }
          if(v.status=="in_use"){
            content+='<span class="label label-info label-xs">'+v.basetray+'/'+v.port+'</span> TO <span class="label label-info label-xs">'+v.back_link_var+'</span>';
          }else if(v.status=="book"){
            content+='<span class="label label-warning label-xs">Booked</span>';
          }else{
            content+='<span class="label label-primary label-xs">Free</span>';
          }
          content += '</label>';
        });
      }
      $('#modal-base').find('.link-spl').html(content)
    });
    var select_core = $('#select_core').select2({
      dropdownParent: $('#modal-base'),
        templateResult: formatCore,
        templateSelection: formatCore,
        escapeMarkup: function(m) { return m; },
        placeholder:"select"
    }); 
    $('#select_kabel').select2({
      templateResult: format,
      templateSelection: format,
      escapeMarkup: function(m) { return m; },
      dropdownParent: $('#modal-base')
    }).change(function(e){
      
      if($(this).val()!=0 && $(this).val()!=null){
        $.get("/ajxkabelcore/"+$(this).val(), function(data, status){
          select_core.select2({
            data:data,
            dropdownParent: $('#modal-base'),
            templateResult: formatCore,
            templateSelection: formatCore,
            escapeMarkup: function(m) { return m; },
            placeholder:"select"
          });
        });
      }
      
    });
    $('#modal-base').on('show.bs.modal', function (event) {
      var rt = $(event.relatedTarget)
      var modal = $(this)
      $('#select_core').val(0);
      $('#hidden_basetray').val(rt.data('basetray'));
      $('#hidden_port').val(rt.data('port'));
      $('#hidden_portid').val(0);
      $('#select_kabel').val(0).trigger('change');
      select_core.select2({
            data:null,
            placeholder:"Select Kabel First"
          });
      $('#old_spl_link').val(0);
      $('#old_select_splitter').val(0);
      $('#old_core').val(0);
      $('#old_nama_kabel').val(0);
      $('#back_link_var').val(null);

      if(rt.data('portid')){
        $('#hidden_portid').val(rt.data('portid'));
        var port = portarray[rt.data('basetray')][rt.data('port')];
        $('#back_link_var').val(port.back_link_var);
        if(port.kbl_id){
          $('#select_kabel').val(port.kbl_id).trigger('change');
          $('#select_core').val(port.core);
          $('#old_core').val(port.core);
          $('#old_nama_kabel').val(port.nama_kabel);
        }else{
          select_core.select2({
            data:null,
            placeholder:"Select Kabel First"
          });
        }
      }
      if(rt.data('frontlink')){
        spl.val(port.spl_id).trigger('change');
        $('#old_spl_link').val(port.spl_number);
        $('#old_select_splitter').val(port.urutan);
      }else{
        spl.val(0).change();
      }
      modal.find('.modal-title').text('Link Basetray:'+rt.data('basetray')+' Port:'+rt.data('port'))
    })
    $('.new_spl').hide();
    $('input[name=splitter]').change(function(){
      if($(this).val()=="new"){
        $('.existing_spl').hide();
        $('.new_spl').show();
        $('#modal-base').find('.link-spl').html('')
        $('#new_splitter').val(0).trigger('change');
      }else{
        spl.trigger('change');
        $('.new_spl').hide();
        $('.existing_spl').show();
      }
    })
    
    var new_splitter = $('#new_splitter');
    new_splitter.select2({
      dropdownParent: $('#modal-base')
    }).change(function(e){
      var text='';
      if($(this).val()!=0){
        for (let i = 0; i <= $(this).val(); i++) {
          if(i){
            text += '<label class="custom-control custom-radio" for="spl_link_'+i+'"><input type="radio" id="spl_link_'+i+'" name="spl_link" class="custom-control-input" value="'+i+'"><span class="custom-control-indicator"></span>OUT-'+i+':<span class="label label-primary label-xs">Free</span></label>';
          }else{
            text += '<label class="custom-control custom-radio" for="spl_link_0"><input type="radio" id="spl_link_0" name="spl_link" class="custom-control-input" value="'+i+'"><span class="custom-control-indicator"></span>IN:<span class="label label-primary label-xs">Free</span></label>';
          }
        }
      }
      $('#modal-base').find('.link-spl').html(text)
    });
    $('#modal-kabel').on('show.bs.modal', function (event) {
      var rt = $(event.relatedTarget)
      var modal = $(this)
      modal.find('#kabel_id').val(rt.data('kblid'));
      if(rt.data('kblid')){
        modal.find('.modal-title').text('Form Update Kabel')
        modal.find('#jenis_kabel').val(kblarray[rt.data('kblid')].jenis_kabel);
        modal.find('#kapasitas').val(kblarray[rt.data('kblid')].kap);
        modal.find('#nama_kabel').val(kblarray[rt.data('kblid')].nama_kabel);

        modal.find('#jenis_kabel_old').val(kblarray[rt.data('kblid')].jenis_kabel);
        modal.find('#kapasitas_old').val(kblarray[rt.data('kblid')].kap);
        modal.find('#nama_kabel_old').val(kblarray[rt.data('kblid')].nama_kabel);

        modal.find('.reti_count').html(kblarray[rt.data('kblid')].reti_count);
        modal.find('.rt').show();
      }else{
        modal.find('.modal-title').text('Form Input Kabel');
        modal.find('#jenis_kabel').val(1);
        modal.find('#kapasitas').val(24);
        modal.find('#nama_kabel').val('');
        modal.find('.reti_count').html(0);
        modal.find('.rt').hide();
      }
      
    });
    $('#modal-status-port').on('show.bs.modal', function (event) {
      var rt = $(event.relatedTarget)
      var modal = $(this)
      modal.find('#sp_basetray').val(rt.data('basetray'));
      modal.find('#sp_port').val(rt.data('port'));
      modal.find('#sp_portid').val(rt.data('portid'));
    });
    $('#modal-booking-spl').on('show.bs.modal', function (event) {
      var rt = $(event.relatedTarget)
      var modal = $(this)
      modal.find('#bs_spl_id').val(rt.data('splid'));
      modal.find('#bs_spl_number').val(rt.data('splnumber'));
    });
    $('#modal-lepas-spl').on('show.bs.modal', function (event) {
      var rt = $(event.relatedTarget)
      var modal = $(this)
      modal.find('#ls_front_link').val(rt.data('front_link'));
    });
    $('#modal-set-redaman').on('show.bs.modal', function (event) {
      var rt = $(event.relatedTarget)
      var modal = $(this)
      modal.find('#sr_back_link').val(rt.data('back_link'));
    });
    $('#modal-update-basetray').on('show.bs.modal', function (event) {
      var rt = $(event.relatedTarget)
      var modal = $(this)
      modal.find('#ub_basetray_id').val(rt.data('basetray_id'));
    });
    $('#modal-change-port').on('show.bs.modal', function (event) {
      var rt = $(event.relatedTarget)
      var port = portarray[rt.data('basetray')][rt.data('port')];
      var modal = $(this)
      
      modal.find('#cp_port_id_old').val(0);
      modal.find('#cp_basetray_old').val(0);
      modal.find('#cp_port_old').val(0);
      modal.find('#cp_front_link').val(0);
      modal.find('.modal-title').text('Change Port '+rt.data('basetray')+'/'+rt.data('port')+'/'+ port.spl_number);
      modal.find('#cp_port_id_old').val(rt.data('portid'));
      modal.find('#cp_basetray_old').val(rt.data('basetray'));
      modal.find('#cp_port_old').val(rt.data('port'));
      modal.find('#cp_front_link').val(rt.data('front_link'));

    });
    var cp_port = $('#cp_port').select2();
    $('#cp_basetray').select2({
      dropdownParent: $('#modal-change-port'),
      templateResult: formatBasetray,
      templateSelection: formatBasetray,
      escapeMarkup: function(m) { return m; },
      placeholder:"select"
    }).change(function(){
      if($(this).val()){
        $.get("/ajxbasetrayport/"+$("#ub_odc_id").val()+"/"+$(this).val(), function(data, status){
          cp_port.select2({
            data:data,
            dropdownParent: $('#modal-change-port'),
            templateResult: formatPort,
            templateSelection: formatPort,
            escapeMarkup: function(m) { return m; },
            placeholder:"select"
          });
        });
      }
    });
  });
</script>
@endsection