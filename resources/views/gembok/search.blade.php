@extends('layout')

@section('content')

    <link rel="stylesheet" href="/bower_components/amcharts3/amcharts/plugins/export/export.css" type="text/css" media="all" />
    <form class="row" style="margin-bottom: 20px;">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search Perangkat" name="q"/>
            <span class="input-group-btn">
                <button class="btn btn-default" type="submit">
                    <span class="glyphicon glyphicon-search"></span>
                </button>
            </span>
        </div>
    </form>

    <?php $authLevel = Session::get('auth')->level ?>

    @if ($authLevel == '2')
        <a href="/gembok/create" class="btn btn-info" style="margin: 0 -15px;">
            <span class="glyphicon glyphicon-plus"></span>
            <span>Input Gembok</span>
        </a>
    @endif
    <?php
        $links = array("KALSEL"=>"",
                        "KALTARA"=>"https://t.me/joinchat/Ax8Z_EJkXdL23-UxRCt-zQ",
                        "KALTENG"=>"https://t.me/joinchat/Ax8Z_EGtv8zeqAeNgH6PXQ", 
                        "KALBAR"=>"https://t.me/joinchat/Ax8Z_EIGX40DtCE0rdiCBg",
                        "BALIKPAPAN"=>"https://t.me/joinchat/Ax8Z_EChF9B9bsJIpkKKyQ",
                        "SAMARINDA"=>"https://t.me/joinchat/Ax8Z_EJQau-tRXB0qjxixA",
                        "ALL" => "");
        $link = "<span class='label label-danger'>witel anda belum terdaftar</span>";
        if(session('auth')->witel){
            $link = @$links[session('auth')->witel];
        }
            // echo '<br/><br/><div class="alert alert-warning">Silahkan Sesuaikan profile anda, <a href="/profile">Klik Disini!</a><br>Silahkan join ke grup telegram berikut : <a href="'.$link.'">'.$link.'</a></div>';
    ?>
    @if (count($workUnclosed))
        <div class="panel panel-warning">
            <div class="panel-heading">Pekerjaan sedang berlangsung</div>
            <div class="panel-body table-responsive">
                <table class="table table-bordered table-striped">
                    <tr>
                        <th>Start</th>
                        <th>Durasi</th>
                        <th>Kode Perangkat</th>
                        <th>Pekerjaan</th>
                        <th>User</th>
                        <th width="50">&nbsp;</th>
                    </tr>
                    @foreach($workUnclosed as $log)
                        <?php
                        $dteStart = new \DateTime(date('Y-m-d H:i:s', $log->time_start)); 
                        $dteEnd   = new \DateTime(date('Y-m-d H:i:s')); 
                        $dteDiff  = $dteStart->diff($dteEnd); 
                        $durasi   = $dteDiff->format("%Hh %Im %Ss");
                        ?>
                        <tr>
                            <td>{{ date('d/m/Y H:i', $log->time_start) }}</td>
                            <td>{{ $durasi }}</td>
                            <td>
                            @if (session('auth')->level == '2' || session('auth')->level == '19')
                                <a href="/gembok/{{ $log->id_gembok }}">{{ $log->kode }}</a>
                            @else
                                {{ $log->kode }}
                            @endif
                            </td>
                            <td>{{ $log->pekerjaan }}</td>
                            <td>{{ $log->nama }}</td>
                            <td>
                                <a href="/work/{{$log->id}}/progress">Detail</a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    @endif

    @if (isset($list))
        <?php $url = $authLevel == '2' || $authLevel == '19'  ? '/gembok/' : '/work/start/' ?>
        <ul class="list-blocks">
            @foreach($list as $item)
                <li>
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle btn-block" type="button" data-toggle="dropdown">{{ $item->kode }}
                        <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="/work/start/{{ $item->id }}">Request Pin</a></li>
                            @if($authLevel == 2)
                                <li><a href="/gembok/{{ $item->id }}">Edit Gembok</a></li>
                                <li><a href="/lib/{{ $item->id  }}">Library Foto</a></li>
                            @endif
                        </ul>
                    </div>
                </li>
            @endforeach
        </ul>
    @endif
@endsection
