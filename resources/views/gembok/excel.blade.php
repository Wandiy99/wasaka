<div class="panel panel-default" id="info">
  <div class="panel-heading">List</div>
  <div id="fixed-table-container-demo" class="fixed-table-container">
    <table class="table table-bordered table-fixed">
      <tr>
        <th>#</th>
        <th>id</th> 
        <th>kode</th> 
        <th>jenis_alpro</th>
        <th>koordinat</th> 
        <th>type</th> 
        <th>kap</th> 
        <th>merk</th> 
        <th>witel</th> 
        <th>regional</th> 
        <th>engsel_odc</th> 
        <th>kerapian_odc</th> 
        <th>Versi</th> 
        <th>last_akses</th> 
        <th>isHapus</th> 
        <th>akses_count</th> 
      </tr>
      @foreach($data as $no => $list) 
        <tr>
          <td>{{ ++$no }}</td>
          <td>{{ $list->id }}</td> 
          <td>{{ $list->kode }}</td> 
          <td>{{ $list->jenis_alpro }}</td>
          <td>{{ $list->koordinat }}</td> 
          <td>{{ $list->type }}</td> 
          <td>{{ $list->kap }}</td> 
          <td>{{ $list->merk }}</td> 
          <td>{{ $list->witel }}</td> 
          <td>{{ $list->regional }}</td> 
          <td>{{ ($list->engsel_odc ? ($list->engsel_odc=='on'?'Berfungsi':'Rusak'):'Not Set') }}</td> 
          <td>{{ ($list->kerapian_odc ? ($list->kerapian_odc=='on'?'Rapih':'Perlu Dirapikan'):'Not Set') }}</td> 
          <td>{{ $list->gembok }}</td> 
          <td>{{ $list->last_akses }}</td> 
          <td>{{ $list->isHapus }}</td> 
          <td>{{ $list->akses_count }}</td> 
        </tr>
      @endforeach
    </table>
  </div>
</div>