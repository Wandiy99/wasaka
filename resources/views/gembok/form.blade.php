@extends('layout')
@section('head')
    <link rel="stylesheet" href="/bower_components/select2/dist/css/select2.css" />
@endsection
@section('content')
    @if (session('auth')->level == '2')
    <div class="panel panel-default">
        <div class="panel-heading">
            @if (isset($gembok))
                Edit Gembok {{ $gembok->kode }}
            @else
                Input Gembok Baru
            @endif
        </div>
        <div class="panel-body">
            <div class="form col-sm-8 col-md-8">
            <form method="post" class="form-horizontal" enctype="multipart/form-data">
                <div class="form-group {{ $errors->has('kode') ? 'has-error' : '' }}">
                    <label for="txtKode" class="col-sm-4 col-md-3 control-label">Kode Perangkat</label>
                    <div class="col-sm-8">
                        <input name="kode" type="text" id="txtKode" class="form-control input-sm"
                               value="{{ old('kode') ?: @$gembok->kode }}"/>
                        @foreach($errors->get('kode') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>

                <div class="form-group {{ $errors->has('pin') ? 'has-error' : '' }}">
                    <label for="txtPin" class="col-sm-4 col-md-3 control-label">Pin</label>
                    <div class="col-sm-2">
                        <input name="pin" type="text" id="txtPin" class="form-control input-sm" value="{{ old('pin') ?: @$gembok->pin }}" maxlength="4">
                        @foreach($errors->get('pin') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>
                <div class="form-group {{ $errors->has('fiberzone') ? 'has-error' : '' }}">
                    <label for="fiberzone" class="col-sm-4 col-md-3 control-label">Fiberzone</label>
                    <div class="col-sm-8">
                        <input type="text" name="fiberzone" id="fzz" class="form-control input-sm" value="{{ session('auth')->fiberzone }}" readonly />
                        @foreach($errors->get('fiberzone') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>
                <div class="form-group {{ $errors->has('pin') ? 'has-error' : '' }}">
                    <label for="witel" class="col-sm-4 col-md-3 control-label">Witel</label>
                    <!-- <input type="text" name="fiberzone" id="fzz" class="form-control input-lg" value="{{ session('auth')->fiberzone }}"/> -->
                    <div class="col-sm-8">
                        <input type="text" name="witel" id="witel" class="form-control input-lg" value="{{ $gembok->witel or '' }}"/>
                        @foreach($errors->get('witel') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>
                <div class="form-group hidden">
                    <label for="tipe" class="col-sm-4 col-md-3 control-label">Type ODC</label>
                    <div class="col-sm-8">
                        <select name="type" id="tipe" class="form-control">
                            <option value="ODC-C" {{ @$gembok->type=="ODC-C" ? "selected" : "" }}>ODC-C</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="merk" class="col-sm-4 col-md-3 control-label">Merk</label>
                    <div class="col-sm-8">
                        <input type="text" name="merk" id="merk" class="form-control input-lg" value="{{ $gembok->merk or '' }}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="kapasitas" class="col-sm-4 col-md-3 control-label">Kapasitas</label>
                    <div class="col-sm-8">
                        <input type="text" name="kap" id="kapasitas" class="form-control input-lg" value="{{ $gembok->kap or '' }}"/>
                    </div>
                </div> 
                <div class="form-group">
                    <label for="kategori" class="col-sm-4 col-md-3 control-label">Kategori</label>
                    <div class="col-sm-8">
                        <input type="text" name="kategori" id="kategori" class="form-control input-lg" value="{{ $gembok->kategori or '' }}"/>
                    </div>
                </div> 
                <div class="form-group">
                    <label for="koordinat" class="col-sm-4 col-md-3 control-label">Koordinat</label>
                    <div class="col-sm-8">
                        <input name="koordinat" type="text" id="koor" class="form-control input-sm" value="{{ old('koordinat') ?: @$gembok->koordinat }}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="fileProfile" class="control-label col-sm-4 col-md-3">Foto Profile</label>
                    <div class="col-sm-8">
                        <input name="profile" id="fileProfile" type="file" accept="image/*" class="form-control input-sm"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-md-offset-3 col-sm-3">
                        <button class="btn btn-primary" type="submit">
                            <span class="glyphicon glyphicon-floppy-disk"></span>
                            <span>Simpan</span>
                        </button>
                    </div>
                </div>
            </form>
            </div>
            <div class="profile col-sm-4 col-md-4 text-center">
                @if (@$gembok->profile)
                    <span class="help-block">
                        <a href="/upload/profile/{{ @$gembok->id }}.jpg">
                            <img src="/upload/profile/{{ $gembok->id }}-th.jpg" alt="" id="gambar">
                        </a>
                    </span>
                @endif
            </div>
            <?php
                $segment = Request::segment(2);
                $url = "http://wasaka.telkomakses.co.id/work/start/".$segment;
            ?>
            @if($segment != "create")
            <div class="col-sm-4 col-md-4 text-center">
                <a href="https://api.qrserver.com/v1/create-qr-code/?data={{ $url }}&#38;size=200x200">
                    <img src="https://api.qrserver.com/v1/create-qr-code/?data={{ $url }}&#38;size=200x200"/>
                </a>
            </div>
            @endif
        </div>
    </div>
    @endif
    @if (count($workUnclosed))
        <div class="panel panel-warning">
            <div class="panel-heading">Pekerjaan Belum Close</div>
            <div class="panel-body table-responsive">
                <table class="table table-bordered table-striped">
                    <tr>
                        <th width="140">Start</th>
                        <th>Durasi</th>
                        <th>Pekerjaan</th>
                        <th>User</th>
                        <th width="50">&nbsp;</th>
                    </tr>
                    @foreach($workUnclosed as $log)
                        <?php
                        $dteStart = new \DateTime(date('Y-m-d H:i:s', $log->time_start)); 
                        $dteEnd   = new \DateTime(date('Y-m-d H:i:s')); 
                        $dteDiff  = $dteStart->diff($dteEnd); 
                        $durasi   = $dteDiff->format("%Hh %Im %Ss");
                        ?>
                        <tr>
                            <td>{{ date('d/m/Y H:i', $log->time_start) }}</td>
                            <td>{{ $durasi }}</td>
                            <td>{{ $log->pekerjaan }}</td>
                            <td>{{ $log->nama }}</td>
                            <td>
                                <a href="/work/{{$log->id}}">Detail</a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    @endif

    @if (count($workClosed))
        <div class="panel panel-default">
            <div class="panel-heading">Aktifitas Terakhir <span class="label label-success">{{ @$gembok->kode }}</span></div>
            <div class="panel-body table-responsive">
                <table class="table table-bordered table-striped">
                    <tr>
                        <th>Start</th>
                        <th>Close</th>
                        <th>Durasi</th>
                        <th>Pekerjaan</th>
                        <th>User</th>
                        @if (session('auth')->level == '2')
                            <th>Pin Baru</th>
                        @endif
                        <th width="50">&nbsp;</th>
                    </tr>
                    @foreach($workClosed as $log)
                        <?php
                        $dteStart = new \DateTime(date('Y-m-d H:i:s', $log->time_start)); 
                        $dteEnd   = new \DateTime(date('Y-m-d H:i:s', $log->time_close)); 
                        $dteDiff  = $dteStart->diff($dteEnd); 
                        $durasi   = $dteDiff->format("%Hh %Im %Ss");
                        ?>
                        <tr>
                            <td>{{ date('d/m/Y H:i', $log->time_start) }}</td>
                            <td>{{ date('d/m/Y H:i', $log->time_close) }}</td>
                            <td>{{ $durasi }}</td>
                            <td>{{ $log->jenis_pekerjaan_id == 15 ? $log->pekerjaan : $log->jenis_pekerjaan }}</td>
                            <td>
                                <a href="/workUser/{{ $log->id_user }}">{{ $log->id_user }}
                                    @if($log->nama)
                                        {{ $log->nama }}
                                    @elseif($log->nama_user)
                                        {{ $log->nama_user }}
                                    @endif
                                </a>
                            </td>
                            @if (session('auth')->level == '2')
                                <td>{{ $log->pin_new }}</td>
                            @endif
                            <td>
                                <a href="/work/{{$log->id}}">Detail</a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>

            @if ($workClosed->total() > $workClosed->perPage())
                <div class="panel-footer panel-pager text-center">
                    {{-- TODO: custom pager view --}}
                    {{ $workClosed->links() }}
                </div>
            @endif
        </div>
    @endif
@endsection
@section('script')
    <script src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/bower_components/select2/dist/js/select2.full.js"></script>
    <script>
        $(function() {
            $('input[type=file]').change(function() {
                console.log(this.name);
                var inputEl = this;
                if (inputEl.files && inputEl.files[0]) {
                  $(inputEl).parent().find('input[type=text]').val(1);
                  var reader = new FileReader();
                  reader.onload = function(e) {
                    $('#gambar').attr('src', e.target.result);
                  }
                  reader.readAsDataURL(inputEl.files[0]);
                }
            });
            var witel = <?= json_encode($witel) ?>;
            var witel = $('#witel').select2({
                data: witel
            });
            $('#kapasitas').select2({
                data: [{"id":"48", "text":"48"},{"id":"144", "text":"144"},{"id":"288", "text":"288"}]
            });
            var merk = <?= json_encode($merk) ?>;
            $('#merk').select2({
                data: merk
            });
            var kat = [{"id":1, "text":"FTM"},{"id":2, "text":"ODC"},{"id":3, "text":"MSAN"},{"id":4, "text":"MDU"},{"id":5, "text":"ONU"},{"id":6, "text":"OLT"}];
            var kategori = $('#kategori').select2({
                data: kat
            });
        });
    </script>
@endsection