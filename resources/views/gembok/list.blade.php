@extends('layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">List Perangkat {{ Request::segment(2) }}</div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped">
                <tr>
                    <th>#</th>
                    <th>Perangkat</th>
                    <th>Koordinat</th>
                    <th>Type</th>
                    <th>Kap</th>
                    <th>Merk</th>
                    @if (session('auth')->level == '2')
                        <th>Pin Baru</th>
                    @endif
                </tr>
                @foreach($listGembok as $no => $lg)
                    <tr>
                        <td>{{ ++$no }}</td>
                        <td>{{ $lg->kode }}</td>
                        <td>{{ $lg->koordinat }}</td>
                        <td>{{ $lg->type }}</td>
                        <td>{{ $lg->kap }}</td>
                        <td>{{ $lg->merk }}</td>
                        @if (session('auth')->level == '2')
                            <td>{{ $lg->pin }}</td>
                        @endif
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection