@extends('layout')

@section('content')
    <div style="margin-bottom: 5px" class="panel panel-default">
        <div class="panel-heading">Pekerjaan <a href="/sendTeleManual/{{ Request::segment(2) }}" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-send"></span>Kirim ke Grup</a></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-3">
                    <label for="" class="control-label">User</label>
                </div>
                <div class="col-sm-6">
                    <p class="control-static">
                        @if ($data->id_karyawan)
                            [{{ $data->id_karyawan }}]
                            {{ $data->nama }}
                        @else
                            {{ $data->id_user }}
                        @endif
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <label for="" class="control-label">Kode Perangkat</label>
                </div>
                <div class="col-sm-6">
                    <p class="control-static">
                        @if (session('auth')->level == '2')
                            <a href="/gembok/{{ $data->id_gembok }}">{{ $data->kode }}</a>
                        @else
                            {{ $data->kode }}
                        @endif
                    </p>
                </div>
            </div>
 
            <div class="row">
                <div class="col-sm-3">
                    <label for="" class="control-label">Jenis Pekerjaan</label>
                </div>
                <div class="col-sm-6">
                    <p class="control-static">{{ $data->jenis_pekerjaan_id == 15 ? $data->pekerjaan : $data->jenis_pekerjaan }}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <label for="" class="control-label">Jam Open</label>
                </div>
                <div class="col-sm-6">
                    <p class="control-static">{{ date('d/m/Y H:i', $data->time_start) }}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <label for="" class="control-label">Jam Close</label>
                </div>
                <div class="col-sm-6">
                    <p class="control-static">{{ date('d/m/Y H:i', $data->time_close) }}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <label for="" class="control-label">Durasi</label>
                </div>
                <?php
                    $context = stream_context_create( [
                        'ssl' => [
                            'verify_peer' => false,
                            'verify_peer_name' => false,
                        ],
                    ]);
                    $dteStart = new \DateTime(date('Y-m-d H:i:s', $data->time_start)); 
                    $dteEnd   = new \DateTime(date('Y-m-d H:i:s', $data->time_close)); 
                    $dteDiff  = $dteStart->diff($dteEnd); 
                    $durasi   = $dteDiff->format("%Hh %Im %Ss");

                    $path="/upload/work/".$data->id;
                    $path2="https://marina.bjm.tomman.app/backupwasaka/work/".$data->id;
                    // dd(get_headers($path2)[0]);
                    $path3="https://marina.bjm.tomman.app/backupwasaka2/work/".$data->id;
                    if(file_exists(public_path().$path."/before.jpg")){
                        $path=$path;
                    }else if(stripos(get_headers($path2."/before.jpg", 0, $context)[0],"200 OK")){
                        $path = $path2;
                    }else if(stripos(get_headers($path3."/before.jpg", 0, $context)[0],"200 OK")){
                        $path = $path3;
                    }else{
                        $path = '/image/placeholder.gif';
                    }
                    $pin="/upload/pin/".$data->id;
                    $pin2="https://marina.bjm.tomman.app/backupwasaka/pin/".$data->id;
                    $pin3="https://marina.bjm.tomman.app/backupwasaka2/pin/".$data->id;
                    $pinacak="/upload/pin/acak/".$data->id;
                    $pinacak2="https://marina.bjm.tomman.app/backupwasaka/pin/acak/".$data->id;
                    $pinacak3="https://marina.bjm.tomman.app/backupwasaka2/pin/acak/".$data->id;
                    if(file_exists(public_path().$pin.".jpg")){
                        $pin=$pin;
                    }else if(stripos(get_headers($pin2.".jpg", 0, $context)[0],"200 OK")){
                        $pin = $pin2;
                    }else if(stripos(get_headers($pin3.".jpg", 0, $context)[0],"200 OK")){
                        $pin = $pin3;
                    }else{
                        $pin = '/image/placeholder.gif';
                    }
                    if(file_exists(public_path().$pinacak.".jpg")){
                        $pinacak=$pinacak;
                    }else if(stripos(get_headers($pinacak2.".jpg", 0, $context)[0],"200 OK")){
                        $pinacak = $pinacak2;
                    }else if(stripos(get_headers($pinacak3.".jpg", 0, $context)[0],"200 OK")){
                        $pinacak = $pinacak3;
                    }else{
                        $pinacak = '/image/placeholder.gif';
                    }
                    // $pin = str_replace('backupwasaka/work', 'backupwasaka2/pin', $path);
                    // dd($pin);
                ?>
                <div class="col-sm-6">
                    <p class="control-static">{{ $durasi }}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <label for="" class="control-label">Uraian</label>
                </div>
                <div class="col-sm-6">
                    <p class="control-static">{!! nl2br($data->uraian) !!}</p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    <label for="" class="control-label">Anggota</label>
                </div>
                <div class="col-sm-6">
                    <p class="control-static">{!! nl2br($data->anggota) !!}</p>
                </div>
            </div>
        </div>

        <div class="panel-heading">Evidence</div>
        <div class="panel-body view-evidence">
            <div class="row col-sm-8">
                <div class="col-sm-6 text-center">
                    <a href="{{ $path }}/before.jpg">
                        <img src="{{ $path }}/before-th.jpg" alt="">
                    </a>
                    <p>Sebelum Pekerjaan</p>
                </div>

                <div class="col-sm-6 text-center">
                    <a href="{{ $path }}/item-before.jpg">
                        <img src="{{ $path }}/item-before-th.jpg" alt="">
                    </a>
                    <p>Perangkat Sebelum Pekerjaan</p>
                </div>

                <div class="col-sm-6 text-center">
                    <a href="{{ $path }}/item-after.jpg">
                        <img src="{{ $path }}/item-after-th.jpg" alt="">
                    </a>
                    <p>Perangkat Setelah Pekerjaan</p>
                </div>
                <div class="col-sm-6 text-center">
                    <a href="{{ $path }}/work-progress.jpg">
                        <img src="{{ $path }}/work-progress-th.jpg" alt="">
                    </a>
                    <p>Proses Pengerjaan</p>
                </div>
            </div>
            <div class="row col-sm-4">
                @if (session('auth')->level == '2')
                <div class="text-center">
                    <a href="{{ $pin }}.jpg">
                        <img src="{{ $pin }}-th.jpg" alt="">
                    </a>
                    <p>Pin</p>
                </div>
                @endif
                @if (session('auth')->level == '2')
                <div class="text-center">
                    <a href="{{ $pinacak }}.jpg">
                        <img src="{{ $pinacak }}-th.jpg" alt="">
                    </a>
                    <p>Pin Acak</p>
                </div>
                @endif
            </div>
        </div>
    </div>
@endsection