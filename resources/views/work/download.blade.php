@extends('layout')
@section('head')
    <link rel="stylesheet" href="/bower_components/select2/dist/css/select2.css" />
@endsection
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Download Form
        </div>
        <div class="panel-body">
            <div class="form col-sm-8 col-md-8">
            <form method="post" class="form-horizontal">
                <div class="form-group">
                    <label for="tgl" class="col-sm-4 col-md-3 control-label">Tgl</label>
                    <div class="col-sm-8">
                        <input name="tgl" type="text" id="tgl" class="form-control input-sm"
                               value="{{date('Y-m')}}"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-md-offset-3 col-sm-3">
                        <button class="btn btn-primary" type="submit">
                            <span class="glyphicon glyphicon-floppy-disk"></span>
                            <span>Download</span>
                        </button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/bower_components/select2/dist/js/select2.full.js"></script>

    <script>
        $(".wait-indicator").remove();
    </script>
@endsection