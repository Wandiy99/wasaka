@extends('layout')

@section('content')
    @if (count($workUnclosed))
        <div class="panel panel-warning">
            <div class="panel-heading">Pekerjaan sedang berlangsung</div>
            <div class="panel-body table-responsive">
                <table class="table table-bordered table-striped">
                    <tr>
                        <th>Start</th>
                        <th>Durasi</th>
                        <th>Kode Perangkat</th>
                        <th>Pekerjaan</th>
                        <th>User</th>
                        <th width="50">&nbsp;</th>
                    </tr>
                    @foreach($workUnclosed as $log)

                        <?php
                        $dteStart = new \DateTime(date('Y-m-d H:i:s', $log->time_start)); 
                        $dteEnd   = new \DateTime(date('Y-m-d H:i:s')); 
                        $dteDiff  = $dteStart->diff($dteEnd); 
                        $durasi   = $dteDiff->format("%Hh %Im %Ss");
                        ?>
                        <tr>
                            <td>{{ date('d/m/Y H:i', $log->time_start) }}</td>
                            <td>{{ $durasi }}</td>
                            <td>
                            @if (session('auth')->level == '2' || session('auth')->level == '19')
                                <a href="/gembok/{{ $log->id_gembok }}">{{ $log->kode }}</a>
                            @else
                                {{ $log->kode }}
                            @endif
                            </td>
                            <td>{{ $log->jenis_pekerjaan_id == 15 ? $log->pekerjaan : $log->jenis_pekerjaan }}</td>
                            <td>
                                @if($log->nama)
                                    {{ $log->nama }}
                                @elseif($log->nama_user)
                                    {{ $log->nama_user }}
                                @endif
                            </td>
                            <td>
                                @if (session('auth')->id_user == $log->id_user)
                                    <a href="/work/{{$log->id}}/progress">Detail</a>
                                @else
                                    Other
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
            
        </div>
    @endif
        <div class="panel panel-default">
            <div class="panel-heading">History </div>
            <div class="panel-body table-responsive">
                <table class="table table-bordered table-striped">
                    <tr>
                        <form method="post" class="form-horizontal" action="/historySearch" enctype="multipart/form-data">
                        <th>Start<input name="start" type="text" class="form-control input-sm"/></th>
                        <th>Close<input name="close" type="text" class="form-control input-sm"/></th>
                        <th width="120" style="vertical-align: middle;">Durasi</th>
                        <th>Kode Perangkat<input name="odc" type="text" class="form-control input-sm"/></th>
                        <th>Pekerjaan<input name="work" type="text" class="form-control input-sm"/></th>
                        <th>User<input name="user" type="text" class="form-control input-sm"/></th>
                        @if (session('auth')->level == '2')
                            <th>Pin Baru</th><th>Pin Acak</th>
                        @endif
                        <th width="50"><br/><button class="btn btn-info btn-sm" type="submit">
                            <span class="glyphicon glyphicon-search"></span>
                        </button></th>
                        </form>
                    </tr>
                    @foreach($workClosed as $log)
                        <?php
                        $dteStart = new \DateTime(date('Y-m-d H:i:s', $log->time_start)); 
                        $dteEnd   = new \DateTime(date('Y-m-d H:i:s', $log->time_close)); 
                        $dteDiff  = $dteStart->diff($dteEnd); 
                        $durasi   = $dteDiff->format("%Hh %Im %Ss");
                        ?>
                        <tr>
                            <td>{{ date('d/m/Y H:i', $log->time_start) }}</td>
                            <td>{{ date('d/m/Y H:i', $log->time_close) }}</td>
                            <td>{{ $durasi }}</td>
                            <td>
                            <a href="/gembok/{{ $log->id_gembok }}">{{ $log->kode }}</a>
                            
                            </td>
                            <td>{{ $log->jenis_pekerjaan_id == 15 ? $log->pekerjaan : $log->jenis_pekerjaan }}</td>
                            <td>
                                <a href="/workUser/{{ $log->id_user }}">{{ $log->id_user }}
                                @if($log->nama)
                                    {{ $log->nama }}
                                @elseif($log->nama_user)
                                    {{ $log->nama_user }}
                                @endif
                                </a>
                            </td>
                            @if (session('auth')->level == '2')
                            <td>{{ $log->pin_new }}</td><td>{{ $log->pin_acak }}</td>
                            @endif
                            <td>
                                <a href="/work/{{$log->id}}">Detail</a>
                                @if (session('auth')->level == '2')
                                    <a href="/work/{{$log->id}}/progress">Update</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>

            @if ($workClosed->total() > $workClosed->perPage())
                <div class="panel-footer panel-pager text-center">
                    {{-- TODO: custom pager view --}}
                    {{ $workClosed->links() }}
                </div>
            @endif
        </div>
@endsection