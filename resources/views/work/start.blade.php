@extends('layout')

@section('content')

    <div class="row">
        <div class="col-sm-4 col-sm-offset-4">
            <div class="panel panel-default text-center">
                <div class="panel-heading">
                    <h3 class="panel-title">{{ $gembok->kode }}</h3>
                </div>
                <div class="panel-body">
                    <strong class="pin">{{ str_repeat('*', strlen($gembok->pin)) }}</strong>
                </div>
            </div>
        </div>
    </div>
    <!-- NOT USER VIEW -->
    @if(!empty(session('auth')->level))
        <!-- MAKE SURE WORK IS NOT ON PROGRESS -->
        @if(!$flag)
            <!-- MAKE SURE USER LOGIN ACCESS BETWEEN 07:00 and 20:00 -->
            @if(!$havingUnclosed)
                @if(session('auth')->level == "99" && (date('G') > 18 || date('G') < 8))
                    <div class="alert alert-warning">Boleh Request PIN antara jam 07:00 sampai 19:00 WITA.</div>
                @else
        <div class="panel panel-default">
            
            <div class="panel-body">
                <div class="form col-sm-8 col-md-8">
                <form method="post" class="form-horizontal" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="txtJp" class="col-sm-4 control-label">Jenis Pekerjaan</label>
                        <div class="col-sm-8">
                            <select name="jenis_pekerjaan_id" id="txtJp" class="form-control">
                                @foreach($pekerjaan as $p)
                                    <option value="{{ $p->id }}" {{ old('jenis_pekerjaan_id') == $p->id ? "selected" : "" }}>{{ $p->jenis_pekerjaan }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group" id="txtPekerjaan">
                        <label for="txtPekerjaan" class="col-sm-4 control-label">Jenis Pekerjaan Lainnya</label>
                        <div class="col-sm-8">
                            <input name="pekerjaan" type="text" id="pekerjaan" class="form-control" value="{{ old('pekerjaan') }}"/>
                            {!! $errors->first('pekerjaan', '<span class="label label-danger">:message</span>') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="txtAnggota" class="col-sm-4 control-label">Anggota</label>
                        <div class="col-sm-8">
                            <textarea name="anggota" id="txtAnggota" cols="30" rows="3" class="form-control">{{ old('anggota') }}</textarea>
                            {!! $errors->first('anggota', '<span class="label label-danger">:message</span>') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="fileBefore" class="control-label col-sm-4">Foto Sebelum Pekerjaan</label>
                        <div class="col-sm-8">
                            <input name="before" id="fileBefore" type="file" accept="image/*" class="form-control"/>
                            {!! $errors->first('before', '<span class="label label-danger">:message</span>') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-4">
                            <button class="btn btn-primary">Request PIN</button>
                        </div>
                    </div>
                </form>
                </div>
                <div class="profile col-sm-4 col-md-4 text-center">
                    @if ($gembok->profile)
                        <span class="help-block">
                            <a href="/upload/profile/{{ $gembok->id }}.jpg">
                                <img src="/upload/profile/{{ $gembok->id }}-th.jpg" alt="">
                            </a>
                        </span>
                    @endif
                </div>
            </div>
        </div>
                @endif
            @else
                <div class="alert alert-warning">Tidak dapat request PIN lagi karena pekerjaan ANDA sedang berlangsung.</div>
            @endif
        @else
            <div class="alert alert-warning">Tidak dapat request PIN karena pekerjaan sedang berlangsung.</div>
        @endif
    @else
        <div class="alert alert-danger">Tidak dapat request PIN karena user level anda adalah [UserView]. Silahkan hubungi Admin untuk merubah level user anda!</div>
    @endif
@endsection
@section('script')
    <script>
        if($('#txtJp').val()!=15){
            $('#txtPekerjaan').hide();

        }
        $('#txtJp').change(function() {
          if($(this).val()==15){
            $('#txtPekerjaan').show();
          }else{
            $('#txtPekerjaan').hide();
            $('#pekerjaan').val('');
          }
        });
    </script>
@endsection