@extends('layout')

@section('content')
    <div class="row">
        <div class="col-sm-4 col-sm-offset-4">
            <div class="panel panel-default text-center">
                <div class="panel-heading">
                    <h3 class="panel-title">{{ $data->kode }}</h3>
                </div>
                <div class="panel-body">
                    <strong class="pin">{{ $data->pin }}</strong>
                </div>
            </div>
        </div>
    </div>

    <div style="margin-bottom: 5px" class="panel panel-default">
        <div class="panel-heading">Pekerjaan</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-3">
                    <label for="" class="control-label">Jenis Pekerjaan</label>
                </div>
                <div class="col-sm-6">
                    <p class="control-static">{{ $data->jenis_pekerjaan_id == 15 ? $data->pekerjaan : $data->jenis_pekerjaan }}</p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    <label for="" class="control-label">Uraian</label>
                </div>
                <div class="col-sm-6">
                    <p class="control-static">{!! nl2br($data->uraian) !!}</p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    <label for="" class="control-label">Anggota</label>
                </div>
                <div class="col-sm-6">
                    <p class="control-static">{!! nl2br($data->anggota) !!}</p>
                </div>
            </div>
        </div>

        <div class="panel-heading">Evidence</div>
        <div class="panel-body view-evidence">
            <div class="row">
                <div class="col-sm-4 text-center">
                    <a href="/upload/work/{{ $data->id }}/before.jpg">
                        <img src="/upload/work/{{ $data->id }}/before-th.jpg" alt="">
                    </a>
                    <p>Sebelum Pekerjaan</p>
                </div>

                <div class="col-sm-4 text-center">
                    <a href="/upload/work/{{ $data->id }}/item-before.jpg">
                        <img src="/upload/work/{{ $data->id }}/item-before-th.jpg" alt="">
                    </a>
                    <p>Perangkat Sebelum Pekerjaan</p>
                </div>

                <div class="col-sm-4 text-center">
                    <a href="/upload/work/{{ $data->id }}/item-after.jpg">
                        <img src="/upload/work/{{ $data->id }}/item-after-th.jpg" alt="">
                    </a>
                    <p>Perangkat Setelah Pekerjaan</p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4 text-center">
                    <a href="/upload/work/{{ $data->id }}/work-progress.jpg">
                        <img src="/upload/work/{{ $data->id }}/work-progress-th.jpg" alt="">
                    </a>
                    <p>Proses Pengerjaan</p>
                </div>
            </div>
        </div>
    </div>

    <a style="margin-bottom: 25px" href="/work/{{ $data->id }}/progress" class="btn btn-default">
        Edit
    </a>

    <div class="panel panel-info form-closing">
        <div class="panel-heading">Closing</div>
        <div class="panel-body">
            <form class="form form-horizontal" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="txtUraian" class="col-sm-3 control-label">Sesuaikan PIN Baru:</label>
                    <div class="col-sm-6">
                        <strong class="pin">{{ $data->pin_new }}</strong>
                        <input type="hidden" id="txtPin" name="pin" class="form-control" maxlength="4" value="{!! nl2br($data->pin_new) !!}" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label for="fileResetPin" class="control-label col-sm-3">Foto PIN Baru</label>
                    <div class="col-sm-6">
                        <input name="resetPin" id="fileResetPin" type="file" accept="image/*" class="form-control"/>
                        {!! $errors->first('resetPin', '<span class="label label-danger">:message</span>') !!}
                    </div>
                </div>
                <div class="form-group">
                    <label for="txtUraian" class="col-sm-3 control-label">Silahkan Acak PIN:</label>
                    <div class="col-sm-6">
                        <strong class="pin">{{ $data->pin_acak }}</strong>
                        <input type="hidden" id="txtPin" name="pinacak" class="form-control" maxlength="4" value="{!! nl2br($data->pin_acak) !!}" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label for="fileResetPin" class="control-label col-sm-3">Foto Acak PIN</label>
                    <div class="col-sm-6">
                        <input name="acakPin" id="fileAcakPin" type="file" accept="image/*" class="form-control"/>
                        {!! $errors->first('acakPin', '<span class="label label-danger">:message</span>') !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-3">
                        <button class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection