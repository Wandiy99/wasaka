    <div class="panel panel-default" id="info">
        <div class="panel-heading">List Akses Masuk Wasaka</div>
         <div id="fixed-table-container-demo" class="fixed-table-container">
            <table class="table table-bordered table-fixed">
              <tr>
                <th>#</th>
                <th>kode</th>
                <th>wtl</th>
                <th>koordinat</th>
                <th>user</th>
                <th>nama user</th>
                <th>anggota</th>
                <th>jenis pekerjaan</th>
                <th>uraian pekerjaan</th>
                <th>start</th>
                <th>close</th>
              </tr>
            @foreach($data as $no => $list) 
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $list->kode }}</td>
                    <td>{{ $list->wtl }}</td>
                    <td>{{ $list->koordinat }}</td>
                    <td>{{ $list->id_user }}</td>
                    <td>{{ $list->nama_user }}</td>
                    <td>{{ $list->anggota }}</td>
                    <td>{{ $list->jenis_pekerjaan }}</td>
                    <td>{{ $list->uraian }}</td>
                    <td>{{ date('Y-m-d H:i:s', $list->time_start) }}</td>
                    <td>{{ date('Y-m-d H:i:s', $list->time_close) }}</td>
                </tr>
            @endforeach
          </table>
      </div>
    </div>