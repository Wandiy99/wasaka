@extends('layout')

@section('content')
    <div id="unmatch_pin" class="modal" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Don't be Panic</h4>
                </div>
                <input type="hidden" name="individu" class="form-control" id="individu_id">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4 text-center">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Try This Pin</h3>
                                </div>
                                <div class="panel-body">
                                    <strong class="pin">{{ $last->pin_last }}</strong>
                                </div>
                            </div>
                        </div>
                    
                        @if ($last->id_last)
                            <div class="col-sm-4 col-sm-offset-4 text-center">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">or This</h3>
                                    </div>
                                    <div class="panel-body">
                                        <span class="help-block">
                                            <a href="/upload/pin/{{ $last->id_last }}.jpg">
                                                <img src="/upload/pin/{{ $last->id_last }}-th.jpg" alt="">
                                            </a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>  
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4 col-sm-offset-4 text-center">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">{{ $data->kode }}
                        <span class="dropdown">
                          <button class="btn btn-warning btn-xs dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Panic Button
                            <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="#">Jaringan Lelet</a></li>
                            <li><a href="#" data-toggle="modal" data-target="#unmatch_pin">Pin Tidak Sesuai</a></li>
                          </ul>
                        </span>
                    </h3>
                </div>
                <div class="panel-body">
                    <strong class="pin">{{ $data->pin }}</strong>
                </div>
            </div>
        </div>
    </div>

    <form method="post" class="form-horizontal" enctype="multipart/form-data">
        <div class="panel panel-default">
            <div class="panel-heading">Data Starting</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="txtJp" class="col-sm-3 control-label">Jenis Pekerjaan</label>
                    <div class="col-sm-6">
                        <select name="jenis_pekerjaan_id" id="txtJp" class="form-control">
                            @foreach($pekerjaan as $p)
                                <option value="{{ $p->id }}" {{ @$data->jenis_pekerjaan_id == $p->id ? "selected" : "" }}>{{ $p->jenis_pekerjaan }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group" id="txtPekerjaan">
                    <label for="txtKode" class="col-sm-3 control-label">Jenis Pekerjaan</label>
                    <div class="col-sm-6">
                        <input name="pekerjaan" type="text" id="txtKode" class="form-control"
                               value="{{ old('pekerjaan') ?: @$data->pekerjaan }}"/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="txtAnggota" class="col-sm-3 control-label">Anggota</label>
                    <div class="col-sm-6">
                        <textarea name="anggota" id="txtAnggota" cols="30" rows="3"
                                  class="form-control">{{ old('anggota') ?: @$data->anggota }}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="fileBefore" class="control-label col-sm-3">Foto Sebelum Pekerjaan</label>
                    <div class="col-sm-6">
                        <input name="before" id="fileBefore" type="file" accept="image/*" class="form-control"/>
                        @if ($data->hasPhoto['before'])
                            <span class="help-block">
                                <a href="/upload/work/{{ $data->id }}/before.jpg">
                                    <img src="/upload/work/{{ $data->id }}/before-th.jpg" alt="">
                                </a>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">Data Progress</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="txtJp" class="col-sm-3 control-label">Uraian Pekerjaan</label>
                    <div class="col-sm-6">
                        <textarea name="uraian" id="txtUraian" cols="30" rows="3" class="form-control">{{ old('uraian') ?: @$data->uraian }}</textarea>
                        {!! $errors->first('uraian', '<span class="label label-danger">:message</span>') !!}
                    </div>
                </div>
                <?php 
                    $file = 0;
                ?>
                <div class="form-group">
                    <label for="fileItemBefore" class="control-label col-sm-3">Foto Perangkat Sebelum Pekerjaan</label>
                    <div class="col-sm-6">
                        <input name="item-before" id="fileItemBefore" type="file" accept="image/*" class="form-control"/>
                        @if ($data->hasPhoto['item-before'])
                            <?php 
                                $file = 1;
                            ?>
                            <span class="help-block">
                                <a href="/upload/work/{{ $data->id }}/item-before.jpg">
                                    <img src="/upload/work/{{ $data->id }}/item-before-th.jpg" alt="">
                                </a>
                            </span>
                        @endif
                        {!! $errors->first('item-before', '<span class="label label-danger">:message</span>') !!}
                        <input type="hidden" name="item_before_exist" value="{{ $file }}">
                    </div>
                </div>
                <?php 
                    $file = 0;
                ?>
                <div class="form-group">
                    <label for="fileWorkProgress" class="control-label col-sm-3">Foto Kegiatan</label>
                    <div class="col-sm-6">
                        <input name="work-progress" id="fileWorkProgress" type="file" accept="image/*" class="form-control"/>
                        @if ($data->hasPhoto['work-progress'])
                            <?php 
                                $file = 1;
                            ?>
                            <span class="help-block">
                                <a href="/upload/work/{{ $data->id }}/work-progress.jpg">
                                    <img src="/upload/work/{{ $data->id }}/work-progress-th.jpg" alt="">
                                </a>
                            </span>
                        @endif
                        {!! $errors->first('work-progress', '<span class="label label-danger">:message</span>') !!}
                        <input type="hidden" name="work_progress_exist" value="{{ $file }}">
                    </div>
                </div>
                <?php 
                    $file = 0;
                ?>
                <div class="form-group">
                    <label for="fileItemAfter" class="control-label col-sm-3">Foto Perangkat Setelah Pekerjaan</label>
                    <div class="col-sm-6">
                        <input name="item-after" id="fileItemAfter" type="file" accept="image/*" class="form-control"/>
                        @if ($data->hasPhoto['item-after'])
                            <?php 
                                $file = 1;
                            ?>
                            <span class="help-block">
                                <a href="/upload/work/{{ $data->id }}/item-after.jpg">
                                    <img src="/upload/work/{{ $data->id }}/item-after-th.jpg" alt="">
                                </a>
                            </span>
                        @endif
                        {!! $errors->first('item-after', '<span class="label label-danger">:message</span>') !!}
                        <input type="hidden" name="item_after_exist" value="{{ $file }}">
                    </div>
                </div>

                {{--<div class="form-group">--}}
                {{--<label for="txtPin" class="col-sm-3 control-label">Pin Baru</label>--}}
                {{--<div class="col-sm-2">--}}
                {{--<input name="pin" type="text" id="txtPin" class="form-control"/>--}}
                {{--</div>--}}
                {{--</div>--}}

                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-3">
                        <button class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    
@endsection

@section('script')
    <script>
        if($('#txtJp').val()!=15){
            $('#txtPekerjaan').hide();
        }
        $('#txtJp').change(function() {
          if($(this).val()==15){
            $('#txtPekerjaan').show();
          }else{
            $('#txtPekerjaan').hide();
          }
        });
    </script>
@endsection