@extends('layout')

@section('content')
    @if (count($listUnclosed))
        <div class="panel panel-warning">
            <div class="panel-heading">Pekerjaan sedang berlangsung</div>
            <div class="list-group">
                @foreach($listUnclosed as $data)
                    <a href="/work/{{ $data->id }}/close" class="list-group-item">
                        <strong class="list-group-item-heading">{{ $data->kode }}</strong>
                        <br>
                        <span class="label label-info">
                            {{ date('d/m/Y H:i', $data->time_start) }}
                        </span>
                        <p class="list-group-text">{{ $data->pekerjaan }}</p>
                    </a>
                @endforeach
            </div>
        </div>
    @endif

    @if (count($listClosed))
        <div class="panel panel-default">
            <div class="panel-heading">History</div>
            <div class="list-group">
                @foreach($listClosed as $data)
                    <a href="/work/{{ $data->id }}" class="list-group-item">
                        <strong class="list-group-item-heading">{{ $data->kode }}</strong>
                        <br>
                        <span class="label label-info">
                            {{ date('d/m/Y H:i', $data->time_start) }}
                        </span>
                        <span class="label label-success" style="margin-left: 10px">
                            {{ date('d/m/Y H:i', $data->time_close) }}
                        </span>
                        <p class="list-group-text">{{ $data->pekerjaan }}</p>
                    </a>
                @endforeach
            </div>
            @if ($listClosed->total() > $listClosed->perPage())
                <div class="panel-footer panel-pager text-center">
                    {{-- TODO: custom pager view --}}
                    {{ $listClosed->links() }}
                </div>
            @endif
        </div>
    @endif
@endsection