<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterManciWorkPekerjaanId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manci_work', function (Blueprint $table) {
            $table->integer('jenis_pekerjaan_id')->nullable();
            $table->string('program')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manci_work', function (Blueprint $table) {
            $table->dropColumn('jenis_pekerjaan_id');
            $table->dropColumn('program');
        });
    }
}
