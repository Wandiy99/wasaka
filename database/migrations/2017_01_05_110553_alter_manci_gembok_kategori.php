<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterManciGembokKategori extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manci_gembok', function (Blueprint $table) {
            $table->tinyInteger('kategori')->nullable()->comment('1:ftm;2:odc;');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manci_gembok', function (Blueprint $table) {
            $table->dropColumn('kategori');
            
        });
    }
}
