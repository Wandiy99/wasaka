<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGembok2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manci_gembok', function (Blueprint $table) {
            $table->string('type')->nullable();
            $table->string('kap')->nullable();
            $table->string('merk')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manci_gembok', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('kap');
            $table->dropColumn('merk');
        });
    }
}
