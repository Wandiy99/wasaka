<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Work extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manci_work', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_gembok');
            
            $table->integer('time_start');
            $table->integer('time_close');

            $table->string('pekerjaan');
            $table->string('anggota');
            $table->string('uraian');
            $table->string('pin_new');

            $table->char('id_user', 50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('manci_work');
    }
}
