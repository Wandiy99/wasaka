<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterManciProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manci_profile', function (Blueprint $table) {
            $table->tinyInteger('level')->nullable();
            $table->string('fiberzone', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manci_profile', function (Blueprint $table) {
            $table->dropColumn('level');
            $table->dropColumn('fiberzone');
        });
    }
}
